<?php echo $header; ?>
<link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="">View Market Data </h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('admin');?>" title="Home">Home</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url('market');?>" title="Market">Market</a></li>
                            <li class="breadcrumb-item active" title="View Import Data">View Import Data</li>
                        </ol>
                    </div>
                    <div>
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                          <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                                <div class="card">
                                    <div class="card-body">
                                        <!--<input type="button" value="Go Fullscreen" id="fsbutton" />-->
                                        <div class="table-responsive" id="testgrid">
                                            <div style="margin-bottom: 10px; ">
                                            <label class="m-t-20">Select COT</label>
                                            <select class="form-control" onchange="cotchange(this.value)">
                                                <option value="-1">Select COT</option>
                                                <?php foreach ($allcots as $key=>$value) { 
                                                        $COT_type = $value['COT_type']==1?'Future Only':'Future Option';
                                                    ?>                                                
                                                    <option value="<?php echo $value['_id'] ?>"><?php echo $value['COT_name'].' '.$COT_type; ?></option>
                                                <?php } ?>
                                            </select>
                                            <br>
                                            </div>
                                            <div id="grid1"></div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        
<?php echo $footer; ?>
<script>
function callTypeDropDownEditor(container, options) {
    var categories = [
        { CategoryName: "LAM"}, 
        { CategoryName: "PSAP" },
    ];
    var crudServiceBaseUrl = "<?php echo base_url(); ?>";
    $('<input data-value-field="continent" data-bind="value:' + options.field + '" name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataTextField: "CategoryName",
            dataValueField: "CategoryName",
            dataSource: {   
                data: categories
            }
        });
}
function cotchange(value){
    
    if(value == 1 || value == 2 ){
        
        
        var crudServiceBaseUrl = "<?php echo base_url(); ?>"+ "markets/get_all_data?value="+value,
        dataSource = new kendo.data.DataSource({
            type: "odata",
            transport: {
                read:  {
                    url: crudServiceBaseUrl,
                },
                update: {
                    url: "<?php echo base_url(); ?>"+ "markets/update_data?value="+value,
                    type: "POST",
                    dataType: "json"
                },
//                read:  crudServiceBaseUrl,
//                update: "<?php //echo base_url(); ?>"+ "markets/get_all_data?value="+value,
            },
            batch: true,
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            schema: {
                model: {
                    id: "_id",
                    fields: {
                        _id: { editable: false, nullable: true },
                        Market_and_Exchange_Names: { editable: false},
                        As_of_Date_In_Form_YYMMDD: { type: "number",validation: { required: true } },
                        Report_Date_as_MM_DD_YYYY: { type: "date" },
                        CFTC_Contract_Market_Code: {  },
                        CFTC_Market_Code: {  },
                        CFTC_Region_Code: {  },
                        CFTC_Commodity_Code: {  },
                        Open_Interest_All: {  },
                        Dealer_Positions_Long_All: {  },
                        Dealer_Positions_Short_All: {  },
                        Dealer_Positions_Spread_All: {  },
                        Asset_Mgr_Positions_Long_All: {  },
                        Asset_Mgr_Positions_Short_All: {  },
                        Asset_Mgr_Positions_Spread_All: {  },
                        Lev_Money_Positions_Long_All: {  },
                        Lev_Money_Positions_Short_All: {  },
                        Lev_Money_Positions_Spread_All: {  },
                        Other_Rept_Positions_Long_All: {  },
                        Other_Rept_Positions_Short_All: {  },
                        Other_Rept_Positions_Spread_All: {  },
                        Tot_Rept_Positions_Long_All: {  },
                        Tot_Rept_Positions_Short_All: {  },
                        NonRept_Positions_Long_All: {  },
                        NonRept_Positions_Short_All: {  },
                        Change_in_Open_Interest_All: {  },
                        Change_in_Dealer_Long_All: {  },
                        Change_in_Dealer_Short_All: {  },
                        Change_in_Dealer_Spread_All: {  },
                        Change_in_Asset_Mgr_Long_All: {  },
                        Change_in_Asset_Mgr_Short_All: {  },
                        Change_in_Asset_Mgr_Spread_All: {  },
                        Change_in_Lev_Money_Long_All: {  },
                        Change_in_Lev_Money_Short_All: {  },
                        Change_in_Lev_Money_Spread_All: {  },
                        Change_in_Other_Rept_Long_All: {  },
                        Change_in_Other_Rept_Short_All: {  },
                        Change_in_Other_Rept_Spread_All: {  },
                        Change_in_Tot_Rept_Long_All: {  },
                        Change_in_Tot_Rept_Short_All: {  },
                        Change_in_NonRept_Long_All: {  },
                        Change_in_NonRept_Short_All: {  },
                        Pct_of_Open_Interest_All: {  },
                        Pct_of_OI_Dealer_Long_All: {  },
                        Pct_of_OI_Dealer_Short_All: {  },
                        Pct_of_OI_Dealer_Spread_All: {  },
                        Pct_of_OI_Asset_Mgr_Long_All: {  },
                        Pct_of_OI_Asset_Mgr_Short_All: {  },
                        Pct_of_OI_Asset_Mgr_Spread_All: {  },
                        Pct_of_OI_Lev_Money_Long_All: {  },
                        Pct_of_OI_Lev_Money_Short_All: {  },
                        Pct_of_OI_Lev_Money_Spread_All: {  },
                        Pct_of_OI_Other_Rept_Long_All: {  },
                        Pct_of_OI_Other_Rept_Short_All: {  },
                        Pct_of_OI_Other_Rept_Spread_All: {  },
                        Pct_of_OI_Tot_Rept_Long_All: {  },
                        Pct_of_OI_Tot_Rept_Short_All: {  },
                        Pct_of_OI_NonRept_Long_All: {  },
                        Pct_of_OI_NonRept_Short_All: {  },
                        Traders_Tot_All: {  },
                        Traders_Dealer_Long_All: {  },
                        Traders_Dealer_Short_All: {  },
                        Traders_Dealer_Spread_All: {  },
                        Traders_Asset_Mgr_Long_All: {  },
                        Traders_Asset_Mgr_Short_All: {  },
                        Traders_Asset_Mgr_Spread_All: {  },
                        Traders_Lev_Money_Long_All: {  },
                        Traders_Lev_Money_Short_All: {  },
                        Traders_Lev_Money_Spread_All: {  },
                        Traders_Other_Rept_Long_All: {  },
                        Traders_Other_Rept_Short_All: {  },
                        Traders_Other_Rept_Spread_All: {  },
                        Traders_Tot_Rept_Long_All: {  },
                        Traders_Tot_Rept_Short_All: {  },
                        Conc_Gross_LE_4_TDR_Long_All: {  },
                        Conc_Gross_LE_4_TDR_Short_All: {  },
                        Conc_Gross_LE_8_TDR_Long_All: {  },
                        Conc_Gross_LE_8_TDR_Short_All: {  },
                        Conc_Net_LE_4_TDR_Long_All: {  },
                        Conc_Net_LE_4_TDR_Short_All: {  },
                        Conc_Net_LE_8_TDR_Long_All: {  },
                        Conc_Net_LE_8_TDR_Short_All: {  },
                        Contract_Units: {  },
                        CFTC_Contract_Market_Code_Quotes: {  },
                        CFTC_Market_Code_Quotes: {  },
                        CFTC_Commodity_Code_Quotes: {  },
                        CFTC_SubGroup_Code: {  },
                        FutOnly_or_Combined: {  }
                        
                    }
                }
            }
        });
        $("#grid1").kendoGrid({

        dataSource: dataSource,
        navigatable: true,
        pageable: true,
        sortable: true,
        filterable: {
            mode: "row"
        },
        height: 550,
        toolbar: [ { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
            { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
            { name: "excel", iconClass: "fa fa-file", text: "&nbsp; Export to Excel" },
            { template: '<span id="grid_fullscreen" class="k-button" href="\\#" onclick="buttonClickHandler()"><i class="fas fa-expand"></i> &nbsp; Full Screen</span>'}
        ],
        pdf: {
            allPages: true,
            avoidLinks: true,
            paperSize: "A4",
            margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
            landscape: true,
            repeatHeaders: true,
            template: $("#page-template").html(),
            scale: 0.8
        },
        excel: {
            fileName: "excel.xlsx",
            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
            filterable: true,
            allPages: true
        },
        columns: [
            { field: "Market_and_Exchange_Names", title: "Market and Exchange Names",width:"180px"},
            { field: "As_of_Date_In_Form_YYMMDD", title: "As of Date In Form YYMMDD",width:"80px"},
            { field: "Report_Date_as_MM_DD_YYYY", title: "Report Date as MM DD YYYY",format: "{0:MM/dd/yyyy}",width:"80px"},
            { field: "CFTC_Contract_Market_Code", title: "CFTC Contract Market Code",width:"80px"},
            { field: "CFTC_Market_Code", title: "CFTC Market Code",width:"80px"},
            { field: "CFTC_Region_Code", title: "CFTC Region Code",width:"80px"},
            { field: "CFTC_Commodity_Code", title: "CFTC Commodity Code",width:"80px"},
            { field: "Open_Interest_All", title: "Open Interest All",width:"80px"},
            { field: "Dealer_Positions_Long_All", title: "Dealer Positions Long All",width:"80px"},
            { field: "Dealer_Positions_Short_All", title: "Dealer Positions Short All",width:"80px"},
            { field: "Dealer_Positions_Spread_All", title: "Dealer Positions Spread All",width:"80px"},
            { field: "Asset_Mgr_Positions_Long_All", title: "Asset Mgr Positions Long All",width:"80px"},
            { field: "Asset_Mgr_Positions_Short_All", title: "Asset Mgr Positions Short All",width:"80px"},
            { field: "Asset_Mgr_Positions_Spread_All", title: "Asset Mgr Positions Spread All",width:"80px"},
            { field: "Lev_Money_Positions_Long_All", title: "Lev Money Positions Long All",width:"80px"},
            { field: "Lev_Money_Positions_Short_All", title: "Lev Money Positions Short All",width:"80px"},
            { field: "Lev_Money_Positions_Spread_All", title: "Lev Money Positions Spread All",width:"80px"},
            { field: "Other_Rept_Positions_Long_All", title: "Other Rept Positions Long All",width:"80px"},
            { field: "Other_Rept_Positions_Short_All", title: "Other Rept Positions Short All",width:"80px"},
            { field: "Other_Rept_Positions_Spread_All", title: "Other Rept Positions Spread All",width:"80px"},
            { field: "Tot_Rept_Positions_Long_All", title: "Tot Rept Positions Long All",width:"80px"},
            { field: "Tot_Rept_Positions_Short_All", title: "Tot Rept Positions Short All",width:"80px"},
            { field: "NonRept_Positions_Long_All", title: "NonRept Positions Long All",width:"80px"},
            { field: "NonRept_Positions_Short_All", title: "NonRept Positions Short All",width:"80px"},
            { field: "Change_in_Open_Interest_All", title: "Change in Open Interest All",width:"80px"},
            { field: "Change_in_Dealer_Long_All", title: "Change in Dealer Long All",width:"80px"},
            { field: "Change_in_Dealer_Short_All", title: "Change in Dealer Short All",width:"80px"},
            { field: "Change_in_Dealer_Spread_All", title: "Change in Dealer Spread All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Long_All", title: "Change in Asset Mgr Long All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Short_All", title: "Change in Asset Mgr Short All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Spread_All", title: "Change in Asset Mgr Spread All",width:"80px"},
            { field: "Change_in_Lev_Money_Long_All", title: "Change in Lev Money Long All",width:"80px"},
            { field: "Change_in_Lev_Money_Short_All", title: "Change in Lev Money Short All",width:"80px"},
            { field: "Change_in_Lev_Money_Spread_All", title: "Change in Lev Money Spread All",width:"80px"},
            { field: "Change_in_Other_Rept_Long_All", title: "Change in Other Rept Long All",width:"80px"},
            { field: "Change_in_Other_Rept_Short_All", title: "Change in Other Rept Short All",width:"80px"},
            { field: "Change_in_Other_Rept_Spread_All", title: "Change in Other Rept Spread All",width:"80px"},
            { field: "Change_in_Tot_Rept_Long_All", title: "Change in Tot Rept Long All",width:"80px"},
            { field: "Change_in_Tot_Rept_Short_All", title: "Change in Tot Rept Short All",width:"80px"},
            { field: "Change_in_NonRept_Long_All", title: "Change in NonRept Long All",width:"80px"},
            { field: "Change_in_NonRept_Short_All", title: "Change in NonRept Short All",width:"80px"},
            { field: "Pct_of_Open_Interest_All", title: "Pct of Open Interest All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Long_All", title: "Pct of OI Dealer Long All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Short_All", title: "Pct of OI Dealer Short All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Spread_All", title: "Pct of OI Dealer Spread All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Long_All", title: "Pct of OI Asset Mgr Long All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Short_All", title: "Pct of OI Asset Mgr Short All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Spread_All", title: "Pct of OI Asset Mgr Spread All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Long_All", title: "Pct of OI Lev Money Long All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Short_All", title: "Pct of OI Lev Money Short All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Spread_All", title: "Pct of OI Lev Money Spread All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Long_All", title: "Pct of OI Other Rept Long All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Short_All", title: "Pct of OI Other Rept Short All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Spread_All", title: "Pct of OI Other Rept Spread All",width:"80px"},
            { field: "Pct_of_OI_Tot_Rept_Long_All", title: "Pct of OI Tot Rept Long All",width:"80px"},
            { field: "Pct_of_OI_Tot_Rept_Short_All", title: "Pct of OI Tot Rept Short All",width:"80px"},
            { field: "Pct_of_OI_NonRept_Long_All", title: "Pct of OI NonRept Long All",width:"80px"},
            { field: "Pct_of_OI_NonRept_Short_All", title: "Pct of OI NonRept Short All",width:"80px"},
            { field: "Traders_Tot_All", title: "Traders Tot All",width:"80px"},
            { field: "Traders_Dealer_Long_All", title: "Traders Dealer Long All",width:"80px"},
            { field: "Traders_Dealer_Short_All", title: "Traders Dealer Short All",width:"80px"},
            { field: "Traders_Dealer_Spread_All", title: "Traders Dealer Spread All",width:"80px"},
            { field: "Traders_Asset_Mgr_Long_All", title: "Traders Asset Mgr Long All",width:"80px"},
            { field: "Traders_Asset_Mgr_Short_All", title: "Traders Asset Mgr Short All",width:"80px"},
            { field: "Traders_Asset_Mgr_Spread_All", title: "Traders Asset Mgr Spread All",width:"80px"},
            { field: "Traders_Lev_Money_Long_All", title: "Traders Lev Money Long All",width:"80px"},
            { field: "Traders_Lev_Money_Short_All", title: "Traders Lev Money Short All",width:"80px"},
            { field: "Traders_Lev_Money_Spread_All", title: "Traders Lev Money Spread All",width:"80px"},
            { field: "Traders_Other_Rept_Long_All", title: "Traders Other Rept Long All",width:"80px"},
            { field: "Traders_Other_Rept_Short_All", title: "Traders Other Rept Short All",width:"80px"},
            { field: "Traders_Other_Rept_Spread_All", title: "Traders Other Rept Spread All",width:"80px"},
            { field: "Traders_Tot_Rept_Long_All", title: "Traders Tot Rept Long All",width:"80px"},
            { field: "Traders_Tot_Rept_Short_All", title: "Traders Tot Rept Short All",width:"80px"},
            { field: "Conc_Gross_LE_4_TDR_Long_All", title: "Conc Gross LE 4 TDR Long All",width:"80px"},
            { field: "Conc_Gross_LE_4_TDR_Short_All", title: "Conc Gross LE 4 TDR Short All",width:"80px"},
            { field: "Conc_Gross_LE_8_TDR_Long_All", title: "Conc Gross LE 8 TDR Long All",width:"80px"},
            { field: "Conc_Gross_LE_8_TDR_Short_All", title: "Conc Gross LE 8 TDR Short All",width:"80px"},
            { field: "Conc_Net_LE_4_TDR_Long_All", title: "Conc Net LE 4 TDR Long All",width:"80px"},
            { field: "Conc_Net_LE_4_TDR_Short_All", title: "Conc Net LE 4 TDR Short All",width:"80px"},
            { field: "Conc_Net_LE_8_TDR_Long_All", title: "Conc Net LE 8 TDR Long All",width:"80px"},
            { field: "Conc_Net_LE_8_TDR_Short_All", title: "Conc Net LE 8 TDR Short All",width:"80px"},
            { field: "Contract_Units", title: "Contract Units",width:"80px"},
            { field: "CFTC_Contract_Market_Code_Quotes", title: "CFTC Contract Market Code Quotes",width:"80px"},
            { field: "CFTC_Market_Code_Quotes", title: "CFTC Market Code Quotes",width:"80px"},
            { field: "CFTC_Commodity_Code_Quotes", title: "CFTC_Commodity Code Quotes",width:"80px"},
            { field: "CFTC_Commodity_Code_Quotes", title: "CFTC Commodity Code Quotes",width:"80px"},
            { field: "FutOnly_or_Combined", title: "FutOnly or Combined",width:"80px"},
        ],
        editable: true,
        scrollable:true,
        filterable: true,
        sortable: true,
        pageable: true,
    });
    }
    if(value == 3  ){
        
         var crudServiceBaseUrl = "<?php echo base_url(); ?>"+ "markets/get_all_data?value="+value,
        dataSource = new kendo.data.DataSource({
            type: "odata",
            transport: {
                read:  {
                    url: crudServiceBaseUrl,
                },
                update: {
                    url: "<?php echo base_url(); ?>"+ "markets/update_data?value="+value,
                    type: "POST",
                    dataType: "json"
                },
//                read:  crudServiceBaseUrl,
//                update: "<?php //echo base_url(); ?>"+ "markets/get_all_data?value="+value,
            },
            batch: true,
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            schema: {
                model: {
                    id: "_id",
                    fields: {
                        _id: { editable: false, nullable: true },
                        Market_and_Exchange_Names: { editable: false},
                        As_of_Date_in_Form_YYMMDD: { type: "number",validation: { required: true } },
                        Report_Date_as_MM_DD_YYYY: { type: "date" },
                        CFTC_Contract_Market_Code: {  },
                        CFTC_Market_Code: {  },
                        CFTC_Region_Code: {  },
                        CFTC_Commodity_Code: {  },
                        Open_Interest_All: {  },
                        Dealer_Positions_Long_All: {  },
                        Dealer_Positions_Short_All: {  },
                        Dealer_Positions_Spread_All: {  },
                        Asset_Mgr_Positions_Long_All: {  },
                        Asset_Mgr_Positions_Short_All: {  },
                        Asset_Mgr_Positions_Spread_All: {  },
                        Lev_Money_Positions_Long_All: {  },
                        Lev_Money_Positions_Short_All: {  },
                        Lev_Money_Positions_Spread_All: {  },
                        Other_Rept_Positions_Long_All: {  },
                        Other_Rept_Positions_Short_All: {  },
                        Other_Rept_Positions_Spread_All: {  },
                        Tot_Rept_Positions_Long_All: {  },
                        Tot_Rept_Positions_Short_All: {  },
                        NonRept_Positions_Long_All: {  },
                        NonRept_Positions_Short_All: {  },
                        Change_in_Open_Interest_All: {  },
                        Change_in_Dealer_Long_All: {  },
                        Change_in_Dealer_Short_All: {  },
                        Change_in_Dealer_Spread_All: {  },
                        Change_in_Asset_Mgr_Long_All: {  },
                        Change_in_Asset_Mgr_Short_All: {  },
                        Change_in_Asset_Mgr_Spread_All: {  },
                        Change_in_Lev_Money_Long_All: {  },
                        Change_in_Lev_Money_Short_All: {  },
                        Change_in_Lev_Money_Spread_All: {  },
                        Change_in_Other_Rept_Long_All: {  },
                        Change_in_Other_Rept_Short_All: {  },
                        Change_in_Other_Rept_Spread_All: {  },
                        Change_in_Tot_Rept_Long_All: {  },
                        Change_in_Tot_Rept_Short_All: {  },
                        Change_in_NonRept_Long_All: {  },
                        Change_in_NonRept_Short_All: {  },
                        Pct_of_Open_Interest_All: {  },
                        Pct_of_OI_Dealer_Long_All: {  },
                        Pct_of_OI_Dealer_Short_All: {  },
                        Pct_of_OI_Dealer_Spread_All: {  },
                        Pct_of_OI_Asset_Mgr_Long_All: {  },
                        Pct_of_OI_Asset_Mgr_Short_All: {  },
                        Pct_of_OI_Asset_Mgr_Spread_All: {  },
                        Pct_of_OI_Lev_Money_Long_All: {  },
                        Pct_of_OI_Lev_Money_Short_All: {  },
                        Pct_of_OI_Lev_Money_Spread_All: {  },
                        Pct_of_OI_Other_Rept_Long_All: {  },
                        Pct_of_OI_Other_Rept_Short_All: {  },
                        Pct_of_OI_Other_Rept_Spread_All: {  },
                        Pct_of_OI_Tot_Rept_Long_All: {  },
                        Pct_of_OI_Tot_Rept_Short_All: {  },
                        Pct_of_OI_NonRept_Long_All: {  },
                        Pct_of_OI_NonRept_Short_All: {  },
                        Traders_Tot_All: {  },
                        Traders_Dealer_Long_All: {  },
                        Traders_Dealer_Short_All: {  },
                        Traders_Dealer_Spread_All: {  },
                        Traders_Asset_Mgr_Long_All: {  },
                        Traders_Asset_Mgr_Short_All: {  },
                        Traders_Asset_Mgr_Spread_All: {  },
                        Traders_Lev_Money_Long_All: {  },
                        Traders_Lev_Money_Short_All: {  },
                        Traders_Lev_Money_Spread_All: {  },
                        Traders_Other_Rept_Long_All: {  },
                        Traders_Other_Rept_Short_All: {  },
                        Traders_Other_Rept_Spread_All: {  },
                        Traders_Tot_Rept_Long_All: {  },
                        Traders_Tot_Rept_Short_All: {  },
                        Conc_Gross_LE_4_TDR_Long_All: {  },
                        Conc_Gross_LE_4_TDR_Short_All: {  },
                        Conc_Gross_LE_8_TDR_Long_All: {  },
                        Conc_Gross_LE_8_TDR_Short_All: {  },
                        Conc_Net_LE_4_TDR_Long_All: {  },
                        Conc_Net_LE_4_TDR_Short_All: {  },
                        Conc_Net_LE_8_TDR_Long_All: {  },
                        Conc_Net_LE_8_TDR_Short_All: {  },
                        Contract_Units: {  },
                        CFTC_Contract_Market_Code_Quotes: {  },
                        CFTC_Market_Code_Quotes: {  },
                        CFTC_Commodity_Code_Quotes: {  },
                        CFTC_SubGroup_Code: {  },
                        FutOnly_or_Combined: {  }
                        
                    }
                }
            }
        });
        $("#grid1").kendoGrid({

        dataSource: dataSource,
        navigatable: true,
        pageable: true,
        sortable: true,
        filterable: {
            mode: "row"
        },
        height: 550,
        toolbar: [ { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
            { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
            { name: "excel", iconClass: "fa fa-file", text: "&nbsp; Export to Excel" },
            { template: '<span id="grid_fullscreen" class="k-button" href="\\#" onclick="buttonClickHandler()"><i class="fas fa-expand"></i> &nbsp; Full Screen</span>'}
        ],
        pdf: {
            allPages: true,
            avoidLinks: true,
            paperSize: "A4",
            margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
            landscape: true,
            repeatHeaders: true,
            template: $("#page-template").html(),
            scale: 0.8
        },
        excel: {
            fileName: "excel.xlsx",
            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
            filterable: true,
            allPages: true
        },
        columns: [
            { field: "Market_and_Exchange_Names", title: "Market and Exchange Names",width:"180px"},
            { field: "As_of_Date_In_Form_YYMMDD", title: "As of Date In Form YYMMDD",width:"80px"},
            { field: "Report_Date_as_MM_DD_YYYY", title: "Report Date as MM DD YYYY",format: "{0:MM/dd/yyyy}",width:"80px"},
            { field: "CFTC_Contract_Market_Code", title: "CFTC Contract Market Code",width:"80px"},
            { field: "CFTC_Market_Code", title: "CFTC Market Code",width:"80px"},
            { field: "CFTC_Region_Code", title: "CFTC Region Code",width:"80px"},
            { field: "CFTC_Commodity_Code", title: "CFTC Commodity Code",width:"80px"},
            { field: "Open_Interest_All", title: "Open Interest All",width:"80px"},
            { field: "Dealer_Positions_Long_All", title: "Dealer Positions Long All",width:"80px"},
            { field: "Dealer_Positions_Short_All", title: "Dealer Positions Short All",width:"80px"},
            { field: "Dealer_Positions_Spread_All", title: "Dealer Positions Spread All",width:"80px"},
            { field: "Asset_Mgr_Positions_Long_All", title: "Asset Mgr Positions Long All",width:"80px"},
            { field: "Asset_Mgr_Positions_Short_All", title: "Asset Mgr Positions Short All",width:"80px"},
            { field: "Asset_Mgr_Positions_Spread_All", title: "Asset Mgr Positions Spread All",width:"80px"},
            { field: "Lev_Money_Positions_Long_All", title: "Lev Money Positions Long All",width:"80px"},
            { field: "Lev_Money_Positions_Short_All", title: "Lev Money Positions Short All",width:"80px"},
            { field: "Lev_Money_Positions_Spread_All", title: "Lev Money Positions Spread All",width:"80px"},
            { field: "Other_Rept_Positions_Long_All", title: "Other Rept Positions Long All",width:"80px"},
            { field: "Other_Rept_Positions_Short_All", title: "Other Rept Positions Short All",width:"80px"},
            { field: "Other_Rept_Positions_Spread_All", title: "Other Rept Positions Spread All",width:"80px"},
            { field: "Tot_Rept_Positions_Long_All", title: "Tot Rept Positions Long All",width:"80px"},
            { field: "Tot_Rept_Positions_Short_All", title: "Tot Rept Positions Short All",width:"80px"},
            { field: "NonRept_Positions_Long_All", title: "NonRept Positions Long All",width:"80px"},
            { field: "NonRept_Positions_Short_All", title: "NonRept Positions Short All",width:"80px"},
            { field: "Change_in_Open_Interest_All", title: "Change in Open Interest All",width:"80px"},
            { field: "Change_in_Dealer_Long_All", title: "Change in Dealer Long All",width:"80px"},
            { field: "Change_in_Dealer_Short_All", title: "Change in Dealer Short All",width:"80px"},
            { field: "Change_in_Dealer_Spread_All", title: "Change in Dealer Spread All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Long_All", title: "Change in Asset Mgr Long All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Short_All", title: "Change in Asset Mgr Short All",width:"80px"},
            { field: "Change_in_Asset_Mgr_Spread_All", title: "Change in Asset Mgr Spread All",width:"80px"},
            { field: "Change_in_Lev_Money_Long_All", title: "Change in Lev Money Long All",width:"80px"},
            { field: "Change_in_Lev_Money_Short_All", title: "Change in Lev Money Short All",width:"80px"},
            { field: "Change_in_Lev_Money_Spread_All", title: "Change in Lev Money Spread All",width:"80px"},
            { field: "Change_in_Other_Rept_Long_All", title: "Change in Other Rept Long All",width:"80px"},
            { field: "Change_in_Other_Rept_Short_All", title: "Change in Other Rept Short All",width:"80px"},
            { field: "Change_in_Other_Rept_Spread_All", title: "Change in Other Rept Spread All",width:"80px"},
            { field: "Change_in_Tot_Rept_Long_All", title: "Change in Tot Rept Long All",width:"80px"},
            { field: "Change_in_Tot_Rept_Short_All", title: "Change in Tot Rept Short All",width:"80px"},
            { field: "Change_in_NonRept_Long_All", title: "Change in NonRept Long All",width:"80px"},
            { field: "Change_in_NonRept_Short_All", title: "Change in NonRept Short All",width:"80px"},
            { field: "Pct_of_Open_Interest_All", title: "Pct of Open Interest All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Long_All", title: "Pct of OI Dealer Long All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Short_All", title: "Pct of OI Dealer Short All",width:"80px"},
            { field: "Pct_of_OI_Dealer_Spread_All", title: "Pct of OI Dealer Spread All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Long_All", title: "Pct of OI Asset Mgr Long All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Short_All", title: "Pct of OI Asset Mgr Short All",width:"80px"},
            { field: "Pct_of_OI_Asset_Mgr_Spread_All", title: "Pct of OI Asset Mgr Spread All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Long_All", title: "Pct of OI Lev Money Long All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Short_All", title: "Pct of OI Lev Money Short All",width:"80px"},
            { field: "Pct_of_OI_Lev_Money_Spread_All", title: "Pct of OI Lev Money Spread All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Long_All", title: "Pct of OI Other Rept Long All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Short_All", title: "Pct of OI Other Rept Short All",width:"80px"},
            { field: "Pct_of_OI_Other_Rept_Spread_All", title: "Pct of OI Other Rept Spread All",width:"80px"},
            { field: "Pct_of_OI_Tot_Rept_Long_All", title: "Pct of OI Tot Rept Long All",width:"80px"},
            { field: "Pct_of_OI_Tot_Rept_Short_All", title: "Pct of OI Tot Rept Short All",width:"80px"},
            { field: "Pct_of_OI_NonRept_Long_All", title: "Pct of OI NonRept Long All",width:"80px"},
            { field: "Pct_of_OI_NonRept_Short_All", title: "Pct of OI NonRept Short All",width:"80px"},
            { field: "Traders_Tot_All", title: "Traders Tot All",width:"80px"},
            { field: "Traders_Dealer_Long_All", title: "Traders Dealer Long All",width:"80px"},
            { field: "Traders_Dealer_Short_All", title: "Traders Dealer Short All",width:"80px"},
            { field: "Traders_Dealer_Spread_All", title: "Traders Dealer Spread All",width:"80px"},
            { field: "Traders_Asset_Mgr_Long_All", title: "Traders Asset Mgr Long All",width:"80px"},
            { field: "Traders_Asset_Mgr_Short_All", title: "Traders Asset Mgr Short All",width:"80px"},
            { field: "Traders_Asset_Mgr_Spread_All", title: "Traders Asset Mgr Spread All",width:"80px"},
            { field: "Traders_Lev_Money_Long_All", title: "Traders Lev Money Long All",width:"80px"},
            { field: "Traders_Lev_Money_Short_All", title: "Traders Lev Money Short All",width:"80px"},
            { field: "Traders_Lev_Money_Spread_All", title: "Traders Lev Money Spread All",width:"80px"},
            { field: "Traders_Other_Rept_Long_All", title: "Traders Other Rept Long All",width:"80px"},
            { field: "Traders_Other_Rept_Short_All", title: "Traders Other Rept Short All",width:"80px"},
            { field: "Traders_Other_Rept_Spread_All", title: "Traders Other Rept Spread All",width:"80px"},
            { field: "Traders_Tot_Rept_Long_All", title: "Traders Tot Rept Long All",width:"80px"},
            { field: "Traders_Tot_Rept_Short_All", title: "Traders Tot Rept Short All",width:"80px"},
            { field: "Conc_Gross_LE_4_TDR_Long_All", title: "Conc Gross LE 4 TDR Long All",width:"80px"},
            { field: "Conc_Gross_LE_4_TDR_Short_All", title: "Conc Gross LE 4 TDR Short All",width:"80px"},
            { field: "Conc_Gross_LE_8_TDR_Long_All", title: "Conc Gross LE 8 TDR Long All",width:"80px"},
            { field: "Conc_Gross_LE_8_TDR_Short_All", title: "Conc Gross LE 8 TDR Short All",width:"80px"},
            { field: "Conc_Net_LE_4_TDR_Long_All", title: "Conc Net LE 4 TDR Long All",width:"80px"},
            { field: "Conc_Net_LE_4_TDR_Short_All", title: "Conc Net LE 4 TDR Short All",width:"80px"},
            { field: "Conc_Net_LE_8_TDR_Long_All", title: "Conc Net LE 8 TDR Long All",width:"80px"},
            { field: "Conc_Net_LE_8_TDR_Short_All", title: "Conc Net LE 8 TDR Short All",width:"80px"},
            { field: "Contract_Units", title: "Contract Units",width:"80px"},
            { field: "CFTC_Contract_Market_Code_Quotes", title: "CFTC Contract Market Code Quotes",width:"80px"},
            { field: "CFTC_Market_Code_Quotes", title: "CFTC Market Code Quotes",width:"80px"},
            { field: "CFTC_Commodity_Code_Quotes", title: "CFTC_Commodity Code Quotes",width:"80px"},
            { field: "CFTC_Commodity_Code_Quotes", title: "CFTC Commodity Code Quotes",width:"80px"},
            { field: "FutOnly_or_Combined", title: "FutOnly or Combined",width:"80px"},
        ],
        editable: true,
        scrollable:true,
        filterable: true,
        sortable: true,
        pageable: true,
    });
    }
    
}
</script>