<?php

Class Common extends CI_Model {

    public $mongodb;
    public function __construct() {

        require_once $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";

        $manager = new MongoDB\Client("mongodb://localhost:27017");
        $this->mongodb = $manager->piafx;
        
    }
    // insert database
    function insert_data($data, $tablename)
    {
        if ($this->db->insert($tablename, $data))
        {

            return true;
        }
        else
        {
            return false;
        }
    }

    // insert database
    function insert_data_getid($data, $tablename)
    {
        if ($this->db->insert($tablename, $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
    function add_reset_link($reques_data)
    {
        if($this->db->insert('resetsetting',$reques_data))
        {
            return $this->db->insert_id();
        }
    }
    function  checktoken($tablename,$columnname,$columnvalue)
    {
        $this->db->where($columnname, $columnvalue);
         $this->db->where('adminemail', 'masteradmin');
        $query = $this->db->get($tablename);
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    // update database
    function update_data($data, $tablename, $columnname, $columnid)
    {
        $where = array($columnname => $columnid);
        $data = array( '$set' => $data);
        $collection = $this->mongodb->$tablename;
        $record = $collection->updateOne($where , $data);
        // print_r($where);
        // print_r($data);
        // print_r($record);exit();
        if($record)
        {
            return true;
        }
        else
        {
            return false;
        }
        // $record=$collection->find(array("_id"=>"$id"));
        
        // if($record)
        // {

        //     foreach($record as $rec)
        //     {

        //         // print_r($rec['settingvalue']);
        //         return nl2br(($rec['settingvalue']));
        //         break;
        //     }
        // }
        // else{
        //     return false;
        // }



        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
              
    function delete_data_by_multiple_where($tablename, $condition)
    {
        $this->db->where($condition);
        if ($this->db->delete($tablename))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function select_database_where_in($tablename, $columnname, $data)
    {
        $this->db->select('*');
        $this->db->where_in($columnname,$data);
        $query =  $this->db->get($tablename);
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
    
    // select data using colum id
    function select_database_id($tablename, $columnname, $columnid, $data = '*')
    {

        $collection = $this->mongodb->$tablename;
        $condition = array($columnname => $columnid);

        $record=$collection->find($condition);
        if(count($record) > 0)
        {
            return $record = $record->toArray();
        }
        else
        {
            return array();
        }


        // $this->db->select($data);
        // $this->db->where($columnname, $columnid);
        // $query = $this->db->get($tablename);
        // if ($query->num_rows() > 0)
        // {
        //     return $query->result_array();
        // }
        // else
        // {
        //     return array();
        // }
    }
    

    function get_data_all($tablename)
    {
        $this->db->from($tablename);
        $result = $this->db->get();
        return $result->result_array();
    }

    // delete data
    function delete_data($tablename, $columnname, $columnid)
    {
        $this->db->where($columnname, $columnid);
        if ($this->db->delete($tablename))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // change status
    function change_status($data, $tablename, $columnname, $columnid)
    {
        $this->db->where($columnname, $columnid);
        if ($this->db->update($tablename, $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // check unique avaliblity
    function check_unique_avalibility($tablename, $columname1, $columnid1_value, $columname2 = '', $columnid2_value = '', $condition_array = array())
    {

        if ($columnid2_value != '')
        {
            $this->db->where($columname2 . " !=", $columnid2_value);
        }
        $this->db->where($columname1, $columnid1_value);

        if (!empty($condition_array))
        {
            foreach ($condition_array as $field_name => $field_value)
            {
                $this->db->where($field_name, $field_value);
            }
        }

        $query = $this->db->get($tablename);

        if ($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            echo FALSE;
        }
    }

    //get all record 
    function get_all_category($tablename, $data = '*', $sortby = '', $orderby = '')
    {
        $this->db->select($data);
        $this->db->from($tablename);
        $this->db->where('status', 'Enable');
        $this->db->where('isdeleted', 'No');

        if ($sortby != '' && $orderby != "")
        {
            $this->db->order_by($sortby, $orderby);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    //get all record 
    function get_all_record($tablename, $data = '*', $sortby = '', $orderby = '')
    {
        $this->db->select($data);
        $this->db->from($tablename);

        if ($sortby != '' && $orderby != "")
        {
            $this->db->order_by($sortby, $orderby);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    //get record 
    function get_record($tablename, $fieldname, $value)
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($fieldname, $value);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    //table records count
    function get_count_of_table($table)
    {

        $collection = $this->mongodb->$table;
        // print_r($collection->count());exit();
        return $collection->count();

        // $query = $this->db->get($table)->num_rows();


        // return $query;
    }

    //table records count by id
    function get_count_of_table_by_id($table, $fieldname, $id)
    {
        $this->db->where($fieldname, $id);
        $query = $this->db->get($table)->num_rows();


        return $query;
    }

    //table records count by type
    function get_count_of_table_by_type($table, $type)
    {

        $this->db->where('usertype', $type);


        $query = $this->db->get($table)->num_rows();


        return $query;
    }

    //get deleted record
    function get_count_delete_record($tablename)
    {
        if ($tablename == "category")
        {
            $this->db->where('isdeleted', 'No');
        }
        if ($tablename == "language")
        {
            $this->db->where('is_delete', '0');
        }

        $query = $this->db->get($tablename);
        return $query->num_rows();
    }

    //get deleted record
    function get_count_subcategory()
    {
        //Executing Query
        $this->db->select('sc.*,c.category_name');
        $this->db->from('subcategory sc');
        $this->db->join('category c', 'c.categoryid=sc.categoryid', 'left');
        $this->db->where('c.isdeleted', 'No');

        $query = $this->db->get();
        return $query->num_rows();
    }

    //-----------------sending mail when status has been changed ---------------------//
    function mailForChangeStatus($mailformatid = "", $status = "", $data = array())
    {
        //data[0] for senders mail id
        //data[1] sender name
        // Mail For Spa Admin

        $mail = $this->get_email_byid($mailformatid);
        $subject = $mail[0]['varsubject'];
        $mailformat = $mail[0]['varmailformat'];

        $sitename = $this->common->get_setting_value(1);
        $siteurl = $this->common->get_setting_value(2);
        $site_email = $this->common->get_setting_value(3);
        $this->load->library('email');

        $this->email->from($site_email, $sitename);
        $this->email->to($data[0]);
        $this->email->subject($subject);
        $mail_body = str_replace("%firstname%", ucfirst($data[1]), str_replace("%status%", $status, str_replace("%sitename%", $sitename, str_replace("%siteurl%", $siteurl, stripslashes($mailformat)))));
        $this->email->message($mail_body);
//        echo "<pre>";
//        echo $data[0];
//        print_r($mail_body);
//        die();
        // if($this->email->send());
    }

    //Function for getting all Settings
    function get_all_setting($sortby = 'settingid', $orderby = 'ASC')
    {
        //Ordering Data
        $this->db->order_by($sortby, $orderby);

        //Executing Query
        $query = $this->db->get('setting');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    //Getting setting value for editing By id
    function get_setting_byid($intid)
    {
        $query = $this->db->get_where('setting', array('settingid' => $intid));

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    //Getting setting value By id
    function get_setting_value($id)
    {

        
        $collection = $this->mongodb->setting;
        $record=$collection->find(array("_id"=>"$id"));
        
        if($record)
        {

            foreach($record as $rec)
            {

                // print_r($rec['settingvalue']);
                return nl2br(($rec['settingvalue']));
                break;
            }
        }
        else{
            return false;
        }
        // print_r($record);exit();
        // exit();

        // $query = $this->db->get_where('setting', array('settingid' => $id));
        // if ($query->num_rows() > 0)
        // {
        //     $result = $query->result_array();
        //     return nl2br(($result[0]['settingvalue']));
        // }
        // else
        // {
        //     return false;
        // }
    }

    //Getting setting field name By id
    function get_setting_fieldname($intid)
    {
        $query = $this->db->get_where('setting', array('settingid' => $intid));

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            return ($result[0]['title']);
        }
        else
        {
            return false;
        }
    }

    function get_email_byid($id)
    {

        $query = $this->db->get_where('mail_templates', array('id' => $id));

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    public function get_admin_detail($fields, $admin_id)
    {
        $this->db->select($fields);
        $this->db->where('admin_id', $admin_id);
        $query = $this->db->get('admin');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    public function get_admin_data($fields, $admin_id)
    {
        $this->db->select($fields);
        $this->db->where('admin_id', $admin_id);
        $query = $this->db->get('admin');
        if ($query->num_rows() > 0)
        {
            $res = $query->result_array();
            return $res[0][$fields];
        }
        else
        {
            return array();
        }
    }

    function get_group_detail($temp)
    {

        $query = $this->db->get_where('admin_groups', array('id' => $temp, 'status' => 'Enable'));

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    function get_all_menuurl()
    {
        $this->db->select('url');
        $query = $this->db->get('admin_menu');

        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            //print_r($result);die();

            return $result;
        }
        else
        {
            return false;
        }
    }

    // get url value
    function get_url_byid($urlid)
    {
        $this->db->select('url');
        $query = $this->db->get_where('admin_menu', array('uniqueid' => $urlid));

        if ($query->num_rows() > 0)
        {
            // echo "<pre>"; print_r($this->data['urlval']);die();  
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

    function get_mail_byid($emailid)
    {
        $mail = $this->db->get_where('email_format', array('emailid' => $emailid));
        if ($mail->num_rows() > 0)
        {
            return $mail->result_array();
        }
        else
        {
            return array();
        }
    }

    public function get_user_data()
    {
        $this->db->select('u.*');
        $this->db->from('user u');
        $this->db->where('u.status !=', 'Deleted');
        $this->db->order_by('u.user_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_user_data_by_userid($userid)
    {
        $this->db->select('u.*');
        $this->db->from('user u');
        $this->db->where('u.status !=', 'Deleted');
        $this->db->where('u.userid', $userid);
        $result = $this->db->get();
        return $result->result_array();
    }
    
 // select data using colum id
    function select_database_by_muliple_where($tablename, $condition, $data = '*', $order_by = '', $dir = 'DESC')
    {
        $collection = $this->mongodb->$tablename;
        if($dir == 'ASC'){
            $dir = 1;
        }
        else
        {
            $dir = -1;
        }
    

        $filter  = [];
        $options = [];

        if ($order_by != '' && $dir != '')
        {
            $options = array('sort' => array($order_by => $dir));
        }

        $record=$collection->find($condition, $options);
        if(count($record) > 0)
        {
            return $record = $record->toArray();
        }
        else
        {
            return array();
        }

        

        // exit();

        // $this->db->select($data);
        // $this->db->where($condition);
        // if ($order_by != '' && $dir != '')
        // {
        //     $this->db->order_by($order_by, $dir);
        // }
        // if ($order_by != '' && $dir == '')
        // {
        //     $this->db->order_by($order_by, 'ASC');
        // }
        // $query = $this->db->get($tablename);
        // if ($query->num_rows() > 0)
        // {
        //     return $query->result_array();
        // }
        // else
        // {
        //     return array();
        // }
    }
    
    function select_data_by_condition_where_or($tablename, $contition_array = array(),$contition_array1 = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(),$groupby = '')
    {
        
        $this->db->select($data);
        //if join_str array is not empty then implement the join query
        if (!empty($join_str))
        {
            foreach ($join_str as $join)
            {
                if ($join['join_type'] == '')
                {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                }
                else
                {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }

        //condition array pass to where condition
        $this->db->where($contition_array);
        
        $this->db->or_where($contition_array1);
        
        //Setting Limit for Paging
        if ($limit != '' && $offset == 0)
        {
            $this->db->limit($limit);
        }
        else if ($limit != '' && $offset != 0)
        {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '')
        {
            $this->db->order_by($sortby, $orderby);
        }
        
        //group by query
        if ($groupby != '')
        {
            $this->db->group_by($groupby);
        }

        $query = $this->db->get($tablename);
        //if limit is empty then returns total count
        if ($limit == '')
        {
            $query->num_rows();
        }
        //if limit is not empty then return result array
        return $query->result_array();
    }
    
    function select_data_by_condition($tablename, $contition_array = array(), $data = '*', $sortby = '', $orderby = '', $limit = '', $offset = '', $join_str = array(), $groupby = '')
    {

        $collection = $this->mongodb->test_collection;
        if($orderby == 'ASC'){
            $orderby = 1;
        }
        else
        {
            $orderby = -1;
        }
    

        $filter  = [];
        $options = [];


        
        if($limit != '')
        {
            $options['limit']  = (int) $limit; 
        }

        if($offset != '')
        {

            $options['skip']  = (int) $offset; 
        }

        if ($sortby != '' && $orderby != '')
        {
            $sort = array($sortby => $orderby);
            $options['sort']  = $sort;            
        }
        echo "string";exit();
        $date = new Date('2016-12-27');
        $contition_array = array('Report_Date_as_MM_DD_YYYY' => array( '$eq' => $date ));
        print_r($contition_array);exit();
        $record=$collection->find($contition_array, $options);
         // print_r($record);exit();
        if(count($record) > 0)
        {
            return $record = $record->toArray();
        }
        else
        {
            return array();
        }

        exit();


        $this->db->select($data);
        //if join_str array is not empty then implement the join query
        if (!empty($join_str))
        {
            foreach ($join_str as $join)
            {
                if ($join['join_type'] == '')
                {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id']);
                }
                else
                {
                    $this->db->join($join['table'], $join['join_table_id'] . '=' . $join['from_table_id'], $join['join_type']);
                }
            }
        }

        //condition array pass to where condition
        $this->db->where($contition_array);


        //Setting Limit for Paging
        if ($limit != '' && $offset == 0)
        {
            $this->db->limit($limit);
        }
        else if ($limit != '' && $offset != 0)
        {
            $this->db->limit($limit, $offset);
        }
        //order by query
        if ($sortby != '' && $orderby != '')
        {
            $this->db->order_by($sortby, $orderby);
        }
        
        //group by query
        if ($groupby != '')
        {
            $this->db->group_by($groupby);
        }

        $query = $this->db->get($tablename);
        //if limit is empty then returns total count
        if ($limit == '')
        {
            $query->num_rows();
        }
        //if limit is not empty then return result array
        return $query->result_array();
    }

    public function multipleEvent($id, $value, $columname, $tablename)
    {
        if ($value == "Enable")
        {
            $data = array('status' => 'Enable');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if ($value == "Disable")
        {
            $data = array('status' => 'Disable');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if ($value == "Delete")
        {
            $data = array('status' => 'Deleted');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function multipleDeleteEvent($id, $value, $columname, $tablename)
    {
        if ($value == "Delete")
        {
            $this->db->where_in($columname, $id);
            if ($this->db->delete($tablename))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    function sendEmail($app_name = '', $app_email = '', $to_email = '', $subject = '', $mail_body = '')
    {
        $this->config->load('email', TRUE);
        $this->cnfemail = $this->config->item('email');

        //Loading E-mail Class
        $this->load->library('email');
        $this->email->initialize($this->cnfemail);

        $this->email->from($app_email, $app_name);

        $this->email->to($to_email);

        $this->email->subject($subject);




        $this->email->message("<table border='0' cellpadding='0' cellspacing='0'><tr><td></td></tr><tr><td>" . $mail_body . "</td></tr></table>");
        $this->email->send();
        return;
    }

        public function multipleEvents($id, $value, $columname, $tablename, $fieldname)
    {
        if ($value == "Enable")
        {
            $data = array($fieldname => 'Enable');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if ($value == "Disable")
        {
            $data = array($fieldname => 'Disable');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if ($value == "Delete")
        {
            $data = array($fieldname => 'Deleted');
            $this->db->where_in($columname, $id);
            if ($this->db->update($tablename, $data))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    
    function getCarDetail($agency_id)
    {
        $this->db->select('v.*,vt.vehicle_type_name');
        $this->db->from('vehicle v');
        $this->db->join('vehicle_type vt','v.vehicle_type_id = vt.vehicle_type_id','left');
        $this->db->where('v.agency_type', $agency_id);
        $this->db->where('v.vehicle_status !=', 'Deleted');
        $this->db->order_by('vehicle_id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }
    
    function update_data_by_multiple_where($data,$tablename,$where = array())
    {
        $this->db->where($where);
        if($this->db->update($tablename,$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function permission()
    {

        $session_array = $this->session->userdata('admin_data');
        $collection = $this->mongodb->permission;
        $condition = array('role_id' => $session_array['role'] );
        $record = $collection->aggregate([['$match'=>$condition],['$lookup' => [ 'from' => 'users',  'localField' => 'role_id', 'foreignField'  =>  'user_role', 'as'  => 'userdetails']]]);
        $permission_list = array();
        if(count($record) > 0)
        {
            $permission = $record->toArray();

            foreach ($permission as $value)
            {
                $permission_list[] = $value['permission_name'];
            }
            return $permission_list;
        }
        else
        {
            return array();
        }



        // $this->db->select('permission.permission_name');
        // $this->db->join('users', 'users.user_role = permission.role_id', 'left');
        // $this->db->where('user_role', $session_array['role']);
        // $query = $this->db->get('permission');
        
        // if ($query->num_rows() > 0)
        // {
        //     $permission = $query->result_array();
        //     foreach ($permission as $value)
        //     {
        //         $permission_list[] = $value['permission_name'];
        //     }
        //     return $permission_list;
        // }
        // else
        // {
        //     return array();
        // }
    }
}
