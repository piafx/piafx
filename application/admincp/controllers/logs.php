<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('aspl_hrmadmin_data')) {
//If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

//Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'admin';

        $this->load->model('common');
        $res = $this->common->select_database_id('masteradmin', 'adminid', (int) 1, '*');
        $this->data['logo'] = $res[0]['image'];
        
//Load leftsidemenu and save in variable
        $this->data['name'] = $this->session->userdata['aspl_hrmadmin_data']['name'];
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->data['sidebar'] = $this->load->view('sidebar', $this->data, true);

        $this->load->model('common');
         $this->load->model('log_modal');
    }

    public function index() {

        
       $data1 = $this->log_modal->get_log_data();
       
       $i=0;
       $this->data['data']=array();
       foreach ($data1 as $d)
       {    
           
           if($d['role_name']=='Admin')
           {
                
               $id=$d['log_id'];
               $just=$this->log_modal->get_user_data($id);
               if($just[0]['method_name']=='Login' || $just[0]['method_name']=='New Password' || $just[0]['method_name']=='Reset Password')
               {
                   $just[0]['fname']='';
                   $just[0]['lname']='';
               }
               array_push($this->data['data'], $just);
               
           }
           else
           {
               $id=$d['log_id'];
               $just=$this->log_modal->get_user_data1($id);
               $just[0]['adminname']=$just[0]['fn'].' '.$just[0]['ln'] ;
               if($just[0]['method_name']=='Login' || $just[0]['method_name']=='New Password' || $just[0]['method_name']=='Reset Password')
               {
                   $just[0]['fname']='';
                   $just[0]['lname']='';
               }
               array_push($this->data['data'], $just);
           }
           $i++;
       }
       
       $this->load->view('log/index', $this->data);
    }


}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */
            