<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newpassword extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data,$request_data;

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        if ($this->session->userdata('aspl_hrmadmin_data')) {

            redirect('admin', 'refresh');
        }
        
        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Change Password';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(4);

        //Load leftsidemenu and save in variable
        $this->data['topmenu'] = '';

        //Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
         $this->load->model('common');
    }

    public function index() {
        // LOAD LIBRARIES
           
        
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        // LOAD HELPERS
        $this->load->helper('form');
        
         // SET VALIDATION RULES
        //$this->form_validation->set_rules('user_email', 'email', 'required');
        
        $this->form_validation->set_error_delimiters('<em>', '</em>');
        // has the form been submitted and with valid form info (not empty values)
        
        //get token
        $token=$this->uri->segment(3);
        
        if($token!=NULL)
        {
            
            $admin_data = $this->common->checktoken('resetsetting','resettoken',$token);
            
            if($admin_data!=null)
            {
                $email = $admin_data[0]['adminemail'];
                $resettoken = $admin_data[0]['resettoken'];
                $this->data['email']=$email;
                $this->data['resettoken']=$resettoken;
                $this->load->view('newpassword', $this->data);
            }
            else {
            $this->session->set_flashdata('message', 'You are not arthorized');
             redirect('adminlogin', 'refresh');
            }
        }
        else {
            $this->session->set_flashdata('message', 'Please request again');
            redirect('resetpassword', 'refresh');
        }


        
    }
    function change()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
        
       $this->form_validation->set_rules('pass', 'Pass', 'required|required|matches[cpass]|min_length[6]');
       $this->form_validation->set_rules('cpass', 'Password Confirmation', 'required|min_length[6]');
       $adminemail =  $this->input->post('adminemail');
       $resettoken = $this->input->post('resettoken');
        if ($this->form_validation->run() == TRUE) {
        
            //$adminemail = $this->input->post('adminemail');
            $pass = $this->input->post('pass');
            $cpass = $this->input->post('cpass');       
            if($pass!= '' && $cpass != '' && $pass == $cpass)
            {
                
            
            $admin_data = $this->common->update_data( array('adminpassword'=>base64_encode($pass)),'masteradmin','adminemail',$adminemail);
             
            if (!empty($admin_data)) 
                     {
                       $admin_data1 = $this->common->select_database_by_muliple_where('masteradmin', array('adminemail' => $adminemail), $data = '*', $order_by = '', $dir = '');
                       $admin_id = $admin_data1[0]['adminid'];
                       $role_id = $admin_data1[0]['role_id'];
                       $this->add_log($admin_id, 'New Password', 1,$role_id);
                       $this->common->delete_data('resetsetting','adminemail',$adminemail);
                       $this->send_verification_mail($adminemail);
                       $this->session->set_flashdata('success', 'Your Password is changed successfully.');
                      
                       redirect('adminlogin', 'refresh');
                 }
             }  
         
            else
            {
            $this->session->set_flashdata('message', 'Please request again. Something Wrong');
            $this->data['email']=$adminemail;
             redirect('newpassword', 'refresh');
            }
        }
        else
        {
             
            $this->data['email']=$adminemail;
             $this->data['resettoken']=$resettoken;
             $this->session->set_flashdata('message', 'Your new password does not match with confirm password');
            redirect('newpassword/index/'.$resettoken, 'refresh');
        }
    }
     public function send_verification_mail($user_email)
    {
        $config = array();
        $res = $this->common->select_database_id('mail', 'mail_id', 1, '*');
        if(!empty($res))
        {
                    $host = $res[0]['mail_smpt_host'];
                    $port = $res[0]['mail_smpt_port'];
                    $user = $res[0]['mail_smpt_user'];
                    $password = base64_decode($res[0]['mail_smpt_password']);

                    $config['protocol'] = "smtp";
                    $config['smtp_host'] = $host;
                    $config['smtp_port'] = $port;
                    $config['smtp_user'] = $user;
                    $config['smtp_pass'] = $password;
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "html";
                    $config['newline'] = "\r\n";
                    $from_email = "aspltest3@gmail.com";
                    $to_email = "aspltest3@gmail.com";
                    $this->load->library('email');
                    $this->email->initialize($config);
                    $this->email->from($from_email, 'Support');
                    $this->email->to($user_email);
                    $this->email->subject('Reset Your Password');

                    $message = /* -----------email body starts----------- */
                     "Dear Employee,<br/><br/>You have recently change your password for login into HRM panel<br/><br/>"
                    ."Sincerely,<br/>
            Ashapurasoftech Pvt. Ltd.";
                    /* -----------email body ends----------- */

                     $this->email->message($message);


                    //Send mail
                    if ($this->email->send()) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            } else {
                return FALSE;
            }
    } 
    public function add_log($to_id,$method_name,$result,$role_id)
    {
        $ci =& get_instance();
        $controller_name=$ci->router->fetch_class();
        $by_id=$to_id;
        $role=$role_id;
        $data1 = array(
                'by_id'=>$by_id,
                'role_id'=>$role,
                'to_id'=>$to_id,
                'controller_name'=> $controller_name,
                'method_name' => $method_name,
                'created_date' => date('Y-m-d H:i:s'),
                'result'=> $result,  
                   );
        $this->common->insert_data($data1, 'admin_log');
        
    }
 
}


