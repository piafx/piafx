<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Market extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Market';
        
        $session_array = $this->session->userdata('admin_data');
        $res = $this->common->select_database_id('users', 'user_id',(int) $session_array['ad_id'] , '*');
        $this->data['user_role'] = $res[0]['user_role'];
        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('excel');
        
        $this->load->model('common');
        $this->check_permission();
        
    }

    public function index() {
        
        $join = array(
            array(
                'table'=>'market_assign_to',
                'join_table_id'=>'market_assign_to.market_id',
                'from_table_id'=>'market.market_id',
                'join_type'=>'left',
            ),
            array(
                'table'=>'users as u',
                'join_table_id'=>'u.user_id',
                'from_table_id'=>'market_assign_to.assign_to_id',
                'join_type'=>'left',
            ),
            array(
                'table'=>'users as c',
                'join_table_id'=>'c.user_id',
                'from_table_id'=>'client',
                'join_type'=>'left',
            )
        );
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
             $this->data['data'] = $this->common->select_data_by_condition_where_or('market',array('client'=>$session_array['ad_id']), array('market_assign_to.assign_to_id'=>$session_array['ad_id']),'u.user_firstname as fu,u.user_lastname as lu,c.user_firstname as cfu,c.user_lastname as clu,market.*', '', '', '', '', $join,'market.market_id');  
             $this->load->view('market/index', $this->data);
        }
        else
        {
             $this->data['data'] = $this->common->select_data_by_condition('market', array(),'u.user_firstname as fu,u.user_lastname as lu,c.user_firstname as cfu,c.user_lastname as clu,market.*', '', '', '', '', $join,'market.market_id');  
             $this->load->view('market/index', $this->data);
        }
       
    }
    
    
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'insertdata')
        {
            $method = 'add';
        }
        if($method == 'editform' || $method == 'update')
        {
            $method = 'edit';
        }
        if($method == 'import' || $method == 'import_data' || $method == 'view_import' || $method == 'get_import' || $method == 'update_import' || $method == 'get_users' || $method == 'get_coordinator')
        {
            $method = 'import';
        }
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }
    
    public function add()
    {
//        $session_array = $this->session->userdata('admin_data');
//        if($session_array['role'] != 1)
//        {
//            $this->data['data'] = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>3,'coordinator'=>$session_array['ad_id']), '*', '', '');
//        }
//        else
//        {
//            $this->data['data'] = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>3), '*', '', '');
//        }
        
//        $this->data['data'] = $this->common->get_data_all('users');
        $this->data['data'] = $this->common->select_database_by_muliple_where('users',array('user_status' => 'Enable'), '*', '', '');
        $this->load->view('market/add', $this->data);
    }
    
    public function get_users(){
        
        $var = $_GET['$callback'];
        $token=$this->uri->segment(3);
        //$res = $this->common->get_data_all('users');
//        $res1['user_firstname'] = "Select User From List";
//        $res1['user_lastname'] = "Select User From List";
//        $res1['user_id'] = "";
//        array_unshift($res, $res1);
//        print_r($res);exit();
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
            $res = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>3,'coordinator'=>$session_array['ad_id']), '*', '', '');
        }
        else
        {
            $res = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>3,'coordinator'=>$token), '*', '', '');
        }
        
        foreach ($res as $key => $value)
        {
            $res[$key]['user_firstname'] = $value['user_firstname'].' '.$value['user_lastname'];
        }
        $data['d']['results'] = $res;
        echo $var.'('.json_encode($data).')';
        
    }
    public function get_coordinator(){
        
        $var = $_GET['$callback'];
        $token=$this->uri->segment(3);
        //$res = $this->common->get_data_all('users');
//        $res1['user_firstname'] = "Select User From List";
//        $res1['user_lastname'] = "Select User From List";
//        $res1['user_id'] = "";
//        array_unshift($res, $res1);
//        print_r($res);exit();
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
            $join = array(
                array(
                    'table'=>'market_assign_to',
                    'join_table_id'=>'market_assign_to.assign_to_id',
                    'from_table_id'=>'users.user_id',
                    'join_type'=>'left',
                ),
                array(
                    'table'=>'market',
                    'join_table_id'=>'market_assign_to.market_id',
                    'from_table_id'=>'market.market_id',
                    'join_type'=>'left',
                ),
            );
            $res = $this->common->select_data_by_condition('users', array('users.user_status'=>'Enable','users.user_role'=>2,'market_assign_to.market_id'=>$token,'users.coordinator'=>$session_array['ad_id']),'users.*', '', '', '', '', $join,'users.user_id');
            
            //$res = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>2,'coordinator'=>$session_array['ad_id']), '*', '', '');
        }
        else
        {
            $join = array(
                array(
                    'table'=>'market_assign_to',
                    'join_table_id'=>'market_assign_to.assign_to_id',
                    'from_table_id'=>'users.user_id',
                    'join_type'=>'left',
                ),
                array(
                    'table'=>'market',
                    'join_table_id'=>'market_assign_to.market_id',
                    'from_table_id'=>'market.market_id',
                    'join_type'=>'left',
                ),
            );
            $res = $this->common->select_data_by_condition('users', array('users.user_status'=>'Enable','users.user_role'=>2,'market_assign_to.market_id'=>$token),'users.*', '', '', '', '', $join,'users.user_id');
            
            //$res = $this->common->select_database_by_muliple_where('users', array('user_status' => 'Enable','user_role'=>2), '*', '', '');
        }
        foreach ($res as $key => $value)
        {
            $res[$key]['user_firstname'] = $value['user_firstname'].' '.$value['user_lastname'];
        }
        $data['d']['results'] = $res;
        echo $var.'('.json_encode($data).')';
        
    }

    public function insertdata()
    {
        $this->form_validation->set_rules('market_name', 'Market Name', 'required');
        $this->form_validation->set_rules('market_desc');
        $this->form_validation->set_rules('assignto', 'Assign To', 'required');
//        $this->form_validation->set_rules('client', 'Client', 'required');
        
        if ($this->form_validation->run() == TRUE)
        {
                     $market_name = $this->input->post('market_name');
                     $market_desc = $this->input->post('market_desc');
                     $assignto[] = $this->input->post('assignto');
                     $client = $this->input->post('client');
//                     print_r($assignto);exit();
                     $data = array(
                         'market_name' => $market_name,
                         'market_desc' => $market_desc,
                    //     'assignto' => $assignto,
                         'client' => $client,
                         'created_date'=>date('Y-m-d'),
                     );

             $res_insert = $this->common->insert_data_getid($data, 'market');
             if($res_insert)
             {
                 foreach ($assignto[0] as $assign)
                 {
                     $data = array(
                         'market_id' => $res_insert,
                         'assign_to_id' => $assign
                     );
                     $this->common->insert_data_getid($data, 'market_assign_to');
                 }
                 
                 $this->session->set_flashdata('success', 'Record has been inserted successfully.');
                 redirect('market', 'refresh');
             }
             else
             {
                 $log->warn("Error while inserting record.");
                 $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                 redirect('market', 'refresh');
             }
        }
        else
        {
            $this->data['data'] = $this->common->get_data_all('users');
            $this->load->view('market/add', $this->data);
        }
    }

    function editform()
    {
        
        $token=$this->uri->segment(3);
        if($token != '')
        {
            $token = $this->data['edit_id'] = base64_decode($token);
            
            $session_array = $this->session->userdata('admin_data');
            if($session_array['role'] != 1)
            {
                $res_assign = $this->common->select_data_by_condition('market_assign_to', array('market_id'=>(int) $token,'assign_to_id'=>(int)$session_array['ad_id']), '', 'market_id', 'DESC', '', '');
                if(empty($res_assign))
                {
                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('market', 'refresh');
                    exit();
                }
            } 
            
            $join = array(
                array(
                    'table'=>'market_assign_to',
                    'join_table_id'=>'market_assign_to.market_id',
                    'from_table_id'=>'market.market_id',
                    'join_type'=>'left',
                )
            );
            $res = $this->common->select_data_by_condition('market', array('market.market_id'=>(int) $token),'market.*,GROUP_CONCAT(market_assign_to.assign_to_id) as assign_to1', '', '', '', '', $join,'market.market_id');  
            if(!empty($res))
            {
                $this->data['data'] = $this->common->select_database_by_muliple_where('users',array('user_status' => 'Enable'), '*', '', '');
                $this->data['market'] = $res;
                $this->load->view('market/edit', $this->data);
            }
            
        }
        else
        {
            $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
            redirect('market', 'refresh');
        }
        
        
    }

    public function update()
    {
        $edit_id     = $this->input->post('id');        
        if($edit_id != '')
        {
            
            $this->form_validation->set_rules('market_name', 'Market Name', 'required');
            $this->form_validation->set_rules('market_desc');
            $this->form_validation->set_rules('assignto', 'Assign To', 'required');
//            $this->form_validation->set_rules('client', 'Client', 'required');
            
            if ($this->form_validation->run() == TRUE)
            {
                $session_array = $this->session->userdata('admin_data');
                if($session_array['role'] != 1)
                {
                    $res_assign = $this->common->select_data_by_condition('market_assign_to', array('market_id'=>(int) $token,'assign_to_id'=>(int)$session_array['ad_id']), '', 'market_id', 'DESC', '', '');
                    if(empty($res_assign))
                    {
                        $this->session->set_flashdata('message', 'Record not found.');
                        redirect('market', 'refresh');
                        exit();
                    }
                } 
                
                $market_name = $this->input->post('market_name');
                $market_desc = $this->input->post('market_desc');
                $assignto = $this->input->post('assignto');
                $client = $this->input->post('client');
                $data = array(
                    'market_name' => $market_name,
                    'market_desc' => $market_desc,
//                    'assignto' => $assignto,
                    'client' => $client,
                );
                if ($this->common->update_data($data, 'market', 'market_id', (int) base64_decode($edit_id)))
                {
                    $this->common->delete_data_by_multiple_where('market_assign_to', array('market_id'=>(int) base64_decode($edit_id)));
                    foreach ($assignto as $assign)
                    {
                        $data = array(
                            'market_id' => (int) base64_decode($edit_id),
                            'assign_to_id' => $assign
                        );
                        $this->common->insert_data_getid($data, 'market_assign_to');
                    }
                    $this->session->set_flashdata('success', 'Record has been updated successfully.');
                    redirect('market', 'refresh');
                }
            }
            else
            {
                $this->data['data'] = $this->common->get_data_all('users');
                $this->data['edit_id'] = '';
                $this->data['market'] = $this->common->select_database_id('market', 'market_id', (int) base64_decode($edit_id), '*');
                $this->load->view('market/edit', $this->data);
            }
        }
        else
        {
            
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('market', 'refresh');
        }
        
        
    }

    public function import()
    {
        $session_array = $this->session->userdata('admin_data');
        $join = array(
            array(
                'table'=>'market_assign_to',
                'join_table_id'=>'market_assign_to.market_id',
                'from_table_id'=>'market.market_id',
                'join_type'=>'',
            )
        );
        if($session_array['role'] != 1)
        {
             $this->data['data'] = $this->common->select_data_by_condition_where_or('market',array('client'=>$session_array['ad_id']), array('market_assign_to.assign_to_id'=>$session_array['ad_id']),'*', '', '', '', '',$join,'market.market_id');
        }
        else
        {
             $this->data['data'] = $this->common->select_data_by_condition('market', array(),'market.*', '', '', '', '', $join,'market.market_id');
        }
        
        //$this->data['data'] = $this->common->get_data_all('market');
        $this->load->view('market/import', $this->data);
    }
    
    public function import_data()
    {
        $this->form_validation->set_rules('market', 'Market', 'required');
        if ($this->form_validation->run()) 
        {
            if(isset($_FILES["file"]["tmp_name"]))
            {
                    $path = $_FILES["file"]["tmp_name"];
                    $object = PHPExcel_IOFactory::load($path);
                    foreach($object->getWorksheetIterator() as $worksheet)
                    {
                        $highestRow = $worksheet->getHighestRow();
			$highestColumn = $worksheet->getHighestColumn();
                        for($row=2; $row<=$highestRow; $row++)
                        {
                                $market = $this->input->post('market');
                                $market_name = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                                $cascade_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                                $planned_cell_Id = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                                $carrier_sector_id = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                                $phone_type = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                                $call_type = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                                $cell_site_common_name = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                                
                                $inbuildsite = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                                
                                $site_lat = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                                $site_long = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                                $test_location_lat = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                                $test_location_long = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
                                $PSAP_name = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
                                $PSAP_fcc_id = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                                $pani = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                                $enodeB_id = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                                $freq_band_class = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
                                $freq = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                                $pci = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                                $EARFCN = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
                                $azimuth = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
                                $street_add = $worksheet->getCellByColumnAndRow(21, $row)->getValue();
                                $city = $worksheet->getCellByColumnAndRow(22, $row)->getValue();
                                $state = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
                                $CNTY = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
                                $antenna_lo = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
                                $test_date = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
                                
                                
                                
                                $tester_name = $worksheet->getCellByColumnAndRow(27, $row)->getValue();
                                $seq = $worksheet->getCellByColumnAndRow(28, $row)->getValue();
                                $disatance = $worksheet->getCellByColumnAndRow(29, $row)->getValue();
                                $PSAP_contact = $worksheet->getCellByColumnAndRow(30, $row)->getValue();
                                $PSAP_ontact_name = $worksheet->getCellByColumnAndRow(31, $row)->getValue();
                                $PSAP_requirements = $worksheet->getCellByColumnAndRow(32, $row)->getValue();
                                $callback = $worksheet->getCellByColumnAndRow(33, $row)->getValue();
                                $IMSI = $worksheet->getCellByColumnAndRow(34, $row)->getValue();
                                $cell_id_tested = $worksheet->getCellByColumnAndRow(35, $row)->getValue();
                                $call_type_final = $worksheet->getCellByColumnAndRow(36, $row)->getValue();
                                $latitude = $worksheet->getCellByColumnAndRow(37, $row)->getValue();
                                $longtude = $worksheet->getCellByColumnAndRow(38, $row)->getValue();
                                $test_time_date = $worksheet->getCellByColumnAndRow(39, $row)->getValue();
                                $test_time_date_end = $worksheet->getCellByColumnAndRow(40, $row)->getValue();
                                $contract = $worksheet->getCellByColumnAndRow(41, $row)->getValue();
                                $tester_name_final = $worksheet->getCellByColumnAndRow(42, $row)->getValue();
    //                                $position_search = $worksheet->getCellByColumnAndRow(39, $row)->getValue();
    //                                $position_search1 = $worksheet->getCellByColumnAndRow(40, $row)->getValue();
                                $comments = $worksheet->getCellByColumnAndRow(43, $row)->getValue();
                                $ESRK = $worksheet->getCellByColumnAndRow(44, $row)->getValue();
                                $ground_truth_latitude = $worksheet->getCellByColumnAndRow(45, $row)->getValue();
                                $ground_truth_longtude = $worksheet->getCellByColumnAndRow(46, $row)->getValue();
                                $LAT_WCQT = $worksheet->getCellByColumnAndRow(47, $row)->getValue();
                                $LONG_WCQT = $worksheet->getCellByColumnAndRow(48, $row)->getValue();
                                $LAM_latitude = $worksheet->getCellByColumnAndRow(49, $row)->getValue();
                                $LAM_longitude = $worksheet->getCellByColumnAndRow(50, $row)->getValue();
                                $LAM_uncertainty = $worksheet->getCellByColumnAndRow(51, $row)->getValue();
                                $disatance_formula = $worksheet->getCellByColumnAndRow(52, $row)->getValue();
                                $attempted_calls = $worksheet->getCellByColumnAndRow(53, $row)->getValue();
                                $calls_not_selected = $worksheet->getCellByColumnAndRow(54, $row)->getValue();
                                $cell_id = $worksheet->getCellByColumnAndRow(55, $row)->getValue();
                                $sector = $worksheet->getCellByColumnAndRow(56, $row)->getValue();
                                $PSAP = $worksheet->getCellByColumnAndRow(57, $row)->getValue();
                                $locked_cell = $worksheet->getCellByColumnAndRow(58, $row)->getValue();
                                $distance_btwn = $worksheet->getCellByColumnAndRow(59, $row)->getValue();
                                $lrf_cmnt = $worksheet->getCellByColumnAndRow(60, $row)->getValue();
                                $team = $worksheet->getCellByColumnAndRow(61, $row)->getValue();
                                
                                $operator_id = $worksheet->getCellByColumnAndRow(62, $row)->getValue();
                                $operator_id_verify = $worksheet->getCellByColumnAndRow(63, $row)->getValue();
                                $operator_id_issue = $worksheet->getCellByColumnAndRow(64, $row)->getValue();
                                $e911address = $worksheet->getCellByColumnAndRow(65, $row)->getValue();
                                $e911address_verify = $worksheet->getCellByColumnAndRow(66, $row)->getValue();
                                $e911address_issue = $worksheet->getCellByColumnAndRow(67, $row)->getValue();
                                $e911phIIcall = $worksheet->getCellByColumnAndRow(68, $row)->getValue();
                                $e9111strabid = $worksheet->getCellByColumnAndRow(69, $row)->getValue();
                                $e9112ndrabid = $worksheet->getCellByColumnAndRow(70, $row)->getValue();
                                $degree_radio = $worksheet->getCellByColumnAndRow(71, $row)->getValue();
                                $e911contact_verify = $worksheet->getCellByColumnAndRow(72, $row)->getValue();
                                $e911contact_issue = $worksheet->getCellByColumnAndRow(73, $row)->getValue();
                                $e911name_verify = $worksheet->getCellByColumnAndRow(74, $row)->getValue();
                                $E911name_issue = $worksheet->getCellByColumnAndRow(75, $row)->getValue();
                                $long_verify = $worksheet->getCellByColumnAndRow(76, $row)->getValue();
                                $long_issue = $worksheet->getCellByColumnAndRow(77, $row)->getValue();
                                $lat_verify = $worksheet->getCellByColumnAndRow(78, $row)->getValue();
                                $lat_issue = $worksheet->getCellByColumnAndRow(79, $row)->getValue();
                                $esrkrange_verify = $worksheet->getCellByColumnAndRow(80, $row)->getValue();
                                $esrkrange_issue = $worksheet->getCellByColumnAndRow(81, $row)->getValue();
                                $defaultesrk = $worksheet->getCellByColumnAndRow(82, $row)->getValue();
                                $defaultesrk_verify = $worksheet->getCellByColumnAndRow(83, $row)->getValue();
                                $defaultesrk_issue = $worksheet->getCellByColumnAndRow(84, $row)->getValue();
                                $betweenesrk = $worksheet->getCellByColumnAndRow(85, $row)->getValue();
                                $betweenesrk_verify = $worksheet->getCellByColumnAndRow(86, $row)->getValue();
                                $betweenesrk_issue = $worksheet->getCellByColumnAndRow(87, $row)->getValue();
                                $notinrange = $worksheet->getCellByColumnAndRow(88, $row)->getValue();
                                $uncertainty = $worksheet->getCellByColumnAndRow(89, $row)->getValue();
                                $uncertainty_verfiy = $worksheet->getCellByColumnAndRow(90, $row)->getValue();
                                $uncertainty_issue = $worksheet->getCellByColumnAndRow(91, $row)->getValue();
                                $uncertaintymeter = $worksheet->getCellByColumnAndRow(92, $row)->getValue();
                                $uncertaintymeter_verify = $worksheet->getCellByColumnAndRow(93, $row)->getValue();
                                $uncertaintymeter_issue = $worksheet->getCellByColumnAndRow(94, $row)->getValue();
                                $cbmatch = $worksheet->getCellByColumnAndRow(95, $row)->getValue();
                                $e911match = $worksheet->getCellByColumnAndRow(96, $row)->getValue();
                                $addressmatch = $worksheet->getCellByColumnAndRow(97, $row)->getValue();
                                $phase = $worksheet->getCellByColumnAndRow(98, $row)->getValue();
                                $comments_testing = $worksheet->getCellByColumnAndRow(99, $row)->getValue();
                                
                                
                                //$res = $this->common->select_database_id('users', 'user_firstname+" "+ user_lastname', $team, '*');
                                //$res = $this->common->select_database_id('market_data', 'cascade_id', $cascade_id, '*');
                                $res = $this->common->select_database_by_muliple_where('market_data', array('planned_cell_Id' => $planned_cell_Id,'market_id'=>$market), $data = '*', $order_by = '', $dir = '');
                                if($tester_name != '')
                                {
                                    $res1 = $this->common->select_database_by_muliple_where('users', array('CONCAT(user_firstname, " ", user_lastname) =' => $tester_name), $data = '*', $order_by = '', $dir = '');
                                }
                                else
                                {
                                    $res1 = array();
                                }
                                if($tester_name_final != '')
                                {
//                                echo $this->db->last_query();
                                $res2 = $this->common->select_database_by_muliple_where('users', array('CONCAT(user_firstname, " ", user_lastname) =' => $tester_name_final), $data = '*', $order_by = '', $dir = '');
                                }
                                else
                                {
                                    $res2 = array();
                                }
                                if($team != '')
                                {
                                    $res3 = $this->common->select_database_by_muliple_where('users', array('CONCAT(user_firstname, " ", user_lastname) =' => $team), $data = '*', $order_by = '', $dir = '');                                }
                                else
                                {
                                    $res3 = array();
                                }
//                                echo $this->db->last_query();
//                                print_r($res1);
//                                print_r($res2);
//                                echo $res1[0]['user_id'];
//                                echo $res2[0]['user_id']."vishal";
//                                exit();
                                if(!empty($res1))
                                {
                                    $tester_id = $res1[0]['user_id'];
                                }
                                else
                                {
                                    $tester_id = 0;
                                    $tester_name = '';
                                }
                                if(!empty($res2))
                                {
                                    $tester_id_final = $res2[0]['user_id'];
                                }
                                else
                                {
                                    $tester_id_final = 0;
                                    $tester_name_final = '';
                                }
                                if(!empty($res3))
                                {
                                    $team_id = $res3[0]['user_id'];
                                }
                                else
                                {
                                    $team_id = 0;
                                    $team = '';
                                }
                                if(empty($res))
                                {
                                    
                                    $data = array(
                                            'inbuildsite'=>$inbuildsite,
                                            'seq'=>$seq,
                                            'disatance'=>$disatance,
                                            'test_time_date_end'=>$test_time_date_end,
                                            'longtude' => $longtude,
                                            'test_time_date' => $test_time_date,
                                            'tester_name_final' => $tester_name_final,
                                            'tester_id_final' => $tester_id_final,
//                                            'position_search' => $position_search,
//                                            'position_search1' => $position_search1,
                                            'comments' => $comments,
                                            'disatance_formula'                   =>	$disatance_formula,
                                            'attempted_calls'			=>	$attempted_calls,
                                            'calls_not_selected'		=>	$calls_not_selected,
                                            'cell_id'		=>	$cell_id,
                                            'e911'			=>	$PSAP,
                                            'ESRK'                   =>	$ESRK,
                                            'ground_truth_latitude'			=>	$ground_truth_latitude,
                                            'ground_truth_longtude'		=>	$ground_truth_longtude,
                                            'LAT_WCQT'		=>	$LAT_WCQT,
                                            'LONG_WCQT'			=>	$LONG_WCQT,
                                            'LAM_latitude'			=>	$LAM_latitude,
                                            'LAM_longitude'			=>	$LAM_longitude,
                                            'LAM_uncertainty'			=>	$LAM_uncertainty,
                                            'e911_requirements'                   =>	$PSAP_requirements,
                                            'callback'			=>	$callback,
                                            'IMSI'		=>	$IMSI,
                                            'cell_id_tested'		=>	$cell_id_tested,
                                            'call_type_final'			=>	$call_type_final,
                                            'latitude'			=>	$latitude,
                                            'test_date'                   =>	$test_date,
                                            'team'			=>	$team,
                                            'team_id'			=>	$team_id,
                                            'tester_name'		=>	$tester_name,
                                            'tester_id'		=>	$tester_id,
                                            'e911_contact'			=>	$PSAP_contact,
                                            'e911_ontact_name'			=>	$PSAP_ontact_name,
                                            'street_add'                   =>	$street_add,
                                            'city'			=>	$city,
                                            'state'		=>	$state,
                                            'CNTY'		=>	$CNTY,
                                            'antenna_lo'			=>	$antenna_lo,
                                            'market_name'                   =>	$market_name,
                                            'cascade_id'			=>	$cascade_id,
                                            'planned_cell_Id'		=>	$planned_cell_Id,
                                            'carrier_sector_id'		=>	$carrier_sector_id,
                                            'phone_type'			=>	$phone_type,
                                            'call_type'		=>	$call_type,
                                            'cell_site_common_name'			=>	$cell_site_common_name,
                                            'site_lat'				=>	$site_lat,
                                            'site_long'		=>	$site_long,
                                            'test_location_lat'			=>	$test_location_lat,
                                            'test_location_long'		=>	$test_location_long,
                                            'e911_name'			=>	$PSAP_name,
                                            'e911_fcc_id'				=>	$PSAP_fcc_id,
                                            'pani'		=>	$pani,
                                            'enodeB_id'			=>	$enodeB_id,
                                            'freq_band_class'		=>	$freq_band_class,
                                            'freq'			=>	$freq,
                                            'pci'				=>	$pci,
                                            'EARFCN'		=>	$EARFCN,
                                            'azimuth'			=>	$azimuth,
                                            'market_id'=> $market,
                                            'lrf_cmnt' => $lrf_cmnt,
                                            'distance_btwn' => $distance_btwn,
                                            'locked_cell'=>$locked_cell,
                                            'sector'=>$sector,
                                            'operator_id'=>$operator_id,
                                            'operator_id_verify'=>$operator_id_verify,
                                            'operator_id_issue'=>$operator_id_issue,
                                            'e911address'=>$e911address,
                                            'e911address_verify'=>$e911address_verify,
                                            'e911address_issue'=>$e911address_issue,
                                            'e911phIIcall'=>$e911phIIcall,
                                            'e9111strabid'=>$e9111strabid,
                                            'e9112ndrabid'=>$e9112ndrabid,
                                            'degree_radio'=>$degree_radio,
                                            'e911contact_verify'=>$e911contact_verify,
                                            'e911contact_issue'=>$e911contact_issue,
                                            'e911name_verify'=>$e911name_verify,
                                            'E911name_issue'=>$E911name_issue,
                                            'long_verify'=>$long_verify,
                                            'long_issue'=>$long_issue,
                                            'lat_verify'=>$lat_verify,
                                            'lat_issue'=>$lat_issue,
                                            'esrkrange_verify'=>$esrkrange_verify,
                                            'esrkrange_issue'=>$esrkrange_issue,
                                            'defaultesrk'=>$defaultesrk,
                                            'defaultesrk_verify'=>$defaultesrk_verify,
                                            'defaultesrk_issue'=>$defaultesrk_issue,
                                            'betweenesrk'=>$betweenesrk,
                                            'betweenesrk_verify'=>$betweenesrk_verify,
                                            'betweenesrk_issue'=>$betweenesrk_issue,
                                            'notinrange'=>$notinrange,
                                            'uncertainty'=>$uncertainty,
                                            'uncertainty_verfiy'=>$uncertainty_verfiy,
                                            'uncertainty_issue'=>$uncertainty_issue,
                                            'uncertaintymeter'=>$uncertaintymeter,
                                            'uncertaintymeter_verify'=>$uncertaintymeter_verify,
                                            'uncertaintymeter_issue'=>$uncertaintymeter_issue,
                                            'cbmatch'=>$cbmatch,
                                            'e911match'=>$e911match,
                                            'addressmatch'=>$addressmatch,
                                            'phase'=>$phase,
                                            'comments_testing'=>$comments_testing
                                    );
                                    $this->common->insert_data($data, 'market_data');
                                }
                                else
                                {

                                    $data = array(
                                            'inbuildsite'=>$inbuildsite,
                                            'seq'=>$seq,
                                            'disatance'=>$disatance,
                                            'test_time_date_end'=>$test_time_date_end,
                                            'longtude' => $longtude,
                                            'test_time_date' => $test_time_date,
                                            'tester_name_final' => $tester_name_final,
                                            'tester_id_final' => $tester_id_final,
//                                            'position_search' => $position_search,
//                                            'position_search1' => $position_search1,
                                            'comments' => $comments,
                                            'disatance_formula'                   =>	$disatance_formula,
                                            'attempted_calls'			=>	$attempted_calls,
                                            'calls_not_selected'		=>	$calls_not_selected,
                                            'cell_id'		=>	$cell_id,
                                            'e911'			=>	$PSAP,
                                            'ESRK'                   =>	$ESRK,
                                            'ground_truth_latitude'			=>	$ground_truth_latitude,
                                            'ground_truth_longtude'		=>	$ground_truth_longtude,
                                            'LAT_WCQT'		=>	$LAT_WCQT,
                                            'LONG_WCQT'			=>	$LONG_WCQT,
                                            'LAM_latitude'			=>	$LAM_latitude,
                                            'LAM_longitude'			=>	$LAM_longitude,
                                            'LAM_uncertainty'			=>	$LAM_uncertainty,
                                            'e911_requirements'                   =>	$PSAP_requirements,
                                            'callback'			=>	$callback,
                                            'IMSI'		=>	$IMSI,
                                            'cell_id_tested'		=>	$cell_id_tested,
                                            'call_type_final'			=>	$call_type_final,
                                            'latitude'			=>	$latitude,
                                            'test_date'                   =>	$test_date,
                                            'team'			=>	$team,
                                            'team_id'			=>	$team_id,
                                            'tester_name'		=>	$tester_name,
                                            'tester_id'		=>	$tester_id,
                                            'e911_contact'			=>	$PSAP_contact,
                                            'e911_ontact_name'			=>	$PSAP_ontact_name,
                                            'street_add'                   =>	$street_add,
                                            'city'			=>	$city,
                                            'state'		=>	$state,
                                            'CNTY'		=>	$CNTY,
                                            'antenna_lo'			=>	$antenna_lo,
                                            'market_name'                   =>	$market_name,
                                            'cascade_id'			=>	$cascade_id,
                                            'planned_cell_Id'		=>	$planned_cell_Id,
                                            'carrier_sector_id'		=>	$carrier_sector_id,
                                            'phone_type'			=>	$phone_type,
                                            'call_type'		=>	$call_type,
                                            'cell_site_common_name'			=>	$cell_site_common_name,
                                            'site_lat'				=>	$site_lat,
                                            'site_long'		=>	$site_long,
                                            'test_location_lat'			=>	$test_location_lat,
                                            'test_location_long'		=>	$test_location_long,
                                            'e911_name'			=>	$PSAP_name,
                                            'e911_fcc_id'				=>	$PSAP_fcc_id,
                                            'pani'		=>	$pani,
                                            'enodeB_id'			=>	$enodeB_id,
                                            'freq_band_class'		=>	$freq_band_class,
                                            'freq'			=>	$freq,
                                            'pci'				=>	$pci,
                                            'EARFCN'		=>	$EARFCN,
                                            'azimuth'			=>	$azimuth,
                                            'market_id'=> $market,
                                            'lrf_cmnt' => $lrf_cmnt,
                                            'distance_btwn' => $distance_btwn,
                                            'locked_cell'=>$locked_cell,
                                            'sector'=>$sector,
                                            'operator_id'=>$operator_id,
                                            'operator_id_verify'=>$operator_id_verify,
                                            'operator_id_issue'=>$operator_id_issue,
                                            'e911address'=>$e911address,
                                            'e911address_verify'=>$e911address_verify,
                                            'e911address_issue'=>$e911address_issue,
                                            'e911phIIcall'=>$e911phIIcall,
                                            'e9111strabid'=>$e9111strabid,
                                            'e9112ndrabid'=>$e9112ndrabid,
                                            'degree_radio'=>$degree_radio,
                                            'e911contact_verify'=>$e911contact_verify,
                                            'e911contact_issue'=>$e911contact_issue,
                                            'e911name_verify'=>$e911name_verify,
                                            'E911name_issue'=>$E911name_issue,
                                            'long_verify'=>$long_verify,
                                            'long_issue'=>$long_issue,
                                            'lat_verify'=>$lat_verify,
                                            'lat_issue'=>$lat_issue,
                                            'esrkrange_verify'=>$esrkrange_verify,
                                            'esrkrange_issue'=>$esrkrange_issue,
                                            'defaultesrk'=>$defaultesrk,
                                            'defaultesrk_verify'=>$defaultesrk_verify,
                                            'defaultesrk_issue'=>$defaultesrk_issue,
                                            'betweenesrk'=>$betweenesrk,
                                            'betweenesrk_verify'=>$betweenesrk_verify,
                                            'betweenesrk_issue'=>$betweenesrk_issue,
                                            'notinrange'=>$notinrange,
                                            'uncertainty'=>$uncertainty,
                                            'uncertainty_verfiy'=>$uncertainty_verfiy,
                                            'uncertainty_issue'=>$uncertainty_issue,
                                            'uncertaintymeter'=>$uncertaintymeter,
                                            'uncertaintymeter_verify'=>$uncertaintymeter_verify,
                                            'uncertaintymeter_issue'=>$uncertaintymeter_issue,
                                            'cbmatch'=>$cbmatch,
                                            'e911match'=>$e911match,
                                            'addressmatch'=>$addressmatch,
                                            'phase'=>$phase,
                                            'comments_testing'=>$comments_testing
                                    );
                                    $this->common->update_data($data, 'market_data', 'market_data_id', $res[0]['market_data_id'] );
                                }
                                
                        }
                        $this->session->set_flashdata('success', 'Record has been inserted successfully.');
                        redirect('market', 'refresh');
                        break;
                    }
            }
            else
            {
                $this->session->set_flashdata('message', 'Please try again.');
                redirect('market/import', 'refresh');
            }
        }
        else
        {
            $this->data['data'] = $this->common->get_data_all('market');
            $this->load->view('market/import', $this->data);
        }
    }
    
    public function update_import()
    {
        
        $var = $_GET['callback'];
        $token=$this->uri->segment(3);
        
        if($token != '')
        {
            $data = $this->input->post("models");
            $data = json_decode($data);
            $ids = array();
            //print_r($data);exit();
            if(!empty($data))
            {
                foreach ($data as $value)
                {
                    $value = json_decode(json_encode($value), True);
                    if(isset($value['market_data_id']))
                    {
                        if(isset($value['tester_name']) && $value['tester_name'] != '')
                        {
                            $res_user = $this->common->select_database_by_muliple_where('users', array('CONCAT(user_firstname, " ", user_lastname) =' => $value['tester_name'],'user_status'=>'Enable'), $data = '*', $order_by = '', $dir = '');
                            if(!empty($res_user))
                            {
                                $value['tester_id'] = $res_user[0]['user_id'];
                            }
                        }
                        if(isset($value['team']) && $value['team'] != '')
                        {
                            $res_user1 = $this->common->select_database_by_muliple_where('users', array('CONCAT(user_firstname, " ", user_lastname) =' => $value['team'],'user_status'=>'Enable'), $data = '*', $order_by = '', $dir = '');
                            if(!empty($res_user1))
                            {
                                $value['team_id'] = $res_user1[0]['user_id'];
                            }
                        }
                        if(!is_null($value['e911']))
                        {
                            if($value['e911'])
                            {
                               $value['e911'] = 'true'; 
                            }
                            else
                            {
                                $value['e911'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['cbmatch']))
                        {
                            if($value['cbmatch'])
                            {
                               $value['cbmatch'] = 'true'; 
                            }
                            else
                            {
                                $value['cbmatch'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['operator_id_verify']))
                        {
                            if($value['operator_id_verify'])
                            {
                               $value['operator_id_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['operator_id_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e911address_verify']))
                        {
                            if($value['e911address_verify'])
                            {
                               $value['e911address_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['e911address_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e911phIIcall']))
                        {
                            if($value['e911phIIcall'])
                            {
                               $value['e911phIIcall'] = 'true'; 
                            }
                            else
                            {
                                $value['e911phIIcall'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e9111strabid']))
                        {
                            if($value['e9111strabid'])
                            {
                               $value['e9111strabid'] = 'true'; 
                            }
                            else
                            {
                                $value['e9111strabid'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e9112ndrabid']))
                        {
                            if($value['e9112ndrabid'])
                            {
                               $value['e9112ndrabid'] = 'true'; 
                            }
                            else
                            {
                                $value['e9112ndrabid'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e911contact_verify']))
                        {
                            if($value['e911contact_verify'])
                            {
                               $value['e911contact_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['e911contact_verify'] = 'false'; 
                            }
                        }
                       
                        if(!is_null($value['e911name_verify']))
                        { 
                            if($value['e911name_verify'])
                            {
                               $value['e911name_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['e911name_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['long_verify']))
                        {
                            if($value['long_verify'])
                            {
                               $value['long_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['long_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['lat_verify']))
                        {
                            if($value['lat_verify'])
                            {
                               $value['lat_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['lat_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['esrkrange_verify']))
                        {
                            if($value['esrkrange_verify'])
                            {
                               $value['esrkrange_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['esrkrange_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['defaultesrk_verify']))
                        {
                            if($value['defaultesrk_verify'])
                            {
                               $value['defaultesrk_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['defaultesrk_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['betweenesrk_verify']))
                        {
                            if($value['betweenesrk_verify'])
                            {
                               $value['betweenesrk_verify'] = 'true'; 
                            }
                            else
                            {
                                $value['betweenesrk_verify'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['notinrange']))
                        {
                            if($value['notinrange'])
                            {
                               $value['notinrange'] = 'true'; 
                            }
                            else
                            {
                                $value['notinrange'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['uncertainty_verfiy']))
                        {
                            if($value['uncertainty_verfiy'])
                            {
                               $value['uncertainty_verfiy'] = 'true'; 
                            }
                            else
                            {
                                $value['uncertainty_verfiy'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['e911match']))
                        {
                            if($value['e911match'])
                            {
                               $value['e911match'] = 'true'; 
                            }
                            else
                            {
                                $value['e911match'] = 'false'; 
                            }
                        }
                        
                        if(!is_null($value['addressmatch']))
                        {
                            if($value['addressmatch'])
                            {
                               $value['addressmatch'] = 'true'; 
                            }
                            else
                            {
                                $value['addressmatch'] = 'false'; 
                            }
                        }
                        $id = $value['market_data_id'];
                        unset($value['market_data_id']);
                        if($this->common->update_data($value, 'market_data', 'market_data_id', (int) ($id)))
                        {  
                           $ids[] = $id; 
                        }
                    }
                    
                }
                
                //print_r($ids);exit();
                $token = base64_decode($token);
                $res = $this->common->select_database_where_in('market_data', 'market_data_id', $ids);
                echo $var.'('.json_encode($res).')';
            }
        }
        else
        {
            $res = array("massage"=>"No data found.");
            echo json_encode($res);
        }
    }
    public function get_import()
    {
        $token=$this->uri->segment(3);
        $var = $_GET['callback'];
        if($token != '')
        {
            $token = base64_decode($token);
            $res = $this->common->select_database_id('market_data', 'market_id', (int) $token, '*');
            echo $var.'('.json_encode($res).')';
        }
        else
        {
            $res = array("massage"=>"No data found.");
            echo json_encode($res);
        }
    }
    public function view_import()
    {
        $token=$this->uri->segment(3);
        if($token != '')
        {
            
            $token = $this->data['edit_id'] = base64_decode($token);
           
            $session_array = $this->session->userdata('admin_data');
            if($session_array['role'] != 1)
            {
                $res_assign = $this->common->select_data_by_condition('market_assign_to', array('market_id'=>(int) $token,'assign_to_id'=>(int)$session_array['ad_id']), '', 'market_id', 'DESC', '', '');
                if(empty($res_assign))
                {
                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('market', 'refresh');
                    exit();
                }
            } 
            
            $res = $this->common->select_database_id('market_data', 'market_id', (int) $token, '*');
            if(!empty($res))
            {
                $res1 = $this->common->select_database_id('market', 'market_id', (int) $token, '*');
                $this->data['data'] = $res1[0];
                $this->load->view('market/view_import', $this->data);
            }
            else
            {
                
                $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
                redirect('market', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
            redirect('market', 'refresh');
        }
    }
}