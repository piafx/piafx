<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        if (!$this->session->userdata('admin_data')) {
        //If no session, redirect to login page
            redirect('adminlogin', 'refresh');
        }

        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Setting';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(4);
        
        $this->data['permission_list'] = $this->common->permission();
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->load->model('common');
        $this->check_permission();
    }

    public function index() {
        $this->data['settings'] = $this->common->get_all_record('setting');
        $this->data['total'] = $this->common->get_count_of_table('setting');
        //Loading View File
        $this->load->view('setting/index', $this->data);
    }
    
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'update')
        {
            $method = 'edit';
        }
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }


    public function edit($settingid) {
      
        if (!$settingid) {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('setting', 'refresh');
        }

        $this->data['settings'] = $this->common->get_setting_byid($settingid);
        if (count($this->data['settings']) > 0) {
            $this->load->view('setting/edit', $this->data);
        } else {
            $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
            redirect('setting', 'refresh');
        }
        
    }

    //Updating the record
    public function update() {
        if ($this->input->post('settingid')) {
            
               $this->load->helper(array('form', 'url'));
                $this->load->library('form_validation');

            $settingid = base64_decode($this->input->post('settingid'));
            $settingname = $this->input->post('settingname');
            $this->form_validation->set_rules('settingvalue',$settingname, 'required');
             
            if ($this->form_validation->run() == TRUE) {

                $data = array();

                $data = array(
                    'settingvalue' => $this->input->post('settingvalue')
                );

//            echo "<pre>";print_r($data);die();
                if ($this->common->update_data($data, 'setting', 'settingid', (int) $settingid)) {
                    $this->session->set_flashdata('success', ' Record has been updated successfully.');
                    redirect('setting', 'refresh');
                } else {
                    $this->session->set_flashdata('message', $settingname . ' Please try again.');
                    redirect('setting', 'refresh');
                }
            } else {
               
                 $this->session->set_flashdata('messagea', "The $settingname field is required."); 
                redirect('setting/edit/'.$settingid, 'refresh');
            }
        } else {
            
             $this->session->set_flashdata('message', 'Specified id not found.');
             redirect('setting', 'refresh');
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
