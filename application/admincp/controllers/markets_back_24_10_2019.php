<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Markets extends CI_Controller {

    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

        $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Market';
        
        $session_array = $this->session->userdata('admin_data');

        $res = $this->common->select_database_id('users', '_id',(string) $session_array['ad_id'] , '*');
        // print_r($res);exit();
        $this->data['user_role'] = $res[0]['user_role'];
        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('excel');
        
        
        $this->check_permission();
        require_once $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
    }

    public function index() {
        
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
             $this->data['allcots'] = $this->common->select_database_by_muliple_where('COT_master',array(), '*', '', '');  
             $this->load->view('market/view_import', $this->data);
        }
        else
        {
             $this->data['allcots'] = $this->common->select_database_by_muliple_where('COT_master',array(), '*', '', '');
             $order = array(4,3,1,2,5,6);
             usort($this->data['allcots'], function ($a, $b) use ($order) {
                $pos_a = array_search($a['_id'], $order);
                $pos_b = array_search($b['_id'], $order);
                return $pos_a - $pos_b;
            });
             $this->load->view('market/view_import', $this->data);
        }
        
       
    }

    public function testfun()
    {
        $new_data = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>'ZF','type'=>1,'cftc_market'=>'CBT'), '*', '', '', '', '',array(),'');
        echo "<pre>";
        print_r($new_data);
        exit();
    }
    
    public function update_data(){
        

        $cot_id = $_GET['value'];
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        $updated_ids = array();
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $update_id = $model['_id'];
            array_push($updated_ids, $update_id);
            if($cot_id == 1){
                $this->common->update_data($model, 'tff_future_only', '_id', $update_id);
            }
            if($cot_id == 2){
                $this->common->update_data($model, 'tff_future_and_option', '_id', $update_id);
            }
            if($cot_id == 3){
                $this->common->update_data($model, 'legacy_future_and_option', '_id', $update_id);
            }
            if($cot_id == 4){
                $this->common->update_data($model, 'legacy_future_only', '_id', $update_id);
            }            
            if($cot_id == 5){
                $this->common->update_data($model, 'disaggregated_future_only_new', '_id', $update_id);
            }
            if($cot_id == 6){
                $this->common->update_data($model, 'disaggregated_future_and_option', '_id', $update_id);
            }
        }
    if($cot_id == 1){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('tff_future_only', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('tff_future_only');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    if($cot_id == 2){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('tff_future_and_option', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('tff_future_and_option');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    if($cot_id == 3){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('legacy_future_and_option', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('legacy_future_and_option');  
        $data1['d']['results'] = $res;
        // $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    if($cot_id == 4){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('legacy_future_only', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('legacy_future_only');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    if($cot_id == 5){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('disaggregated_future_only_new', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('disaggregated_future_only_new');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    if($cot_id == 6){

        $condition['_id'] = array( '$in' => $updated_ids);
        $res = $this->common->select_data_by_condition('disaggregated_future_and_option', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('disaggregated_future_and_option');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
        
        
    }
    
    public function get_all_data() {
        
        $limit = isset($_GET['$top'])?$_GET['$top']:'10';
        $offset = isset($_GET['$skip'])?$_GET['$skip']:'0';
        $var = isset($_GET['$callback'])?$_GET['$callback']:'';
        $type = isset($_GET['value'])?$_GET['value']:'';
        $filter = isset($_GET['$filter'])?$_GET['$filter']:'';
        $orderby = isset($_GET['$orderby'])?$_GET['$orderby']:'';
        $orderby_col = '';
        $dir = '';
        if($orderby != ''){            
            $order_array = explode(' ', $orderby);
            if(count($order_array) == 2){
                $dir = 'DESC';
                $orderby_col = $order_array[0]; 
            }
            else{
                $dir = 'ASC';
                $orderby_col = $order_array[0];
            }
        }
        if($orderby_col == 'As_of_Date_in_Form_YYYYMMDD'){
            $orderby_col = 'As_of_Date_in_Form_YYYY-MM-DD';
        }
        // echo $orderby_col;exit();
        $condition =  array();
        //$condition['showdashboard'] = "1";
        $likecondition = array();
        $substringcondition = array();
        if($filter != ''){
            
            $filter = str_replace("(", "", $filter);
            $filter = str_replace(")", "", $filter);
            $filter = str_replace("'", "", $filter);
            $filter_array = explode(' and ',$filter);
            // print_r($filter_array);exit();
            foreach($filter_array as $fil){
                
                if (strpos($fil, 'startswith') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'substringof') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'endswith') !== false) {
                    $needtoexplode = false;
                }
                else{
                    $needtoexplode = true;
                }
                
                if($needtoexplode){

                    $fil_array = explode(' ', $fil);
                    // $rangeQuery = array('_id' => array( '$gt' => 10, '$lt' => 12 ));
                    if(count($fil_array) >= 3){
                        if(count($fil_array) == 3){
                                if($fil_array[0] == 'Report_Date_as_MM_DD_YYYY' || $fil_array[0] == 'As_of_Date_in_Form_YYYYMMDD' || $fil_array[0] == 'Report_Date_as_YYYY_MM_DD_disaggregated'){
                                    $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                    $fil_array[2] = str_replace("'", "", $fil_array[2]);

                                    $fil_array[2] =Date("Y-m-d", strtotime($fil_array[2]));
                                    // echo $fil_array[2];exit();
                                }

                                if($fil_array[1] === 'ge'){
                                    $fil_array[1] = 'gte';
                                }
                                if($fil_array[1] === 'le'){
                                    $fil_array[1] = 'lte';
                                }
                                $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                        else{

                            $fil_array_value = explode(' '.$fil_array[1].' ', $fil);
                            // echo $fil;

                            $fil_array[2] = $fil_array_value[1];

                            // echo "string".$fil_array[2];
                            if($fil_array[0] == 'Report_Date_as_MM_DD_YYYY' || $fil_array[0] == 'As_of_Date_in_Form_YYYYMMDD' || $fil_array[0] == 'Report_Date_as_YYYY_MM_DD_disaggregated'){
                                $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                $fil_array[2] = str_replace("'", "", $fil_array[2]);

                                $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                            }

                            if($fil_array[1] === 'ge'){
                                $fil_array[1] = 'gte';
                            }
                            if($fil_array[1] === 'le'){
                                $fil_array[1] = 'lte';
                            }
                            $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }

                        // print_r($condition);exit();
                        // if($fil_array[1] === 'neq'){
                        //     $condition[$fil_array[0]] = " != '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'ne'){
                        //     $condition[$fil_array[0]] = " != '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'gte'){
                        //     $condition[$fil_array[0]] = " >= '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'ge'){
                        //     $condition[$fil_array[0]] = " >= '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'gt'){
                        //     $condition[$fil_array[0]] = " > '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'lte'){
                        //     $condition[$fil_array[0]] = " <= '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'le'){
                        //     $condition[$fil_array[0]] = " <= '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'lt'){
                        //     $condition[$fil_array[0]] = " < '".$fil_array[2]."'";
                        // }
                        // if($fil_array[1] === 'isnull'){
                        //     $condition[$fil_array[0]] = "NULL";
                        // }
                        // if($fil_array[1] === 'isnotnull'){
                        //     $condition[$fil_array[0]] =  " !=  'NULL'";
                        // }
                                
                    }
                }
                else{

                    if (strpos($fil, 'startswith') !== false) {

                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  '^'.$value_field;
                        // echo "field : ".$field."<br>";
                        // echo "value : ".$value_field;exit();

                    }
                    else if(strpos($fil, 'endswith') !== false) {
                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  $value_field.'^';
                    }
                    else if(strpos($fil, 'substringof') !== false) {

                        $fil_without = str_replace('substringof', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[1];
                        $value_field = $fil_array[0];
                        $substringcondition[$field] =  $value_field;
                        // print_r($substringcondition);exit();

                    }
                    
                }
                
                
            }
        }

        $session_array = $this->session->userdata('admin_data');

        $allconditions = '';
        // print_r($condition);exit();
        $join = array(
            array('from'=>'ticker_master_new',
            'localField'=>'ticker_id',
            'foreignField'=>'_id',
            'as'=>'tickers')   
        );
        $condition['tickers.showdashboard'] = '1';
        if($session_array['role'] != 1)
        {

            if($type == 1){
               

                $res = $this->common->select_data_by_condition('tff_future_only', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                $res_count1 = $this->common->get_count_of_table('tff_future_only',$condition,$likecondition,$substringcondition,$join);  
            }
            else if($type == 2){    
                $res = $this->common->select_data_by_condition('tff_future_and_option', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                $res_count1 = $this->common->get_count_of_table('tff_future_and_option',$condition,$likecondition,$substringcondition,$join);     
            }
            else if($type == 3){
                    $res = $this->common->select_data_by_condition('legacy_future_and_option', $condition, '*', $orderby_col, $dir, $limit, $offset ,$join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('legacy_future_and_option',$condition,$likecondition,$substringcondition,$join);       
                }
            else if($type == 4){
                $res = $this->common->select_data_by_condition('legacy_future_only', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                $res_count1 = $this->common->get_count_of_table('legacy_future_only',$condition,$likecondition,$substringcondition,$join);       
            }
            else if($type == 5){
                $res = $this->common->select_data_by_condition('disaggregated_future_only_new', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                $res_count1 = $this->common->get_count_of_table('disaggregated_future_only_new',$condition,$likecondition,$substringcondition,$join);       
            }
            else if($type == 6){
                $res = $this->common->select_data_by_condition('disaggregated_future_and_option', $condition, '*', $orderby_col, $dir, $limit, $offset ,$join, '',$likecondition,$substringcondition);
                $res_count1 = $this->common->get_count_of_table('disaggregated_future_and_option',$condition,$likecondition,$substringcondition,$join);       
            }
            
            
            $data['d']['results'] = $res;
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';

            // $allconditions = '';
            // foreach($condition as $colname => $colval){
            //     if($allconditions != ''){
            //         $allconditions .= "and column_name LIKE '%".$colname."%' and data_value ".$colval;
            //     }
            //     else{
            //         $allconditions .= "column_name LIKE '%".$colname."%' and data_value ".$colval;
            //     }
                
            // }

            // if($allconditions != ''){
            //     $res = $this->db->query("SELECT cdm.*,COT_column_master.* FROM `COT_data_master` as cdm join COT_column_master on cot_column_id = column_id INNER JOIN (SELECT DISTINCT(sheet_row_id) FROM `COT_data_master` join COT_column_master on cot_column_id = column_id where cot_id=$type and ($allconditions) Limit $offset,$limit) as cdm2 on cdm.sheet_row_id = cdm2.sheet_row_id");

            //     $res_count = $this->db->query("SELECT count(DISTINCT(sheet_row_id)) as counts  FROM `COT_data_master` join COT_column_master on cot_column_id = column_id where cot_id=$type and $allconditions");
            // }
            // else{
            //     $res = $this->db->query("SELECT cdm.*,COT_column_master.* FROM `COT_data_master` as cdm join COT_column_master on cot_column_id = column_id INNER JOIN (SELECT DISTINCT(sheet_row_id) FROM `COT_data_master` join COT_column_master on cot_column_id = column_id where cot_id=$type Limit $offset,$limit) as cdm2 on cdm.sheet_row_id = cdm2.sheet_row_id");
            //     $res_count = $this->db->query("SELECT count(DISTINCT(sheet_row_id)) as counts  FROM `COT_data_master` where cot_id=$type");
            // }
                
            // $new_array = array();
            // $res1 = $res->result_array();

            // $res_count1 = $res_count->result_array();
            // foreach ($res1 as $value)
            // {
            //     $new_array[$value['sheet_row_id']][$value['column_name']] = $value['data_value'];
            // }
            // $new_array = array_values($new_array);
            
            // $data['d']['results'] = $new_array;
            // $data['d']['__count'] = $res_count1[0]['counts'];
            
            // echo $var.'('.json_encode($data).')';
        }
        else
        {
            
            
            // if($allconditions != ''){

            //     if($type == 1){
            //         $res = $this->common->select_data_by_condition('tff_future_only', $condition, '*', '', '', $limit, $offset , array(), '');
            //         $res_count1 = $this->common->get_count_of_table('tff_future_only',$condition);      
            //     }
            //     else if($type == 2){
            //         $res = $this->common->select_database_by_muliple_where('tff_future_and_option',array(), '*', '', '');
            //         $res_count1 = $this->common->get_count_of_table('tff_future_and_option');          
            //     }
            //     else if($type == 3){
            //         $res = $this->common->select_database_by_muliple_where('legacy_future_only',array(), '*', '', '');
            //         $res_count1 = $this->common->get_count_of_table('legacy_future_only');          
            //     }

            // }
            // else{
                // $join = array(
                //     array('from'=>'ticker_master_new',
                //     'localField'=>'ticker_id',
                //     'foreignField'=>'_id',
                //     'as'=>'tickers')   
                // );
                // $condition['tickers.showdashboard'] = '1';
                if($type == 1){
                    
                    
                    $res = $this->common->select_data_by_condition('tff_future_only', $condition, '*', $orderby_col, $dir,  $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('tff_future_only',$condition,$likecondition,$substringcondition,$join);
                    
                
                    // $res_count1 = 28407;
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['Report_Date_as_MM_DD_YYYY'] = date("Y-m-d", (((array)$value['Report_Date_as_MM_DD_YYYY'])['milliseconds']/1000));
                        $increment_var++;
                    }    
                }
                else if($type == 2){    
                    $res = $this->common->select_data_by_condition('tff_future_and_option', $condition, '*', $orderby_col, $dir,  $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('tff_future_and_option',$condition,$likecondition,$substringcondition,$join);     
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['Report_Date_as_MM_DD_YYYY'] = date("Y-m-d", (((array)$value['Report_Date_as_MM_DD_YYYY'])['milliseconds']/1000));
                        $increment_var++;
                    }
                }
                else if($type == 3){
                    $res = $this->common->select_data_by_condition('legacy_future_and_option', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('legacy_future_and_option',$condition,$likecondition,$substringcondition,$join);
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['As_of_Date_in_Form_YYYYMMDD'] = date("Y-m-d", (((array)$value['As_of_Date_in_Form_YYYY-MM-DD'])['milliseconds']/1000));
                        $increment_var++;
                    }       
                }
                else if($type == 4){
                    $res = $this->common->select_data_by_condition('legacy_future_only', $condition, '*', $orderby_col, $dir, $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('legacy_future_only',$condition,$likecondition,$substringcondition,$join);    
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['As_of_Date_in_Form_YYYYMMDD'] = date("Y-m-d", (((array)$value['As_of_Date_in_Form_YYYY-MM-DD'])['milliseconds']/1000));
                        $increment_var++;
                    }   
                }
                else if($type == 5){
                    $res = $this->common->select_data_by_condition('disaggregated_future_only_new', $condition, '*', $orderby_col, $dir,  $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('disaggregated_future_only_new',$condition,$likecondition,$substringcondition,$join);
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['Report_Date_as_YYYY_MM_DD_disaggregated'] = date("Y-m-d", (((array)$value['Report_Date_as_YYYY_MM_DD_disaggregated'])['milliseconds']/1000));
                        $increment_var++;
                    }       
                }
                else if($type == 6){
                    $res = $this->common->select_data_by_condition('disaggregated_future_and_option', $condition, '*', $orderby_col, $dir,  $limit, $offset , $join, '',$likecondition,$substringcondition);
                    $res_count1 = $this->common->get_count_of_table('disaggregated_future_and_option',$condition,$likecondition,$substringcondition,$join);       
                    $increment_var = 0;
                    foreach($res as $value) {
                        $res[$increment_var]['Report_Date_as_YYYY_MM_DD_disaggregated'] = date("Y-m-d", (((array)$value['Report_Date_as_YYYY_MM_DD_disaggregated'])['milliseconds']/1000));
                        $increment_var++;
                    } 
                }
                
            // }
            
            
            $data['d']['results'] = $res;
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
       
    }
    
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'get_all_data' || $method == 'testfun'){
            $method = 'index';
        }
        if($method == 'insertdata')
        {
            $method = 'add';
        }
        if($method == 'editform' || $method == 'update' || $method == 'update_data')
        {
            $method = 'edit';
        }
        if($method == 'import' || $method == 'import_data' || $method == 'view_import' || $method == 'get_import' || $method == 'update_import' || $method == 'get_users' || $method == 'get_coordinator')
        {
            $method = 'import';
        }
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }
    
}
