<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Changepassword extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('aspl_hrmadmin_data')) {
            redirect('adminlogin', 'refresh');
        }

        $this->load->model('common');
        $res = $this->common->select_database_id('masteradmin', 'adminid', (int) 1, '*');
        $this->data['logo'] = $res[0]['image'];
        
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Change Password';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

        $this->data['name'] = $this->session->userdata['aspl_hrmadmin_data']['name'];
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->data['sidebar'] = $this->load->view('sidebar', $this->data, true);

        $this->load->model('common');
    }

    public function index() {
        $admin_id = $this->session->userdata['aspl_hrmadmin_data']['ad_id'];
        $data = $this->common->select_database_by_muliple_where('masteradmin', array('adminid' => $admin_id), $data = '*', $order_by = '', $dir = '');

        $this->data['password'] = $data[0]['adminpassword'];
        $this->load->view('changepassword', $this->data);
    }


    public function change() {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldpassword', 'Old password', 'required');
        
        $this->form_validation->set_rules('passconf', 'Confirm password', 'required');
        $this->form_validation->set_rules('password', 'New Password', 'required|matches[passconf]');

        if ($this->form_validation->run() == TRUE) {

//        echo '<pre>';print_r($this->input->post(NULL, TRUE));die();

            $oldpassword = $this->input->post('oldpassword');
            $newpassword = $this->input->post('password');
            $confirmpass = $this->input->post('passconf');
            $admin_id = $this->session->userdata['aspl_hrmadmin_data']['ad_id'];

            $data = $this->common->select_database_by_muliple_where('masteradmin', array('adminid' => $admin_id, 'adminpassword' => base64_encode($oldpassword)), $data = '*', $order_by = '', $dir = '');
            if ($data) {
                $data = array(
                    'adminpassword' => base64_encode(strip_tags($newpassword)),
                );
                if ($oldpassword == $newpassword) {
                    $this->session->set_flashdata('message', 'You are trying to change same password');
                    $this->add_log(0,'Change Password',0);
                    redirect('changepassword', 'refresh');
                } elseif ($this->common->update_data($data, 'masteradmin', 'adminid', (int) $admin_id)) {
                    $this->session->set_flashdata('success', 'Password has been updated successfully.');
                    $this->add_log(0,'Change Password',1);
                    redirect('changepassword', 'refresh');
                } else {
                    $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                    $this->add_log(0,'Change Password',0);
                    redirect('changepassword', 'refresh');
                }
            } else {
                $this->session->set_flashdata('message', 'Please enter correct old password.');
                $this->add_log(0,'Change Password',0);
                redirect('changepassword', 'refresh');
            }
        } else {
            $this->load->view('changepassword', $this->data);
        }
    }
    
    public function view($semid = NULL) {

        if ($semid == NULL) {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('sem', 'refresh');
        }

        $this->data['sems'] = $this->common->select_database_id('sem', 'semid', (int) $semid, '*');
//        echo '<pre>';print_r($this->data['sems']);die();
//Loading View File
        $this->load->view('sem/view', $this->data);
    }

//    public function delete($semid = NULL) {
//
//        if ($semid == NULL) {
//            $this->session->set_flashdata('message', 'Specified id not found.');
//            redirect('sem', 'refresh');
//        }
//        if ($this->common->delete_data('sem', 'semid', $semid)) {
//            $this->session->set_flashdata('success', 'Sem setting deleted successfully.');
//            redirect('sem', 'refresh');
//        } else {
//            $this->session->set_flashdata('message', 'Sem setting not deleted successfully.');
//            redirect('sem', 'refresh');
//        }
//        $this->session->set_flashdata('message', 'Sem setting not deleted successfully.');
//        redirect('sem', 'refresh');
//    }

    public function edit($semid) {
        if (!$semid) {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('sem', 'refresh');
        }

        $this->data['sems'] = $this->common->select_database_id('sem', 'semid', (int) $semid, '*');
//        echo "<pre>";
//        print_r($this->data['settings']);
//        die(); 
//Loading View File
        $this->load->view('sem/edit', $this->data);
    }

//Updating the record
    public function update() {
        if ($this->input->post('semid')) {

            $semid = base64_decode($this->input->post('semid'));

            $data = array(
                'semvalue' => $this->input->post('semvalue')
            );

            if ($this->common->update_data($data, 'sem', 'semid', (int) $semid)) {
                $this->session->set_flashdata('success', 'Sem setting updated successfully.');
                redirect('sem', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Sem setting not updated successfully.');
                redirect('sem', 'refresh');
            }
        } else {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('sem', 'refresh');
        }
    }
     public function add_log($to_id=0,$method_name,$result)
    {
        $ci =& get_instance();
        $controller_name=$ci->router->fetch_class();
        $by_id=$this->session->userdata['aspl_hrmadmin_data']['ad_id'];
        $role=$this->session->userdata['aspl_hrmadmin_data']['role'];
        $data1 = array(
                'by_id'=>$by_id,
                'role_id'=>$role,
                'to_id'=>$to_id,
                'controller_name'=> $controller_name,
                'method_name' => $method_name,
                'created_date' => date('Y-m-d H:i:s'),
                'result'=> $result,  
                   );
        $this->common->insert_data($data1, 'admin_log');
        
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
