<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gettest extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
       parent::__construct();
       ini_set('memory_limit', '-1');
       ini_set('post_max_size', '64M');
       ini_set('upload_max_filesize', '64M');
       ini_set('max_execution_time', 600);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');
       require_once $_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php";
    }

    public function cot_data(){

        // echo "asd";exit();
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $direcory_arrry = $this->getDirContents("/var/www/html/cot_data");
        // print_r($direcory_arrry);exit();
        // foreach ($direcory_arrry as $market => $marketdir) {
            foreach ($direcory_arrry as $yearkey => $year) {
                foreach ($year as $files) {
                    $file = "/var/www/html/cot_data/".$yearkey."/".$files;
                    $this->Legacy_future_only_historical_data_new($file);
                    //$this->Legacy_future_and_option_historical_data($file);
                    $this->drop_file_from_dir($file);
                    // exit();
                }
            }
        // }
        exit();
    }
    public function drop_file_from_dir($file){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        unlink($file);
        echo "string";
    }
    public function getDirContents($dir_path, &$results = array()){

        $rdi = new \RecursiveDirectoryIterator($dir_path);

        $rii = new \RecursiveIteratorIterator($rdi);

        $tree = [];

        foreach ($rii as $splFileInfo) {
            $file_name = $splFileInfo->getFilename();

            // Skip hidden files and directories.
            if ($file_name[0] === '.') {
                continue;
            }

            $path = $splFileInfo->isDir() ? array($file_name => array()) : array($file_name);

            for ($depth = $rii->getDepth() - 1; $depth >= 0; $depth--) {
                $path = array($rii->getSubIterator($depth)->current()->getFilename() => $path);
            }

            $tree = array_merge_recursive($tree, $path);
        }

        return $tree;
    }
    public function Legacy_future_only_historical_data_new($file){

            $this->load->library('PHPExcel');
            $object = PHPExcel_IOFactory::load($file);
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('legacy_future_only');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(125, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col+87]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    // echo $ticker_id.' '.$date_col.'<br>';
                    $record_exitst = $this->common->select_data_by_condition('legacy_future_only', array('ticker_id'=>$ticker_id,'As_of_Date_in_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'legacy_future_only','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'legacy_future_only');
                    }
                }
            }
    }
    public function Legacy_future_and_option_historical_data($file){

            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            // $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/deafut.txt");
            // $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            // fwrite($myfile, $file_data);
            // fclose($myfile);

            $object = PHPExcel_IOFactory::load($file);
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('legacy_future_and_option');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(125, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col+87]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    // echo $ticker_id.' '.$date_col.'<br>';
                    $record_exitst = $this->common->select_data_by_condition('legacy_future_and_option', array('ticker_id'=>$ticker_id,'As_of_Date_in_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'legacy_future_and_option','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'legacy_future_and_option');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();
    }
    public function Legacy_future_only_historical_data(){

            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            // $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/deafut.txt");
            // $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            // fwrite($myfile, $file_data);
            // fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/new_leg_f.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('legacy_future_only');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(125, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col+87]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    // echo $ticker_id.' '.$date_col.'<br>';
                    $record_exitst = $this->common->select_data_by_condition('legacy_future_only', array('ticker_id'=>$ticker_id,'As_of_Date_in_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'legacy_future_only','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'legacy_future_only');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();
    }
    public function import_data_desaggregated() {

            
            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');
            $object = PHPExcel_IOFactory::load('/var/www/html/c_year.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('disaggregated_future_only_new');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(185, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 5;
                        }
                        

                        $col_name = $COT_column_master[$col+216]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('disaggregated_future_only_new', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD_disaggregated'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'disaggregated_future_only_new','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'disaggregated_future_only_new');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();


    }
    public function import_data_desaggregated_future_option() {

            
            error_reporting(E_ALL);
            // ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');
            $object = PHPExcel_IOFactory::load('/var/www/html/c_year1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('disaggregated_future_and_option');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(185, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 5;
                        }
                        

                        $col_name = $COT_column_master[$col+216]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('disaggregated_future_and_option', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD_disaggregated'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'disaggregated_future_and_option','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'disaggregated_future_and_option');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();


    }
    public function desaggregated_future_only() {

            
            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/f_disagg.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('disaggregated_future_only_new');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(185, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 5;
                        }
                        

                        $col_name = $COT_column_master[$col+216]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('disaggregated_future_only_new', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD_disaggregated'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                         $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'disaggregated_future_only_new','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'disaggregated_future_only_new');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();


    }
    public function desaggregated_future_and_option() {

            
            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/c_disagg.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('disaggregated_future_and_option');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(185, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 5;
                        }
                        

                        $col_name = $COT_column_master[$col+216]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('disaggregated_future_and_option', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD_disaggregated'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                         $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'disaggregated_future_and_option','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'disaggregated_future_and_option');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();


    }
    public function tff_future_only_current_data(){

            // error_reporting(E_ALL);
            // ini_set('memory_limit', '248M');
            // ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/FinFutWk.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('tff_future_only');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(81, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('tff_future_only', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'tff_future_only','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'tff_future_only');
                    }
                }
            }
            // echo "<pre>";print_r($data);
            exit();
    }
    public function tff_future_and_option_current_data(){

            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/FinComWk.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('tff_future_and_option');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(81, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }
                    $record_exitst = $this->common->select_data_by_condition('tff_future_and_option', array('ticker_id'=>$ticker_id,'As_of_Date_In_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'tff_future_and_option','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'tff_future_and_option');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();
    }
    public function Legacy_future_only_current_data(){

            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/deafut.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('legacy_future_only');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(125, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col+87]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    // echo $ticker_id.' '.$date_col.'<br>';
                    $record_exitst = $this->common->select_data_by_condition('legacy_future_only', array('ticker_id'=>$ticker_id,'As_of_Date_in_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'legacy_future_only','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'legacy_future_only');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();
    }
    public function Legacy_future_and_option_current_data(){

            error_reporting(E_ALL);
            ini_set('memory_limit', '248M');
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/deafut.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);

            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            $last_record_id = $this->common->last_record_id('legacy_future_and_option');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=1; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {

                        
                        if($col == 0){

                            $ticker_col = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                            $main_col = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                            $market_col = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                            $date_col = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                            $contract_col = $worksheet->getCellByColumnAndRow(125, $row)->getValue();

                            


                            $record_exitst_ticker = $this->common->select_data_by_condition('ticker_master_new', array('cftc_code'=>$main_col), '*', '', '', '', '',array(),'');

                            if(count($record_exitst_ticker) > 0 ){

                                // echo "ticker update";print_r($dataticker);

                                $ticker_id = $record_exitst_ticker[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master_new');
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_col,'cftc_code'=>$main_col, 'cftc_market'=> $market_col, 'contract_unit' => $contract_col , 'eod_data' => "-",'barchart'=>"-",'alpha_vantag'=>"-",'group'=>"-","type"=>1);

                                    // echo "ticker insert";print_r($dataticker);
                                    $this->common->insert_data($dataticker, 'ticker_master_new');
                                }
                                $ticker_id = $newticker_id;
                            }
                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $data[$row]['market_id'] = "-";
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                        }
                        

                        $col_name = $COT_column_master[$col+87]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    // echo $ticker_id.' '.$date_col.'<br>';
                    $record_exitst = $this->common->select_data_by_condition('legacy_future_and_option', array('ticker_id'=>$ticker_id,'As_of_Date_in_Form_YYMMDD'=>$date_col), '*', '', '', '', '',array(),'');

                    if(count($record_exitst) > 0)
                    {
                        $data[$row]['_id'] = $record_exitst[0]['_id'];
                        // echo "main update";print_r($data[$row]);
                        $this->common->update_data($data[$row],'legacy_future_and_option','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        // echo "main insert";print_r($data[$row]);
                        $this->common->insert_data($data[$row], 'legacy_future_and_option');
                    }
                }
            }
            echo "Done";
            // echo "<pre>";print_r($data);
            exit();
    }
    public function tff_future_only_current_data_backup() {

            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            // $ticker_id_array = $this->common->select_database_by_like_where('ticker_master', 'ticker_name', "DOW JONES INDUSTRIAL AVG - x $5" );
            // $datamarket = array('_id'=>'2','Report_Date_as_MM_DD_YYYY'=> '12/12/2012');
            // $datamarket_array = $this->common->insert_data_getid($datamarket, 'test_collection');
            // print_r($ticker_id_array);exit();
            $this->load->library('PHPExcel');
            $file_data = file_get_contents("https://www.cftc.gov/dea/newcot/FinFutWk.txt");
            $myfile = fopen("/var/www/html/dd1.txt", "w") or die("Unable to open file!");
            fwrite($myfile, $file_data);
            fclose($myfile);
            $object = PHPExcel_IOFactory::load('/var/www/html/dd1.txt');
            $last_record_id = $this->common->last_record_id('tff_future_only');
            // print_r($last_record_id);exit();
            $COT_column_master = $this->common->select_database_id_mysql('COT_column_master', 'column_id !=', 0, '*');
            // print_r($COT_column_master);exit();
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                // exit();
                for($row=1; $row<= $highestRow; $row++)
                {  

                    for($col=0; $col<87; $col++)
                    {  

                    //     if($col == 0){

                    //         $last_record_id = $last_record_id + 1;
                    //         $data[$row]['_id']  = $last_record_id;
                    //         $main_col = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    //         $main_col_array = explode('-' , $main_col);
                    //         $last_element = $main_col_array[count($main_col_array)-1];
                    //         $market_id_array = $this->common->select_database_by_like_where('markets', 'market_name', $last_element );
                    //         // uset($main_col_array[count($main_col_array)-1]);
                    //         // array_pop($main_col_array);
                    //         // print_r(array_values($main_col_array));exit();
                    //         $ticker_name = '';
                    //         for($i = 0; $i < count($main_col_array) - 1 ; $i++){
                    //             if($ticker_name != '')
                    //             {
                    //                 $ticker_name  =  $ticker_name.' - '.$main_col_array[$i];
                    //             }
                    //             else{
                    //                 $ticker_name  =  $main_col_array[$i];
                    //             }
                    //         }
                    //         // echo $ticker_name;
                    //         // $ticker_name = implode(' - ', array_values($main_col_array));
                    //         $ticker_id_array = $this->common->select_database_by_like_where('ticker_master', 'ticker_name', $ticker_name );

                    //         //ticker id
                    //         if(count($ticker_id_array) > 0){
                    //             $ticker_id = $ticker_id_array[0]['_id'];
                    //         }
                    //         else{

                    //             $last_ticker_id = $this->common->last_record_id('ticker_master');
                    //             // exit();
                    //             $newticker_id = 0;
                    //             if($last_ticker_id){
                    //                 $newticker_id = $last_ticker_id + 1;
                    //                 $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_name,'ticker_code'=>$ticker_name, 'ticker_description'=> $ticker_name, 'created_date' => date('Y-m-d h:i:s') , 'modified_date' => date('Y-m-d h:i:s'));
                    //                 $this->common->insert_data($dataticker, 'ticker_master');
                    //             }
                    //             $ticker_id = $newticker_id;
                    //         }
                    //         //market id
                    //         if(count($market_id_array) > 0){
                    //             $market_id = $market_id_array[0]['_id'];
                    //         }
                    //         else{

                    //             $last_market_id = $this->common->last_record_id('markets');
                    //             $newmarket_id = 0;
                    //             if($last_market_id){
                    //                 $newmarket_id = $last_market_id + 1;
                    //                 $datamarket = array('_id'=>$newmarket_id,'market_name'=>$last_element);
                    //                 $this->common->insert_data($datamarket, 'markets');
                    //             }
                    //             $market_id = $newmarket_id;
                    //         }
                    //         $data[$row]['market_id'] = $market_id;
                    //         $data[$row]['ticker_id'] = $ticker_id;
                    //         $data[$row]['cot_id'] = 1;
                    //         // print_r($data);exit();
                    //         // echo "<pre>";print_r($market_id_array);
                    //     }
                    //     $col_name = $COT_column_master[$col]['column_name'];
                    //     $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    // }
                    if($col == 3){

                            $last_record_id = $last_record_id + 1;
                            $data[$row]['_id']  = $last_record_id;
                            $main_col = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $main_col_array = explode('-' , $main_col);
                            $last_element = $main_col_array[count($main_col_array)-1];
                            $market_id_array = $this->common->select_database_by_like_where('markets', 'market_name', $last_element );
                            // uset($main_col_array[count($main_col_array)-1]);
                            // array_pop($main_col_array);
                            // print_r(array_values($main_col_array));exit();
                            $ticker_name = '';
                            for($i = 0; $i < count($main_col_array) - 1 ; $i++){
                                if($ticker_name != '')
                                {
                                    $ticker_name  =  $ticker_name.' - '.$main_col_array[$i];
                                }
                                else{
                                    $ticker_name  =  $main_col_array[$i];
                                }
                            }
                            // echo $ticker_name;
                            // $ticker_name = implode(' - ', array_values($main_col_array));
                            $ticker_id_array = $this->common->select_database_by_like_where('ticker_master', 'ticker_name', $ticker_name );

                            //ticker id
                            if(count($ticker_id_array) > 0){
                                $ticker_id = $ticker_id_array[0]['_id'];
                            }
                            else{

                                $last_ticker_id = $this->common->last_record_id('ticker_master');
                                // exit();
                                $newticker_id = 0;
                                if($last_ticker_id){
                                    $newticker_id = $last_ticker_id + 1;
                                    $dataticker = array('_id'=>$newticker_id,'ticker_name'=>$ticker_name,'ticker_code'=>$ticker_name, 'ticker_description'=> $ticker_name, 'created_date' => date('Y-m-d h:i:s') , 'modified_date' => date('Y-m-d h:i:s'));
                                    $this->common->insert_data($dataticker, 'ticker_master');
                                }
                                $ticker_id = $newticker_id;
                            }
                            //market id
                            if(count($market_id_array) > 0){
                                $market_id = $market_id_array[0]['_id'];
                            }
                            else{

                                $last_market_id = $this->common->last_record_id('markets');
                                $newmarket_id = 0;
                                if($last_market_id){
                                    $newmarket_id = $last_market_id + 1;
                                    $datamarket = array('_id'=>$newmarket_id,'market_name'=>$last_element);
                                    $this->common->insert_data($datamarket, 'markets');
                                }
                                $market_id = $newmarket_id;
                            }
                            $data[$row]['market_id'] = $market_id;
                            $data[$row]['ticker_id'] = $ticker_id;
                            $data[$row]['cot_id'] = 1;
                            // print_r($data);exit();
                            // echo "<pre>";print_r($market_id_array);
                        }
                        $col_name = $COT_column_master[$col]['column_name'];
                        $data[$row][$col_name] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                    }

                    $record_exitst = $this->common->select_data_by_condition('tff_future_only', array('ticker_id'=>$data[$row]['ticker_id'],'As_of_Date_In_Form_YYMMDD'=>$data[$row]['As_of_Date_In_Form_YYMMDD']), '*', '', '', '', '',array(),'');

                    // echo "<pre>";print_r($record_exitst);
                    if(count($record_exitst) > 0)
                    {
                        $this->common->update_data($data[$row],'tff_future_only','_id', (string)$record_exitst[0]['_id']);
                    }
                    else{
                        echo $data[$row]['_id']." ".$ticker_name;echo "vosja;";exit();
                        $this->common->insert_data($data[$row], 'tff_future_only');
                    }
                    
                }
               
                   
            }
            echo "string";
            die();

    }
    
    public function grab_eod_data(){

        echo "string";
        exit();
    }

   


}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */
            
