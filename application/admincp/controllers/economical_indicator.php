<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Economical_indicator extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
       ini_set('memory_limit', '640M');
       ini_set('post_max_size', '640M');
       ini_set('upload_max_filesize', '640M');
       ini_set('max_execution_time', 60000);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Price Data';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
    public function economical_ind_fred()
    {
        $economical_indicator_details = $this->common->select_data_by_condition('economical_indicator_details', array('data_source1'=>'FRED'), '*', '', '', '', '',array(),'');
        if(count($economical_indicator_details) > 0){
            // echo "<pre>";
            // print_r($economical_indicator_details);exit();
            foreach ($economical_indicator_details as $key => $value) {
                // print_r($value);
                $code = $value['data_source_code1'];
                $country = $value['country_id'];
                $encl = $value['enic_id'];
                $url = "https://api.stlouisfed.org/fred/series/observations?series_id=".$code."&api_key=80e64df5d476abc3809725f9b4c643c5&file_type=json";
                // echo "<br>";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $result = json_decode($result);
                if (property_exists($result,'observations')){
                    // echo "<pre>";
                    // print_r($result);
                    foreach ($result->observations as $allobserve) {
                        // print_r($allobserve->date);
                        $effectiveDate = date('Y-m-d', strtotime($allobserve->date));
                        if($effectiveDate >= '1999-01-01'){
                            // echo $effectiveDate; 
                            // echo " | ";
                            // print_r($allobserve->value);
                            // echo "<br>";
                            $isdatafound = $this->common->select_data_by_condition('economical_indicators_value_master', array("enic_id"=>$encl,'date'=>$effectiveDate,'country_id'=>$country), '*', '', '', '', '',array(),'');
                            if(count($isdatafound) == 0){
                             
                                $val_main = number_format((float)$allobserve->value, 2, '.', '');
                                $enic_array = array("enic_id"=>$encl,"country_id"=>$country,"value"=>$val_main,"date"=>$effectiveDate);
                                // echo "<pre>";print_r($enic_array);
                                $this->common->insert_data($enic_array, 'economical_indicators_value_master');

                            }
                        }
                    }
                }
            }
            exit();
        }
    }
    public function economical_ind_bsi($value='')
    {
        $this->downloadnewfile();
        $this->load->library('PHPExcel');

        $object = PHPExcel_IOFactory::load("/var/www/html/enic/test.txt");
        foreach($object->getWorksheetIterator() as $worksheet)
        {
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
            for($row=2; $row<= $highestRow; $row++)
            {  
                $insert_data = array();

                $duration_type = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                if($duration_type == 'M:Monthly')  {
                    
                }
                else{
                    continue;
                }


                $country = $worksheet->getCellByColumnAndRow(1, $row)->getValue();

                if($country == 'CA:Canada' || $country == 'GB:United Kingdom' || $country == 'IN:India' || $country == 'CH:Switzerland' || $country == 'CN:China' || $country == 'MX:Mexico' || $country == 'BR:Brazil' || $country == 'RU:Russia' || $country == 'NZ:New Zealand' || $country == 'AU:Australia' || $country == 'JP:Japan' || $country == 'US:United States' || $country == 'XM:Euro area')
                {

                }
                else{
                    continue;
                }
                $carray = explode(":", $country);
                echo $carray[0];
                echo "<br>";
                echo $carray[1];
                echo "<br>";
                //is_numeric (var_name) indicator id , country id, date, value, tag
                $iscounryfound = $this->common->select_data_by_condition('country_master', array('name'=>$carray[1]), '*', '', '', '', '',array(),'');
                if(count($iscounryfound) > 0){
                    $new_record_id = $iscounryfound[0]['_id'];
                }
                else{

                    $last_record_id = $this->common->last_record_id('country_master');
                    $new_record_id = $last_record_id + 1;
                    $insert_data = array("_id"=>$new_record_id,"name"=>$carray[1],"countrycode"=>$carray[0]);
                    $this->common->insert_data($insert_data, 'country_master');
                }
                // if($row == 3){
                //     echo $new_record_id;
                //     exit();
                // }
                
                for($col=0; $col<$highestColumnIndex; $col++)
                {
                    
                    if($col >= 639){

                        $month = $col-3;
                        $effectiveDate = date('Y-m-d', strtotime("+".$month." months", strtotime('1946-01-01')));
                        $val_main = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if($val_main != '' && is_numeric ($val_main)){
                            // echo "1";
                            // echo " | ";
                            // echo $new_record_id;
                            // echo " | ";
                            // echo $val_main;
                            // echo " | ";
                            // echo $effectiveDate;
                            // echo "<br>";

                            $isdatafound = $this->common->select_data_by_condition('economical_indicators_value_master', array("enic_id"=>1,'date'=>$effectiveDate,'country_id'=>$new_record_id), '*', '', '', '', '',array(),'');
                            if(count($isdatafound) == 0){
                                $enic_array = array("enic_id"=>1,"country_id"=>$new_record_id,"value"=>$val_main,"date"=>$effectiveDate);
                                $this->common->insert_data($enic_array, 'economical_indicators_value_master');
                            }
                        }

                    }
                    
                }
                
            }
        }
    }
    public function downloadnewfile()
    {
        $path = '/var/www/html/enic/test.txt';
        $url = 'https://stats.bis.org/statx/srs/table/l1?f=csv';
        $newfname = $path;
        $file = fopen ($url, 'rb');
        if ($file) {
            $newf = fopen ($newfname, 'wb');
            if ($newf) {
                while(!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($newf) {
            fclose($newf);
        }
    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        
        foreach ( (array) $xmlObject as $index => $node )
        {
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        }
        return $out;
    }
   


}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */