<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Economicalindicator extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
         parent::__construct();
        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }
       ini_set('memory_limit', '640M');
       ini_set('post_max_size', '640M');
       ini_set('upload_max_filesize', '640M');
       ini_set('max_execution_time', 60000);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Economical Indicator';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->check_permission();
         error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
    
    public function index()
    {
        // print_r($this->common->distinct_column_values());exit();
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $join = array();
        $session_array = $this->session->userdata('admin_data');
        $id=$this->uri->segment(3);
        $country=$this->uri->segment(4);
        if($id != ''){
            $this->data['selected_option'] = base64_decode($id);
            $this->data['selected_country'] = base64_decode($country);
        }
        else{
            $this->data['selected_option'] = ""; 
            $this->data['selected_country'] = "";
        }
        if($session_array['role'] != 1)
        {
            // $condition = array('eod_data'=>array('$ne'=>'-'),'barchart'=>array('$ne'=>'-'),'alpha_vantag'=>array('$ne'=>'-'));
            $this->data['enic'] = $this->common->select_data_by_condition('economical_indicators_master', array(), '', '_id', 'DESC', '', '', array());
            // print_r($this->data['data']);exit();
            $this->load->view('economicalindicator/index_1', $this->data);
        }
        else
        {
            $this->data['enic'] = $this->common->select_data_by_condition('economical_indicators_master', array(), '', '_id', 'DESC', '', '', array());
            // print_r($this->data['data']);exit();
            $this->load->view('economicalindicator/index_1', $this->data);
        }   
    }
    public function ecintoticker($value='')
    {
        
        $join = array();
        $session_array = $this->session->userdata('admin_data');
        $id=$this->uri->segment(3);
        $country=$this->uri->segment(4);
        if($id != ''){
            $this->data['selected_option'] = base64_decode($id);
            $this->data['selected_country'] = base64_decode($country);
        }
        else{
            $this->data['selected_option'] = ""; 
            $this->data['selected_country'] = "";
        }
        if($session_array['role'] != 1)
        {
            // $condition = array('eod_data'=>array('$ne'=>'-'),'barchart'=>array('$ne'=>'-'),'alpha_vantag'=>array('$ne'=>'-'));
            $this->data['tickers'] = $this->common->select_data_by_condition('ticker_master_new', array('showdashboard'=>"1"), '', '_id', 'DESC', '', '', array());


            $this->data['enic'] = $this->common->select_data_by_condition('economical_indicators_master', array(), '', '_id', 'DESC', '', '', array());
            
            //$this->data['tabledata'] = $this->common->select_data_by_condition('ecin_ticker_master', array(), '', '_id', 'DESC', '', '', array());
            
            $this->load->view('economicalindicator/ecin_ticker', $this->data);
        }
        else
        {
            $this->data['tickers'] = $this->common->select_data_by_condition('ticker_master_new', array('showdashboard'=>"1"), '', '_id', 'DESC', '', '', array());

            $this->data['enic'] = $this->common->select_data_by_condition('economical_indicators_master', array(), '', '_id', 'DESC', '', '', array());
            
            //$this->data['tabledata'] = $this->common->select_data_by_condition('ecin_ticker_master', array(), '', '_id', 'DESC', '', '', array());


            $this->load->view('economicalindicator/ecin_ticker', $this->data);
        }
    }
    //ecin_data show
    public function getallecin_data(){
        $join = array(array(
                    'from'=>'country_master',
                    'localField'=>'country_id',
                    'foreignField'=>'_id',
                    'as'=>'countries'
                ),array(
                    'from'=>'economical_indicators_master',
                    'localField'=>'ecin_id',
                    'foreignField'=>'_id',
                    'as'=>'ecin'
                ),array(
                    'from'=>'ticker_master_new',
                    'localField'=>'ticker_id',
                    'foreignField'=>'_id',
                    'as'=>'tickers'
                ));

            $ecintabledata['ecintabledata']= $this->common->select_data_by_condition('ecin_ticker_master', array(), '', '_id', 'DESC', '', '',$join);
            //echo"<pre>";
            //print_r($ecintabledata);die;
            echo json_encode($ecintabledata);

    }
    //ecin data delete
    public function ecin_delete_data(){

        $id = $_POST['id'];
        $join = array();
        //$del_data['ids'] = $del_id;
        if($id != '')
        {

                $this->common->delete_data('ecin_ticker_master','_id', $id);
                $data['status'] = 'success';
                $data['msg'] = 'Record Deleted successfully.';
                // $this->session->set_flashdata('success', 'Record Deleted successfully.');
                echo json_encode($data);

        }
        else{
            // $this->session->set_flashdata('message', 'Record not found.');
            $data['status'] = 'success';
            $data['msg'] = 'Record not found.';
            echo json_encode($data);
        }
       
        

}
    public function adddata(){
        
        
        $ecin_id = $this->input->post('ecin_id');
        $ticker_id = $this->input->post('ticker_id');
        $country = $this->input->post('country');
        
        $this->form_validation->set_rules('ecin_id', 'Economic Indicator', 'required');
        $this->form_validation->set_rules('ticker_id', 'Ticker', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');

        if ($this->form_validation->run()) 
        {
            $condition = array(
                'ecin_id'=> (int) $ecin_id,
                'country_id'=> (int) $country,
                'ticker_id'=> (int) $ticker_id,
            ); 
            $res = $this->common->select_data_by_condition('ecin_ticker_master', $condition, '*', '', '', (int)10, (int)0 , array(), '');

            if(count($res) > 0){

                echo json_encode(array('status'=>'FALSE','message'=>'Record already exists'));    
                
            }
            else{

                $insert_data_tobe = array(
                    'ecin_id'=> (int) $ecin_id,
                    'country_id'=> (int) $country,
                    'ticker_id'=> (int) $ticker_id,
                ); 
                $this->common->insert_data($insert_data_tobe, 'ecin_ticker_master');    
                echo json_encode(array('status'=>'TRUE','message'=>'Record added successfully'));
            }
            
        }
        else
        {
            echo json_encode(array('status'=>'FALSE','message'=>'Something went wrong.'));
        }
    }
    public function updatedata(){
        
        
        $ecin_id = $this->input->post('ecin_id');
        $ticker_id = $this->input->post('ticker_id');
        $country = $this->input->post('country');
        $update_id = $this->input->post('_id');
             
        $this->form_validation->set_rules('ecin_id', 'Economic Indicator', 'required');
        $this->form_validation->set_rules('ticker_id', 'Ticker', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');

        if ($this->form_validation->run()) 
        {
            $condition = array(
               
                'ecin_id'=> (int) $ecin_id ,
                'country_id'=> (int) $country,
                'ticker_id'=> (int)  $ticker_id
            ); 
          
            // $res = $this->common->select_data_by_condition('ecin_ticker_master', $condition);
            //  print_r($res);
             $update_data = $this->common->update_data($condition, 'ecin_ticker_master', '_id',$update_id);
              
                if($update_data == TRUE){
                    echo json_encode(array('status'=>TRUE,'message'=>'Record Updated successfully')); 
                }
                else{
                    echo json_encode(array('status'=>FALSE,'message'=>'Record not updated successfully'));
                }
             
            // if(count($res) > 0){

            //     $this->common->update_data($condition, 'ecin_ticker_master', '_id', (int)$update_id);

            //     echo json_encode(array('status'=>'TRUE','message'=>'Record Updated successfully'));    
                
            // }
            // else{

                   
            //    
            // }
            
        }
        else
        {
            echo json_encode(array('status'=>'FALSE','message'=>'Something went wrong.'));
        }
    }

    public function edit()
    {
        $token=$this->uri->segment(3);
        $join = array();
        if($token != '')
        {

            $data_all = $this->common->select_database_id('economical_indicators_value_master', '_id', $token, '*');

            $join = array(array(
                    'from'=>'country_master',
                    'localField'=>'country_id',
                    'foreignField'=>'_id',
                    'as'=>'countries'
            ),array(
                    'from'=>'economical_indicators_master',
                    'localField'=>'ecin_id',
                    'foreignField'=>'_id',
                    'as'=>'ecin'
            ));
            // $join = array();
            // echo $token;
            $condition = array('_id'=>$token);
            $data_all = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '', '_id', 'DESC', '', '', $join);

            // echo "<pre>";print_r($data_all);
            // $newid = (array)($data_all[0]['_id']);
            // print_r($newid['oid']);exit();
            if(!empty($data_all)){

                $newid = (array)($data_all[0]['_id']);
                
                $data = array(
                    'value'=>$data_all[0]['value'],
                    'id'=>$newid['oid'],
                    'date'=>$data_all[0]['date'],
                    'country_name' => $data_all[0]['countries'][0]['name'],
                    'ecin_name' =>$data_all[0]['ecin'][0]['name']
                );
                $this->data['edit_id'] = $token;
                $this->data['data'] = $data;
                // print_r($this->data['data']);exit();
                $this->load->view('economicalindicator/edit', $this->data);
            }
            else{

                $this->session->set_flashdata('message', 'Record not found.');
                redirect('economicalindicator', 'refresh');
                exit();
            }

        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('economicalindicator', 'refresh');
            exit();
        }
    }
    public function delete()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $id = $this->uri->segment(4);
        $ticker_id= $this->uri->segment(3);
        $join = array();
        if($id != '')
        {
            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$ticker_id), '', '', '', '', '', $join);
            if(empty($ticker_data))
            {
                $this->session->set_flashdata('message', 'Record not found.');
                redirect('pricedata', 'refresh');
                exit();
            }
            else{

                $ticker_code = $ticker_data[0]['eod_data'];
                $this->common->delete_data($ticker_code,'_id', $id);
                $this->session->set_flashdata('success', 'Record Updated successfully.');
                redirect('pricedata/index/'.base64_encode($ticker_id), 'refresh');
            }
        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('pricedata', 'refresh');
            exit();
        }
    }
    public function getallcountryperenic()
    {
        $enic_id = $_REQUEST['enic_id'];

        $condition = array('ecin_id'=>(int)$enic_id);
        $join = array(array(
                'from'=>'country_master',
                'localField'=>'country_id',
                'foreignField'=>'_id',
                'as'=>'countries'
        ));
        // $join = array();
        $data = $this->common->select_data_by_condition('economical_indicator_details', $condition, '', '_id', 'DESC', '', '', $join);
        if(count($data) > 0){
            echo json_encode($data);
        }
        else{
            echo json_encode(array());   
        }
    }
    public function update_data(){
        

        $country_id = $_GET['country_id'];
        $ecin_id = $_GET['ecin_id'];
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        $updated_ids = array();
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $date_new = $model['date'];
            $date_new_formated = date("Y-m-d", strtotime('+5 hour +30 minutes',strtotime($date_new)));
            $add_id = $model['_id'];
            $update_array = (array)$model['_id'];
            $update_id = $update_array['$oid'];
            array_push($updated_ids, ['$elemMatch'=>$add_id]);
            $update_data_tobe = array(
                'date'=> $date_new_formated,
                'value'=>$model['value'],
                'consensus'=>$model['consensus'],
                'previous'=>$model['previous'],
                'note'=>$model['note'],
                'service'=>$model['service']
            ); 
            $this->common->update_data($update_data_tobe, 'economical_indicators_value_master', '_id', $update_id);
        }
        // print_r($updated_ids);exit();
        // $condition['_id'] = array( '$in' => $updated_ids);
        $condition['_id'] = array( '$in' => array());
        $res = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('economical_indicators_value_master');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    public function getecin_data()
    {
        
        $limit = isset($_GET['$top'])?$_GET['$top']:'10';
        $offset = isset($_GET['$skip'])?$_GET['$skip']:'0';
        $var = isset($_GET['$callback'])?$_GET['$callback']:'';
        $country_id = isset($_GET['country_id'])?$_GET['country_id']:'';
        $ecin_id = isset($_GET['ecin_id'])?$_GET['ecin_id']:'';
        $filter = isset($_GET['$filter'])?$_GET['$filter']:'';
        $orderby = isset($_GET['$orderby'])?$_GET['$orderby']:'';
        $orderby_col = '';
        $dir = '';
        if($orderby != ''){            
            $order_array = explode(' ', $orderby);
            if(count($order_array) == 2){
                $dir = 'DESC';
                $orderby_col = $order_array[0]; 
            }
            else{
                $dir = 'ASC';
                $orderby_col = $order_array[0];
            }
        }
        $condition =  array();
        $likecondition = array();
        $substringcondition = array();
        if($filter != ''){
            
            $filter = str_replace("(", "", $filter);
            $filter = str_replace(")", "", $filter);
            $filter = str_replace("'", "", $filter);
            $filter_array = explode(' and ',$filter);
            // print_r($filter_array);exit();
            foreach($filter_array as $fil){
                
                if (strpos($fil, 'startswith') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'substringof') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'endswith') !== false) {
                    $needtoexplode = false;
                }
                else{
                    $needtoexplode = true;
                }
                
                if($needtoexplode){

                    $fil_array = explode(' ', $fil);
                    // $rangeQuery = array('_id' => array( '$gt' => 10, '$lt' => 12 ));
                    if(count($fil_array) >= 3){
                        if(count($fil_array) == 3){
                                if($fil_array[0] == 'date'){
                                    $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                    $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                    $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                                }

                                if($fil_array[1] === 'ge'){
                                    $fil_array[1] = 'gte';
                                }
                                if($fil_array[1] === 'le'){
                                    $fil_array[1] = 'lte';
                                }
                                $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                        else{

                            $fil_array_value = explode(' '.$fil_array[1].' ', $fil);
                            // echo $fil;

                            $fil_array[2] = $fil_array_value[1];

                            // echo "string".$fil_array[2];
                            if($fil_array[0] == 'date'){
                                $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                            }

                            if($fil_array[1] === 'ge'){
                                $fil_array[1] = 'gte';
                            }
                            if($fil_array[1] === 'le'){
                                $fil_array[1] = 'lte';
                            }
                            $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                                
                    }
                }
                else{

                    if (strpos($fil, 'startswith') !== false) {

                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  '^'.$value_field;
                        // echo "field : ".$field."<br>";
                        // echo "value : ".$value_field;exit();

                    }
                    else if(strpos($fil, 'endswith') !== false) {
                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  $value_field.'^';
                    }
                    else if(strpos($fil, 'substringof') !== false) {

                        $fil_without = str_replace('substringof', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[1];
                        $value_field = $fil_array[0];
                        $substringcondition[$field] =  $value_field;
                        // print_r($substringcondition);exit();

                    }
                    
                }
                
                
            }
        }      
        $condition['ecin_id']  = (int)$ecin_id;
        $condition['country_id']  = (int)$country_id;
        $join = array();

        $res = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '', $orderby_col, $dir, $limit, $offset, $join,$likecondition,$substringcondition);
        $res_count1 = $this->common->get_count_of_table('economical_indicators_value_master',$condition,$likecondition,$substringcondition);
        // print_r($condition);
        // print_r($dir);
        // exit();
        if($ecin_id == 9 || $ecin_id == 8 || $ecin_id == 7 || $ecin_id == 5){
            foreach ($res as $value) {
                $value['value'] = number_format($value['value'], 1);
                
            }
        }
        if(count($res) > 0){
            // echo json_encode($data);
            $data['d']['results'] = $res;
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
        else{
            // echo json_encode(array());   
            $data['d']['results'] = array();
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
    }
    public function createnew()
    {
        $country_id = $_GET['country_id'];
        $ecin_id = $_GET['ecin_id'];
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        $updated_ids = array();
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $date_new = $model['date'];
            $date_new_formated = date("Y-m-d", strtotime('+5 hour +30 minutes',strtotime($date_new)));
            $insert_data_tobe = array(
                'ecin_id'=>(int)$ecin_id,
                'country_id'=>(int)$country_id,
                'date'=> $date_new_formated,
                'value'=>$model['value'],
                'add_type'=>'Manual'
            ); 
            // print_r($insert_data_tobe);
            $this->common->insert_data($insert_data_tobe, 'economical_indicators_value_master');
        }
        // exit();
        // print_r($updated_ids);exit();
        // $condition['_id'] = array( '$in' => $updated_ids);
        $condition['_id'] = array( '$in' => array());
        $res = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('economical_indicators_value_master');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();

    }
    public function uploadhistorical()
    {
        $this->load->library('PHPExcel');
        $ecin_id = $this->input->post('ecin_name');
        $country_id = $this->input->post('country_name');
        $this->form_validation->set_rules('country_name', 'Country Name', 'required');
        $this->form_validation->set_rules('ecin_name', 'Economical Indicator Name', 'required');
        if ($this->form_validation->run()) 
        {
            if(isset($_FILES["historical_data"]["tmp_name"]))
            {
                    
                    $file_ext = pathinfo($_FILES["historical_data"]["name"], PATHINFO_EXTENSION);
                    $path = $_FILES["historical_data"]["tmp_name"];
                    if($file_ext == 'csv'){
                        $object = PHPExcel_IOFactory::load($path);
                        foreach($object->getWorksheetIterator() as $worksheet)
                        {
                            $highestRow = $worksheet->getHighestRow();
                            $highestColumn = $worksheet->getHighestColumn();
                            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                            if($highestColumnIndex == 6){
                                for($row=1; $row<= $highestRow; $row++)
                                {  

                                    $date = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                                    $value = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                                    $consensus = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                                    $previous = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                                    $note = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                                    $service = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                                    if(strtotime($date) >= strtotime('1999-01-01')){

                                        $insert_data_tobe = array(
                                            'ecin_id'=>(int)$ecin_id,
                                            'country_id'=>(int)$country_id,
                                            'date'=> $date,
                                            'value'=>$value,
                                            'service'=>$service,
                                            'note'=>$note,
                                            'previous'=>$previous,
                                            'consensus'=>$consensus
                                        ); 

                                        $record_exitst = $this->common->select_data_by_condition('economical_indicators_value_master', array('ecin_id'=>(int)$ecin_id,'country_id'=>(int)$country_id,'date'=>$date), '*', '', '', '', '',array(),'');

                                            if(count($record_exitst) > 0)
                                            {
                                                // echo "main update";print_r($insert_data_tobe);
                                                $this->common->update_data($insert_data_tobe,'economical_indicators_value_master','_id',$record_exitst[0]['_id']);
                                            }
                                            else{
                                                // echo "main insert";print_r($insert_data_tobe);
                                                $this->common->insert_data($insert_data_tobe, 'economical_indicators_value_master');
                                            }
                                    }

                                }
                                $this->session->set_flashdata('success', 'Records have been inserted or updated successfully.');
                                redirect('economicalindicator/index/'.base64_encode($ecin_id).'/'.base64_encode($country_id), 'refresh');
                            }
                            else
                            {
                                 $this->session->set_flashdata('message', 'Please Select proper format file.');
                                 redirect('economicalindicator/index/'.base64_encode($ecin_id).'/'.base64_encode($country_id), 'refresh');
                            }
                        }
                    }
                    else
                    {
                         $this->session->set_flashdata('message', 'Please Select csv file only.');
                         redirect('economicalindicator/index/'.base64_encode($ecin_id).'/'.base64_encode($country_id), 'refresh');
                    }
            }
            else
            {
                 $this->session->set_flashdata('message', 'Please Select File.');
                 redirect('economicalindicator/index/'.base64_encode($ecin_id).'/'.base64_encode($country_id), 'refresh');
            }
        }
        else
        {
             $this->session->set_flashdata('message', 'Please select economical indicator and Country.');
             redirect('economicalindicator', 'refresh');
        }
    }
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'getecin_data' || $method == 'getallcountryperenic' || $method == 'ecintoticker' || $method == 'getallecin_data' || $method == 'ecin_delete_data'){
            $method = 'index';
        }
        if($method == 'insertdata' || $method == 'createnew' || $method == 'uploadhistorical' || $method == 'adddata' )
        {
            $method = 'add';
        }
        if($method == 'edit_data' || $method == 'update' || $method == 'updateticker_data' || $method == 'update_data' || $method == 'updatedata')
        {
            $method = 'edit';
        }
        
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }

}

/* End of file economical_indicator.php */
            /* Location: ./application/controllers/economical_indicator.php */