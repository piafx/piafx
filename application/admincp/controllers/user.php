<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('admin_data'))
        {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

        $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Users';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
        
        $this->check_permission();
    }

    public function index()
    {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $join = array(array(
                'from'         => 'role',
                'foreignField' => '_id',
                'localField' => 'user_role',
                'as'     => 'userdetails',
        ));
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
            $this->data['data'] = $this->common->select_data_by_condition('users', array('user_status !='=>'Enable'), '', '_id', 'DESC', '', '', $join);
            $this->load->view('user', $this->data);
            $this->check_permission();
        }
        else
        {
            $this->data['data'] = $this->common->select_data_by_condition('users', array('user_status'=>'Enable'), '', '', '', '', '', $join);
            // print_r($this->data['data']);exit();
            $this->load->view('user', $this->data);
            $this->check_permission();
        }
        
    }

    public function add()
    {
        $this->data['role'] = $this->common->get_data_all('role');
        $this->load->view('users/add', $this->data);
    }
    
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'add_data')
        {
            $method = 'add';
        }
        if($method == 'change_status' || $method == 'edit_data')
        {
            $method = 'edit';
        }
        if($method == 'viewmodal')
        {
            $method = 'delete';
        }
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }
    
     public function viewmodal($mode, $id)
    {
        
        $this->data['mode'] =   $this->input->post('mode');
        // $data['type_status'] = $type_stage_active;
        $this->data['mode'] =  $mode;
        $this->data['id'] = $id;

        echo $this->load->view('users/model', $this->data);
    }
    
    public function delete()
    {
        $token=$this->uri->segment(3);
        if($token != '')
        {
            $token = base64_decode($token);
            
            $session_array = $this->session->userdata('admin_data');
            if($session_array['role'] != 1)
            {
                $res_assign = $this->common->select_data_by_condition('users', array('user_id'=>(int) $token,'user_status !='=>'Deleted','coordinator'=>(int)$session_array['ad_id']), '', 'user_id', 'DESC', '', '');
                if(empty($res_assign))
                {
                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('user', 'refresh');
                    exit();
                }
            }
            
            $data = array(
                    'user_status' => 'Deleted',
            );
            if ($this->common->update_data($data, 'users', 'user_id', (int) $token ))
            {
                $this->session->set_flashdata('success', 'Record has been deleted successfully.');
                //echo json_encode(array('message'=>'Success','status'=>true));
                redirect('user', 'refresh');
            }
        }
     }

    public function exist($key)
    {
        $id = $this->data['eee'];
        $records = $this->common->select_database_by_muliple_where('users', array('user_email' => $key,'user_status'=>'Enable'),'*', '','');
        if (count($records) > 1)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    function edit_data()
    {
        $edit_id     = $this->input->post('id');
        $this->data['eee']= $edit_id;
        if($edit_id != '')
        {
            
            $this->form_validation->set_rules('first_name', 'Fisrt Name', 'required|callback_usernameregex');
            $this->form_validation->set_message('usernameregex', 'Only Characters allowed.');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|callback_usernameregex');
            $this->form_validation->set_rules('role', 'Role', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_exist');
            $this->form_validation->set_message('exist', 'Email has been already been Used.');
            $this->form_validation->set_rules('contact', 'Contact', 'numeric');
            if ($this->form_validation->run() == TRUE)
            {
                $id = base64_decode($this->input->post('id'));
                $session_array = $this->session->userdata('admin_data');
                if($session_array['role'] != 1)
                {
                    $res_assign = $this->common->select_data_by_condition('users', array('_id'=>(string) $id,'user_status'=>'Enable'), '', '_id', 'DESC', '', '');
                    if(empty($res_assign))
                    {
                        $this->session->set_flashdata('message', 'Record not found.');
                        redirect('user', 'refresh');
                        exit();
                    }
                }
                
                
                $fname     = $this->input->post('first_name');
                $lname     = $this->input->post('last_name');
                $email     = $this->input->post('email');
                $role     = $this->input->post('role');
                $contact     = $this->input->post('contact');

                $rand_link = random_string();
                //$id = base64_decode($this->input->post('id'));

                $data = array(
                    'user_firstname' => $fname,
                    'user_lastname' => $lname,
                    'user_email' => $email,
                    'user_role'=> $role,
                    'user_contact' => $contact,
                );
                if ($this->common->update_data($data, 'users', '_id', (string) $id))
                {
                    $this->session->set_flashdata('success', 'Record has been updated successfully.');
                    redirect('user', 'refresh');
                }
            }
            else
            {
                $this->data['role'] = $this->common->get_data_all('role');
                $this->data['edit_id'] = '';
                $this->data['id'] = base64_decode($edit_id);
                $res = $this->common->select_database_id('users', '_id', (string) base64_decode($edit_id), '*');
                if(!empty($res))
                {
                    $this->data['selected_role']=$res[0]['user_role'];
                }
                $this->load->view('users/edit', $this->data);
            }
        }
        else
        {
            
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('user', 'refresh');
        }
        
    }

    
    
    public function edit() {
        
        $token=$this->uri->segment(3);
        if($token != '')
        {
            $token = $this->data['edit_id'] = base64_decode($token);

            $session_array = $this->session->userdata('admin_data');
            if($session_array['role'] != 1)
            {
                $res_assign = $this->common->select_data_by_condition('users', array('_id'=>$token,'user_status'=>'Enable'), '', 'user_id', 'DESC', '', '');
                if(empty($res_assign))
                {
                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('user', 'refresh');
                    exit();
                }
            }
            $res = $this->common->select_database_id('users', '_id', $token, '*');
            if(!empty($res))
            {
                $this->data['role'] = $this->common->get_data_all('role');
                $this->data['id']=$res[0]['_id'];
                $this->data['fname']=$res[0]['user_firstname'];
                $this->data['lname']=$res[0]['user_lastname'];
                $this->data['email']=$res[0]['user_email'];
                $this->data['selected_role']=$res[0]['user_role'];
                $this->data['contact']=$res[0]['user_contact'];
                $this->load->view('users/edit', $this->data);
            }
            else
            { 
                $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
                redirect('user', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
            redirect('user', 'refresh');
        }
        
        
    }
    
    public function exist_add($key)
    {
        $records = $this->common->select_database_by_muliple_where('users', array('user_email' => $key,'user_status'=>'Enable'), '*', '', '');
        if (!empty($records))
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    public function usernameregex($userName) {
        
        if(preg_match('/^[A-Za-z() \']+$/', $userName ) ) 
        {
          return TRUE;
        } 
        else 
        {
          return FALSE;
        }
    }
    public function add_data()
    {
        //^[A-Za-z0-9()\/-]+$
        $this->form_validation->set_rules('first_name', 'Fisrt Name', 'required|callback_usernameregex');
        $this->form_validation->set_message('usernameregex', 'Only Characters allowed.');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|callback_usernameregex');
        $this->form_validation->set_rules('role', 'Role', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_exist_add');
        $this->form_validation->set_message('exist_add', 'Email has been already been Used.');
        $this->form_validation->set_rules('contact', 'Contact', 'numeric|max_length[16]|min_length[10]');
        
        if ($this->form_validation->run() == TRUE)
        {
            $fname     = $this->input->post('first_name');
            $lname     = $this->input->post('last_name');
            $email     = $this->input->post('email');
            $role     = $this->input->post('role');
            $contact     = $this->input->post('contact');
            $rand_link = random_string();

            $data = array(
                'user_firstname'    => strip_tags($fname),
                'user_lastname'    => strip_tags($lname),
                'user_email'    => strip_tags($email),
                'user_password'=> base64_encode($rand_link),
                'user_role' => strip_tags($role),
                'user_contact'  => strip_tags($contact),
                'user_status'      => 'Enable',
                'user_created_date'=>date('Y-m-d'),
                'user_resetpassword'=>'',
                'modifieddate'=>date('Y-m-d'),
            );
            if ($this->common->insert_data($data, 'users'))
            {
                //$message = "Thank you for resiter with us. Your Password is : ".$rand_link.". You can login to : ". base_url();
                //$sub = "Welcome to Insight";
                    
                    
                $email_temp = $this->common->select_database_by_muliple_where('email_template', array('_id' => '3'),'*','','');
                // print_r($email_temp);exit();
                $sub = $email_temp[0]['subject'];
                $message = $email_temp[0]['mailformat'];
                $link = "\r\n" . base_url() . 'adminlogin/';
                $message = str_replace("%firstname%", $fname, $message);
                $message = str_replace("%lastname%", $lname, $message);
                $message = str_replace("%email%", $email, $message);
                $message = str_replace("%password%", $rand_link, $message);
                $message = str_replace("%link%", $link, $message);    
                                    
                $this->send_verification_mail($email,$rand_link, $message, $sub );
                $this->session->set_flashdata('success', 'Record has been inserted successfully.');
                redirect('user', 'refresh');
            }
        }
        else
        {
            $this->data['role'] = $this->common->get_data_all('role');
            $this->load->view('users/add', $this->data);
        }
    }
    
    function send_verification_mail($user_email, $rand_link, $message, $sub) {
        
        
            $to = $user_email;
            $from = "aspltest3@gmai.com";
            $subject = $sub;
            $from_name = "Insite";
            $content = $message;

            $this->config->load('email', TRUE);
            $this->cnfemail = $this->config->item('email');

            //Loading E-mail Class
            $this->load->library('email');
            $this->email->initialize($this->cnfemail);

            $this->email->from($from, $from_name);
            $this->email->to($to); 

            $this->email->subject($subject);

            $this->email->message($content);
            
            if ($this->email->send()) {
                return TRUE;
            } else {
                return FALSE;
            }
//        $url = "http://ashapurasoftech.com/testmail/res_mail.php?to=$user_email&from=donotreply@insight.com&subject=$sub&from_name=Test&content=$message&filename=";
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_HEADER, 1);
//        curl_setopt($ch, CURLOPT_NOBODY, 1);
//        $output = curl_exec($ch);
//        curl_close($ch);
//        return TRUE;
        
//        $config = array();
//        $config['protocol'] = "smtp";
//        $config['smtp_host'] = "ssl://smtp.gmail.com";
//        $config['smtp_port'] = "465";
//        $config['smtp_user'] = "aspltest3@gmail.com";
//        $config['smtp_pass'] = "aspl@123";
//        $config['charset'] = "utf-8";
//        $config['mailtype'] = "html";
//        $config['newline'] = "\r\n";
//
//
//        $from_email = "aspltest3@gmail.com";
//        $to_email = "aspltest3@gmail.com";
//        $this->load->library('email');
//
//        $this->email->initialize($config);
//
//        $this->email->from($from_email, 'Support');
//        $this->email->to($user_email);
//        $this->email->subject($sub);
//
//        $this->email->message($message);
//
//
//        //Send mail
//        if ($this->email->send()) {
//            return TRUE;
//        } else {
//            return FALSE;
//        }
    }
    
    public function change_status()
    {


        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('status', 'status', 'required');


        if ($this->form_validation->run() == TRUE)
        {

            $status = $this->input->post('status');
            $userID = $this->input->post('userid');
            $session_array = $this->session->userdata('admin_data');
            if($session_array['role'] != 1)
            {
                $res_assign = $this->common->select_data_by_condition('users', array('user_id'=>(int) $userID,'user_status !='=>'Deleted','coordinator'=>(int)$session_array['ad_id']), '', 'user_id', 'DESC', '', '');
                if(empty($res_assign))
                {
                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('user', 'refresh');
                    exit();
                }
            }   
            $data = array(
                'user_status' => $status,
            );
            if ($this->common->update_data($data, 'users', 'user_id', (int) $userID))
            {


                $this->session->set_flashdata('success', 'Status updated successfully.');
                redirect('user', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                redirect('user', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong. Please try again');
            redirect('user', 'refresh');
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
