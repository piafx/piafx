<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuration extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('aspl_hrmadmin_data')) {
//If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

//Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        
        $this->data['section_title'] = 'admin';

        $this->load->model('common');
        $res = $this->common->select_database_id('masteradmin', 'adminid', (int) 1, '*');
        $this->data['logo'] = $res[0]['image'];

//Load leftsidemenu and save in variable
        $this->data['name'] = $this->session->userdata['aspl_hrmadmin_data']['name'];
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->data['sidebar'] = $this->load->view('sidebar', $this->data, true);

        $this->load->model('common');
        $this->load->model('configuration_modal');
    }

    public function index() {
        
        $this->data['fields']= $this->configuration_modal->get_fields_data();
        $data= $this->configuration_modal->get_config_data1();
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        die();
        $i=0;
        foreach ($data as $aa)
        {
            $type= explode(',', $aa['names']);
            $data[$i]['name']=$type;
            $type= explode(',', $aa['type']);
            $data[$i]['type']=$type;
            $values1= explode(',', $aa['values1']);
            $data[$i]['values1']=$values1;
            $reference= explode(',', $aa['reference']);
            $data[$i]['reference']=$reference;
            $i++;
        }
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        die();
        
        $this->data['data']=$data;
        $this->load->view('configuration/aa', $this->data);
        
    }
    
    public function get_salary_details()
    {
            $this->load->helper(array('form', 'url'));
            $employee_id=$this->input->post("employee_id");
            if($employee_id!='')
            {
                $res=$this->common->select_database_id('employee_salary', 'employee_id', $employee_id);
                if(!empty($res))
                {
                $res1=$this->configuration_modal->select_database_id('salary_config sc', 'employee_id', $employee_id);
//                echo "<pre>";
//                print_r($res1);
//                echo "</pre>";
//                die();
                $fields= $this->configuration_modal->get_fields_data();
                $base_salary=$res[0]['base_salary'];
                if($base_salary>10000)
                {
                    $pt=200;
                    
                }
                else
                {
                    $pt=150;
                }
                $data['Totalsalary']=$base_salary;
                $data['Totalsalarypay']=0;
                foreach ($res1 as $a)
                {
                    
                    if($a['reference']==1)
                    {
                        if($a['type']=='percentage')
                        {
                            $fieldres=$this->common->select_database_id('salary_fields', 'field_id', $a['field_id']);
                            $field=$fieldres[0]['name'];
                            $type=$fieldres[0]['type'];
                            $val=$a['value'];
                            $data[$field] = ($val / 100) * $base_salary;
                            
                        }
                        else
                        {
                            $fieldres=$this->common->select_database_id('salary_fields', 'field_id', $a['field_id']);
                            $field=$fieldres[0]['name'];
                            $type=$fieldres[0]['type'];
                            $data[$field]=$a['value'];
                        }
                    }
                    else
                    {
                        if($a['type']=='percentage')
                        {
                            $fieldres=$this->common->select_database_id('salary_fields', 'field_id', $a['field_id']);
                            $field=$fieldres[0]['name'];
                            $type=$fieldres[0]['type'];
                            $val=$a['value'];
                            $data[$field] = ($val / 100) * $data['Basic'];
                        }
                        else
                        {
                            $fieldres=$this->common->select_database_id('salary_fields', 'field_id', $a['field_id']);
                            $field=$fieldres[0]['name'];
                            $type=$fieldres[0]['type'];
                            $data[$field]=$a['value'];
                        }
                    }
                    if($type==0)
                    {
                        $data['Totalsalarypay']=$data['Totalsalarypay']+$data[$field];
                    }
                    else
                    {
                        $data['Totalsalarypay']=$data['Totalsalarypay']-$data[$field];
                    }
                    
                }
                $data['Totalsalarypay']=$data['Totalsalarypay']-$pt;
                $data['pt']=$pt;
                echo json_encode($data);
                }
                else
                {
                    echo json_encode("");
                }
            }
    }
    
    
     public function add_data() {
         
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name1', 'Employee Name', 'required');
          if ($this->form_validation->run() == TRUE) {
              
            $fields= $this->configuration_modal->get_fields_data();
            $employee_id=$this->input->post('name1');
            
            foreach ($fields as $a)
            {
                $ii=$a['field_id'];
                $value = $this->input->post('value_'.$ii);
                $type = $this->input->post('type_'.$ii);
                $reference = $this->input->post('reference_'.$ii);
                $data = array(
                    'employee_id' => strip_tags($employee_id),
                    'field_id' => $ii,
                    'value' => $value,
                    'type' => $type,
                    'reference' => $reference,
                    'status' => 'Enable',
                );
                
                if ($this->common->insert_data($data, 'salary_config')) {
                        
                }
                
            }
            $this->add_log($employee_id, 'Insert', 1);
            $this->session->set_flashdata('success', 'Configuration has been added successfully.');
            redirect('configuration/index', 'refresh');
          }
          else
          {
             $this->session->set_flashdata('message', 'Somethig Goes Wrong');
            redirect('configuration/index', 'refresh');
          }
     }
    public function add() {
        
        $this->data['emp']= $this->configuration_modal->get_employee_data();
        $this->data['fields']= $this->configuration_modal->get_fields_data();
        $this->load->view('configuration/add', $this->data);
    }

    public function edit() {
         $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $id = $this->uri->segment(3);
        
        if ($id != '') {
            
            $this->data['fields']= $this->configuration_modal->get_fields_data();
            $id= base64_decode($id);
            $this->data['data']=$this->configuration_modal->get_edit_data($id);
            $this->load->view('configuration/edit', $this->data);
            
        }
        
    }
    
     function edit_data() {

        $this->load->helper(array('form', 'url'));
        
        $this->load->library('form_validation');
        $id = $this->input->post('employee_id');
        
        if ($id != '') {
            $employee_id= base64_decode($id);
            $fields= $this->configuration_modal->get_fields_data();
            
            foreach ($fields as $a)
            {
                $ii=$a['field_id'];
                $value = $this->input->post('value_'.$ii);
                $type = $this->input->post('type_'.$ii);
                $reference = $this->input->post('reference_'.$ii);
                $data = array(
                    
                    'value' => $value,
                    'type' => $type,
                    'reference' => $reference,
                    
                );
                
                $this->configuration_modal->udpate_data($data,$employee_id,$ii);
            }
            $this->add_log($employee_id, 'Update', 1);
            $this->session->set_flashdata('success', 'Configuration has been Updated successfully.');
            redirect('configuration/index', 'refresh');
          
        }
     }
    
    public function change_status() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('status', 'status', 'required');


        if ($this->form_validation->run() == TRUE) {

            $status = $this->input->post('status');
            $userID = $this->input->post('userid');

            $data = array(
                  'status' => $status,
            );
            if ($this->common->update_data($data, 'salary_config', 'employee_id', (int) $userID)) {
            $this->add_log($userID, 'Change Status', 1);
            $this->session->set_flashdata('success', 'Status updated successfully.');
                redirect('configuration/index', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                redirect('configuration/index', 'refresh');
            }
        } else {
            $this->load->view('configuration/index', $this->data);
        }   
    }
    
    
    public function add_log($to_id,$method_name,$result)
    {
        $ci =& get_instance();
        $controller_name=$ci->router->fetch_class();
        $by_id=$this->session->userdata['aspl_hrmadmin_data']['ad_id'];
        $role=$this->session->userdata['aspl_hrmadmin_data']['role'];
        $data1 = array(
                'by_id'=>$by_id,
                'role_id'=>$role,
                'to_id'=>$to_id,
                'controller_name'=> $controller_name,
                'method_name' => $method_name,
                'created_date' => date('Y-m-d H:i:s'),
                'result'=> $result,  
                   );
        $this->common->insert_data($data1, 'admin_log');
        
    }
    
}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */
            