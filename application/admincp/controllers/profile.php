<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class profile extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

        $this->load->model('common');
        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Profile';


        //Load leftsidemenu and save in variable
        $this->data['permission_list'] = $this->common->permission();
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        
    }

    public function index() {
        
        $session_array = $this->session->userdata('admin_data');
        //print_r($session_array);exit();
        if(!empty($session_array))
        {
            $this->data['data'] = $this->common->select_database_id('users', '_id', (string) $session_array['ad_id'] , $data = '*');
            $this->load->view('profile', $this->data);
        }
        else
        {
            $this->session->set_flashdata('message', 'Page you are trying to access is not accessible.');
            redirect('admin', 'refresh');
        }
        
    }

    
     public function usernameregex($userName) {
        
        if(preg_match('/^[A-Za-z() \']+$/', $userName ) ) 
        {
          return TRUE;
        } 
        else 
        {
          return FALSE;
        }
    }
    public function add_data() {
        
        $this->load->helper(array('form', 'url'));
        $session_array = $this->session->userdata('admin_data');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'required|callback_usernameregex');
        $this->form_validation->set_message('usernameregex', 'Only Characters allowed.');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required|callback_usernameregex');
        $this->form_validation->set_rules('contact', 'Contact', 'required|numeric|max_length[16]|min_length[10]');

        if ($this->form_validation->run() == TRUE) {
            
            $fname = $this->input->post('first_name');
            $lname = $this->input->post('last_name');
            $contact= $this->input->post('contact');

            $data = array(
                'user_firstname' => strip_tags($fname),
                'user_lastname' => strip_tags($lname),
                'user_contact' => strip_tags($contact)
            );

            if ($this->common->update_data($data, 'users', '_id', (string) $session_array['ad_id'])) {

                $sess_array = array(
                    'name'=> strip_tags($fname).' '.strip_tags($lname),
                    'email' => $session_array['email'],
                    'ad_id' => $session_array['ad_id'],
                    'role'  => $session_array['role'],
                );
                $this->session->set_userdata('admin_data', $sess_array);
                $this->session->set_flashdata('success', 'Your profile has been successfully updated.');
                redirect('profile', 'refresh');
            }
        } else {

            $this->data['data'] = $this->common->select_database_id('users', '_id', (string) $session_array['ad_id'] , $data = '*');
            $this->load->view('profile', $this->data);
        }
    }
    public function change_password()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('current_password', 'Current Password', 'required|max_length[20]|min_length[6]');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|max_length[20]|min_length[6]');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'required|max_length[20]|min_length[6]');

        if ($this->form_validation->run() == TRUE) {
            
            $session_array = $this->session->userdata('admin_data');
            $user_date = $this->common->select_database_id('users', '_id', (string) $session_array['ad_id'] , $data = '*');
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password= $this->input->post('confirm_new_password');
            if($user_date[0]['user_password'] == base64_encode($current_password))
            {
                if($confirm_new_password == $new_password)
                {
                    $data = array(
                        'user_password' => strip_tags(base64_encode($new_password))
                    );

                    if ($this->common->update_data($data, 'users', '_id', (string) $session_array['ad_id'])) {

                        $this->session->set_flashdata('success', 'Your password has been successfully updated.');
                        redirect('profile', 'refresh');
                    }
                }
                else{
                    $this->session->set_flashdata('message', 'Your password and confirm password are not matching.');
                    redirect('profile', 'refresh');
                }
            }
            else{
                $this->session->set_flashdata('message', 'Your have entered wrong current password.');
                redirect('profile', 'refresh');
            }
            
        } else {

            $this->session->set_flashdata('message', 'Please try again.');
            redirect('profile', 'refresh');
        }
    }

    


}
