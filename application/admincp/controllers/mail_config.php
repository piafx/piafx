<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mail_config extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        
        parent::__construct();

        if (!$this->session->userdata('aspl_hrmadmin_data')) {
//If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }
          
//Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'admin';


        $this->load->model('common');
        $res = $this->common->select_database_id('masteradmin', 'adminid', (int) 1, '*');
        $this->data['logo'] = $res[0]['image'];
        
//Load leftsidemenu and save in variable
        $this->data['name'] = $this->session->userdata['aspl_hrmadmin_data']['name'];
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        $this->data['sidebar'] = $this->load->view('sidebar', $this->data, true);

        $this->load->model('common');
        $this->load->model('mail_modal');
        }

    public function index() {
        $this->data['data']= $this->mail_modal->get_mail_data();
        $this->load->view('mail/index', $this->data);
    }
    
    public function edit() {

        $token = $this->uri->segment(3);
        $this->data['edit_id'] = $token;
        $token = base64_decode($token);
        
        $res = $this->common->select_database_id('mail', 'mail_id', (int) $token, '*');
        if(!empty($res))
        {
                    $this->data['id'] = $res[0]['mail_id'];
                    $this->data['host'] = $res[0]['mail_smpt_host'];
                    $this->data['port'] = $res[0]['mail_smpt_port'];
                    $this->data['user'] = $res[0]['mail_smpt_user'];
                    $this->data['password'] = base64_decode($res[0]['mail_smpt_password']);
                    $this->load->view('mail/edit', $this->data);
        }
        else
        {
                    $this->index();
        }   
        
    }
    
    
    function edit_data() {

        $this->load->helper(array('form', 'url'));
        
        $this->load->library('form_validation');
        $id = $this->input->post('id');
            
        if ($id != '') {
            $id= base64_decode($id);
            $this->form_validation->set_rules('host', 'Host', 'required');
            $this->form_validation->set_rules('port', 'Port', 'required');
            $this->form_validation->set_rules('user', 'User', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required'); 
           
            if ($this->form_validation->run() == TRUE) {
                
                        $host = $this->input->post('host');
                        $port = $this->input->post('port');
                        $user = $this->input->post('user');
                        $password = $this->input->post('password');
                       
                $data = array(
                    'mail_smpt_host' => strip_tags($host),
                    'mail_smpt_port' => strip_tags($port),
                    'mail_smpt_user' => strip_tags($user),
                    'mail_smpt_password' => strip_tags(base64_encode($password)),
                );
                if ($this->common->update_data($data, 'mail', 'mail_id', (int) $id)) {
                    
                    $this->add_log(0, 'Update',1);
                    $this->session->set_flashdata('success', 'Mail Configurations has been updated successfully.');
                    redirect('mail_config/index', 'refresh');
                }   
                else
                {
                    $this->add_log(0, 'Update',0);
                }
            } else {
                
                    $this->add_log(0, 'Update',0);    
                $this->data['id'] = $id;
                $this->data['edit_id'] = '';
                //redirect('admin/edit/'.$id, 'refresh');
                $this->load->view('mail/edit', $this->data);
            }
        } else {
            $this->session->set_flashdata('message', 'Something Goes Wrong.');
            redirect('mail_config/index', 'refresh');
        }
    }
    
    
    public function add_log($to_id,$method_name,$result)
    {
        $ci =& get_instance();
        $controller_name=$ci->router->fetch_class();
        $by_id=$this->session->userdata['aspl_hrmadmin_data']['ad_id'];
        $role=$this->session->userdata['aspl_hrmadmin_data']['role'];
        $data1 = array(
                'by_id'=>$by_id,
                'role_id'=>$role,
                'to_id'=>$to_id,
                'controller_name'=> $controller_name,
                'method_name' => $method_name,
                'created_date' => date('Y-m-d H:i:s'),
                'result'=> $result,  
                   );
        $this->common->insert_data($data1, 'admin_log');
        
    }


    
}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */
            