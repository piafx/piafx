<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ecinmaster extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
         parent::__construct();
        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }
       ini_set('memory_limit', '640M');
       ini_set('post_max_size', '640M');
       ini_set('upload_max_filesize', '640M');
       ini_set('max_execution_time', 60000);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Economical Indicator';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->check_permission();
         error_reporting(E_ALL);
        ini_set('display_errors', 1);

    }
    
    public function index()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $session_array = $this->session->userdata('admin_data');
        if($session_array['role'] != 1)
        {
            $this->load->view('economicalindicator/master_index', $this->data);
        }
        else
        {
            $this->load->view('economicalindicator/master_index', $this->data);
        } 
    }
    public function edit()
    {
        $token=$this->uri->segment(3);
        $join = array();
        if($token != '')
        {

            $data_all = $this->common->select_database_id('economical_indicators_value_master', '_id', $token, '*');

            $join = array(array(
                    'from'=>'country_master',
                    'localField'=>'country_id',
                    'foreignField'=>'_id',
                    'as'=>'countries'
            ),array(
                    'from'=>'economical_indicators_master',
                    'localField'=>'ecin_id',
                    'foreignField'=>'_id',
                    'as'=>'ecin'
            ));
            // $join = array();
            // echo $token;
            $condition = array('_id'=>$token);
            $data_all = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '', '_id', 'DESC', '', '', $join);

            // echo "<pre>";print_r($data_all);
            // $newid = (array)($data_all[0]['_id']);
            // print_r($newid['oid']);exit();
            if(!empty($data_all)){

                $newid = (array)($data_all[0]['_id']);
                
                $data = array(
                    'value'=>$data_all[0]['value'],
                    'id'=>$newid['oid'],
                    'date'=>$data_all[0]['date'],
                    'country_name' => $data_all[0]['countries'][0]['name'],
                    'ecin_name' =>$data_all[0]['ecin'][0]['name']
                );
                $this->data['edit_id'] = $token;
                $this->data['data'] = $data;
                // print_r($this->data['data']);exit();
                $this->load->view('economicalindicator/edit', $this->data);
            }
            else{

                $this->session->set_flashdata('message', 'Record not found.');
                redirect('economicalindicator', 'refresh');
                exit();
            }

        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('economicalindicator', 'refresh');
            exit();
        }
    }
    public function delete()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $id = $this->uri->segment(4);
        $ticker_id= $this->uri->segment(3);
        $join = array();
        if($id != '')
        {
            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$ticker_id), '', '', '', '', '', $join);
            if(empty($ticker_data))
            {
                $this->session->set_flashdata('message', 'Record not found.');
                redirect('pricedata', 'refresh');
                exit();
            }
            else{

                $ticker_code = $ticker_data[0]['eod_data'];
                $this->common->delete_data($ticker_code,'_id', $id);
                $this->session->set_flashdata('success', 'Record Updated successfully.');
                redirect('pricedata/index/'.base64_encode($ticker_id), 'refresh');
            }
        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('pricedata', 'refresh');
            exit();
        }
    }
    public function getallcountryperenic()
    {
        $enic_id = $_REQUEST['enic_id'];

        $condition = array('ecin_id'=>(int)$enic_id);
        $join = array(array(
                'from'=>'country_master',
                'localField'=>'country_id',
                'foreignField'=>'_id',
                'as'=>'countries'
        ));
        // $join = array();
        $data = $this->common->select_data_by_condition('economical_indicator_details', $condition, '', '_id', 'DESC', '', '', $join);
        if(count($data) > 0){
            echo json_encode($data);
        }
        else{
            echo json_encode(array());   
        }
    }
    public function update_data(){
        

        $country_id = $_GET['country_id'];
        $ecin_id = $_GET['ecin_id'];
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        $updated_ids = array();
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $add_id = $model['_id'];
            $update_array = (array)$model['_id'];
            $update_id = $update_array['$oid'];
            array_push($updated_ids, ['$elemMatch'=>$add_id]);
            $update_data_tobe = array(
                'value'=>$model['value'],
            ); 
            $this->common->update_data($update_data_tobe, 'economical_indicators_value_master', '_id', $update_id);
        }
        // print_r($updated_ids);exit();
        // $condition['_id'] = array( '$in' => $updated_ids);
        $condition['_id'] = array( '$in' => array());
        $res = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('economical_indicators_value_master');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    public function getecin_data()
    {
        
        $limit = isset($_GET['$top'])?$_GET['$top']:'10';
        $offset = isset($_GET['$skip'])?$_GET['$skip']:'0';
        $var = isset($_GET['$callback'])?$_GET['$callback']:'';
        $country_id = isset($_GET['country_id'])?$_GET['country_id']:'';
        $ecin_id = isset($_GET['ecin_id'])?$_GET['ecin_id']:'';
        $filter = isset($_GET['$filter'])?$_GET['$filter']:'';
        $orderby = isset($_GET['$orderby'])?$_GET['$orderby']:'';
        $orderby_col = '';
        $dir = '';
        if($orderby != ''){            
            $order_array = explode(' ', $orderby);
            if(count($order_array) == 2){
                $dir = 'DESC';
                $orderby_col = $order_array[0]; 
            }
            else{
                $dir = 'ASC';
                $orderby_col = $order_array[0];
            }
        }
        $condition =  array();
        $likecondition = array();
        $substringcondition = array();
        if($filter != ''){
            
            $filter = str_replace("(", "", $filter);
            $filter = str_replace(")", "", $filter);
            $filter = str_replace("'", "", $filter);
            $filter_array = explode(' and ',$filter);
            // print_r($filter_array);exit();
            foreach($filter_array as $fil){
                
                if (strpos($fil, 'startswith') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'substringof') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'endswith') !== false) {
                    $needtoexplode = false;
                }
                else{
                    $needtoexplode = true;
                }
                
                if($needtoexplode){

                    $fil_array = explode(' ', $fil);
                    // $rangeQuery = array('_id' => array( '$gt' => 10, '$lt' => 12 ));
                    if(count($fil_array) >= 3){
                        if(count($fil_array) == 3){
                                if($fil_array[0] == 'date'){
                                    $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                    $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                    $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                                }

                                if($fil_array[1] === 'ge'){
                                    $fil_array[1] = 'gte';
                                }
                                if($fil_array[1] === 'le'){
                                    $fil_array[1] = 'lte';
                                }
                                $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                        else{

                            $fil_array_value = explode(' '.$fil_array[1].' ', $fil);
                            // echo $fil;

                            $fil_array[2] = $fil_array_value[1];

                            // echo "string".$fil_array[2];
                            if($fil_array[0] == 'date'){
                                $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                            }

                            if($fil_array[1] === 'ge'){
                                $fil_array[1] = 'gte';
                            }
                            if($fil_array[1] === 'le'){
                                $fil_array[1] = 'lte';
                            }
                            $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                                
                    }
                }
                else{

                    if (strpos($fil, 'startswith') !== false) {

                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  '^'.$value_field;
                        // echo "field : ".$field."<br>";
                        // echo "value : ".$value_field;exit();

                    }
                    else if(strpos($fil, 'endswith') !== false) {
                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  $value_field.'^';
                    }
                    else if(strpos($fil, 'substringof') !== false) {

                        $fil_without = str_replace('substringof', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[1];
                        $value_field = $fil_array[0];
                        $substringcondition[$field] =  $value_field;
                        // print_r($substringcondition);exit();

                    }
                    
                }
                
                
            }
        }      
        $condition['ecin_id']  = (int)$ecin_id;
        $condition['country_id']  = (int)$country_id;
        $join = array();
        $res = $this->common->select_data_by_condition('economical_indicators_value_master', $condition, '', $orderby_col, $dir, $limit, $offset, $join,$likecondition,$substringcondition);
        $res_count1 = $this->common->get_count_of_table('economical_indicators_value_master',$condition,$likecondition,$substringcondition);
        if(count($res) > 0){
            // echo json_encode($data);
            $data['d']['results'] = $res;
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
        else{
            // echo json_encode(array());   
            $data['d']['results'] = array();
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
    }
    public function getallcountry()
    {
        $var = $_GET['$callback'];
        $res = $this->common->select_database_by_muliple_where('country_master', array(), '*', '', '');
        $data['d']['results'] = $res;
        echo $var.'('.json_encode($data).')';
    }
    public function getecinall(){

        $limit = isset($_GET['$top'])?$_GET['$top']:'10';
        $offset = isset($_GET['$skip'])?$_GET['$skip']:'0';
        $var = isset($_GET['$callback'])?$_GET['$callback']:'';
        $filter = isset($_GET['$filter'])?$_GET['$filter']:'';
        $orderby = isset($_GET['$orderby'])?$_GET['$orderby']:'';
        $orderby_col = '';
        $dir = '';
        if($orderby != ''){            
            $order_array = explode(' ', $orderby);
            if(count($order_array) == 2){
                $dir = 'DESC';
                $orderby_col = $order_array[0]; 
            }
            else{
                $dir = 'ASC';
                $orderby_col = $order_array[0];
            }
        }
        else{
            $dir = 'DESC';
            $orderby_col = '_id';
        }
        $condition =  array();
        $likecondition = array();
        $substringcondition = array();
        if($filter != ''){
            
            $filter = str_replace("(", "", $filter);
            $filter = str_replace(")", "", $filter);
            $filter = str_replace("'", "", $filter);
            $filter_array = explode(' and ',$filter);
            // print_r($filter_array);exit();
            foreach($filter_array as $fil){
                
                if (strpos($fil, 'startswith') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'substringof') !== false) {
                    $needtoexplode = false;
                }
                else if(strpos($fil, 'endswith') !== false) {
                    $needtoexplode = false;
                }
                else{
                    $needtoexplode = true;
                }
                
                if($needtoexplode){

                    $fil_array = explode(' ', $fil);
                    // $rangeQuery = array('_id' => array( '$gt' => 10, '$lt' => 12 ));
                    if(count($fil_array) >= 3){
                        if(count($fil_array) == 3){
                                if($fil_array[0] == 'date'){
                                    $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                    $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                    $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                                }

                                if($fil_array[1] === 'ge'){
                                    $fil_array[1] = 'gte';
                                }
                                if($fil_array[1] === 'le'){
                                    $fil_array[1] = 'lte';
                                }
                                $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                        else{

                            $fil_array_value = explode(' '.$fil_array[1].' ', $fil);
                            // echo $fil;

                            $fil_array[2] = $fil_array_value[1];

                            // echo "string".$fil_array[2];
                            if($fil_array[0] == 'date'){
                                $fil_array[2] = str_replace("datetime", "", $fil_array[2]);
                                $fil_array[2] = str_replace("'", "", $fil_array[2]);
                                $fil_array[2] = date("Y-m-d", strtotime($fil_array[2]));
                            }

                            if($fil_array[1] === 'ge'){
                                $fil_array[1] = 'gte';
                            }
                            if($fil_array[1] === 'le'){
                                $fil_array[1] = 'lte';
                            }
                            $condition[$fil_array[0]] = array( '$'.$fil_array[1] => $fil_array[2]);
                        }
                                
                    }
                }
                else{

                    if (strpos($fil, 'startswith') !== false) {

                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  '^'.$value_field;
                        // echo "field : ".$field."<br>";
                        // echo "value : ".$value_field;exit();

                    }
                    else if(strpos($fil, 'endswith') !== false) {
                        $fil_without = str_replace('startswith', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[0];
                        $value_field = $fil_array[1];
                        $likecondition[$field] =  $value_field.'^';
                    }
                    else if(strpos($fil, 'substringof') !== false) {

                        $fil_without = str_replace('substringof', '', $fil);
                        $fil_array = explode(',', $fil_without);
                        $field = $fil_array[1];
                        $value_field = $fil_array[0];
                        $substringcondition[$field] =  $value_field;
                        // print_r($substringcondition);exit();

                    }
                    
                }
                
                
            }
        }      
        $condition = array();
        $join = array(array(
                'from'=>'country_master',
                'localField'=>'country_id',
                'foreignField'=>'_id',
                'as'=>'countries'
        ),array(
                'from'=>'economical_indicators_master',
                'localField'=>'ecin_id',
                'foreignField'=>'_id',
                'as'=>'ecin'
        ));
        // print_r($limit);
        // print_r($offset);exit();
        // print_r($condition);exit();
        $res = $this->common->select_data_by_condition('economical_indicator_details', $condition, '', $orderby_col, $dir, $limit, $offset, $join,$likecondition,$substringcondition);
        $res_count1 = $this->common->get_count_of_table('economical_indicator_details',$condition,$likecondition,$substringcondition);
        // echo "<pre>"; print_r($res);
        foreach ($res as $key => $value) {
            if(count($value['countries']) > 0){
                $res[$key]['country_name'] = $value['countries'][0]->name;
            }
            else{
                $res[$key]['country_name'] = "";   
            }
            // echo "<pre>";
            // print_r($value['ecin']);
            if(count($value['ecin']) > 0){
                $res[$key]['ecin'] = $value['ecin'][0]->name;
            }
            else{
                $res[$key]['ecin'] = "";   
            }
        }
        // exit();
        if(count($res) > 0){
            // echo json_encode($data);
            $data['d']['results'] = $res;
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }
        else{
            // echo json_encode(array());   
            $data['d']['results'] = array();
            $data['d']['__count'] = $res_count1;
            
            echo $var.'('.json_encode($data).')';
        }

    }
    public function updateecin_data()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        $updated_ids = array();
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $add_id = $model['_id'];
            $update_array = (array)$model['_id'];
            $update_id = $update_array['$oid'];
            array_push($updated_ids, ['$elemMatch'=>$add_id]);
            $country_name_new = $model['country_name'];
            $ecin_new = $model['ecin'];
            $country_id_array = $this->common->select_database_by_like_where('country_master', 'name', $country_name_new );
            $ecin_id_array = $this->common->select_database_by_like_where('economical_indicators_master', 'name', $ecin_new );
            //print_r($country_id_array);exit();
            $country_id_new = 0;
            if(count($country_id_array) > 0){
                $country_id_new = $country_id_array[0]['_id'];
            }
            $ecin_id_new = 0;
            if(count($ecin_id_array) > 0){
                $ecin_id_new = $ecin_id_array[0]['_id'];   
            }
            if($ecin_id_new != 0 && $country_id_new != 0){
                $update_data_tobe = array(
                    'ecin_id'=>$ecin_id_new,
                    'country_id'=>$country_id_new,
                    'ecIn_code'=>$model['ecIn_code'],
                    'description'=>$model['description'],
                    'category'=>$model['category'],
                    'data_source1'=>$model['data_source1'],
                    'data_source_code1'=>$model['data_source_code1'],
                    'unit'=>$model['unit'],
                    'multiplier'=>$model['multiplier'],
                    'frequency'=>$model['frequency'],
                    'seasonally_Adjusted'=>$model['seasonally_Adjusted'],
                    'official_source_organization'=>$model['official_source_organization']
                );    
            }
            else{
                if($ecin_id_new == 0){
                    $update_data_tobe = array(
                        'country_id'=>$country_id_new,
                        'ecIn_code'=>$model['ecIn_code'],
                        'description'=>$model['description'],
                        'category'=>$model['category'],
                        'data_source1'=>$model['data_source1'],
                        'data_source_code1'=>$model['data_source_code1']
                    );
                }
                elseif($country_id_new == 0){
                    $update_data_tobe = array(
                        'ecin_id'=>$ecin_id_new,
                        'ecIn_code'=>$model['ecIn_code'],
                        'description'=>$model['description'],
                        'category'=>$model['category'],
                        'data_source1'=>$model['data_source1'],
                        'data_source_code1'=>$model['data_source_code1']
                    );  
                }
                else{
                    $update_data_tobe = array(
                        'ecIn_code'=>$model['ecIn_code'],
                        'description'=>$model['description'],
                        'category'=>$model['category'],
                        'data_source1'=>$model['data_source1'],
                        'data_source_code1'=>$model['data_source_code1']
                    );
                }
            }
             
            // print_r($update_data_tobe);
            $this->common->update_data($update_data_tobe, 'economical_indicator_details', '_id', $update_id);
        }
        // print_r($updated_ids);exit();
        // $condition['_id'] = array( '$in' => $updated_ids);
        $condition['_id'] = array( '$in' => array());
        $res = $this->common->select_data_by_condition('economical_indicator_details', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('economical_indicator_details');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    public function createnew()
    {
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        $models = $data->models;
        foreach($models as $model){
            $model = json_decode(json_encode($model), true);
            $country_id_new = $model['country_name']['_id'];
            $ecin_new = $model['ecin'];
            $ecin_id_array = $this->common->select_database_by_like_where('economical_indicators_master', 'name', $ecin_new );
            if(count($ecin_id_array) > 0){
                $ecin_id_new = $ecin_id_array[0]['_id'];   
            }
            else{

                $last_ticker_id = $this->common->last_record_id('economical_indicators_master');
                $newticker_id = 0;
                if($last_ticker_id){
                    $newticker_id = $last_ticker_id + 1;
                    $dataticker = array('_id'=>$newticker_id,'name'=>$ecin_new);
                    $this->common->insert_data($dataticker, 'economical_indicators_master');
                }
                $ecin_id_new = $newticker_id;
            }

            $insert_data_tobe = array(
                'ecin_id'=>$ecin_id_new,
                'country_id'=>$country_id_new,
                'ecIn_code'=>$model['ecIn_code'],
                'description'=>$model['description'],
                'category'=>$model['category'],
                'data_source1'=>$model['data_source1'],
                'data_source_code1'=>$model['data_source_code1'],
                'unit'=>$model['unit'],
                'multiplier'=>$model['multiplier'],
                'frequency'=>$model['frequency'],
                'seasonally_Adjusted'=>$model['seasonally_Adjusted'],
                'official_source_organization'=>$model['official_source_organization']
            ); 
            // print_r($insert_data_tobe);
            $this->common->insert_data($insert_data_tobe, 'economical_indicator_details');
        }
        $condition['_id'] = array( '$in' => array());
        $res = $this->common->select_data_by_condition('economical_indicator_details', $condition, '*', '', '', (int)10, (int)0 , array(), '');
        $res_count1 = $this->common->get_count_of_table('economical_indicator_details');  
        $data1['d']['results'] = $res;
        $data1['d']['__count'] = $res_count1;
        echo json_encode($data1);
        exit();
    }
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'getecin_data' || $method == 'getallcountryperenic' || $method == 'getecinall' || $method == 'getallcountry'){
            $method = 'index';
        }
        if($method == 'insertdata' || $method == 'createnew')
        {
            $method = 'add';
        }
        if($method == 'edit_data' || $method == 'update' || $method == 'updateticker_data' || $method == 'update_data' || $method == 'updateecin_data')
        {
            $method = 'edit';
        }
        
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }

}

/* End of file economical_indicator.php */
            /* Location: ./application/controllers/economical_indicator.php */