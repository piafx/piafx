<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        if ($this->session->userdata('admin_data')) {

            redirect('admin', 'refresh');
        }


        $this->load->model('common');
        
        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Login';
        $this->data['site_name'] = $this->common->get_setting_value(1);
        $this->data['site_url'] = $this->common->get_setting_value(4);

        //Load leftsidemenu and save in variable
        $this->data['topmenu'] = '';
        
        //Load header and save in variable
        $this->data['header'] = $this->load->view('login_header', $this->data, true);
        $this->data['footer'] = $this->load->view('login_footer', $this->data, true);
        
        // LOAD LIBRARIES
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        // LOAD HELPERS
        $this->load->helper('form');
        
    }

    public function index() {
       
        

        // SET VALIDATION RULES
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'required|max_length[20]|min_length[6]');
        // has the form been submitted and with valid form info (not empty values)

        if ($this->form_validation->run()) {
            
            $user_name = $this->input->post('user_email');
            $user_pass = $this->input->post('user_password');
            
            //query the database
            $admin_data = $this->common->select_database_by_muliple_where('users', array('user_email' => $user_name, 'user_password' => base64_encode($user_pass)), $data = '*', $order_by = '', $dir = '');
 
            if (!empty($admin_data)) {
                
                if($admin_data[0]['user_status'] == 'Enable')
                {
                    $admin_id = $admin_data[0]['_id'];
                    
                    $name = $admin_data[0]['user_email'];
                    
                    $pass = $admin_data[0]['user_password'];

                    if ($user_name == $name && base64_encode($user_pass) == $pass) {
                        $sess_array = array(
                            
                            'name'=> $admin_data[0]['user_firstname'].' '.$admin_data[0]['user_lastname'],
                            'email' => $name,
                            'ad_id' => $admin_id,
                            'role'  => $admin_data[0]['user_role']
                            
                        );
                        $this->session->set_userdata('admin_data', $sess_array);
                        // user has been logged in
                        
                        redirect('admin', 'refresh');
                    } else {
                        
                        $this->session->set_flashdata('message', 'Invalid email or password.');
                        redirect('adminlogin', 'refresh');
                    }
                }   
                else
                {
                    $this->session->set_flashdata('message', 'Your account not valid.');
                    redirect('adminlogin', 'refresh');
                }
            } else {
                $this->session->set_flashdata('message', 'Invalid email or password.');
                redirect('adminlogin', 'refresh');
            }
        } else {

            $this->load->view('adminlogin', $this->data);
        }


        
    }
    
    public function forgot(){
        
        // SET VALIDATION RULES
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        // has the form been submitted and with valid form info (not empty values)

        if ($this->form_validation->run()) {
            
            $user_name = $this->input->post('user_email');
            
            //query the database
            $admin_data = $this->common->select_database_by_muliple_where('users', array('user_email' => $user_name), $data = '*', $order_by = '', $dir = '');
            
            if (!empty($admin_data)) {
                
                
                if($admin_data[0]['user_status'] == 'Enable')
                {

                    $email_temp = $this->common->select_database_by_muliple_where('email_template', array('_id' => "2"), $data = '*', $order_by = '', $dir = '');
                    // print_r($email_temp);exit();
                    if(!empty($email_temp))
                    {
                        $rs = random_string('alnum', 12);
                        $data = array(
                            'user_resetpassword' => $rs
                        );
                        if($this->common->update_data($data, 'users', '_id', $admin_data[0]['_id']))
                        {
                            $message = $email_temp[0]['mailformat'];
                            $sub = $email_temp[0]['subject'];
                            $link = "\r\n" . base_url() . 'adminlogin/new_password/' . $rs;
                            $message = str_replace("%firstname%", $admin_data[0]['user_firstname'], $message);
                            $message = str_replace("%lastname%", $admin_data[0]['user_lastname'], $message);
                            $message = str_replace("%email%", $admin_data[0]['user_email'], $message);
                            $message = str_replace("%link%", $link, $message);
                            if($this->send_verification_mail($user_name, $message, $sub))
                            {
                                $this->session->set_flashdata('success', 'Please check your email for further instructions.');
                                redirect('adminlogin/forgot', 'refresh');
                            }
                            else
                            {
                                $this->session->set_flashdata('message', 'Please try again.');
                                redirect('adminlogin/forgot', 'refresh');
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('message', 'Please try again.');
                            redirect('adminlogin/forgot', 'refresh');
                        }
                    }
                }   
                else
                {
                    $this->session->set_flashdata('message', 'Your account not valid.');
                    redirect('adminlogin/forgot', 'refresh');
                }
            } else {
                $this->session->set_flashdata('message', 'Invalid email.');
                redirect('adminlogin/forgot', 'refresh');
            }
        } else {

            $this->load->view('forgot', $this->data);
        }
    }
    
    function send_verification_mail($user_email, $message,$sub) {
        
            $to = $user_email;
            $from = "aspltest3@gmai.com";
            $subject = $sub;
            $from_name = "Insite";
            $content = $message;

            $this->config->load('email', TRUE);
            $this->cnfemail = $this->config->item('email');

            //Loading E-mail Class
            $this->load->library('email');
            $this->email->initialize($this->cnfemail);

            $this->email->from($from, $from_name);
            $this->email->to($to); 

            $this->email->subject($subject);

            $this->email->message($content);
            
            if ($this->email->send()) {
                return TRUE;
            } else {
                return FALSE;
            }
            
    }
    public function new_password()
    {
        $token = $this->uri->segment(3);
        $record = $this->common->select_database_by_muliple_where('users', array('user_resetpassword' => $token), $data = '*', $order_by = '', $dir = '');
        if (!empty($record) && $token != '') {
            
                // SET VALIDATION RULES
                $this->form_validation->set_rules('user_password', 'Password', 'required|max_length[20]|min_length[6]');
                $this->form_validation->set_rules('user_confirm_password', 'Confirm Password', 'required|max_length[20]|min_length[6]|matches[user_password]');
                // has the form been submitted and with valid form info (not empty values)

                if ($this->form_validation->run()) {
                    
                    $user_pass = $this->input->post('user_password');
                    $user_confirm_pass = $this->input->post('user_confirm_password');
                    if($user_pass == $user_confirm_pass)
                    {
                        $data = array(
                            'user_password' => base64_encode($user_pass),
                            'user_resetpassword' => ''
                        );
                        if($this->common->update_data($data, 'users', 'user_resetpassword', $token))
                        {
                            $this->session->set_flashdata('success', 'Your password is successfully changed.');
                            redirect('adminlogin', 'refresh');
                        }
                    }
                    else
                    {
                        $this->load->view('newpassword', $this->data);
                    }
                } else {

                    $this->load->view('newpassword', $this->data);
                }
        }
        else
        {
            $this->session->set_flashdata('message', 'Please try again.');
            redirect('adminlogin', 'refresh');
        }
        
    }
  


 
}
