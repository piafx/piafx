<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Emailtemplate extends CI_Controller {

    public $paging;
    public $data;

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('admin_data'))
        {
            //If no session, redirect to login page
            redirect('adminlogin', 'refresh');
        }
        // include('include.php');
        $this->load->model('emailformats');

        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Email Template';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);
        
        $this->data['permission_list'] = $this->common->permission();
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->model('common');
        $this->check_permission();
    }

    //load category listing view
    public function index()
    {
        $this->data['emailformat'] = $this->emailformats->getAllMails();
        $this->data['total'] = count($this->data['emailformat']);
        $this->load->view('emailformat/index', $this->data);
    }
    
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'update')
        {
            $method = 'edit';
        }
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }

    //load edit email format view
    public function edit($emailformatid = '')
    {
        // $log = Logger::getLogger(__CLASS__);
        if ($emailformatid != '' && $emailformatid != 0)
        {

            $this->data['emailformat'] = $this->emailformats->get_emailformat_byid($emailformatid);
//            echo '<pre>'; print_r($this->data['emailformat']); die();
            if (count($this->data['emailformat']) > 0)
            {
                $this->load->helper('ckeditor');
                $this->data['ckeditor'] = array(
                    //ID of the textarea that will be replaced
                    'id' => 'mailformat',
                    'path' => '../ckeditor',
                    //Optionnal values
                    'config' => array(
                        'toolbar' => "Full", //Using the Full toolbar
                        'width' => "auto", //a custom width
                        'height' => "300px", //a custom height
                    ),
                );

                //Loading View File
                $this->load->view('emailformat/edit', $this->data);
            }
            else
            {
                $log->error("Try to use invalid id.");
                $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
                redirect('emailtemplate', 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Record you are trying to find is not found.');
            redirect('emailtemplate', 'refresh');
        }
    }

    //Updating the Record
    public function update()
    {
        // $log = Logger::getLogger(__CLASS__);
        // //If Old Record Update
        if ($this->input->post('emailid'))
        {
            //Getting emailformatid
            $emailformatid = base64_decode($this->input->post('emailid'));

            //Getting value
            $title = ($this->input->post('title'));
            $subject = ($this->input->post('subject'));
            $mailformat = $this->input->post('mailformat');
//            echo $emailformatid.'/'.$title.'/'.$subject.'/'.$mailformat; die();
            if ($mailformat != '')
            {
                //Updating Record
                if ($this->emailformats->update($emailformatid, $title, $subject, $mailformat))
                {
                    $this->session->set_flashdata('success', 'Record has been updated successfully.');
                    redirect('emailtemplate', 'refresh');
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('message', 'Please try again.');
                    redirect('emailtemplate', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('message', 'Email Template must not be empty.');
                redirect('emailtemplate/edit/' . $emailformatid, 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Record not found with specified id. Try later!');
            redirect('emailtemplate', 'refresh');
        }
    }

}

?>