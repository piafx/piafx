<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class resetpassword extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        if ($this->session->userdata('aspl_hrmadmin_data')) {

            redirect('admin', 'refresh');
        }
        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Reset Password';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(4);

        //Load leftsidemenu and save in variable
        $this->data['topmenu'] = '';

        //Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
    }

    public function index() {
        // LOAD LIBRARIES
        $this->load->library(array('encrypt', 'form_validation', 'session'));
        // LOAD HELPERS
        $this->load->helper('form');

        // SET VALIDATION RULES
        $this->form_validation->set_rules('user_email', 'Email', 'required');

        $this->form_validation->set_error_delimiters('<em>', '</em>');
        // has the form been submitted and with valid form info (not empty values)

        if ($this->form_validation->run()) {

            $user_email = $this->input->post('user_email');



            //query the database
             $admin_data = $this->common->select_database_by_muliple_where('masteradmin', array('adminemail' => $user_email), $data = '*', $order_by = '', $dir = '');

            if (!empty($admin_data)) {


                $admin_id = $admin_data[0]['adminid'];
                 $role_id = $admin_data[0]['role_id'];
                $name = $admin_data[0]['adminemail'];
                $pass = $admin_data[0]['adminpassword'];
                 $fname = $admin_data[0]['adminname'];
                $lname = '';
                if ($user_email == $name) {


                    $rand_link = random_string() . $ctime = date('dmyhis');
                    

                    $this->common->delete_data('resetsetting','adminemail',$user_email);
                    $request_data= array(
                            'resettoken'=>$rand_link,
                            'adminemail' => $name
                        );
                    $this->common->add_reset_link($request_data);
                    $this->add_log($admin_id, 'Reset Password','1',$role_id);
                    $this->send_verification_mail($user_email,$rand_link,$fname,$lname);
                    $this->session->set_flashdata('success', 'You will get mail for resetting password.');
                    // user password send to their register email
                    redirect('resetpassword', 'refresh');
                } else {
                    $this->session->set_flashdata('message', 'You are not existing in this system.');
                    redirect('resetpassword', 'refresh');
                }
            } else {
                $this->session->set_flashdata('message', 'You are not existing in this system.');
                redirect('resetpassword', 'refresh');
            }
        } else {

            $this->load->view('resetpassword', $this->data);
        }
    }

     public function send_verification_mail($user_email,$rand_link,$fname,$lname)
    {
        $config = array();
        $res = $this->common->select_database_id('mail', 'mail_id', 1, '*');
        if(!empty($res))
        {
                    $host = $res[0]['mail_smpt_host'];
                    $port = $res[0]['mail_smpt_port'];
                    $user = $res[0]['mail_smpt_user'];
                    $password = base64_decode($res[0]['mail_smpt_password']);

                    $config['protocol'] = "smtp";
                    $config['smtp_host'] = $host;
                    $config['smtp_port'] = $port;
                    $config['smtp_user'] = $user;
                    $config['smtp_pass'] = $password;
                    $config['charset'] = "utf-8";
                    $config['mailtype'] = "html";
                    $config['newline'] = "\r\n";


                    $from_email = "aspltest3@gmail.com";
                    $to_email = "aspltest3@gmail.com";
                    $this->load->library('email',$config);

                    $this->email->initialize($config);

                    $this->email->from($from_email, 'Support');
                    $this->email->to($user_email);
                    $this->email->subject('Reset Your Password');

                    $message = /* -----------email body starts----------- */
                     "Dear ".$fname." ".$lname.",<br/><br/>There was recently a request to change the password for your account.<br/><br/>If you requested this change, set a new password here:<br/><br/>".
                            base_url() . 'newpassword/index/' . $rand_link.

                    "<br/><br/>".
                    "If you did not make this request, you can ignore this email and your password will remain the same.<br/><br/>"
                    ."Sincerely,<br/>
            Ashapurasoftech Pvt. Ltd.";
                    /* -----------email body ends----------- */

                     $this->email->message($message);


                    //Send mail
                    if ($this->email->send()) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
        } else {
                        return FALSE;
                    }             
    } 
    public function add_log($to_id,$method_name,$result,$role_id)
    {
        $ci =& get_instance();
        $controller_name=$ci->router->fetch_class();
        $by_id=$to_id;
        $role=$role_id;
        $data1 = array(
                'by_id'=>$by_id,
                'role_id'=>$role,
                'to_id'=>$to_id,
                'controller_name'=> $controller_name,
                'method_name' => $method_name,
                'created_date' => date('Y-m-d H:i:s'),
                'result'=> $result,  
                   );
        $this->common->insert_data($data1, 'admin_log');
        
    }
}
