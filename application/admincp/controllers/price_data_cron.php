    <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Price_data_cron extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
       ini_set('memory_limit', '640M');
       ini_set('post_max_size', '640M');
       ini_set('upload_max_filesize', '640M');
       ini_set('max_execution_time', 60000);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Price Data';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function get_date_difference($value='')
    {
        // $fp = fopen($_SERVER['DOCUMENT_ROOT']."/price_dates/D6.txt","wb");
        // $print_array = array("asd","asd","test");
        // $content = implode("\n", $print_array);       
        // fwrite($fp,$content);
        // fclose($fp);
        // exit();
        $ticker_list = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>array('$ne'=>'-')), '*', '', '', '', '',array(),'');
        // print_r($ticker_list);exit();
        foreach ($ticker_list as $value) {
            $code_tab = $value['eod_data'];

            $begin = new DateTime('1999-01-01');
            $end = new DateTime('2019-09-17');

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            $fp = fopen($_SERVER['DOCUMENT_ROOT']."/price_dates/".$code_tab.".txt","wb");

            $print_array = array();
            $i = 0;
            foreach ($period as $dt) {

                $isdateavailable = $this->common->select_data_by_condition($code_tab, array('date'=>$dt->format("Y-m-d")), '*', '', '', '', '',array(),'');
                if(count($isdateavailable) > 0){

                }
                else{
                    $print_array[$i] = $dt->format("Y-m-d").",".$code_tab;
                }
                $i++;
            }
            $content = implode("\n", $print_array);   
            fwrite($fp,$content);
            fclose($fp);
        }
    }

    function getDirContents($dir_path, &$results = array()){
        $rdi = new \RecursiveDirectoryIterator($dir_path);

        $rii = new \RecursiveIteratorIterator($rdi);

        $tree = [];

        foreach ($rii as $splFileInfo) {
            $file_name = $splFileInfo->getFilename();

            // Skip hidden files and directories.
            if ($file_name[0] === '.') {
                continue;
            }

            $path = $splFileInfo->isDir() ? array($file_name => array()) : array($file_name);

            for ($depth = $rii->getDepth() - 1; $depth >= 0; $depth--) {
                $path = array($rii->getSubIterator($depth)->current()->getFilename() => $path);
            }

            $tree = array_merge_recursive($tree, $path);
        }

        return $tree;
    }
    public function economical_ind_bsi($value='')
    {
        $this->load->library('PHPExcel');

        $object = PHPExcel_IOFactory::load("/var/www/html/bis.csv");
        foreach($object->getWorksheetIterator() as $worksheet)
        {
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
            for($row=2; $row<= $highestRow; $row++)
            {  
                $insert_data = array();

                $duration_type = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                if($duration_type == 'M:Monthly')  {
                    
                }
                else{
                    continue;
                }


                $country = $worksheet->getCellByColumnAndRow(1, $row)->getValue();

                if($country == 'CA:Canada' || $country == 'GB:United Kingdom' || $country == 'IN:India' || $country == 'CH:Switzerland' || $country == 'CN:China' || $country == 'MX:Mexico' || $country == 'BR:Brazil' || $country == 'RU:Russia' || $country == 'NZ:New Zealand' || $country == 'AU:Australia' || $country == 'JP:Japan' || $country == 'US:United States' || $country == 'XM:Euro area')
                {

                }
                else{
                    continue;
                }
                $carray = explode(":", $country);
                echo $carray[0];
                echo "<br>";
                echo $carray[1];
                echo "<br>";
                //is_numeric (var_name) indicator id , country id, date, value, tag
                $iscounryfound = $this->common->select_data_by_condition('country_master', array('name'=>$carray[1]), '*', '', '', '', '',array(),'');
                if(count($iscounryfound) > 0){
                    $new_record_id = $iscounryfound[0]['_id'];
                }
                else{

                    $last_record_id = $this->common->last_record_id('country_master');
                    $new_record_id = $last_record_id + 1;
                    $insert_data = array("_id"=>$new_record_id,"name"=>$carray[1],"countrycode"=>$carray[0]);
                    $this->common->insert_data($insert_data, 'country_master');
                }
                // if($row == 3){
                //     echo $new_record_id;
                //     exit();
                // }
                
                for($col=0; $col<$highestColumnIndex; $col++)
                {
                    
                    if($col >= 639){

                        $month = $col-3;
                        $effectiveDate = date('Y-m-d', strtotime("+".$month." months", strtotime('1946-01-01')));
                        $val_main = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if($val_main != '' && is_numeric ($val_main)){
                            // echo "1";
                            // echo " | ";
                            // echo $new_record_id;
                            // echo " | ";
                            // echo $val_main;
                            // echo " | ";
                            // echo $effectiveDate;
                            // echo "<br>";

                            $isdatafound = $this->common->select_data_by_condition('economical_indicators_value_master', array('date'=>$effectiveDate,'country_id'=>$new_record_id), '*', '', '', '', '',array(),'');
                            if(count($isdatafound) == 0){
                                $enic_array = array("enic_id"=>1,"country_id"=>$new_record_id,"value"=>$val_main,"date"=>$effectiveDate);
                                $this->common->insert_data($enic_array, 'economical_indicators_value_master');
                            }
                        }

                    }
                    
                }
                
            }
        }
    }
    public function forex_dir(){
        
        $direcory_arrry = $this->getDirContents("/var/www/html/forex_data");
        foreach ($direcory_arrry as $market => $marketdir) {
            foreach ($marketdir as $yearkey => $year) {
                foreach ($year as $files) {
                    // echo $file_echo1 = "/var/www/html/price_data/".$market."/".$yearkey."/".$files." <br>";
                    $file = "/var/www/html/forex_data/".$market."/".$yearkey."/".$files;
                    $this->grab_forex_data($file,$market);
                    $this->drop_file_from_dir($file);
                    // exit();
                }
            }
        }
        exit();
    }
    public function test_dir(){
        
        $direcory_arrry = $this->getDirContents("/var/www/html/price_data");
        foreach ($direcory_arrry as $market => $marketdir) {
            foreach ($marketdir as $yearkey => $year) {
                foreach ($year as $files) {
                    // echo $file_echo1 = "/var/www/html/price_data/".$market."/".$yearkey."/".$files." <br>";
                    log_message('error', 'cron start for the file '.$market.'/'.$yearkey.'/'.$files);
                    $file = "/var/www/html/price_data/".$market."/".$yearkey."/".$files;
                    $this->grab_eod_data($file,$market);
                    $this->drop_file_from_dir($file);
                    log_message('error', 'cron completed for the file '.$market.'/'.$yearkey.'/'.$files);
                    // exit();
                }
            }
        }
        exit();
    }
    public function drop_file_from_dir($file){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        unlink($file);
        echo "string";
    }
    public function grab_eod_data_directory(){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $files = array();
        $this->load->library('PHPExcel');
        foreach (glob("/var/www/html/CME_2016/*.csv") as $file) {
            $this->grab_eod_data($file);
            $this->drop_file_from_dir($file);
        }
    }
    public function grab_forex_data($file,$cftc_market){

            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $object = PHPExcel_IOFactory::load($file);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=2; $row<= $highestRow; $row++)
                {  
                    $insert_data = array();
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {
                        // echo $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if($col == 0){
                            $ticker_code = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $insert_data['symbol'] = $ticker_code;
                            $symbol = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 1){
                            
                            $date = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $new_date = date('Y-m-d', strtotime($date));
                            $insert_data['date'] = $new_date;
                        }
                        else if($col == 2){
                            $insert_data['open'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 3){
                            $insert_data['high'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 4){
                            $insert_data['low'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 5){
                            $insert_data['close'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 6){
                            $insert_data['volume'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 7){
                            // $openinterest = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            // if($openinterest){
                            //     $insert_data['openinterest'] = $openinterest;
                            // }
                            // else{
                                $insert_data['openinterest'] = '-';   
                            // }
                        }
                        $insert_data['source'] = "FOREX";
                    }

                    $new_date = date('Y-m-d', strtotime($date));
                    $checksymbol = str_replace('.C', '', $symbol);
                    $isfound = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$checksymbol,'type'=>3,'cftc_market'=>$cftc_market), '*', '', '', '', '',array(),'');
                    $isfound1 = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$symbol,'type'=>3,'cftc_market'=>$cftc_market), '*', '', '', '', '',array(),'');
                    if(count($isfound) > 0 || count($isfound1) > 0){

                        $iscollectionavailable = $this->common->collection_exist($symbol);
                        if($iscollectionavailable){

                            $isrecordavailable = $this->common->select_data_by_condition($symbol, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                            
                            // print_r($insert_data);
                            if(count($isrecordavailable) > 0)
                            {
                                $insert_data['_id'] = $isrecordavailable[0]['_id'];
                                $this->common->update_data($insert_data,$symbol,'_id', $isrecordavailable[0]['_id']);
                            }
                            else
                            {
                                $this->common->insert_data($insert_data, $symbol);
                            }
                        }
                        else{
                            // print_r($insert_data);
                            if($this->common->createmongocollection($symbol)){
                                $this->common->insert_data($insert_data, $symbol);
                            }
                        }

                        
                    }
                }
            }
    }
    public function grab_eod_data($file,$cftc_market){

            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $object = PHPExcel_IOFactory::load($file);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=2; $row<= $highestRow; $row++)
                {  
                    $insert_data = array();
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {
                        // echo $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if($col == 0){
                            $ticker_code = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $insert_data['symbol'] = $ticker_code;
                            $symbol = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 1){
                            
                            $date = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $new_date = date('Y-m-d', strtotime($date));
                            $insert_data['date'] = $new_date;
                        }
                        else if($col == 2){
                            $insert_data['open'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 3){
                            $insert_data['high'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 4){
                            $insert_data['low'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 5){
                            $insert_data['close'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 6){
                            $insert_data['volume'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 7){
                            $insert_data['openinterest'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        $insert_data['source'] = "EODdata";
                    }

                    $new_date = date('Y-m-d', strtotime($date));
                    $checksymbol = str_replace('.C', '', $symbol);
                    $isfound = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$checksymbol,'type'=>1,'eod_market'=>$cftc_market), '*', '', '', '', '',array(),'');
                    $isfound1 = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$symbol,'type'=>1,'eod_market'=>$cftc_market), '*', '', '', '', '',array(),'');

                    if(count($isfound) > 0 || count($isfound1) > 0){

                        $iscollectionavailable = $this->common->collection_exist($symbol);
                        if($iscollectionavailable){

                            $isrecordavailable = $this->common->select_data_by_condition($symbol, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                            
                            // print_r($insert_data);
                            if(count($isrecordavailable) > 0)
                            {
                                $insert_data['_id'] = $isrecordavailable[0]['_id'];
                                $this->common->update_data($insert_data,$symbol,'_id', $isrecordavailable[0]['_id']);
                            }
                            else
                            {
                                $this->common->insert_data($insert_data, $symbol);
                            }
                        }
                        else{
                            // print_r($insert_data);
                            if($this->common->createmongocollection($symbol)){
                                $this->common->insert_data($insert_data, $symbol);
                            }
                        }

                        
                    }
                }
            }
    }
    public function grab_alphaventage_data_current(){

        $skip = $_GET['skip'];
        $limit = $_GET['limit'];

        $allbarchart_tickers = $this->common->select_data_by_condition('ticker_master_new', array('type'=>3), '*', '', '', $limit, $skip ,array(),'');

        // print_r($allbarchart_tickers);exit();
        
        foreach ($allbarchart_tickers as $key => $value) {

            $symbols = $value['alpha_vantag'];
            // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            $url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol='.$symbols.'&interval=60min&outputsize=full&apikey=MXG3YR2GK3T04D3B';

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $alltickers = json_decode($result);
            $allvalues = json_decode(json_encode($alltickers), True);
            if(!empty($allvalues)){
                if (array_key_exists('Time Series (60min)', $allvalues)) {
                    $allvalues_main = $allvalues['Time Series (60min)'];
                    $key = array_keys($allvalues_main)[0];
                    $new_date = date('Y-m-d', strtotime($key));  
                    $addarray = $allvalues_main[$key];   

                    $symbol = $symbols;
                    $date_insert = $new_date;
                    
                    $isfound = $this->common->select_data_by_condition('ticker_master_new', array('alpha_vantag'=>$symbol,'type'=>3), '*', '', '', '', '',array(),'');
                    
                    if(count($isfound) > 0){

                        $collection = $isfound[0]['eod_data'];
                        $insert_data['symbol'] = $symbol;
                        $insert_data['date'] = $new_date;
                        $insert_data['open'] = $addarray['1. open'];
                        $insert_data['high'] = $addarray['2. high'];
                        $insert_data['low'] = $addarray['3. low'];
                        $insert_data['close'] = $addarray['4. close'];
                        $insert_data['volume'] = $addarray['5. volume'];
                        $insert_data['openinterest'] = '-';
                        $insert_data['source'] = 'alphavantag';

                        // print_r($insert_data);exit();
                        $iscollectionavailable = $this->common->collection_exist($collection);
                        if($iscollectionavailable){

                            $isrecordavailable = $this->common->select_data_by_condition($collection, array('date'=>$date_insert), '*', '', '', '', '',array(),'');

                            

                            if(count($isrecordavailable) > 0)
                            {
                                $insert_data['_id'] = $isrecordavailable[0]['_id'];
                                $this->common->update_data($insert_data,$collection,'_id', $isrecordavailable[0]['_id']);
                            }
                            else
                            {
                                $this->common->insert_data($insert_data, $collection);
                            }
                        }
                        else{
                            if($this->common->createmongocollection($collection)){
                                $this->common->insert_data($insert_data, $collection);
                            }
                        }

                        
                    }
                }
            }
        }

        
    }
    public function grab_barchart_data_current(){

        $allbarchart_tickers = $this->common->select_data_by_condition('ticker_master_new', array('type'=>2), '*', '', '', '', '',array(),'');

        // print_r($allbarchart_tickers);exit();
        $symbols = '';
        foreach ($allbarchart_tickers as $key => $value) {
            if($symbols != ''){
                $symbols .= '%2C'.$value['barchart'].'%2C'.$value['barchart'].'.C';
            }
            else{
                $symbols =  $value['barchart'].'%2C'.$value['barchart'].'.C';
            }
        }
        if($symbols != ''){

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://marketdata.websol.barchart.com/getQuote.json?apikey=5d7ac404b64dd72abdfdd786e297cd8d&symbols='.$symbols.'&fields=fiftyTwoWkHigh%2CfiftyTwoWkHighDate%2CfiftyTwoWkLow%2CopenInterest%2CfiftyTwoWkLowDatecurl');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            
            $alltickers = json_decode($result)->results;
            // echo "<pre>";
            $array = array();
            if($alltickers){
                $array = json_decode(json_encode($alltickers), True);
            }
            
            // print_r($array);exit();

            foreach ($array as $quote) {
                $insert_data = array();
                // print_r($this->xml2array($quote)['@attributes']['Symbol']);
                // echo "<br>";
                // $quote = $this->xml2array($quote);
                $symbol = $quote['symbol'];
                $date_insert = $quote['tradeTimestamp'];
                $new_date = date('Y-m-d', strtotime($date_insert));
                // $isfound = $this->common->select_database_id('ticker_master_new', 'eod_data', $symbol, '*');
                $isfound = $this->common->select_data_by_condition('ticker_master_new', array('barchart'=>$symbol,'type'=>2), '*', '', '', '', '',array(),'');
                // print_r($isfound);exit();

                if(count($isfound) > 0){

                    $collection = $isfound[0]['eod_data'];
                    $insert_data['symbol'] = $symbol;
                    $insert_data['date'] = $new_date;
                    $insert_data['open'] = $quote['open'];
                    $insert_data['high'] = $quote['high'];
                    $insert_data['low'] = $quote['low'];
                    $insert_data['close'] = $quote['close'];
                    $insert_data['volume'] = $quote['volume'];
                    $insert_data['openinterest'] = $quote['openInterest'];
                    $insert_data['source'] = 'barchart';

                    // print_r($insert_data);exit();
                    $iscollectionavailable = $this->common->collection_exist($collection);
                    if($iscollectionavailable){

                        $isrecordavailable = $this->common->select_data_by_condition($collection, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                        

                        if(count($isrecordavailable) > 0)
                        {
                            // print_r($insert_data);exit();
                            $insert_data['_id'] = $isrecordavailable[0]['_id'];
                            $this->common->update_data($insert_data,$collection,'_id', $isrecordavailable[0]['_id']);
                        }
                        else
                        {
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }
                    else{
                        if($this->common->createmongocollection($collection)){
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }

                    
                }
            }

        }
        

    }
    public function grab_eod_data_new(){



        $credentials = "cobaltor:jaleco"; 
        $url = "http://ws.eoddata.com/data.asmx";
        $body = '<?xml version="1.0" encoding="utf-8"?>
        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
          <soap12:Body>
            <Login xmlns="http://ws.eoddata.com/Data">
              <Username>cobaltor</Username>
              <Password>jaleco</Password>
            </Login>
          </soap12:Body>
        </soap12:Envelope>';

        $headers = array( 
            'Content-Type: text/xml; charset=utf-8', 
            'Content-Length: '.strlen($body), 
            'Accept: text/xml', 
            'Cache-Control: no-cache', 
            'Pragma: no-cache', 
            'SOAPAction: http://ws.eoddata.com/Data/Login',
            'Host:ws.eoddata.com',
            ); 

        $ch = curl_init(); 

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        $data = curl_exec($ch);

        $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");

        $ns = $xml->getNamespaces(true);
        $soap = $xml->children($ns['soap']);
        $res = $soap->Body->children();
        $res_array = $this->xml2array($res);

        $token = $res_array['LoginResponse']['LoginResult']['@attributes']['Token'];



        $url = "http://ws.eoddata.com/data.asmx";
        $body = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ExchangeList xmlns="http://ws.eoddata.com/Data">
      <Token>'.$token.'</Token>
    </ExchangeList>
  </soap:Body>
</soap:Envelope>';

        $headers = array( 
            'Content-Type: text/xml; charset=utf-8', 
            'Content-Length: '.strlen($body), 
            'Accept: text/xml', 
            'Cache-Control: no-cache', 
            'Pragma: no-cache', 
            'SOAPAction: http://ws.eoddata.com/Data/ExchangeList',
            'Host:ws.eoddata.com',
            ); 

        $ch = curl_init(); 

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        $data = curl_exec($ch);

        $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");

        $ns = $xml->getNamespaces(true);
        $soap = $xml->children($ns['soap']);
        $res = $soap->Body->children();
        $res_array = $this->xml2array($res);

        $exchanges = $res_array['ExchangeListResponse']['ExchangeListResult']['EXCHANGES']['EXCHANGE'];
        
        // echo "<pre>";print_r($exchanges);exit();

        if(!empty($exchanges)){

            foreach ($exchanges as $exkey => $exvalue) {
                
                $exvalue_new = $this->xml2array($exvalue);

                // echo $exvalue_new['@attributes']['Code'];

                $url = "http://ws.eoddata.com/data.asmx";
                $body = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <QuoteList xmlns="http://ws.eoddata.com/Data">
                      <Token>'.$token.'</Token>
                      <Exchange>'.$exvalue_new['@attributes']['Code'].'</Exchange>
                    </QuoteList>
                  </soap:Body>
                </soap:Envelope>'; /// Your SOAP XML needs to be in this variable
                // $body = 'Username=cobaltor&Password=jaleco';
                $headers = array( 
                    'Content-Type: text/xml; charset=utf-8', 
                    'Content-Length: '.strlen($body), 
                    'Accept: text/xml', 
                    'Cache-Control: no-cache', 
                    'Pragma: no-cache', 
                    'SOAPAction: http://ws.eoddata.com/Data/QuoteList',
                    'Host:ws.eoddata.com',
                    ); 

                $ch = curl_init(); 
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_URL, $url); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POST, true); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $credentials);
                $data = curl_exec($ch); 
                // print_r($data);

                $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
                $ns = $xml->getNamespaces(true);
                $soap = $xml->children($ns['soap']);
                $res = $soap->Body->children();
                $res_array = $this->xml2array($res);

                // echo "<pre>";print_r($res_array['QuoteListResponse']['QuoteListResult']['QUOTES']['QUOTE']);
                $quotes = $res_array['QuoteListResponse']['QuoteListResult']['QUOTES']['QUOTE'];
                foreach ($quotes as $quote) {

                    if (array_key_exists('@attributes', $quote)) {
                        $insert_data = array();
                        // print_r($this->xml2array($quote)['@attributes']['Symbol']);
                        // echo "<br>";
                        $quote = $this->xml2array($quote);
                        $symbol = $quote['@attributes']['Symbol'];
                        // if($symbol == 'RP'){
                        //     echo "string".$exvalue_new['@attributes']['Code'];
                        // }
                        $date_insert = $quote['@attributes']['DateTime'];
                        $new_date = date('Y-m-d', strtotime($date_insert));
                        // $isfound = $this->common->select_database_id('ticker_master_new', 'eod_data', $symbol, '*');
                        $checksymbol = str_replace('.C', '', $symbol);
                        $market_code = $exvalue_new['@attributes']['Code'];
                        if($market_code == 'CBOT'){
                            $market_code = 'CBT';
                        }
                        /*$isfound = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$checksymbol,'type'=>1,'eod_market'=>$market_code), '*', '', '', '', '',array(),'');
                        if(count($isfound) > 0){*/
                        $isfound = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$checksymbol,'type'=>1,'eod_market'=>$market_code), '*', '', '', '', '',array(),'');
                        $isfound1 = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$symbol,'type'=>1,'eod_market'=>$market_code), '*', '', '', '', '',array(),'');
                        if(count($isfound) > 0 || count($isfound1) > 0){


                            // if($symbol == 'RP'){
                            //     echo "string".$exvalue_new['@attributes']['Code'];
                            // }
                            
                            // if($isfound[0]['type'] == 1){
                            //     $quote['@attributes']['source'] = 'eoddata';   
                            // }
                            // else if($isfound[0]['type'] == 2){
                            //     $quote['@attributes']['source'] = 'barchart';   
                            // }
                            // else if($isfound[0]['type'] == 3){
                            //     $quote['@attributes']['source'] = 'alpha_vantag';
                            // }
                            

                            $insert_data['symbol'] = $symbol;
                            $insert_data['date'] = $new_date;//$quote['@attributes']['DateTime'];
                            $insert_data['open'] = $quote['@attributes']['Open'];
                            $insert_data['high'] = $quote['@attributes']['High'];
                            $insert_data['low'] = $quote['@attributes']['Low'];
                            $insert_data['close'] = $quote['@attributes']['Close'];
                            $insert_data['volume'] = $quote['@attributes']['Volume'];
                            $insert_data['openinterest'] = $quote['@attributes']['OpenInterest'];
                            $insert_data['source'] = 'eoddata';

                            $iscollectionavailable = $this->common->collection_exist($symbol);
                            if($iscollectionavailable){

                                $isrecordavailable = $this->common->select_data_by_condition($symbol, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                                

                                if(count($isrecordavailable) > 0)
                                {
                                    echo"hii1<br>";
                                    print_r($insert_data);
                                    $insert_data['_id'] = $isrecordavailable[0]['_id'];
                                    $this->common->update_data($insert_data,$symbol,'_id', $isrecordavailable[0]['_id']);
                                }
                                else
                                {
                                    echo"hii2<br>";
                                    print_r($insert_data);
                                    $this->common->insert_data($insert_data, $symbol);
                                }
                                // $insert_data['symbol'] = $symbol;
                                // $insert_data['date'] = $quote['@attributes']['DateTime'];
                                // $insert_data['open'] = $quote['@attributes']['Open'];
                                // $insert_data['high'] = $quote['@attributes']['High'];
                                // $insert_data['low'] = $quote['@attributes']['Low'];
                                // $insert_data['close'] = $quote['@attributes']['Close'];
                                // $insert_data['volume'] = $quote['@attributes']['Volume'];
                                // $insert_data['openinterest'] = $quote['@attributes']['OpenInterest'];
                            }
                            else{
                                if($this->common->createmongocollection($symbol)){
                                    $this->common->insert_data($insert_data, $symbol);
                                }
                            }

                            
                        }
                    } else {
                        echo '<br>not found';
                    }
                }

            }
            // echo $exchanges['@attributes']['Code'];

            

        }

    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        
        foreach ( (array) $xmlObject as $index => $node )
        {
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        }
        return $out;
    }
   


}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */