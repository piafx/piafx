<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pricedata extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();
        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }
       ini_set('memory_limit', '640M');
       ini_set('post_max_size', '640M');
       ini_set('upload_max_filesize', '640M');
       ini_set('max_execution_time', 60000);
       include APPPATH . 'third_party/excel/reader.php';
       $this->load->model('common');

        //Setting Page Title and Comman Variable
        $this->data['title']         = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Price Data';

        $this->data['permission_list'] = $this->common->permission();
        //Load leftsidemenu and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $this->check_permission();
    }

    public function index(){

        // print_r($this->common->distinct_column_values());exit();
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $join = array();
        $session_array = $this->session->userdata('admin_data');
        $id=$this->uri->segment(3);
        if($id != ''){
            $this->data['selected_option'] = base64_decode($id);
            
            $condition = array( '_id'=> (int) base64_decode($id),
                                'showdashboard'=>"1"
                            );
            $exdata = $this->common->select_data_by_condition('ticker_master_new', $condition, '', '_id', 'DESC', '', '', array());
            if(count($exdata) > 0){
                $this->data['selected_market'] = $exdata[0]['cftc_market'];        
            }
            else{
                $this->data['selected_market'] = "";
            }
        }
        else{
            $this->data['selected_option'] = base64_decode($id); 
            $this->data['selected_market'] = "";
        }
        if($session_array['role'] != 1)
        {
            // $condition = array('eod_data'=>array('$ne'=>'-'),'barchart'=>array('$ne'=>'-'),'alpha_vantag'=>array('$ne'=>'-'));
            $this->data['market'] = $this->common->distinct_column_values("ticker_master_new","cftc_market");
            
            $this->load->view('pricedata/index', $this->data);
        }
        else
        {
            $this->data['market'] = $this->common->distinct_column_values("ticker_master_new","cftc_market");
            // echo "<pre>";print_r($this->data['market']);exit();
            $this->data['market']=array_diff($this->data['market'],['-']);
            $this->load->view('pricedata/index', $this->data);
        }

    }
    public function getalltickerpermarket()
    {

        $market_id = $_REQUEST['market_id'];

        $condition = array('cftc_market'=>$market_id,'showdashboard'=>"1");
        $data = $this->common->select_data_by_condition('ticker_master_new', $condition, '', '_id', 'DESC', '', '', array());
        if(count($data) > 0){
            echo json_encode($data);
        }
        else{
            echo json_encode(array());   
        }
    }
    public function edit()
    {
        $token=$this->uri->segment(4);
        $symbol= $this->uri->segment(3);
        $join = array();
        if($token != '')
        {
            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$symbol,), '', '', '', '', '', $join);
            if(empty($ticker_data)) 
            {
                $this->session->set_flashdata('message', 'Record not found.');
                redirect('pricedata', 'refresh');
                exit();
            }
            else{
                $ticker_code = $ticker_data[0]['eod_data'];
                $data_all = $this->common->select_database_id($ticker_code, '_id', $token, '*');
                if(!empty($data_all)){

                    $data = array(
                        'symbol'=>$data_all[0]['symbol'],
                        'id'=>$data_all[0]['_id'],
                        'date'=>$data_all[0]['date'],
                        'open' => $data_all[0]['open'],
                        'high' =>$data_all[0]['high'],
                        'low'=>$data_all[0]['low'],
                        'close' => $data_all[0]['close'],
                        'volume' => $data_all[0]['volume'],
                        'openinterest' => $data_all[0]['openinterest'],
                        'ticker_id'=> $symbol,
                    );
                    $this->data['edit_id'] = $token;
                    $this->data['data'] = $data;
                    // print_r($this->data['data']);exit();
                    $this->load->view('pricedata/edit', $this->data);
                }
                else{

                    $this->session->set_flashdata('message', 'Record not found.');
                    redirect('pricedata', 'refresh');
                    exit();
                }
            }
        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('pricedata', 'refresh');
            exit();
        }
    }
    public function delete()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $id = $this->uri->segment(4);
        $ticker_id= $this->uri->segment(3);
        $join = array();
        if($id != '')
        {
            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$ticker_id), '', '', '', '', '', $join);
            if(empty($ticker_data))
            {
                $this->session->set_flashdata('message', 'Record not found.');
                redirect('pricedata', 'refresh');
                exit();
            }
            else{

                $ticker_code = $ticker_data[0]['eod_data'];
                $this->common->delete_data($ticker_code,'_id', $id);
                $this->session->set_flashdata('success', 'Record Updated successfully.');
                redirect('pricedata/index/'.base64_encode($ticker_id), 'refresh');
            }
        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('pricedata', 'refresh');
            exit();
        }
    }
    public function edit_data()
    {
        // print_r($_POST);
        // exit();
        $id = base64_decode($_POST['id']);
        $ticker_id= base64_decode($_POST['ticker_id']);
        $join = array();
        if($id != '')
        {

            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$ticker_id), '', '', '', '', '', $join);
            if(!empty($ticker_data)){
                
                $ticker_code = $ticker_data[0]['eod_data'];
                $data = array(
                        'symbol'=>$_POST['symbol'],
                        'date'=>$_POST['date'],
                        'open'=>$_POST['open'],
                        'high' =>$_POST['high'],
                        'low'=>$_POST['low'],
                        'close' => $_POST['close'],
                        'volume' => $_POST['volume'],
                        'openinterest' => $_POST['openinterest'],
                    );
                $this->common->update_data($data ,$ticker_code,'_id', $id);
                $this->session->set_flashdata('success', 'Record Updated successfully.');
                redirect('pricedata/index/'.base64_encode($ticker_id), 'refresh');
            }

        }
        else{
            $this->session->set_flashdata('message', 'Record not found.');
            redirect('pricedata', 'refresh');
            exit();
        }
    }
    public function getticker_data()
    {

        $id = $_POST['ticker_id'];
        if($id !='')
        {
            $join = array();
            $ticker_data = $this->common->select_data_by_condition('ticker_master_new', array('_id'=>(int)$id), '', '', '', '', '', $join);
            if(count($ticker_data)> 0){
                $code = $ticker_data[0]['eod_data'];
                if($code != '' && $code != '-')
                {
                    $code_data = $this->common->select_data_by_condition($code, array(), '', '', '', '', '', $join);
                    if(count($code_data)> 0){

                        $data['tickerdata'] = $ticker_data;
                        $data['pricedata'] = $code_data;
                        echo json_encode($data);
                    }
                    else{
                        $data['tickerdata'] = $ticker_data;
                        $data['pricedata'] = array();
                        echo json_encode($data);
                        // echo json_encode();
                    }
                }
                else{
                    $data['tickerdata'] = $ticker_data;
                    $data['pricedata'] = array();
                    echo json_encode($data);
                }
            }
            else{
                    $data['tickerdata'] = array();
                    $data['pricedata'] = array();
                    echo json_encode($data);
                }
        }
        
    }
    public function updateticker_data()
    {
        $ticker_id = $_POST['ticker_id'];
        $type = $_POST['type'];
        $code = $_POST['code'];
        if($ticker_id != '' && $type != ''){
            if($type == 1){
                $update_array['eod_data'] = $code;    
            }
            elseif($type == 2){
                $update_array['barchart'] = $code;
            }
            elseif($type == 3){
                $update_array['alpha_vantag'] = $code;   
            }
            $update_array['type'] = $type;
            // print_r($update_array);exit();
            if($this->common->update_data($update_array,'ticker_master_new','_id', (int)$ticker_id)){
                echo json_encode(array('status'=>true)); 
            }
            else{
                echo json_encode(array('status'=>false));                 
            }
        }
        else{

           echo json_encode(array('status'=>false)); 

        }
    }
    public function check_permission()
    {
        $this->data['permission_list'] = $this->common->permission();
        $controller = $this->uri->segment(1);
        $method = $this->uri->segment(2) != ''?$this->uri->segment(2):'index';
        
        if($method == 'getticker_data' || $method == 'getalltickerpermarket'){
            $method = 'index';
        }
        if($method == 'insertdata')
        {
            $method = 'add';
        }
        if($method == 'edit_data' || $method == 'update' || $method == 'updateticker_data')
        {
            $method = 'edit';
        }
        
        if(!in_array($controller.'_'.$method, $this->data['permission_list']))
        {
            $this->session->set_flashdata('message', 'You do not have access.');
            redirect('admin', 'refresh');
        }
    }

    public function grab_eod_data(){

            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->load->library('PHPExcel');

            $object = PHPExcel_IOFactory::load('/var/www/html/CME_20190716.csv');
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for($row=2; $row<= $highestRow; $row++)
                {  
                    for($col=0; $col<$highestColumnIndex; $col++)
                    {
                        // echo $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if($col == 0){
                            $ticker_code = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                            $insert_data['symbol'] = $ticker_code;
                        }
                        else if($col == 1){
                            $insert_data['date'] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
                        }
                        else if($col == 2){

                        }
                    }
                    $isfound = $this->common->select_database_id('ticker_master_new', 'eod_data', $ticker_code, '*');
                    // print_r($isfound);exit();
                    if(count($isfound) > 0){

                        if($isfound[0]['type'] == 1){
                            
                        }
                        else if($isfound[0]['type'] == 2){
                            
                        }
                        else if($isfound[0]['type'] == 3){
                            
                        }
                        
                        $iscollectionavailable = $this->common->collection_exist($ticker_code);

                        if($iscollectionavailable){

                        }
                        else{

                        }

                        
                    }
                    echo "<br>";
                }
            }


    }
    public function grab_alphaventage_data_current(){

        $allbarchart_tickers = $this->common->select_data_by_condition('ticker_master_new', array('type'=>3), '*', '', '', '', '',array(),'');

        // print_r($allbarchart_tickers);exit();
        
        foreach ($allbarchart_tickers as $key => $value) {

            $symbols = $value['alpha_vantag'];
            // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
            $url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol='.$symbols.'&interval=60min&outputsize=full&apikey=MXG3YR2GK3T04D3B';
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $alltickers = json_decode($result);
            $allvalues = json_decode(json_encode($alltickers), True);
            if(!empty($allvalues)){
                
                $allvalues_main = $allvalues['Time Series (60min)'];
                $key = array_keys($allvalues_main)[0];
                $new_date = date('Y-m-d', strtotime($key));  
                $addarray = $allvalues_main[$key];   

                $symbol = $symbols;
                $date_insert = $new_date;
                
                $isfound = $this->common->select_data_by_condition('ticker_master_new', array('alpha_vantag'=>$symbol,'type'=>3), '*', '', '', '', '',array(),'');
                
                if(count($isfound) > 0){

                    $collection = $isfound[0]['eod_data'];
                    $insert_data['symbol'] = $symbol;
                    $insert_data['date'] = $new_date;
                    $insert_data['open'] = $addarray['1. open'];
                    $insert_data['high'] = $addarray['2. high'];
                    $insert_data['low'] = $addarray['3. low'];
                    $insert_data['close'] = $addarray['4. close'];
                    $insert_data['volume'] = $addarray['5. volume'];
                    $insert_data['openinterest'] = '-';
                    $insert_data['source'] = 'alphavantag';

                    // print_r($insert_data);exit();
                    $iscollectionavailable = $this->common->collection_exist($collection);
                    if($iscollectionavailable){

                        $isrecordavailable = $this->common->select_data_by_condition($collection, array('date'=>$date_insert), '*', '', '', '', '',array(),'');

                        

                        if(count($isrecordavailable) > 0)
                        {
                            $insert_data['_id'] = $isrecordavailable[0]['_id'];
                            $this->common->update_data($insert_data,$collection,'_id', $isrecordavailable[0]['_id']);
                        }
                        else
                        {
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }
                    else{
                        if($this->common->createmongocollection($collection)){
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }

                    
                }
            }
        }

        
    }
    public function grab_barchart_data_current(){

        $allbarchart_tickers = $this->common->select_data_by_condition('ticker_master_new', array('type'=>2), '*', '', '', '', '',array(),'');

        // print_r($allbarchart_tickers);exit();
        $symbols = '';
        foreach ($allbarchart_tickers as $key => $value) {
            if($symbols != ''){
                $symbols .= '%2C'.$value['barchart'].'%2C'.$value['barchart'].'.C';
            }
            else{
                $symbols =  $value['barchart'].'%2C'.$value['barchart'].'.C';
            }
        }
        if($symbols != ''){

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://marketdata.websol.barchart.com/getQuote.json?apikey=5d7ac404b64dd72abdfdd786e297cd8d&symbols='.$symbols.'&fields=fiftyTwoWkHigh%2CfiftyTwoWkHighDate%2CfiftyTwoWkLow%2CopenInterest%2CfiftyTwoWkLowDatecurl');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            
            $alltickers = json_decode($result)->results;
            // echo "<pre>";
            $array = array();
            if($alltickers){
                $array = json_decode(json_encode($alltickers), True);
            }
            
            // print_r($array);exit();

            foreach ($array as $quote) {
                $insert_data = array();
                // print_r($this->xml2array($quote)['@attributes']['Symbol']);
                // echo "<br>";
                // $quote = $this->xml2array($quote);
                $symbol = $quote['symbol'];
                $date_insert = $quote['tradeTimestamp'];
                $new_date = date('Y-m-d', strtotime($date_insert));
                // $isfound = $this->common->select_database_id('ticker_master_new', 'eod_data', $symbol, '*');
                $isfound = $this->common->select_data_by_condition('ticker_master_new', array('barchart'=>$symbol,'type'=>2), '*', '', '', '', '',array(),'');
                // print_r($isfound);exit();

                if(count($isfound) > 0){

                    $collection = $isfound[0]['eod_data'];
                    $insert_data['symbol'] = $symbol;
                    $insert_data['date'] = $new_date;
                    $insert_data['open'] = $quote['open'];
                    $insert_data['high'] = $quote['high'];
                    $insert_data['low'] = $quote['low'];
                    $insert_data['close'] = $quote['close'];
                    $insert_data['volume'] = $quote['volume'];
                    $insert_data['openinterest'] = $quote['openInterest'];
                    $insert_data['source'] = 'barchart';

                    // print_r($insert_data);exit();
                    $iscollectionavailable = $this->common->collection_exist($collection);
                    if($iscollectionavailable){

                        $isrecordavailable = $this->common->select_data_by_condition($collection, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                        

                        if(count($isrecordavailable) > 0)
                        {
                            // print_r($insert_data);exit();
                            $insert_data['_id'] = $isrecordavailable[0]['_id'];
                            $this->common->update_data($insert_data,$collection,'_id', $isrecordavailable[0]['_id']);
                        }
                        else
                        {
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }
                    else{
                        if($this->common->createmongocollection($collection)){
                            $this->common->insert_data($insert_data, $collection);
                        }
                    }

                    
                }
            }

        }
        

    }
    public function grab_eod_data_new(){



        $credentials = "cobaltor:jaleco"; 
        $url = "http://ws.eoddata.com/data.asmx";
        $body = '<?xml version="1.0" encoding="utf-8"?>
        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
          <soap12:Body>
            <Login xmlns="http://ws.eoddata.com/Data">
              <Username>cobaltor</Username>
              <Password>jaleco</Password>
            </Login>
          </soap12:Body>
        </soap12:Envelope>';

        $headers = array( 
            'Content-Type: text/xml; charset=utf-8', 
            'Content-Length: '.strlen($body), 
            'Accept: text/xml', 
            'Cache-Control: no-cache', 
            'Pragma: no-cache', 
            'SOAPAction: http://ws.eoddata.com/Data/Login',
            'Host:ws.eoddata.com',
            ); 

        $ch = curl_init(); 

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        $data = curl_exec($ch);

        $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");

        $ns = $xml->getNamespaces(true);
        $soap = $xml->children($ns['soap']);
        $res = $soap->Body->children();
        $res_array = $this->xml2array($res);

        $token = $res_array['LoginResponse']['LoginResult']['@attributes']['Token'];



        $url = "http://ws.eoddata.com/data.asmx";
        $body = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
          <soap:Body>
            <ExchangeList xmlns="http://ws.eoddata.com/Data">
              <Token>'.$token.'</Token>
            </ExchangeList>
          </soap:Body>
        </soap:Envelope>';

        $headers = array( 
            'Content-Type: text/xml; charset=utf-8', 
            'Content-Length: '.strlen($body), 
            'Accept: text/xml', 
            'Cache-Control: no-cache', 
            'Pragma: no-cache', 
            'SOAPAction: http://ws.eoddata.com/Data/ExchangeList',
            'Host:ws.eoddata.com',
            ); 

        $ch = curl_init(); 

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        $data = curl_exec($ch);

        $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");

        $ns = $xml->getNamespaces(true);
        $soap = $xml->children($ns['soap']);
        $res = $soap->Body->children();
        $res_array = $this->xml2array($res);

        $exchanges = $res_array['ExchangeListResponse']['ExchangeListResult']['EXCHANGES']['EXCHANGE'];
        
        // echo "<pre>";print_r($exchanges);exit();

        if(!empty($exchanges)){

            foreach ($exchanges as $exkey => $exvalue) {
                
                $exvalue_new = $this->xml2array($exvalue);

                // echo $exvalue_new['@attributes']['Code'];

                $url = "http://ws.eoddata.com/data.asmx";
                $body = '<?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                  <soap:Body>
                    <QuoteList xmlns="http://ws.eoddata.com/Data">
                      <Token>'.$token.'</Token>
                      <Exchange>'.$exvalue_new['@attributes']['Code'].'</Exchange>
                    </QuoteList>
                  </soap:Body>
                </soap:Envelope>'; /// Your SOAP XML needs to be in this variable
                // $body = 'Username=cobaltor&Password=jaleco';
                $headers = array( 
                    'Content-Type: text/xml; charset=utf-8', 
                    'Content-Length: '.strlen($body), 
                    'Accept: text/xml', 
                    'Cache-Control: no-cache', 
                    'Pragma: no-cache', 
                    'SOAPAction: http://ws.eoddata.com/Data/QuoteList',
                    'Host:ws.eoddata.com',
                    ); 

                $ch = curl_init(); 
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_URL, $url); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POST, true); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($ch, CURLOPT_USERPWD, $credentials);
                $data = curl_exec($ch); 
                print_r($data);

                $xml = simplexml_load_string($data, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
                $ns = $xml->getNamespaces(true);
                $soap = $xml->children($ns['soap']);
                $res = $soap->Body->children();
                $res_array = $this->xml2array($res);

                // echo "<pre>";print_r($res_array['QuoteListResponse']['QuoteListResult']['QUOTES']['QUOTE']);
                $quotes = $res_array['QuoteListResponse']['QuoteListResult']['QUOTES']['QUOTE'];
                foreach ($quotes as $quote) {
                    $insert_data = array();
                    // print_r($this->xml2array($quote)['@attributes']['Symbol']);
                    // echo "<br>";
                    $quote = $this->xml2array($quote);
                    $symbol = $quote['@attributes']['Symbol'];
                    // if($symbol == 'RP'){
                    //     echo "string".$exvalue_new['@attributes']['Code'];
                    // }
                    $date_insert = $quote['@attributes']['DateTime'];
                    $new_date = date('Y-m-d', strtotime($date_insert));
                    // $isfound = $this->common->select_database_id('ticker_master_new', 'eod_data', $symbol, '*');
                    $checksymbol = str_replace('.C', '', $symbol);
                    $isfound = $this->common->select_data_by_condition('ticker_master_new', array('eod_data'=>$checksymbol,'type'=>1,'cftc_market'=>$exvalue_new['@attributes']['Code']), '*', '', '', '', '',array(),'');
                    if(count($isfound) > 0){

                        if($symbol == 'RP'){
                            echo "string".$exvalue_new['@attributes']['Code'];
                        }
                        
                        // if($isfound[0]['type'] == 1){
                        //     $quote['@attributes']['source'] = 'eoddata';   
                        // }
                        // else if($isfound[0]['type'] == 2){
                        //     $quote['@attributes']['source'] = 'barchart';   
                        // }
                        // else if($isfound[0]['type'] == 3){
                        //     $quote['@attributes']['source'] = 'alpha_vantag';
                        // }
                        

                        $insert_data['symbol'] = $symbol;
                        $insert_data['date'] = $new_date;//$quote['@attributes']['DateTime'];
                        $insert_data['open'] = $quote['@attributes']['Open'];
                        $insert_data['high'] = $quote['@attributes']['High'];
                        $insert_data['low'] = $quote['@attributes']['Low'];
                        $insert_data['close'] = $quote['@attributes']['Close'];
                        $insert_data['volume'] = $quote['@attributes']['Volume'];
                        $insert_data['openinterest'] = $quote['@attributes']['OpenInterest'];
                        $insert_data['source'] = 'eoddata';

                        $iscollectionavailable = $this->common->collection_exist($symbol);
                        if($iscollectionavailable){

                            $isrecordavailable = $this->common->select_data_by_condition($symbol, array('date'=>$new_date), '*', '', '', '', '',array(),'');

                            

                            if(count($isrecordavailable) > 0)
                            {
                                $insert_data['_id'] = $isrecordavailable[0]['_id'];
                                $this->common->update_data($insert_data,$symbol,'_id', $isrecordavailable[0]['_id']);
                            }
                            else
                            {
                                $this->common->insert_data($insert_data, $symbol);
                            }
                            // $insert_data['symbol'] = $symbol;
                            // $insert_data['date'] = $quote['@attributes']['DateTime'];
                            // $insert_data['open'] = $quote['@attributes']['Open'];
                            // $insert_data['high'] = $quote['@attributes']['High'];
                            // $insert_data['low'] = $quote['@attributes']['Low'];
                            // $insert_data['close'] = $quote['@attributes']['Close'];
                            // $insert_data['volume'] = $quote['@attributes']['Volume'];
                            // $insert_data['openinterest'] = $quote['@attributes']['OpenInterest'];
                        }
                        else{
                            if($this->common->createmongocollection($symbol)){
                                $this->common->insert_data($insert_data, $symbol);
                            }
                        }

                        
                    }
                }

            }
            // echo $exchanges['@attributes']['Code'];

            

        }

    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        
        foreach ( (array) $xmlObject as $index => $node )
        {
            $out[$index] = ( is_object ( $node ) ) ? $this->xml2array ( $node ) : $node;
        }
        return $out;
    }
   


}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */