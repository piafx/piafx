<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('voicecallrec_data')) {
//If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }

//Setting Page Title and Comman Variable
        $this->data['title'] = $this->settings->get_setting_value(1);
        $this->data['section_title'] = 'Notification';
      

//Load leftsidemenu and save in variable

        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->model('common');
        }
        

        function index() {

            $this->load->library(array('encrypt', 'form_validation', 'session'));
            // LOAD HELPERS
            $this->load->helper('form');

            // SET VALIDATION RULES
            $this->form_validation->set_rules('message', 'message', 'required');
            $this->form_validation->set_error_delimiters('<em>', '</em>');
            // has the form been submitted and with valid form info (not empty values)

            if ($this->form_validation->run()) {

                $message = $this->input->post('message');


                $data = $this->common->get_data_all('user_login');

                $gcm_ids = array();
                foreach ($data as $gcm) {
                    array_push($gcm_ids, $gcm['gcm_id']);
                }

                
                $url = 'https://android.googleapis.com/gcm/send';

                $fields = array(
                    'registration_ids' => $gcm_ids,
                    'data' => $message,
                );
                
                $headers = array(
                    'Authorization: key=AIzaSyC-pFtYWKNIRkAsCQLEG8Wbz0H2Bg5Dlew',
                    'Content-Type: application/json'
                );
// Open connection
                $ch = curl_init();

// Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);

                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

// Execute post
                $result = curl_exec($ch);
                if ($result === false) {
                $this->session->set_flashdata('message', 'Error into sending notification.');
            } else {
                $this->session->set_flashdata('success', 'Notification send successfully');
            }

            curl_close($ch);

            redirect('notification', 'refresh');
        } else {
            $this->load->view('notification', $this->data);
        }
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
