<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $data;

    public function __construct() {
        parent::__construct();

        if (!$this->session->userdata('admin_data')) {
            //If no session, redirect to login user
            redirect('adminlogin', 'refresh');
        }
        $this->data['permission_list'] = $this->common->permission();
        // print_r($this->data['permission_list']);exit();
          
        $this->load->model('common');
        //Setting Page Title and Comman Variable
        $this->data['title'] = $this->common->get_setting_value(1);
        $this->data['section_title'] = 'Dashboard';


        //Load leftsidemenu and save in variable

        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        
    }

    public function index() {

            $this->data['cots'] = $this->common->select_database_by_muliple_where('COT_master', array(), '*', '', '');
            $this->data['tickers'] = $this->common->select_database_by_muliple_where('ticker_master', array(), '*', '', '');
            $this->data['markets'] = $this->common->select_database_by_muliple_where('markets', array(), 'DISTINCT(market_name) as market_names', '', '');
            $this->load->view('dashboard', $this->data);
          
    }
    
    function logout() {

        
        $this->session->sess_destroy();
        redirect('adminlogin', 'refresh');
    }

}

/* End of file dashboard.php */
            /* Location: ./application/controllers/dashboard.php */
            