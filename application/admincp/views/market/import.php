<?php echo $header; ?>
 <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item" title="Market"><a href="<?php echo base_url('market') ?>">Market</a></li>
                            <li class="breadcrumb-item active" title="Import Market Data">Import Market Data</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12" style="padding-left: 0px;padding-right: 0px; ">
                                    <div class="col-md-8" style="padding-left: 0px; ">
                                        <h2 class="card-title">Import Market Data</h2>
                                    </div>
                                    <div class="col-md-4" style="padding-right: 0px;">
                                        <a download="sample.xlsx" href="<?php echo base_url('sample/sample.xlsx') ?>" title="Sample Data"><input type="submit" value="Download Import Data Sample" class="btn btn-success"/></a>
                                    </div>
                                </div>
                                <form class="form-material m-t-40" method="post" action="<?php echo base_url('market/import_data'); ?>" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label>Market Name <span style="color: red">*</span></label>
                                        <select class="form-control" name="market" required="">
                                            <option value="">Select Market</option>
                                            <?php
                                             foreach($data as $value) {
                                            ?>
                                               <option value="<?= $value['market_id'] ?>" <?php echo set_select('market',$value['market_id'], ( !empty($data) && $data == $value['market_id'] ? TRUE : FALSE )); ?>><?= $value['market_name']; ?></option>
                                            <?php
                                             } 
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload File <span style="color: red">*</span></label>
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-secondary btn-file" style="color: black !important;"> 
                                                <span class="fileinput-new">Select File</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="file" required="">
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-secondary fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                    </div>
                                    <button title="Submit" type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button></a>
                                        <a href="<?php echo base_url('market'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<?php echo $footer; ?>
    <script>
        $("form").submit(function(e){
            $('.preloader').show();
        });
    </script>
