<?php echo $header; ?>
<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                             <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                             <li class="breadcrumb-item" title="Market"><a href="<?php echo base_url('market');?>">Market</a></li>
                            <li class="breadcrumb-item active" title="Edit Market">Edit Market</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php //echo $market[0]['assign_to1']; ?>
                <div class="row">
                    <div class="col-12">
                            <div class="card">
                            <div class="card-body">

                                        <h2 class="card-title">Edit Market</h2>
                                 <?php foreach ($market as $value){
                                    
                                     $assign_to = $value['assign_to1'];
                                     $assign_to = explode(',', $assign_to);
                                 ?>
                                
                                <form class="form-material m-t-40"  action="<?php echo base_url('market/update/'); ?>" method="post">
                                    
                                    <input type="hidden" id="id" name="id" value="<?php echo base64_encode($value['market_id']);?>">
                                    <div class="form-group">

                                        <label>Market Name <span style="color: red">*</span></label>
                                        <input title="Market Name"  type="text"  name="market_name" class="form-control form-control-line" value="<?php echo $edit_id!=''?$value['market_name']:set_value('market_name'); ?>" required=""> 
                                        <?php echo form_error('market_name'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea  title="Description" class="form-control" rows="5" name="market_desc"><?php echo $edit_id!=''?$value['market_desc']:set_value('market_desc'); ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Assign To  <span style="color: red">*</span></label>
                                        <!--<select title="Assign To" class="form-control" name="assignto" value="<?php //echo $value['assignto'];?>" required="">-->
                                        <select title="Assign To" class="form-control select2 m-b-10 select2-multiple" name="assignto[]" multiple="multiple" required="">
                                            <option value="">Select Assign To</option>
                                        <?php
                                         foreach($data as $value1) {
                                            if($value1['user_role']==2){
                                        ?>
                                            <option value="<?= $value1['user_id'] ?>" <?php echo in_array($value1['user_id'],$assign_to)?'selected':''; ?>><?= $value1['user_firstname'].' '.$value1['user_lastname'] ?></option>
                                        <?php
                                            }   
                                         } 
                                         ?>
                                     </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Client </label>
                                        <select title="Client" class="form-control" name="client">
                                            <option value="">Select Client</option>
                                         <?php
                                         foreach($data as $value1) {
                                         if($value1['user_role']==4){
                                         ?>
                                           <option value="<?= $value1['user_id'] ?>" <?php echo $value['client'] == $value1['user_id']?'selected':''; ?>><?= $value1['user_firstname'].' '.$value1['user_lastname'] ?></option>
                                              <?php
                                           } } ?>
                                        </select>
                                    </div>    
                                    <button title="Update" type="submit" class="btn btn-success  waves-effect waves-light m-r-10 " name="btn-submit">Update</button>
                                    <a href="<?php echo base_url('market'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                               
                                </form>
                                 <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="red" class="red-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme working">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="red-dark" class="red-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme ">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/1.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/2.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/3.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/4.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/5.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/6.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/7.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="assets/images/users/8.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
<?php echo $footer; ?>
