<?php echo $header; ?>
   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                            <li class="breadcrumb-item" title="Market"><a href="<?php echo base_url('market');?>">Market</a></li>
                            <li class="breadcrumb-item active" title="Add Market">Add Market</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                 
                                <h2 class="card-title">Add Market</h2>
                                <form class="form-material m-t-40"  action="<?php echo base_url('market/insertdata'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Market Name <span style="color: red">*</span></label>
                                        <input title="Market Name" type="text"  name="market_name" class="form-control form-control-line" required="" value="<?php echo set_value('market_name'); ?>"> 
                                        <?php echo form_error('market_name'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Description </label>
                                        <textarea title="Description" class="form-control" rows="5" name="market_desc" value="<?php echo set_value('market_desc'); ?>"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Assign To <span style="color: red">*</span></label>
                                        <select title="Assign To" class="form-control select2 m-b-10 select2-multiple" name="assignto[]" multiple="multiple" required="">
                                            <!--<option value="">Select Assign To</option>-->
                                        <?php
                                         foreach($data as $value) {
                                            if($value['user_role']==2){
                                        ?>
                                           <option value="<?= $value['user_id'] ?>" <?php echo set_select('assignto',$value['user_id'], ( !empty($data) && $data == $value['user_id'] ? TRUE : FALSE )); ?>><?= $value['user_firstname'].' '.$value['user_lastname'] ?></option>
                                        <?php
                                           }
                                         } 
                                        ?>
                                        </select>  
                                        <?php echo form_error('assignto'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Client </label>
                                        <select title="Client" class="form-control" name="client">
                                            <option value="">Select Client</option>
                                         <?php
                                         foreach($data as $value) 
                                         {
                                            if($value['user_role']==4){
                                         ?>
                                           <option value="<?= $value['user_id'] ?>" <?php echo set_select('client',$value['user_id'], ( !empty($data) && $data == $value['user_id'] ? TRUE : FALSE )); ?>><?= $value['user_firstname'].' '.$value['user_lastname'] ?></option>
                                              <?php
                                           } 
                                         } ?>
                                        </select>
                                        <?php echo form_error('client'); ?>
                                    </div>    
                                    <button title="Submit" type="submit" class="btn btn-success waves-effect waves-light m-r-10 " name="btn-submit">Submit</button>
                                    <a href="<?php echo base_url('market'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
<?php echo $footer; ?>
