<?php echo $header; ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                            <li class="breadcrumb-item active" title="Market">Market</li>
                        </ol>
                    </div>
                    <div>
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                          <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-4" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="mdi mdi-book-open-variant"></i>&nbsp;Market</h2>
                                </div>
                                <div class="col-md-8" style="display: inline-block">

                                    <!--<div class="col-md-6" style="display: inline-block">-->
                                    <a class="<?php echo ((in_array('market_add', $permission_list))?'':'hide'); ?>" href="<?php echo base_url('market/add'); ?>"><button title="Add New Market" class="btn btn-success pull-right" style="float: right;margin-left: 8px;margin-top: 1px;">Add New Market</button></a>
<!--                                    </div> 
                                    <div class="col-md-5 pull-right" style="display: inline-block">-->
                                    &nbsp;<a class="<?php echo ((in_array('market_import', $permission_list))?'':'hide'); ?>" href="<?php echo base_url('market/import'); ?>"><button title="Import Market Data" class="btn btn-success pull-right" style="float: right;margin-top: 1px;">Import Market Data</button></a>
                                    <!--</div>-->
                                </div>                                
                                <div class="table-responsive m-t-40">
                                    <table id="market_tab" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Market-Name</th>
                                                <th>Market-Description</th>
                                                <!--<th>AssignTo</th>-->
                                                <th>Client</th>
                                                <th class="text-center <?php echo ((!in_array('market_edit', $permission_list)) && (!in_array('market_import', $permission_list)) ?'hide':''); ?>">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                            <?php foreach ($data as $value)
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $value['market_name']; ?></td>
                                                <td><?php echo $value['market_desc']; ?></td>
                                                <!--<td><?php // /echo $value['fu']." ".$value['lu']; ?></td>-->
                                                <td><?php echo $value['cfu']." ".$value['clu']; ?></td>
                                                <td class="text-center <?php echo ((!in_array('market_edit', $permission_list)) && (!in_array('market_import', $permission_list)) ?'hide':''); ?>">   
                                                    <a class="<?php echo ((in_array('market_edit', $permission_list))?'':'hide'); ?>" title="Edit" href="<?php echo site_url('market/editform/' .base64_encode($value['market_id'])); ?>"> <i class="fa fa-pencil-alt"></i></a>
                                                    &nbsp;&nbsp;
                                                    <a class="<?php echo ((in_array('market_import', $permission_list))?'':'hide'); ?>" title="View Import Data" href="<?php echo site_url('market/view_import/' .base64_encode($value['market_id'])); ?>"> <i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<?php echo $footer; ?>
<script>



    $(".mystatus").click(function () {


        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        $('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        //var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Pending" value="Pending"> Pending</div>';
        
        var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Disable"  value="Disable"> <label for="Disable">Disable</label></div>';
        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Enable" value="Enable"> <label for="Enable">Enable</label></div>';
//        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> <label for="Deleted">Delete</label></div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#uid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });
        
            $(document).ready(function() {
                var table = $('#market_tab').DataTable({
                    "displayLength": 25,
                    "columnDefs": [
                        { orderable: false, targets: -1 }
                     ]
                });
            });
    
</script>