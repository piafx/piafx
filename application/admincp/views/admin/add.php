<?php echo $header; ?>
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                       
                        <h2><?php echo $edit_id != ''?'Edit Admin':'Create New Admin'; ?> </h2>
                         <div class="pull-right"><a href="<?php echo base_url('admin');?>"> <button class=" bg-primary mystatus btn" name="addadmin">< Back</button></a></div>       
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                       
                        
                           <?php
                                if ($this->session->flashdata('message')) {
                                    ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?>
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php
                                if ($this->session->flashdata('success')) {
                                    ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        
                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo $edit_id!=''?base_url('admin').'/edit_data':base_url('admin').'/add_data' ?>">

  <?php                          if($edit_id != '')
                            {
       ?>                        
                             <input type="hidden" name="id" required="required" value="<?php echo $id; ?>" class="form-control col-md-7 col-xs-12"> 
         <?php                  
         }
         ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="text" name="fname" required="required" value="<?php echo $edit_id != ''?$fname:set_value('fname'); ?>" class="form-control col-md-7 col-xs-12"> 
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"> <?php echo form_error('fname'); ?></li>
                                </ul>
                                </div>
                                
                               
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="first-name">Last Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="text" name="lname" required="required" value="<?php echo $edit_id != ''?$lname:set_value('lname'); ?>" class="form-control col-md-7 col-xs-12"> 
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"> <?php echo form_error('lname'); ?></li>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="first-name">Company Name <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="text" name="cname" required="required" value="<?php echo $edit_id != ''?$cname:set_value('cname'); ?>" class="form-control col-md-7 col-xs-12"> 
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"> <?php echo form_error('cname'); ?></li>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"  for="first-name">Email <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        
                                    <input type="email" pattern="[a-z0-9_%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" required="required" value="<?php echo $edit_id != ''?$email:set_value('email'); ?>" class="form-control col-md-7 col-xs-12"> 
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"> <?php echo form_error('email'); ?></li>
                                </div>
                                
                            </div>
                            <?php
                            
                            if($edit_id != '')
                            {
                                
                            }
                            else{
                                ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input type="password" minlength="6" name="password"   required="required" class="form-control col-md-7 col-xs-12"> 
                                     <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"> <?php echo form_error('password'); ?></li>
                                </div>
                                
                            </div>
                            <?php
                            }
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contact <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <input onkeypress="return isNumber(event)" value="<?php echo $edit_id != ''?$contact:set_value('contact'); ?>" minlength="8" maxlength='16' type="text" name="contact" required="required" class="form-control col-md-7 col-xs-12"> 
                                    <ul class="parsley-errors-list filled" id="parsley-id-5">
                                    <li class="parsley-required"><?php echo form_error('contact'); ?> </li>
                                </div>
                            </div>
                            <center><b> </b></center>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <a href="<?php echo base_url('admin'); ?>"><button type="button" class="btn btn-primary">Cancel</button></a>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                                
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div>
</div>
<script>
      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
    </script>
    <?php echo $footer; ?> 
