
<?php echo $header; ?>

<!-- Datatables -->
<link href="<?php echo base_url(); ?>../design/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>../design/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>../design/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>../design/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php
            if ($this->session->flashdata('message')) {
                ?>
                <!--  start message-red -->
                <div class="box-body">
                    <div class=" alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <!--  end message-red -->
            <?php } ?>
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <!--  start message-green -->
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <!--  end message-green -->
            <?php } ?>
            <br/>
            <div class="x_panel">



                <div class="x_title">
                    <h2>Admin List </h2>
                    <div class="pull-right"><a href="<?php echo base_url().$this->uri->segment(1).'/add';?>"> <button class=" bg-primary btn" name="addadmin">Add New admin</button></a></div>
                    <ul class="nav navbar-right panel_toolbox">


                    </ul>
                    <div class="clearfix">

                    </div>
                </div>
                <div class="x_content">

                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                  <th>No</th> 
                                  <th style="display: none">id</th>
                                <th>Name</th>
                                <th>Email</th>
                                
                                 <th>Contact</th>
                             
                                
                                <th>Registered Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody>
                            <?php
                            if (!empty($data)) {
                               
                                foreach ($data as $key => $user) {
                                    ?>
                                    <tr>
                                         <td><?php echo $key + 1 ?></td>
                                        <td style="display: none"><?php echo $user['adminid'] ?></td>
                                       
                                        <td><?php echo $user['admin_fname'] . " " . $user['admin_lname'] ?></td>
                                        <td><?php echo $user['adminemail']; ?></td>
                                        <td><?php echo $user['admin_contact']; ?></td>
                                        
                                        <td><?php echo date("d-m-Y", strtotime($user['insertdatetime'])); ?></td>

                                        <td>
                                            <span
                                                data-id="<?php echo $user['adminid']; ?>" data-val="<?php echo $user['admin_active']; ?>"
                                                <?php
                                                if ($user['admin_active'] == "Pending") {

                                                    echo ' class="bg-blue mystatus btn">Pending</span>';
                                                } elseif ($user['admin_active'] == "Disable") {
                                                    echo ' class="bg-orange mystatus btn">Disable</span>';
                                                } elseif ($user['admin_active'] == "Enable") {
                                                    echo ' class="bg-green mystatus btn">Enable</span>';
                                                } else {
                                                    echo 'class="bg-red mystatus btn ">Deleted</span>';
                                                }
                                                ?>
                                        </td>
                                        <td>   <a href="<?php echo base_url('admin').'/edit/'.$user['adminid'] ?>"> <button   class="btn btn-success"  data-email="<?php echo $user['adminemail']; ?>" > <i class="fa fa fa-pencil"></i></button></a>
                                          <!---  <button  class="btn mystatus1 btn-primary"  data-id="<?php echo $user['adminid']; ?>" ><i class="fa fa fa-trash"></i></button>--->
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td></td>
                                <td>No record found</td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>



</div>


<div class="modal fade bs-example-modal-sm" id="modal_status" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">User Status</h4>
            </div>
            <form action="<?php echo base_url("admin/change_status"); ?>" method="post">
                <div class="modal-body" id="new_radio">


                </div>
                <input type="hidden" id="uid" name="userid" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>    

        </div>
    </div>
</div>
<!-- /modals -->

<!-----modal---->
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <form action="<?php echo base_url('admin').'/delete'; ?>" method="post">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure want to Delete?</p>
          
             <input type="hidden" id="uid1" name="userid1" value="">
          <p id="bookId"></p>
        </div>
        <div class="modal-footer">
             <button type="submit" class="btn btn-default bg-green pull-left">Delete</button>
          <button type="button" class="btn btn-default bg-danger pull-right" data-dismiss="modal">Close</button>
        </div>
          </form>
      </div>
      
    </div>
  </div>
  <!---modal--->

<!-- compose -->
<div class="compose col-md-6 col-xs-12">
    <div class="compose-header">
        Email
        <button type="button" class="close compose-close">
            <span>×</span>
        </button>
    </div>
    <form action="<?php echo base_url("user/send_mail") ?>" method="post">
        <div class="compose-body">
            <div id="alerts"></div>

            <br/>
            <br/>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control" required="" placeholder="Email" id="emailid" name="email" type="text">
                </div>
            </div>
            <br/>
            <br/>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Subject</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control" required="" placeholder="Subject" name="subject" type="text">
                </div>
            </div>
            <br/>
            <br/>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Message</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control" required="" rows="3" name="message" placeholder="Message"></textarea>
                </div>

            </div>

        </div>
        <br/>
        <br/>
        <div class="compose-footer">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
            <div class="col-md-9 col-sm-9 col-xs-12">

                <button  id="send" class="btn btn-sm btn-success right" type="submit">Send</button>
            </div>
        </div>
    </form>
</div>
<!-- /compose -->
<!-- /page content -->
<?php echo $footer; ?> 

<!-- Datatables -->
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../design/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Datatables -->
<!-- compose -->
<script>
    $('#compose, .compose-close').click(function () {

        var email = $(this).data("email");
        $("#emailid").val(email);
        $('.compose').slideToggle();
    });</script>
<!-- /compose -->
<script>
    $(".mystatus1").click (function () {
     var myBookId = $(this).data('id');
    
     $(".modal-body #uid1").val( myBookId );
     $("#myModal").modal();
});
    </script>
<script>



    $(".mystatus").click(function () {


        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        $('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Pending" value="Pending"> Pending</div>';
        
        html += '<div class="col-md-2"><input type="radio"  name="status" class="myradio" id="Disable"  value="Disable"> Disable</div>';
        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Enable" value="Enable"> Enable</div>';
        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> Delete</div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#uid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });



    $(document).ready(function () {
        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                    ],
                    responsive: true
                });
            }
        };
        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();
        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
            keys: true
        });
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });
        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });
        var $datatable = $('#datatable-checkbox');
        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();
    });
</script>
<!-- /Datatables -->