<?php echo $header; ?>
<style>
    .hide_for_now{
        display: none;
    }
</style>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="mdi mdi-table"></i>&nbsp;Attendance</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active" title="Attendance">Attendance</li>
                        </ol>
                    </div>
                    <div class="">
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="col-12">
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                    
                    <form class="form-material row" method="post" action="<?php echo base_url('attendance'); ?>">
                        <div class="col-md-4 col-sm-6 col-xs-12" style="display:inline-block;">
                            <div class="row form-material">
                                <div class="col-md-12">
                                    <label class="m-t-20">Choose From Date</label>
                                    <div class="time_inputbox">
                                        <input type="text" title="Choose From Date" name="fromdate" class="form-control" placeholder="<?php echo date("m-d-Y"); ?>" id="mdate">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12" style="display:inline-block;">
                            <div class="row form-material">
                                <div class="col-md-12">
                                    <label class="m-t-20">Choose To Date</label>
                                    <div class="time_inputbox"><input type="text" title="Choose To Date" name="todate" class="form-control"  placeholder="<?php echo date("m-d-Y"); ?>" id="mdate1">
                                         <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="display:inline-block;">
                        <br><br>
                        <div class="row form-material">
                            <div class="col-md-12" >
                               
                                <center><button title="Search" class="btn pull-right" style="background-color: #009efb;color: white;">Search</button></center>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <br>
                <br>
                <?php if($divtoshow)
                {
                ?>
                <div class="card">
                    <div class="card-body">
                        <!--<input type="button" value="Go Fullscreen" id="fsbutton" />-->
                        <div class="table-responsive" id="testgrid1">
                             <div id="grid_attendance"></div>
                        </div>
                    </div>
                </div>
                
                <?php
                }
                ?>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<?php echo $footer; ?>
<script>

        $( document ).ready(function() {
        
        var intit = '<?php echo isset($fromdate)?1:0; ?>';
        if(intit == '1')
        {
            var todate = "<?php echo isset($todate)?$todate:''; ?>";
            var fromdate = "<?php echo isset($fromdate)?$fromdate:''; ?>";
            
            var crudServiceBaseUrl = "<?php echo base_url(); ?>";
//            console.log("asd");
//            $.ajax({
//              url: crudServiceBaseUrl + "attendance/get_data/",
//              data: { 
//                     'fromdate': fromdate, 
//                     'todate': todate 
//              },
//              dataType: "jsonp",
//              type: 'POST',
//              success: function(result) {   
//                console.log("asd123");
//                //console.log(result);
//                //generateGrid(result);
//              }
//            });

            $.ajax({
                type: "POST",
                url: crudServiceBaseUrl + "attendance/get_data/",
                data:{
                    fromdate:fromdate,
                    todate:todate,
                },
                success: function(result){
                   generateGrid(result);
                }
            });
            
            $('#mdate1').val(todate);
            $('#mdate').val(fromdate);
            
            $('#mdate').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false,maxDate : new Date() })
            .on('change', function(e, date) { 
                var dt = formatDate(date);
                $('#mdate1').val(dt);
                $('#mdate1').bootstrapMaterialDatePicker('setMinDate', date);
            });
            var dt1 = strtodate(fromdate);
            $('#mdate1').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false, maxDate : new Date()});
            $('#mdate1').bootstrapMaterialDatePicker('setMinDate', dt1);
            
//            var users = <?php //echo json_encode($users); ?>;        
//            var field_string = "";
//            $.each(users, function( index, value ) {
//                 field_string += value.user_firstname+"_"+ value.user_lastname+" : {},";
//            });
//            console.log(field_string);
            
//            dataSource = new kendo.data.DataSource({
//                transport: {
//                    read:  {
//                        url: crudServiceBaseUrl + "attendance/get_import/",
//                        type: "POST",
//                        data: postData,
//                        dataType: "jsonp"
//                    },
//                    update: {
//                        url: crudServiceBaseUrl + "attendance/update_import/",
//                        type: "POST",
//                        dataType: "jsonp"
//                    },
//                    parameterMap: function(options, operation) {
//                        if (operation !== "read" && options.models) {
//                            return {models: kendo.stringify(options.models)};
//                        }
//                    }
//                },
//                batch: true,
//                pageSize: 20,
//                schema: {
//                    model: {
//                        id: "date",
//                        fields: {
//                            date: { editable: false},
//                            ${field_string}
//                        }
//                    }
//                }
//            });
            
            
            
        }
        else
        {
            $('#mdate').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false,maxDate : new Date() })
            .on('change', function(e, date) { 
                 var dt = formatDate(date);
                 $('#mdate1').val(dt);
                 $('#mdate1').bootstrapMaterialDatePicker('setMinDate', date); 
            });
            $('#mdate1').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false,maxDate : new Date() });
        }
        });
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('-');
        }
        function strtodate(date)
        {
            var parts =date.split('-');
            var mydate = new Date(parts[2], parts[0] - 1, parts[1]); 
            return mydate;
        }
        
        function generateGrid(response) {
            
            var crudServiceBaseUrl = "<?php echo base_url(); ?>";
            var model = generateModel(response);
            var columns = generateColumns(response);

            var grid = $("#grid_attendance").kendoGrid({
              dataSource: {
                transport:{
                  read:  function(options){
                    options.success(JSON.parse(response).data);
                  },
                  update:  function(options){
                    //console.log(options);
                    $.ajax({
                        url: crudServiceBaseUrl + "attendance/set_data/",
                        dataType: 'json',
                        type: 'POST',
                        data: {
                            'attendance' : JSON.stringify(options.data),
                        },
                        success : function (result) {
                            options.success(result);
                        }
                    });
                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models) {
                        return {models: kendo.stringify(options.models)};
                    }
                }
                },
                pageSize: 20,
                
                schema: {
                  model: model
                }
              },
                columns: columns,
                editable:true,
                navigatable: true,
                pageable: true,
                dataBound: function() {
                    for (var i = 0; i < this.columns.length; i++) {
                      this.autoFitColumn(i);
                    }
                  },
                height: 550,
                toolbar: [
                    { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
                    { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
                    {template: '<span id="grid_fullscreen1" class="k-button" href="\\#" onclick="buttonClickHandler1()"><i class="fas fa-expand"></i> &nbsp; Full Screen</span>'}],
            });
      }
      
      function generateColumns(response){
        var columnNames = JSON.parse(response).columns;
//        return columnNames.map(function(key, name){
         return   $.map(columnNames, function(key, name) {
             console.log(key);
             console.log(name);
          return { field: name, title: key , format: (name=='date'? "{0:yyyy-MM-dd}" : ""),width: (name=='date'? "250px" : "100px") };
        })
      }
      $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
            var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
            var event = state ? 'FullscreenOn' : 'FullscreenOff';
             if(state)
             {
                 $('#grid_attendance').css('height','100%');
                 $('#testgrid1').css('height','100%');
                 $('#grid_fullscreen1').addClass('hide');
                 setTimeout(function(){ 
                     $('#grid_attendance').data('kendoGrid').refresh();
                 }, 200);       
             } 
             else
             {
                 $('#grid_fullscreen1').removeClass('hide');
                 $('#grid_attendance').css('height','550px');
                 $('#testgrid1').css('height','550px');
                 setTimeout(function(){ 
                     $('#grid_attendance').data('kendoGrid').refresh();
                 }, 200);
             }
      });
      function buttonClickHandler1()
        {       
            var elem = document.getElementById("testgrid1");
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            }               
        }
      function generateModel(response) {
          
        //console.log(JSON.parse(response).data[0]);
        var sampleDataItem = JSON.parse(response).data[0];

        var model = {};
        var fields = {};
        for (var property in sampleDataItem) {
          
          if(property.indexOf("date") !== -1){
            model["id"] = property;
          }
          var propType = typeof sampleDataItem[property];
          
          if (propType === "number" ) {
            fields[property] = {
              type: "boolean",
            };
//            if(model.id === property){
//              fields[property].editable = false;
//              fields[property].validation.required = false;
//            }
          } else if (propType === "boolean") {
            fields[property] = {
              type: "boolean"
            };
          } else if (propType === "string") {
            var parsedDate = kendo.parseDate(sampleDataItem[property]);
            if (parsedDate) {
              fields[property] = {
               
                editable: false,
                validation: {
                  required: true
                }
              };
             // isDateField[property] = true;
            } else {
              fields[property] = {
                validation: {
                  required: true
                }
              };
            }
          } else {
            fields[property] = {
              validation: {
                required: true
              }
            };
          }
        }

        model.fields = fields;

        return model;
      }
</script>