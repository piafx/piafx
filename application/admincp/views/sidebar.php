  <!-- Left navbar-header -->
  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
      <div class="user-profile">
        <div class="dropdown user-pro-body">
            <div><img src="<?php echo base_url().'uploads/user/'.$logo; ?>" alt="user-img" class="img-circle"></div>
          <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $name; ?><span class="caret"></span></a>
              <ul class="dropdown-menu animated flipInY">
                <li><a href="<?php echo base_url().'changepassword' ?>"><i class="ti-settings"></i> Change Password</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="<?php echo base_url().'profile' ?>"><i class="ti-user"></i> Profile</a></li>
                <li role="separator" class="divider"></li>
                <li><a data-toggle="modal" data-target="#myModall" style="cursor: pointer"><i class="fa fa-power-off"></i> Logout</a></li>
              </ul>
        </div>
      </div>
      
      <ul class="nav" id="side-menu">
        
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <!-- input-group -->
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li><a href="<?php echo base_url('admin') ?>" class="waves-effect"><i class="icon-drawar fa-fw"></i> <span class="hide-menu">Dashboard</a></li>
        
        <li><a href="<?php echo base_url('employee') ?>" class="waves-effect"><i class="icon-people fa-fw"></i> <span class="hide-menu">Employee</a></li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-people fa-fw"></i> <span class="hide-menu">User Management<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="<?php echo base_url().'adduser' ?>">Add User</a></li>
                        <li> <a href="<?php echo base_url().'experience' ?>">Experience</a></li>
                        <li> <a href="<?php echo base_url().'education' ?>">Education</a></li>
                        
                        </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-briefcase fa-fw"></i> <span class="hide-menu">Holidays Management<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">        
                        <li> <a href="<?php echo base_url().'holidays' ?>">Holidays</a></li>
                         </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-link fa-fw"></i> <span class="hide-menu">Leave Management<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">        
                        
                        <li> <a href="#">Leave Quota</a></li>
                        <li> <a href="<?php echo base_url().'leave' ?>">Leave</a></li>
                        <li> <a href="<?php echo base_url().'allotment' ?>">Allotment</a></li>
                        
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-bulb fa-fw"></i> <span class="hide-menu">Employee Configuration<span class="fa arrow"></span></span></a>
                   <ul class="nav nav-second-level">       
                        
                        <li> <a href="<?php echo base_url().'fields' ?>">Fields</a></li>
                        <li> <a href="<?php echo base_url().'configuration' ?>">Configurations</a></li>
                        
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-directions fa-fw"></i> <span class="hide-menu">Company Profile<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">       
                        <li> <a href="<?php echo base_url().'companyprofile' ?>">Company Profile</a></li>
                        <li> <a href="<?php echo base_url().'department' ?>">Department</a></li>
                        <li> <a href="<?php echo base_url().'shift' ?>">Shift</a></li>
                        <li> <a href="<?php echo base_url().'mail_config' ?>">Mail Configuration</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-docs fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">   
<!--                        <li> <a href="<?php echo base_url().'report' ?>">Report</a></li>-->
                        <li> <a href="<?php echo base_url().'reportsalary' ?>">Salary Report</a></li>
                        <li> <a href="<?php echo base_url().'reportpt' ?>">PT Report</a></li>
                        <li> <a href="<?php echo base_url().'reportleave' ?>">Leave Report</a></li>
                    </ul>
                </li>
                
                
                <li><a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money fa-fw"></i>   <span class="hide-menu">Manage Salary<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        
                        <li> <a href="<?php echo base_url().'attendance' ?>">Attendance Management</a></li>
                        <li> <a href="<?php echo base_url().'timesheet' ?>">Time sheet Management</a></li>
                        <li> <a href="<?php echo base_url().'calculatesalary' ?>">Calculate Salary</a></li>                        
                        <li> <a href="<?php echo base_url().'salary' ?>">Salary</a></li>
                        <li> <a href="<?php echo base_url().'slipsalary' ?>">Salary Slip</a></li>
                    </ul>
                </li>
                <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-grid fa-fw"></i>   <span class="hide-menu">Manage Activity<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        <li> <a href="<?php echo base_url().'logs' ?>">Activity</a></li>
                    </ul>
                </li>
      </ul>
    </div>
  </div>
  <!-- Left navbar-header end -->
  <div class="modal fade" id="myModall" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Logout?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure want to Logout?</p>
          
             <input type="hidden" id="uid1" name="userid1" value="">
          <p id="bookId"></p>
        </div>
        <div class="modal-footer">
             <a href="<?php echo base_url('admin/logout'); ?>"><input  type="button" value="Logout" class="btn btn-default bg-green pull-left"></a>
          <button type="button" class="btn btn-default bg-danger pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div></div>