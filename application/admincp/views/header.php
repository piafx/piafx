<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!--<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon.png">-->
    <title><?php echo $title; ?>| <?php echo $section_title ?> </title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--c3 CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/c3-master/c3.min.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2019.1.115/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2019.1.115/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2019.1.115/styles/kendo.material.mobile.min.css" />
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/pages/file-upload.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/pages/float-chart.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo base_url(); ?>css/pages/dashboard1.css" rel="stylesheet">
    
    <link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url(); ?>css/colors/default.css" id="theme" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/datatables/media/css/dataTables.bootstrap4.css">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet"> 
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHD6LqxWqk4ML3HlIU5rvNOcCYVbDhWHQ&callback=InitializeMap"></script>

<script language="javascript" type="text/javascript">

//    var map;
//    var geocoder;
//    function InitializeMap() {
//
//        var latlng = new google.maps.LatLng(42.719870163466545, -75.75532322499993);
//        console.log(latlng);
//        var myOptions =
//        {
//            zoom: 6,
//            center: latlng,
//            mapTypeId: google.maps.MapTypeId.ROADMAP,
//            disableDefaultUI: true
//        };
//        var infoWindow = new google.maps.InfoWindow();
//        var latlngbounds = new google.maps.LatLngBounds();
//        map = new google.maps.Map(document.getElementById("map"), myOptions);
//        // google.maps.event.addListener(map, 'click', function (e) {
//        //     alert("Latitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());
//        // });
//        // var myMarker = new google.maps.Marker({
//        //     position: new google.maps.LatLng(47.651968, 9.478485),
//        //     draggable: true
//        // });
//        // map.setCenter(myMarker.position);
//        // myMarker.setMap(map);
//        var marker = new google.maps.Marker({
//            map: map,
//            draggable: true,
//            position: latlng
//        });
//        map.setCenter(marker.position);
//        marker.setMap(map);
//        google.maps.event.addListener(marker, 'dragend', function (e) {
//            document.getElementById("addressinput1").value = e.latLng.lat();
//            document.getElementById("addressinput2").value = e.latLng.lng();
//          });
//    }
//
//    function FindLocatioon() {
//        geocoder = new google.maps.Geocoder();
//
//        var latlng = new google.maps.LatLng(42.719870163466545, -75.75532322499993);
//        console.log(latlng);
//        var myOptions =
//        {
//            zoom: 6,
//            center: latlng,
//            mapTypeId: google.maps.MapTypeId.ROADMAP,
//            disableDefaultUI: true
//        };
//        var infoWindow = new google.maps.InfoWindow();
//        var latlngbounds = new google.maps.LatLngBounds();
//        map = new google.maps.Map(document.getElementById("map"), myOptions);
//
//        var address1 = document.getElementById("addressinput1").value;
//        var address2 = document.getElementById("addressinput2").value;
//        var address = address1 +','+ address2;
//        geocoder.geocode({ 'address': address }, function (results, status) {
//            if (status == google.maps.GeocoderStatus.OK) {
//                map.setCenter(results[0].geometry.location);
//                var infoWindow = new google.maps.InfoWindow();
//                var latlngbounds = new google.maps.LatLngBounds();                
//                var marker = new google.maps.Marker({
//                    map: map,
//                    draggable: true,
//                    position: results[0].geometry.location
//                });
//                google.maps.event.addListener(marker, 'dragend', function (e) {
//                  document.getElementById("addressinput1").value = e.latLng.lat();
//                  document.getElementById("addressinput2").value = e.latLng.lng();
//                });
//            }
//            else {
//                alert("Geocode was not successful for the following reason: " + status);
//            }
//        });
//
//    }
//
//
//    function Button1_onclick() {
//        FindLocatioon();
//    }
//
//    window.onload = InitializeMap;

</script>
<style>
        form p:last-child{
            color: red;
        }
        .topbar {
            background : #7e8c97 !important
        }
        .topbar .navbar-header{
            background: #7e8c97 !important
        }
        .hide
        {
            display: none !important;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice
        {
            background-color: #398bf7;
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
            color: #ffffff;
        }
</style>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?php echo $title; ?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">
                        <!-- Logo icon --><b><img src="<?php echo base_url(); ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
<!--                            <img src="<?php // echo base_url(); ?>assets/images/logo-icon.png" alt="homepage" class="dark-logo" />-->
                            <!-- Light Logo icon -->
                            <!--<img src="<?php echo base_url(); ?>assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />-->
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span style="font-weight: 500;font-size: 20px;"> <?php echo $title; ?> </span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="bell_anchor"><i class="fa fa-bell"></i></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user" id="new_notifications">
                                    
                                    <!--<li><a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>-->
                                    <!--<li role="separator" class="divider"></li>-->
                                    <!--<li><a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>-->
                                    
                                </ul>
                                <p class="no_noti">No notification available</p>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('admin_data')['name']; ?></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?php echo $this->session->userdata('admin_data')['name']; ?></h4>
                                                <p class="text-muted"><?php echo $this->session->userdata('admin_data')['email']; ?></p><a href="<?php echo base_url('profile') ?>" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
<!--                        <li class="user-profile"> <a class="waves-effect waves-dark" href="#" aria-expanded="false">
                            <span class="hide-menu"><?php echo $this->session->userdata('admin_data')['name']; ?></span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php //echo base_url('profile'); ?>">My Profile </a></li>
                                <li><a href="<?php //echo base_url('admin/logout'); ?>">Logout</a></li>
                            </ul>
                        </li>-->
                        <!--<li class="nav-devider"></li>-->
                         <li class="<?php echo (($this->uri->segment(1) == 'admin')?'active':''); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'admin')?'active':''); ?>"  title="Dashboard" href="<?php echo base_url('admin'); ?>"><i class="mdi mdi-gauge"></i> <span class="hide-menu">Dashboard</span> </a></li>
                         <li class="<?php echo (($this->uri->segment(1) == 'markets')?'active':''); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'markets')?'active':''); ?>"  title="COT Data" href="<?php echo base_url('markets'); ?>"><i class="mdi mdi-chart-bar"></i> <span class="hide-menu">COT Data</span> </a></li>
                         <li class="<?php echo (($this->uri->segment(1) == 'pricedata')?'active':''); ?> <?php echo ((in_array('pricedata_index', $permission_list))?'':'hide'); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'pricedata')?'active':''); ?>"  title="Price Data" href="<?php echo base_url('pricedata'); ?>"><i class="ti-money"></i> <span class="hide-menu">Price Data</span> </a></li>
                         <li class="<?php echo (($this->uri->segment(1) == 'economicalindicator')?'active':''); ?> <?php echo ((in_array('economicalindicator_index', $permission_list))?'':'hide'); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'economicalindicator')?'active':''); ?>"  title="Economical Indicator" href="<?php echo base_url('economicalindicator'); ?>"><i class="ti-clip"></i> <span class="hide-menu">Economical Indicator</span> </a></li>
                         <li class="<?php echo (($this->uri->segment(1) == 'user')?'active':''); ?> <?php echo ((in_array('user_index', $permission_list))?'':'hide'); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'user')?'active':''); ?>"  title="Users" href="<?php echo base_url('user'); ?>"><i class="ti-user"></i> <span class="hide-menu"> Users </span> </a></li>
                         <li class="<?php echo (($this->uri->segment(1) == 'setting')?'active':''); ?> <?php echo ((in_array('setting_index', $permission_list))?'':'hide'); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'setting')?'active':''); ?>"  title="Setting" href="<?php echo base_url('setting'); ?>"><i class="fa fa-cog"></i> <span class="hide-menu">Setting</span> </a></li>

                         <li class="<?php echo (($this->uri->segment(1) == 'emailtemplate')?'active':''); ?> <?php echo ((in_array('emailtemplate_index', $permission_list))?'':'hide'); ?>"><a class="waves-effect waves-dark <?php echo (($this->uri->segment(1) == 'emailtemplate')?'active':''); ?>" title="Email Template" href="<?php echo base_url('emailtemplate'); ?>"><i class="fa fa-envelope"></i> <span class="hide-menu">Email Template</span> </a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>