<?php echo $header; ?>
<?php echo $sidebar; ?>
 <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Add Salary Configuration</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" >  
            <div class="col-lg-3 col-sm-8 col-md-8 col-xs-12 pull-right">
                    <a href="<?php echo base_url('configuration'); ?>" class="btn btn-info pull-right">< Back</a>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="col-xs-12">
              <?php
              if ($this->session->flashdata('message'))
                 {
                  ?>
                  <!--  start message-red -->
                  <div class="box-body" id="modal_msg">
                      <div class=" alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?>
                      </div>
                  </div>
                  <!--  end message-red -->
              <?php } ?>
              <?php
              if ($this->session->flashdata('success'))
               {
                  ?>
                  <!--  start message-green -->
                  <div class="alert alert-success alert-dismissable" id="modal_msg">
                      <button type="button"     class="close" data-dismiss="alert" aria-hidden="true">x</button>
                      <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                  <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <!--  end message-green -->
          <?php } ?>
          </div>
         <div class="col-sm-12">
          <div class="white-box">
            
              <form class="form-horizontal" method="post" action="<?php echo base_url().'configuration/add_data' ?>" data-toggle="validator">
              <div class="form-group">
                
                <div class="col-md-12">
                    <label>Employee Name</label>
                    <select class="form-control select2" name="name1" required="" data-toggle="validator">
                            <?php
                            foreach ($emp as $a)
                            {
                            ?>  
                            <option value="<?php echo $a['employee_id']?>"><?php echo $a['fname'].' '.$a['lname']; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    <span style="color:red"><?php echo form_error('name'); ?></span>    
                </div>
                  
               
              
              </div>
                  
                  
                  <div class="form-group" style="padding: 10px;">
                      
                            <?php
                            foreach ($fields as $a)
                            {
                            ?>  
                  
                  <fieldset>
                      <legend><?php echo $a['name']; ?></legend>
                     
                  <div class="col-md-12">
                  
                
                <div class="col-md-4">
                    <label>type</label>
                    <select class="form-control select2" name="type_<?php echo $a['field_id'] ?>" required="" data-toggle="validator">
                             
                            <option value="Percentage">Percentage</option>
                            <option value="Flat">Flat</option>
                            
                        </select>
                    <span style="color:red"><?php echo form_error('type'); ?></span>    
                </div>
                  
               
              
              
             
                  
             
                
                <div class="col-md-4">
                    <label class="col-md-12" for="example-email">Value</label>
                    <input type="text" class="form-control" placeholder="Value" name="value_<?php echo $a['field_id'] ?>" required="required" value="<?php echo set_value('value_$a[field_id]'); ?>">
                    <span style="color:red"><?php echo form_error('value_$a[field_id]'); ?></span>    
                </div>
             
                  
               
                <div class="col-md-4">
                    <label>reference</label>
                         <select class="form-control select2" name="reference_<?php echo $a['field_id'] ?>" required="" data-toggle="validator">
                             
                            <option value="0">Base Salary</option>
                            <option value="1">Total Salary</option>
                            
                        </select>
                    <span style="color:red"><?php echo form_error('reference'); ?></span>    
                </div>
              
                  </div>
                  </fieldset>
                      <br>
                      <br>
                      
                  
                  <?php
                            }
                            ?>
                  </div>
                <div class="form-group">
                
                <div class="col-md-12">
                    <input type="submit" name="submit" class="btn btn-info" value="Submit">
                </div>
              </div>
             
            </form>
          </div>
        </div>
      </div>
    </div>
 </div>

<script src="<?php echo base_url() ?>../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() ?>../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>../plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />

<script>

// Date Picker
   $(".select2").select2();
    $('.selectpicker').selectpicker();
     
</script>
<script>
    
      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<?php echo $footer; ?>