<?php echo $header; ?>
<script src="<?php echo base_url(); ?>../ckeditor/ckeditor.js"></script>

  <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                            <li class="breadcrumb-item" title="Email Template"><a href="<?php echo base_url('emailtemplate');?>">Email Template</a></li>
                            <li class="breadcrumb-item active" title="Edit Email Template">Edit Email Template</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
              

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Edit Email Template</h2>
                                   <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <br>

                                    <!-- form start -->
                                    <form id="editEmailForm" method="POST" action="<?= site_url('emailtemplate/update') ?>" enctype="multipart/form-data" class="form-material m-t-40">
                                        <input type="hidden" id="emailid" name="emailid" value="<?php echo base64_encode($emailformat[0]['id']) ?>">
                                        

                                        <div class="box-body">
                                           <!--  <div class="col-md-6"> -->

                                                <div class="form-group">
                                        <label>Title</label> <span class="required" style="color:red">*</span>
                                        <input type="text" required="required" name="title" readonly="" class="form-control " id="title"  value="<?php echo $emailformat[0]['title'] ?>"> 
                                     <span id="spantitleerr" style="display: none;color:red"> Please Enter the Title </span></div>
                                          
                                                <div class="form-group"> 
                                                    <label>Subject</label> <span class="required" style="color:red">*</span>
                                                    <input required="required"  type="text" name="subject" id="subject" class="form-control" value="<?php echo $emailformat[0]['subject'] ?>">
                                                   <span id="spansubjerr" style="display: none;color:red"> Please Enter the Subject </span>
                                                </div>
                                             <!--    </div> -->
                                                
                                                  <div class="form-group">
                                                    <label>Variables</label>
                                                    <br>
                                                    <div style="background: #80808066;padding: 10px;"> <span class="help-block"><?php echo $emailformat[0]['variables'] ?></span> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email Format</label>
                                                    <textarea class="form-control ckeditor"  name="mailformat" id="mailformat" ><?php echo ($emailformat[0]['mailformat']) ?></textarea>
                                                </div>
                                               <div class="box-footer">
                                                <button type="submit" id="btnsubmit" name="btn"  class="btn btn-success waves-effect waves-light m-r-10 signin_btn subbtn" Value="Submit" title="Update">Update</button>
                                               <!--<input type="submit" name="btn" id="btnsubmit" class="btn btn-success signin_btn subbtn  " Value="Submit"/>                        -->
                                                <a href="<?php echo site_url('emailtemplate'); ?>"><button class="btn btn-inverse" type="button" title="Cancel">Cancel</button></a>

                                            </div>
                                                
                                                </form>

                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

<script>
    $(document).ready(function () {
        $("#btnsubmit").click(function(){
            var title = $("#title").val();
            var subject = $("#subject").val();
            if(title == ''){
                $("#spantitleerr").show();
                return false;
            }
            else{
                $("#spantitleerr").hide();
            }
            if(subject == ''){
                $("#spansubjerr").show();
                return false;
            }
            else{
                $("#spansubjerr").hide();
            }
            
        });
    });
</script>
<?php echo $footer; ?>