
<?php echo $header; ?>

<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                            <li class="breadcrumb-item active" title="Email Template">Email Template</li>
                        </ol>
                    </div>
                    <div>
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                          <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
<!--                                <div class="col-md-12">-->
                                    <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                        <h2 class="card-title"><i class="fa fa-envelope"></i>&nbsp;Email Template</h2>
                                    </div>
                                   
                                <!--</div>-->
                                <div class="table-responsive">
                                                        
                                         <?php if (!empty($emailformat)) { ?>
                                    <div class="boxcontent"> <?php echo form_open('storage/delete2/', array('class' => 'form-horizontal', 'id' => 'frmdelete', 'method' => 'post', 'name' => 'frmdelete')); ?>
                                    <table id="datatable" class="table table-striped table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <!--<th>Subject</th>-->
                                            <th class="<?php echo ((!in_array('emailtemplate_edit', $permission_list))?'hide':''); ?>">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($emailformat); $i++) { ?>
                                            <tr <?php if (($i + 1) % 2 == 0) { ?>class='alternate-row'<?php } ?>>
                                                <td><?php echo stripslashes($emailformat[$i]['title']); ?></td>
                                                <!--<td><?php // echo stripslashes($emailformat[$i]['subject']); ?></td>-->
                                                <td class="center <?php echo ((!in_array('emailtemplate_edit', $permission_list))?'hide':''); ?>"> 
                                                    
                                                    <a class="<?php echo ((!in_array('emailtemplate_edit', $permission_list))?'hide':''); ?>" title="Edit" href="<?php echo site_url('emailtemplate/edit/' . $emailformat[$i]['id']); ?>"> <i class="fa fa-pencil-alt"></i></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php echo form_close(); ?> </div>
                                <?php } else { ?>
                                    <!--  start message-yellow -->
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        No Record Found.
                                    
                                    </div>
                                    <!--  end message-yellow -->
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Status</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body">
                <div>
                    <input name="group1" type="radio" id="radio_1" checked />
                    <label for="radio_1">Activate</label>
                    <input name="group1" type="radio" id="radio_2" />
                    <label for="radio_2">Deactivate</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

<!-- /page content -->
<?php echo $footer; ?> 
<script>
            $(document).ready(function() {
                var table = $('#datatable').DataTable({
                    "columnDefs": [
                        { orderable: false, targets: -1 }
                     ]
                });
            });
</script>