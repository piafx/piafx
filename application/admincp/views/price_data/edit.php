<?php echo $header; ?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active" title="Price Data"><a href="<?php echo base_url('price_data') ?>">Price Data</a></li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Edit Price Data</h2>
                                <form class="form-material m-t-40" method="post" action="<?php echo base_url('price_data/edit_data'); ?>">
                                    <div class="form-group">
                                        <label>Symbol  <span style="color: red">*</span></label>
                                        <input type="hidden" value="<?php echo base64_encode($data['id']) ?>" name="id" >
                                        <input type="hidden" value="<?php echo base64_encode($data['ticker_id']) ?>" name="ticker_id" >
                                        <input title="Symbol" name="symbol" type="text" class="form-control form-control-line" readonly="readonly" required="" value="<?php echo $edit_id!=''?$data['symbol']:set_value('symbol'); ?>"> 
                                        <?php echo form_error('symbol'); ?>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Date <span style="color: red">*</span></label>
                                        <input title="Date" readonly="readonly"  name="date" type="text" class="form-control form-control-line" required="" value="<?php echo $edit_id!=''?$data['date']:set_value('date'); ?>"> 
                                        <?php echo form_error('date'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Open <span style="color: red">*</span></label>
                                        <input title="Open"  name="open" type="text" class="form-control form-control-line" required="" value="<?php echo $edit_id!=''?$data['open']:set_value('open'); ?>"> 
                                        <?php echo form_error('open'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">High <span style="color: red">*</span></label>
                                        <input title="High"  type="text" name="high" class="form-control" required="" value="<?php echo $edit_id!=''?$data['high']:set_value('high'); ?>">
                                        <?php echo form_error('high'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">Low <span style="color: red">*</span></label>
                                        <input title="Low"  type="text" name="low" class="form-control" required="" value="<?php echo $edit_id!=''?$data['low']:set_value('low'); ?>">
                                        <?php echo form_error('low'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">Close</label>
                                        <input title="Close"  type="text" name="close" class="form-control number" value="<?php echo $edit_id!=''?$data['close']:set_value('close'); ?>"  >
                                        <?php echo form_error('close'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="example-email">Volume</label>
                                        <input title="Volume"  type="text" name="volume" class="form-control number" value="<?php echo $edit_id!=''?$data['volume']:set_value('volume'); ?>">
                                        <?php echo form_error('volume'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">Open Interest</label>
                                        <input title="Open Interest"  type="text" name="openinterest" class="form-control number" value="<?php echo $edit_id!=''?$data['openinterest']:set_value('openinterest'); ?>">
                                        <?php echo form_error('openinterest'); ?>
                                    </div>

                                        <button title="Update"  type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                                        <a href="<?php echo base_url('price_data'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<?php echo $footer; ?>