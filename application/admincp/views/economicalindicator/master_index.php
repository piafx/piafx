<?php echo $header; ?>
<link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Economical Indicator Master</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="ti-money"></i>&nbsp;Economical Indicator Master</h2>
                                </div>
                                
                                <div class="table-responsive m-t-40" id="ecin_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    

<?php echo $footer; ?>
<script>
    
    
    getallenic_data();
    function getallenic_data()
    {
        $('.preloader').show();

        var grid = $("#ecin_data").data("kendoGrid");
        if(grid){
            $("#ecin_data").data().kendoGrid.destroy();
            $("#ecin_data").empty();
        }

        var crudServiceBaseUrl = "<?php echo base_url();?>"+ "ecinmaster/getecinall",
        dataSource = new kendo.data.DataSource({
            type: "odata",
            transport: {
                read:  {
                    url: crudServiceBaseUrl,
                },
                update: {
                    url: "<?php echo base_url();?>"+ "ecinmaster/updateecin_data",
                    type: "POST",
                    dataType: "json"
                },
                create: {
                    url: "<?php echo base_url();?>"+ "ecinmaster/createnew",
                    type: "POST",
                    dataType: "json"
                },
                destroy: {
                    url: "<?php echo base_url();?>"+ "ecinmaster/deleteecin",
                }
            },
            batch: true,
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            schema: {
                model: {
                    id: "_id",
                    fields: {
                        _id: { editable: false, nullable: true },
                        ecin: { validation: { required: true } },
                        country_name : { validation: { required: true } },
                        ecIn_code : {  },
                        description : { },
                        category : {  },
                        data_source1 : { validation: { required: true } },
                        data_source_code1 : { validation: { required: true } },
                        unit : {  },
                        multiplier : {  },
                        frequency : {  },
                        seasonally_Adjusted : {  },
                        official_source_organization : {  },

                    }
                }
            }
        });
        $("#ecin_data").kendoGrid({

            dataSource: dataSource,
            navigatable: true,
            pageable: true,
            sortable: true,
            filterable: {
                mode: "row",
                // extra: false,
            },
            height: 550,
            toolbar: [ { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
                { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
                { name: "create", iconClass: "fa fa-plus", text: "&nbsp; Create New" }, 
            ],
            columns: [
                { field: "ecin", title: "Economical Indicator",width:"180px"},
                { field: "ecIn_code", title: "ECIN code",width:"80px"},
                { field: "description", title: "Description",width:"110px"},
                { field: "unit", title: "Unit",width:"80px"},
                { field: "multiplier", title: "Multiplier",width:"50px"},
                { field: "frequency", title: "Frequency",width:"50px"},
                { field: "seasonally_Adjusted", title: "Seasonally Adjusted",width:"50px"},
                { field: "category", title: "Category",width:"80px"},
                { field: "country_name", title: "Country Name",editor: countryDropDownEditor,width:"110px"},
                { field: "official_source_organization", title: "Official Source Organization ", width:"110px"},
                { field: "data_source1", title: "Data Source",width:"60px"},
                { field: "data_source_code1", title: "Data Source Code",width:"160px"},
                { command: "destroy", title: "&nbsp;", width: 150 }
                
            ],
            editable: true,
            scrollable:true,
            filterable: true,
            sortable: true,
            // pageable: true,
             pageable: {
                alwaysVisible: false,
                pageSizes: [5, 10, 20, 100]
            },
        });
        $('.preloader').hide();
        
    }
    function countryDropDownEditor(container, options) {
                        console.log("container",container);
                        console.log("options",options.model.team_id);
                        var crudServiceBaseUrl = "<?php echo base_url(); ?>";
                        $('<input name="' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: true,
                                dataTextField: "name",
                                dataValueField: "name",
                                dataSource: {
                                    type: "odata",
                                    transport: {
                                        read: crudServiceBaseUrl + "ecinmaster/getallcountry/",
                                    }
                                }
                            });
                    }
    function deleteconfirm(url){
        var result = confirm("Want to delete?");
        if (result) {
            window.location.replace(url);
        }
    }
        
   

    
</script>