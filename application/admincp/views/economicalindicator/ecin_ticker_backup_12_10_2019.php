<?php echo $header; ?>
<link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Economical Indicator</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="ti-money"></i>&nbsp;Economical Indicator</h2>
                                </div>
                                
                                    <div class="col-md-12" style="display: inline-block;padding-left: 0px;">

                                        <div class="col-md-6" style="display: inline-block;padding-left: 0px;"> 
                                            <label>Choose Tickers</label>
                                            <div>
                                            
                                                <select name="ecin_name" id="currentticker" class="js-example-basic-single form-control">
                                                <option></option>
                                                    <?php 
                                                        foreach ($tickers as $tickervalue) {
                                                    ?>
                                                        <option <?php echo $selected_option== $tickervalue['_id']?'selected="selected"':''; ?> value="<?php echo $tickervalue['_id'] ?>"><?php echo $tickervalue['ticker_name'] ?></option>
                                                    <?php 
                                                        }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <label>Choose economic indicator</label>
                                            <div>
                                            
                                                <select name="ecin_name" id="currentenic" class="js-example-basic-single form-control" onchange="getcurrentenic_data(this.value)">
                                                <option></option>
                                                    <?php 
                                                        foreach ($enic as $value) {
                                                    ?>
                                                        <option <?php echo $selected_option== $value['_id']?'selected="selected"':''; ?> value="<?php echo $value['_id'] ?>"><?php echo $value['name'] ?></option>
                                                    <?php 
                                                        }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <label>Choose Country</label>
                                            <div id="select_country">
                                                <select id="currentcountry" name="country_name" class="form-control">
                                                 <option></option>
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="display: inline-block;padding-left: 0px;">
                                            <center>
                                            <label>&nbsp;</label>
                                            <div>
                                                <button class="btn btn-info" onclick="adddata()">
                                                    Save
                                                </button>
                                            </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="historical_div" style="display: inline-block;padding-left: 0px;margin-top: 10px">
                                            <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                                <label> <b>Choose File  : </b></label>
                                                <input type="file" name="historical_data">
                                            </div>
                                            <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <button class="btn btn-info">Upload File</button>

                                            <a download href="<?php echo base_url('sampleforecindata.csv') ?>"><button type="button" class="btn btn-info" style="float: right;"><i class="fa fa-download"></i> &nbsp; sample File</button>
                                            </div></a>
                                           
                                    </div>  
                                <div class="table-responsive m-t-40" id="ecin_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    

<?php echo $footer; ?>
<script>
    $('body').on('DOMNodeInserted', 'select', function () {

        if($(this).hasClass('form-control')){
            $(this).select2({
                placeholder: "Please select an option",
                allowClear: true
              });
        }
    });
    $(document).ready(function() {
          $('#currentenic').select2({
            placeholder: "Please select an option",
            allowClear: true
          });
          $('#currentcountry').select2({
            placeholder: "Please select an option",
            allowClear: true
          });
          $('#currentticker').select2({
            placeholder: "Please select an option",
            allowClear: true
          });

    })
    getcurrentenic_data();
    function getcurrentenic_data(enic_value){

        if (typeof enic_value === "undefined") 
        {
            enic_value = "<?php echo $selected_option ?>";
        }
        $('.preloader').show();
        $('#historical_div').hide();
        $('#ecin_data').hide();
        // alert(typeprovider+" "+currentticker)s3,b6,rl,tx,pa,ad,ph
        if(enic_value != ''){
            var main_url = '<?php echo base_url('economicalindicator/getallcountryperenic');?>';
            $.ajax({
                  type: 'POST',
                  url: main_url,
                  data: {'enic_id':enic_value},
                  success: function(resultData) { 
                        resultData = JSON.parse(resultData);
                        $('.preloader').hide();
                        var selected_option = '<?php echo $selected_country; ?>';
                        
                        finhtml = '<select name="country_name" id="currentcountry" class="form-control"><option></option>';  
                        $.each(resultData, function( index, value ) {
                            if(selected_option == value.countries[0]._id) {
                                finhtml += "<option selected value='"+value.countries[0]._id+"'>"+value.countries[0].name+"</option>";
                                getallenic_data(selected_option,enic_value);
                            }
                            else{
                                finhtml += "<option value='"+value.countries[0]._id+"'>"+value.countries[0].name+"</option>";
                            }
                        })
                        finhtml += '</select>';
                        $('#select_country').html(finhtml);
                  },
                  error: function(error){
                    $('.preloader').hide();
                    alert("Something went wrong");
                  }
            });
        }
        else{
            $('.preloader').hide();
        }
        

    }

    
    function adddata(){

            var currentticker = $("#currentticker").val();
            var currentenic = $("#currentenic").val();
            var country = $('#currentcountry').val();
            
            var main_url = '<?php echo base_url('economicalindicator/adddata');?>';

            if(currentticker != '' && currentenic != ''  && country != '' ) {
                $.ajax({
                      type: 'POST',
                      url: main_url,
                      data: {'ecin_id':currentenic,'ticker_id':currentticker,'country':country},
                      success: function(resultData) { 
                            resultData = JSON.parse(resultData);
                            $('.preloader').hide();
                            alert("Data saved successfully");  
                            $('#currentticker').val([]).trigger('change');
                            $('#currentenic').val([]).trigger('change');
                            $('#currentcountry').val([]).trigger('change');

                      },
                      error: function(error){
                        $('.preloader').hide();
                        alert("Something went wrong");
                      }
                });
            }else{
                alert("Please select required value.");
            }

    }

    
</script>