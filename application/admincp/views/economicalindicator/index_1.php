<?php echo $header; ?>
<link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">

        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Economical Indicator</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="ti-clip"></i>&nbsp;Economical Indicator</h2>
                                </div>
                                <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                    <a href="<?php echo base_url('ecinmaster'); ?>"><button class="btn btn-info add_new_user_btn" style="float: right;">ADD NEW</button></a>
                                </div>
                                <form method="post" action="<?php echo base_url('/economicalindicator/uploadhistorical'); ?>" enctype='multipart/form-data' >
                                    <div class="col-md-12" style="display: inline-block;padding-left: 0px;">

                                        <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <label>Choose economical indicator</label>
                                            <div>
                                            
                                                <select name="ecin_name" id="currentenic" class="js-example-basic-single form-control" onchange="getcurrentenic_data(this.value)">
                                                <option></option>
                                                    <?php 
                                                        foreach ($enic as $value) {
                                                    ?>
                                                        <option <?php echo $selected_option== $value['_id']?'selected="selected"':''; ?> value="<?php echo $value['_id'] ?>"><?php echo $value['name'] ?></option>
                                                    <?php 
                                                        }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <label>Choose Country</label>
                                            <div id="select_country">
                                                <select name="country_name" class="form-control">
                                                 <option>Select option</option>
                                                 </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="historical_div" style="display: inline-block;padding-left: 0px;margin-top: 10px">
                                            <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                                <label> <b>Choose File  : </b></label>
                                                <input type="file" name="historical_data">
                                            </div>
                                            <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                            <button class="btn btn-info">Upload File</button>

                                            <a download href="<?php echo base_url('sampleforecindata.csv') ?>"><button type="button" class="btn btn-info" style="float: right;"><i class="fa fa-download"></i> &nbsp; sample File</button>
                                            </div></a>
                                           
                                    </div>  
                                </form>   
                                <div class="table-responsive m-t-40" id="ecin_data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    

<?php echo $footer; ?>
<script>
    $('body').on('DOMNodeInserted', 'select', function () {

        if($(this).hasClass('form-control')){
            $(this).select2({
                placeholder: "Please select an option",
                allowClear: true
              });
        }
    });
    $(document).ready(function() {
        $('#currentenic').select2({
            placeholder: "Please select an option",
            allowClear: true
          });
    })
    getcurrentenic_data();
    function getcurrentenic_data(enic_value){

        if (typeof enic_value === "undefined") 
        {
            enic_value = "<?php echo $selected_option ?>";
        }
        $('.preloader').show();
        $('#historical_div').hide();
        $('#ecin_data').hide();
        // alert(typeprovider+" "+currentticker)s3,b6,rl,tx,pa,ad,ph
       
        var main_url = '<?php echo base_url('economicalindicator/getallcountryperenic');?>';
        $.ajax({
              type: 'POST',
              url: main_url,
              data: {'enic_id':enic_value},
              success: function(resultData) { 
                    resultData = JSON.parse(resultData);
                    $('.preloader').hide();
                    var selected_option = '<?php echo $selected_country; ?>';
                    
                    finhtml = '<select name="country_name" id="currentcountry" class="form-control" onchange="getallenic_data(this.value,'+enic_value+')"><option></option>';  
                    $.each(resultData, function( index, value ) {
                        if(selected_option == value.countries[0]._id) {
                            finhtml += "<option selected value='"+value.countries[0]._id+"'>"+value.countries[0].name+"</option>";
                            getallenic_data(selected_option,enic_value);
                        }
                        else{
                            finhtml += "<option value='"+value.countries[0]._id+"'>"+value.countries[0].name+"</option>";
                        }
                    })
                    finhtml += '</select>';
                    $('#select_country').html(finhtml);
              },
              error: function(error){
                $('.preloader').hide();
                alert("Something went wrong");
              }
        });

    }

    // getcurrentticker_data();
    function getallenic_data(value1='',enic_value='')
    {

        $('.preloader').show();
        if(value1 == '')
        {
            value1 = "<?php echo $selected_option ?>";
        }
        if(enic_value == '')
        {
            enic_value = "<?php echo $selected_country ?>";
        }

        var grid = $("#ecin_data").data("kendoGrid");
        if(grid){
            $("#ecin_data").data().kendoGrid.destroy();
            $("#ecin_data").empty();
        }

        var crudServiceBaseUrl = "<?php echo base_url();?>"+ "economicalindicator/getecin_data?country_id="+value1+"&ecin_id="+enic_value,
        dataSource = new kendo.data.DataSource({
            type: "odata",
            transport: {
                read:  {
                    url: crudServiceBaseUrl,
                },
                update: {
                    url: "<?php echo base_url();?>"+ "economicalindicator/update_data?country_id="+value1+"&ecin_id="+enic_value,
                    type: "POST",
                    dataType: "json"
                },
                create: {
                    url: "<?php echo base_url();?>"+ "economicalindicator/createnew?country_id="+value1+"&ecin_id="+enic_value,
                    type: "POST",
                    dataType: "json"
                },
            },
            batch: true,
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            schema: {
                model: {
                    id: "_id",
                    fields: {
                        _id: { editable: false, nullable: true },
                        date: { type: "date",validation: { required: true } },
                        value : { type: "number",validation: { required: true } },
                        consensus : { },
                        previous: { },
                        note: { },
                        service: { },
                    }
                }
            }
        });
        $("#ecin_data").kendoGrid({

            dataSource: dataSource,
            navigatable: true,
            pageable: true,
            sortable: true,
            filterable: {
                mode: "row",
                // extra: false,
            },
            toolbar: [ { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
                { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
                { name: "create", iconClass: "fa fa-plus", text: "&nbsp; Create New" },  
            ],
            columns: [
                { field: "date", title: "Date (yyyy-mm-dd)",width:"80px", format: "{0:yyyy-MM-dd}" ,  editor: function(container, options){ var input = $("<input/>"); input.attr("name", options.field); input.appendTo(container); input.kendoDateTimePicker({ format: "yyyy-MM-dd" });} },
                { field: "value", title: "Actual",width:"80px"},
                { field: "consensus", title: "Consensus",width:"80px"},
                { field: "previous", title: "Previous",width:"80px"},
                { field: "note", title: "Note",width:"80px"},
                { field: "service", title: "Service",width:"80px"},
            ],
            editable: true,
            scrollable:true,
            filterable: true,
            sortable: true,
            // pageable: true,
             pageable: {
                alwaysVisible: false,
                pageSizes: [5, 10, 20, 100]
            },
        });
        $('#historical_div').show();
        $('#ecin_data').show();
        $('.preloader').hide();
        
    }
    function deleteconfirm(url){
        var result = confirm("Want to delete?");
        if (result) {
            window.location.replace(url);
        }
    }


    
</script>