<?php echo $header; ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Economical Indicator</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-4" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="ti-clip"></i>&nbsp;Economical Indicator</h2>
                                </div>
                                <div class="col-md-12" style="display: inline-block;padding-left: 0px;">

                                    <div class="col-md-12" style="display: inline-block;padding-left: 0px;">
                                        <label>Choose economical indicator</label>
                                        <div>
                                        
                                            <select id="currentenic" class="js-example-basic-single form-control" onchange="getcurrentenic_data(this.value)">
                                                <?php 
                                                    foreach ($enic as $value) {
                                                ?>
                                                    <option <?php echo $selected_market== $value['_id']?'selected="selected"':''; ?> value="<?php echo $value['_id'] ?>"><?php echo $value['name'] ?></option>
                                                <?php 
                                                    }
                                                ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: inline-block;padding-left: 0px;">
                                        <label>Choose Country</label>
                                        <div id="select_country">
                                            <select class="form-control">
                                             <option>Select option</option>
                                             </select>
                                        </div>
                                    </div>
                                </div>    
                                <div class="table-responsive m-t-40">
                                    <table id="user_tab" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Value</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    

<?php echo $footer; ?>
<script>
    $('body').on('DOMNodeInserted', 'select', function () {
            $(this).select2();
    });
    
    getcurrentenic_data();
    function getcurrentenic_data(enic_value){

        if (typeof enic_value === "undefined") 
        {
            enic_value = "<?php echo $selected_market ?>";
        }
        $('.preloader').show();
        // alert(typeprovider+" "+currentticker)s3,b6,rl,tx,pa,ad,ph
       
        var main_url = '<?php echo base_url('economicalindicator/getallcountryperenic');?>';
        $.ajax({
              type: 'POST',
              url: main_url,
              data: {'enic_id':enic_value},
              success: function(resultData) { 
                    resultData = JSON.parse(resultData);
                    $('.preloader').hide();
                    var selected_option = '<?php echo $selected_option; ?>';
                    
                    finhtml = '<select id="currentcountry" class="form-control" onchange="getallenic_data(this.value,'+enic_value+')"><option>Select option</option>';  
                    $.each(resultData, function( index, value ) {
                        if(selected_option == value.countries[0]._id) {
                            finhtml += "<option selected value='"+value._id+"'>"+value.ticker_name+"</option>";
                            getallenic_data(selected_option,enic_value);
                        }
                        else{
                            finhtml += "<option value='"+value.countries[0]._id+"'>"+value.countries[0].name+"</option>";
                        }
                    })
                    finhtml += '</select>';
                    $('#select_country').html(finhtml);
              },
              error: function(error){
                $('.preloader').hide();
                alert("Something went wrong");
              }
        });

    }

    // getcurrentticker_data();
    function getallenic_data(value1='',enic_value='')
    {
        $('.preloader').show();
        if(value1 == '')
        {
            value1 = "<?php echo $selected_option ?>";
        }
        if(enic_value == '')
        {
            enic_value = "<?php echo $selected_market ?>";
        }
        html = '';
        $('#user_tab').DataTable().clear().destroy();
        var main_url_ajax = '<?php echo base_url('economicalindicator/getecin_data');?>';
        $.ajax({
              type: 'POST',
              url: main_url_ajax,
              data: {'country_id':value1,'ecin_id':enic_value},
              success: function(resultData) { 
                resultData = JSON.parse(resultData);
                console.log(resultData);
                $.each(resultData, function( index, value ) {
                    var editurl = "<?php echo base_url(); ?>";
                    html += '<tr><td>'+value.date+'</td><td>'+value.value+'</td><td><a href="'+editurl+'economicalindicator/edit/'+value._id.$oid+'"><i class="fa fa-pencil-alt"></i></a> &nbsp; <span style="color:#007bff" onclick=deleteconfirm("'+editurl+'economicalindicator/delete/'+value._id.$oid+'")><i class="fa fa-trash"></i></span></td></tr>';
                    console.log(html);
                    // console.log(index);
                });

                $('#tbody').html(html);
                setTimeout(function(){ 

                    if ( $.fn.dataTable.isDataTable( '#user_tab' ) ) {
                            
                            $('#user_tab').DataTable().clear().destroy();
                            setTimeout(function(){ 
                                $('#tbody').html(html);
                                var table = $('#user_tab').DataTable({
                                    "displayLength": 25,
                                    "aaSorting": [],
                                    "columnDefs": [
                                        { orderable: false, targets: -1 }
                                     ]
                                });
                            }, 500);
                    }
                    else {
                        var table = $('#user_tab').DataTable({
                            "displayLength": 25,
                            "aaSorting": [],
                            "columnDefs": [
                                { orderable: false, targets: -1 }
                             ]
                        });
                    }
                    
                }, 1000);
                $('.preloader').hide();
              }
        });
        
    }
    function deleteconfirm(url){
        var result = confirm("Want to delete?");
        if (result) {
            window.location.replace(url);
        }
    }
        
    $(document).ready(function() {
                $('.js-example-basic-single').select2();
                var table = $('#user_tab').DataTable({
                    "displayLength": 25,
                    "aaSorting": [],
                });
            });
            
            function viewModal(mode, status, id) {
                    var ajaxStatus;  // The variable that makes Ajax possible!
                    try
                    {

                        // Opera 8.0+, Firefox, Safari
                        ajaxStatus = new XMLHttpRequest();
                    } catch (e)
                    {

                        // Internet Explorer Browsers
                        try
                        {
                            ajaxStatus = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e)
                        {

                            try
                            {
                                ajaxStatus = new ActiveXObject("Microsoft.XMLHTTP");
                            } catch (e)
                            {

                                // Something went wrong
                                alert("Your browser broke!");
                                return false;
                            }
                        }
                    }
                    ajaxStatus.onreadystatechange = function () {

                        if (ajaxStatus.readyState == 4)
                        {
                            var ajaxDisplay = document.getElementById('modalcontent');
//                              alert(ajaxStatus.responseText);
                            ajaxDisplay.innerHTML = ajaxStatus.responseText;
                            $("#statusmodel").modal({show: true});
                        }
                    }
                    ajaxStatus.open("POST", "user/viewmodal/" + mode  + '/' + id, true);
                    ajaxStatus.send(null);
                }

    
</script>