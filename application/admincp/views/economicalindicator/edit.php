<?php echo $header; ?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active" title="Price Data"><a href="<?php echo base_url('economicalindicator') ?>">Economical Indicator Data</a></li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Edit Economical Indicator Value</h2>
                                <form class="form-material m-t-40" method="post" action="<?php echo base_url('economicalindicator/edit_data'); ?>">
                                    <div class="form-group">
                                        <label>Economical Indicator  <span style="color: red">*</span></label>
                                        <input type="hidden" value="<?php echo base64_encode($data['id']) ?>" name="id" >
                                        <input title="Economical Indicator" name="ecin_name" type="text" class="form-control form-control-line" readonly="readonly" required="" value="<?php echo $edit_id!=''?$data['ecin_name']:set_value('ecin_name'); ?>"> 
                                        <?php echo form_error('ecin_name'); ?>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label>Country Name<span style="color: red">*</span></label>
                                        <input title="Country Name" readonly="readonly"  name="country_name" type="text" class="form-control form-control-line" required="" value="<?php echo $edit_id!=''?$data['country_name']:set_value('country_name'); ?>"> 
                                        <?php echo form_error('country_name'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Date <span style="color: red">*</span></label>
                                        <input title="Date" readonly="readonly"  name="date" type="text" class="form-control form-control-line" required="" value="<?php echo $edit_id!=''?$data['date']:set_value('date'); ?>"> 
                                        <?php echo form_error('date'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="example-email">Value <span style="color: red">*</span></label>
                                        <input title="Value"  type="text" name="value" class="form-control" required="" value="<?php echo $edit_id!=''?$data['value']:set_value('value'); ?>">
                                        <?php echo form_error('value'); ?>
                                    </div>

                                        <button title="Update"  type="submit" class="btn btn-success waves-effect waves-light m-r-10" >Update</button>
                                        <a href="<?php echo base_url('economicalindicator'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<?php echo $footer; ?>