<?php echo $header ?>

       
        
        
       
         <!-- **Main - Starts** --> 
		<div id="main">
        	<div class="parallax full-width-bg">
            	<div class="container">
                	<div class="main-title">
                    	<h1> Register </h1>
                        <div class="breadcrumb">
                            <a href="index.html"> Home </a>
                            <span class="fa fa-angle-right"></span>
                            <a href="tabs-accordions.html"> Pages </a>
                            <span class="fa fa-angle-right"></span>
                            <span class="current"> Register </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dt-sc-margin100"></div>    
            <!-- Primary Starts -->
            <section id="primary" class="content-full-width">
                <div class="full-width-section">
                    <div class="container">
                    	<div class="page_info"> 
                        	<h3 class="aligncenter"> <span> <i class="fa fa-user"></i></span>
                             Member Registration </h3>
                        </div>
                        <div class="dt-sc-margin20"></div>
                    </div>
                </div>  
                <div class="full-width-section parallax full-section-bg">
                    <div class="container">
                        <div class="dt-sc-clear"></div>                            
                        <div class="form-wrapper register">
                            <form method="post" id="reg_form" name="frmRegister">
                                <p class="dt-sc-one-half column first">
                                    <input placeholder="First Name *" id="f_name" name="f_user" type="text">
                                </p>
                                
                                <p class="dt-sc-one-half column">
                                    <input placeholder="Last Name" id="l_name" name="l_user" type="text">
                                </p>
                                
                                <p class="dt-sc-one-half column first">
                                    <input placeholder="Email Address *" id="email_link" name="email" type="email">
                                </p>
                                
                                <p class="dt-sc-one-half column">
                                    <input placeholder="Web Address" id="web_link" name="web" autocomplete="off" type="text">
                                </p>
                                
                                <p class="dt-sc-one-half column first">
                                    <input placeholder="Password *" id="user_pwd" name="pwd" type="password">
                                </p>
                                
                                <p class="dt-sc-one-half column">
                                    <input placeholder="Confirm Password *" name="c_pwd" type="password">
                                </p>                                          
                                
                                <input class="button" value="Create an Account" type="submit">     
                            </form>   
                        </div>
                    </div>
                </div>
                <div class="dt-sc-hr-invisible"></div>
                <div class="full-width-section">	  
                    <div class="container">
                        <div class="newsletter-container">                                            
                            <h3>Subscribe Newsletter for updates</h3>
                            <div class="dt-sc-one-half column first">
                                <p>Nam libero tempore, eu 
volutpat enim diam eget metus cum soluta nobis est eligendi optio cumque
nihil impedit quo minus id quod maxime placeat facere.</p>
                            </div>
                            <div class="dt-sc-one-half column last">
                                <form method="post" class="newsletter-form" name="frmNewsletterContent" action="php/subscribe.php">
                                    <input required="" placeholder="Enter Your Email ID" name="email" type="email">
                                    <input value="Submit" class="button" name="submit" type="submit">
                                </form>
                                <div id="ajax_newsletter_msg_content"></div>
                            </div>
                        </div> 
                    </div>
                </div>                                                               	
                <div class="dt-sc-margin100"></div> 
        	</section>  
		
        </div> <!-- **Main - Ends** --> 
    </div><!-- **inner-wrapper - End** -->
    
</div><!-- **Wrapper - End** -->
<?php echo $footer; ?>
