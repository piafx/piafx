<?php echo $header; ?>
<?php echo $sidebar; ?>
 <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit Employee</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" >  
            <div class="col-lg-3 col-sm-8 col-md-8 col-xs-12 pull-right">
                    <a href="<?php echo base_url('employee'); ?>" class="btn btn-info pull-right">< Back</a>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="col-xs-12">
              <?php
              if ($this->session->flashdata('message'))
              {
                  ?>
                  <!--  start message-red -->
                  <div class="box-body" id="modal_msg">
                      <div class=" alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?>
                      </div>
                  </div>
                  <!--  end message-red -->
              <?php } ?>
              <?php
              if ($this->session->flashdata('success'))
              {
                  ?>
                  <!--  start message-green -->
                  <div class="alert alert-success alert-dismissable" id="modal_msg">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                      <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                  <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <!--  end message-green -->
          <?php } ?>
          </div>
         <div class="col-sm-12">
          <div class="white-box">
            
              <form class="form-horizontal" method="post" action="<?php echo base_url().'employee/edit_data' ?>">
              <div class="form-group">
                
                <div class="col-md-4">
                    <label class="col-md-12" style="padding-left: 0px;">First Name</label>
                    <input type="hidden" name="id" value="<?php echo base64_encode($id); ?>">
                    <input type="text" class="form-control" placeholder="First Name" name="fname" required="required" value="<?php echo $edit_id!=''?$fname:set_value('fname'); ?>">
                    <span style="color:red"><?php echo form_error('fname'); ?></span>    
                </div>
                  
                <div class="col-md-4">
                    <label class="col-md-12" style="padding-left: 0px;">Middle Name</label>
                    <input type="text" class="form-control" placeholder="Middle Name" name="mname" value="<?php echo $edit_id!=''?$mname:set_value('mname'); ?>">
                    <span style="color:red"><?php echo form_error('mname'); ?></span>    
                </div>
                  
                <div class="col-md-4">
                    <label class="col-md-12" style="padding-left: 0px;">Last Name</label>
                    <input type="text" class="form-control" placeholder="Last Name" name="lname" required="required" value="<?php echo $edit_id!=''?$lname:set_value('lname'); ?>">
                    <span style="color:red"><?php echo form_error('lname'); ?></span>    
                </div>
              
              </div>
             
                  
             <div class="form-group">
                <label class="col-md-12" for="example-email">Email</label>
                <div class="col-md-12">
                    <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" placeholder="Email" name="email" required="required" value="<?php echo $edit_id!=''?$email:set_value('email'); ?>">
                    <span style="color:red"><?php echo form_error('email'); ?></span>    
                </div>
              </div>
              
              <div class="form-group">
                <label class="col-md-12" for="example-email">Alternative Email(Peronal Email)</label>
                <div class="col-md-12">
                    <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form-control" placeholder="Alternative Email" name="aemail" value="<?php echo $edit_id!=''?$aemail:set_value('aemail'); ?>">
                    <span style="color:red"><?php echo form_error('aemail'); ?></span>    
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="col-md-12" for="example-email">Employee Code</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Employee Code" name="ecode" required="required" value="<?php echo $edit_id!=''?$ecode:set_value('ecode'); ?>">
                    <span style="color:red"><?php echo form_error('ecode'); ?></span>    
                </div>
              </div>
              
                  
              <div class="form-group">
                <label class="col-md-12" for="example-email">Contact</label>
                <div class="col-md-12">
                    <input type="text" onkeypress="return isNumber(event)" class="form-control" placeholder="Contact" name="contact" required="required" maxlength="12" minlength="10" value="<?php echo $edit_id!=''?$contact:set_value('contact'); ?>">
                    <span style="color:red"><?php echo form_error('contact'); ?></span>    
                </div>
              </div>   
             
              <div class="form-group">
                <label class="col-md-12">Address</label>
                <div class="col-md-12">
                    <textarea class="form-control" placeholder="Address" name="address" required="required"><?php echo $edit_id!=''?$address:set_value('address'); ?></textarea>
                    <span style="color:red"><?php echo form_error('address'); ?></span>    
                </div>
              </div>
                  
              <div class="form-group">
                <label class="col-md-12" for="example-email">Designation</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" placeholder="Designation" name="designation" required="required" value="<?php echo $edit_id!=''?$designation:set_value('designation'); ?>">
                    <span style="color:red"><?php echo form_error('designation'); ?></span>    
                </div>
              </div>
                 
              
              <div class="form-group">
                <label class="col-md-12" for="example-email">Role </label>
                <div class="col-md-12">
                    <select class="form-control" name="role" required="required">
                        <?php 
                                      foreach ($roles as $role)
                                      {
                        ?>
                        <option value="<?php echo $role['role_id']; ?>" <?php echo $role['role_id']==$role1?'Selected':''; ?>><?php echo $role['role_name']; ?></option>
                        <?php 
                                      }
                        ?>
                    </select>
                    
                    <span style="color:red"><?php echo form_error('role'); ?></span>    
                </div>
              </div>
                  
              <div class="form-group">
                <label class="col-md-12" for="example-email">Report To</label>
                <div class="col-md-12">
                    <select class="form-control" name="report_to" required="required">
                        <option value="0">Admin</option>
                        <?php 
                                      foreach ($emps as $emp)
                                      {
                        ?>
                        <option value="<?php echo $emp['employee_id']; ?>" <?php echo $emp['employee_id']==$report1?'Selected':''; ?>> <?php echo $emp['fname'].' '.$emp['lname']; ?></option>
                        <?php 
                                      }
                        ?>
                    </select>
                    
                    <span style="color:red"><?php echo form_error('role'); ?></span>    
                </div>
              </div>    
                  
              <div class="form-group">
                <label class="col-md-12" for="example-email">Joining Date</label>
                
                 <div class="col-md-12">
                     <input type="text" class="form-control" id="datepicker-autoclose" placeholder="Joining Date"  name="joiningdate" required="required" value="<?php echo $edit_id!=''?date("d-m-Y", strtotime($joiningdate)): date("d-m-Y", strtotime(set_value('joiningdate'))); ?>">
                    
                    <span style="color:red"><?php echo form_error('joiningdate'); ?></span>    </div>
              </div>
                  
                  
             <div class="form-group">
                
                <div class="col-md-12">
                    <input type="submit" name="submit" class="btn btn-info" value="Submit">
                </div>
              </div>
             
            </form>
          </div>
        </div>
      </div>
    </div>
 </div>

<script src="<?php echo base_url() ?>../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() ?>../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url() ?>../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



<script>

// Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        setDate : "('#datepicker-autoclose').value"
      });
      
    jQuery('#date-range').datepicker({
        toggleActive: true
      });
    jQuery('#datepicker-inline').datepicker({
        
        todayHighlight: true
      });

</script>
<?php echo $footer; ?>
<script>
    
      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>