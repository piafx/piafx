<?php echo $header; ?>
<?php echo $sidebar; ?>
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Change Password</h4>
        </div>
      </div>
      <div class="row">
          <div class="col-xs-12">
              <?php
              if ($this->session->flashdata('message'))
              {
                  ?>
                  <!--  start message-red -->
                  <div class="box-body" id="modal_msg">
                      <div class=" alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?>
                      </div>
                  </div>
                  <!--  end message-red -->
              <?php } ?>
              <?php
              if ($this->session->flashdata('success'))
              {
                  ?>
                  <!--  start message-green -->
                  <div class="alert alert-success alert-dismissable" id="modal_msg">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                      <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                  <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <!--  end message-green -->
          <?php } ?>
          </div>
         <div class="col-sm-12">
          <div class="white-box">
            
              <form class="form-horizontal" method="post" action="<?php echo base_url().'changepassword/change' ?>">
              <div class="form-group">
                <label class="col-md-12">Old Password</label>
                <div class="col-md-12">
                    <input type="password" class="form-control" placeholder="Old Password" name="oldpassword" required="required" minlength="6">
                    <span style="color:red"><?php echo form_error('oldpassword'); ?></span>    
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-12" for="example-email">New Password</label>
                <div class="col-md-12">
                    <input type="password" class="form-control" placeholder="New Password" name="password" required="required" minlength="6">
                    <span style="color:red"><?php echo form_error('password'); ?></span>    
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-12">Confirm New Password</label>
                <div class="col-md-12">
                    <input type="password" class="form-control" placeholder="Confirm New Password" name="passconf" required="required" minlength="6">
                    <span style="color:red"><?php echo form_error('passconf'); ?></span>    
                </div>
              </div>
                <div class="form-group">
                
                <div class="col-md-12">
                    <input type="submit" name="submit" class="btn btn-info" value="Submit">
                </div>
              </div>
             
            </form>
          </div>
        </div>
      </div>
    </div>
 </div>
<?php echo $footer; ?>