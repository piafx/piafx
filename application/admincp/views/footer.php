

<audio id="sound1" src="http://www.soundjay.com/button/beep-07.wav" ></audio>
<!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/perfect-scrollbar.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.js"></script>
    <script src="https://kendo.cdn.telerik.com/2019.1.115/js/kendo.all.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/moment/moment.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/custom.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jasny-bootstrap.js"></script>    
    <script src="<?php echo base_url(); ?>assets/plugins/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <style>
        .k-header.k-grid-toolbar
        {
            background-color: #009efb !important;
               
        }
        .k-grid .k-header .k-button
        {
            background-color: #009efb !important;
            border: 0px !important;
        }
        .k-grid .k-icon
        {
            background: white;
        }
        
        .k-grid tbody tr{
            height: 50px;
        }

        .k-grid td{
            white-space: nowrap;
        }

    </style>
    <script>
                $(document).ready(function () {
                    
                    $(".select2").select2({
                        placeholder: "Select Assign To",
                    });
//                    document.addEventListener('webkitfullscreenchange', exitHandler, false);
//                    document.addEventListener('mozfullscreenchange', exitHandler, false);
//                    document.addEventListener('fullscreenchange', exitHandler, false);
//                    document.addEventListener('MSFullscreenChange', exitHandler, false);
                    var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read:  {
                                    url: "file1.php",
                                    dataType: "jsonp"
                                },
                                update: {
                                    url: "file1.php",
                                    dataType: "jsonp"
                                },
                                destroy: {
                                    url: "file1.php",
                                    dataType: "jsonp"
                                },
                                create: {
                                    url: "file1.php",
                                    dataType: "jsonp"
                                },
                                parameterMap: function(options, operation) {
                                    if (operation !== "read" && options.models) {
                                        return {models: kendo.stringify(options.models)};
                                    }
                                }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                model: {
                                    id: "ProductID",
                                    fields: {
                                        ProductID: { editable: false, nullable: true },
                                        ProductName: { validation: { required: true } },
                                        UnitPrice: { type: "boolean" },
                                        UnitsInStock: { type: "boolean"},
                                        Discontinued: { type: "boolean" },
                                        
                                    }
                                }
                            }
                        });

                    $("#grid").kendoGrid({
                        dataSource: dataSource,
                        navigatable: true,
                        pageable: true,
                        height: 550,
                        toolbar: ["save", "cancel"],
                        columns: [
                            
                            { field: "ProductName", title: "Dates"},
                            { field: "UnitPrice", title: "Niravkumar Patel",  editor: customBoolEditor },
                            { field: "UnitsInStock", title: " Mazhar Riaz", editor: customBoolEditor },
                            { field: "Discontinued",  title: " Yusal Nasik", editor: customBoolEditor },
                            { field: "test",  title: " Armando Martinezi ", editor: customBoolEditor },
                        ],
                        editable: true
                    });
                });

                function customBoolEditor(container, options) {
                    var guid = kendo.guid();
                    $('<input class="k-checkbox" id="' + guid + '" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
                    $('<label class="k-checkbox-label" for="' + guid + '">​</label>').appendTo(container);
                }
            </script>
            <script>
                $('#user_type').on('change', function() {
                  if( this.value == 3 ){
                      $("#hide_div").show();
                  }
                  else
                  {
                      $("#hide_div").hide();
                  }
                });
            </script>
            <script>
                function get_all_notification()
                {
                        $.ajax({
                                type: 'POST',
                                url: '<?php echo base_url()."admin/get_all_notifications"; ?>',
                                dataType: "text",
                                success: function(resultData) {
                                    resultData = JSON.parse(resultData);
                                    var html = '';
                                    if(resultData.data.length > 0)
                                    {
                                        PlaySound("sound1");
                                        $('#bell_anchor').append('<div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>');
                                    }
                                    var new_count = resultData.data.length;
                                    if(new_count >= 5)
                                    {
                                        $('#new_notifications').find("li").slice(0, 10).remove();
                                    }
                                    else
                                    {
                                        $('#new_notifications').find("li").slice(10-new_count*2, 10).remove();
                                    }
                                    $.each(resultData.data, function (i,index) {
                                        if(new_count < 5)
                                        {
                                            html += '<center><li>'+index.notifications_message+'</li><li role="separator" class="divider"></li></center>';
                                            $('#new_notifications').prepend(html);
                                        }
                                    });
                                }
                        });
                }
                function PlaySound(soundObj) {
                  var sound = document.getElementById(soundObj);
                    sound.play();
                }
                $(document).ready(function () {
                        
                        $(window).bind("pageshow", function(event) {
                            if (event.originalEvent.persisted) {
                                window.location.reload(); 
                            }
                        });
                        var selected_user = $('#user_type').val();
                        if( selected_user == 3 ){
                            $("#hide_div").show();
                        }
                        else
                        {
                            $("#hide_div").hide();
                        }
                        
                        
                        $('#number').keypress(function (e) {
                           //if the letter is not digit then display error and don't type anything
                           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                            if(e.which != 46){
                              //display error message
                              return false;
                            }
                          }
                         });
                         $('.number').keypress(function (e) {

                           //if the letter is not digit then display error and don't type anything
                           if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 )) {
                             if(e.which != 46){
                                  //display error message
                                  return false;                                
                             }
                          }
                         });
                        $(document).on("click","#confirm",function(e) {
                            e.preventDefault();
                            if (confirm("Are you sure want to delete?")) {
                                 var link = $(this).attr('href');
                                 var saveData = $.ajax({
                                        type: 'POST',
                                        url: link,
                                        dataType: "text",
                                        success: function(resultData) { 
                                            window.location.reload();
                                        }
                                  });
                                
                                
                            }
                        });
                        var recordId = "<?php echo $this->uri->segment(3); ?>";
                        
                        var cond = '<?php echo isset($user_role)?$user_role:'';  ?>';
                        if(cond == '1')
                        {

                        var crudServiceBaseUrl = "<?php echo base_url(); ?>",
                        dataSource = new kendo.data.DataSource({
                            type: "odata",
                            transport: {
                                read:  crudServiceBaseUrl + "markets/get_all_data",
                            },
                            batch: true,
                            pageSize: 20,
                            serverPaging: true,
                            schema: {
                                model: {
                                    id: "market_data_id",
                                    fields: {
                                        market_data_id: { editable: false, nullable: true },
                                        market_name: { validation: { required: true } },
                                        planned_cell_Id: { validation: { required: true } },
                                        cascade_id: { validation: { required: true } },
                                        carrier_sector_id: {  },//{ type: "number", validation: { required: true, min: 1} },
                                        phone_type: { },
                                        call_type: {  },
                                        cell_site_common_name: {  },
                                        inbuildsite:{ },
                                        site_lat: {  },
                                        site_long: {  },
                                        test_location_lat: {  },
                                        test_location_long: {  },
                                        e911_name: {  },
                                        e911_fcc_id: {  },
                                        pani: {  },
                                        enodeB_id: {  },
                                        freq_band_class : { type: "number", validation: { required: true, min: 1} },
                                        freq : {  },
                                        pci: {  },
                                        EARFCN: { type: "number", validation: { required: true, min: 1} },
                                        azimuth: { type: "number", validation: { required: true, min: 1} },
                                        street_add: {  },
                                        city: {  },
                                        state: {  },
                                        CNTY: {  },
                                        antenna_lo: {  },
                                        test_date: { type: "date" },
                                        team: {  },
                                        tester_name: {  },
                                        seq:{ },
                                        disatance : { },
                                        e911_contact: {  },
                                        e911_ontact_name: {  },
                                        e911_requirements: {  },
                                        callback: { type: "number",  },
                                        IMSI: {  },
                                        cell_id_tested: {  },
                                        call_type_final: {  },
                                        latitude: {  },
                                        longtude: {  },
                                        test_time_date: { type: "date" },
                                        test_time_date_end: { type: "date"  },
                                        tester_name_final: {  },
                                        //position_search: {  },
                                        //position_search1: {  },
                                        comments: {  },
                                        ESRK: { type: "number",  },
                                        ground_truth_latitude: {  },
                                        ground_truth_longtude: {  },
                                        LAT_WCQT: {  },
                                        LONG_WCQT: {  },
                                        LAM_latitude: {  },
                                        LAM_longitude: {  },
                                        LAM_uncertainty: {  },
                                        disatance_formula: {  },
                                        attempted_calls: { type: "number",  },
                                        calls_not_selected: { type: "number",  },
                                        cell_id: {  },
                                        sector:{ },
                                        e911: { type: "boolean" },
                                        locked_cell : { },
                                        distance_btwn : { },
                                        lrf_cmnt : { },
                                        operator_id : { },
                                        operator_id_verify : {type: "boolean" },
                                        operator_id_issue : { },
                                        e911address : { },
                                        e911address_verify : {type: "boolean" },
                                        e911address_issue : { },
                                        e911phIIcall : {type: "boolean" },
                                        e9111strabid : {type: "boolean" },
                                        e9112ndrabid : {type: "boolean" },
                                        degree_radio : { },
                                        e911contact_verify : {type: "boolean" },
                                        e911contact_issue : { },
                                        e911name_verify : {type: "boolean" },
                                        E911name_issue : { },
                                        long_verify : { type: "boolean"},
                                        long_issue : { },
                                        lat_verify : {type: "boolean" },
                                        lat_issue : { },
                                        esrkrange_verify : { type: "boolean"},
                                        esrkrange_issue : { },
                                        defaultesrk : { },
                                        defaultesrk_verify : {type: "boolean" },
                                        defaultesrk_issue : { },
                                        betweenesrk : { },
                                        betweenesrk_verify : { type: "boolean"},
                                        betweenesrk_issue : { },
                                        notinrange : { },
                                        uncertainty : { },
                                        uncertainty_verfiy : {type: "boolean" },
                                        uncertainty_issue : { },
                                        uncertaintymeter : { },
                                        uncertaintymeter_verify : {type: "boolean" },
                                        uncertaintymeter_issue : { },
                                        cbmatch : { type: "boolean" },
                                        e911match : {type: "boolean" },
                                        addressmatch : {type: "boolean" },
                                        phase : { },
                                        comments_testing:{ },
                                    }
                                }
                            }
                        });
                    }
                    else
                    {
                        var crudServiceBaseUrl = "<?php echo base_url(); ?>",
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read:  {
                                    url: crudServiceBaseUrl + "market/get_import/" + recordId,
                                    type: "POST",
                                    dataType: "jsonp"
                                },
                                update: {
                                    url: crudServiceBaseUrl + "market/update_import/" + recordId,
                                    type: "POST",
                                    dataType: "jsonp"
                                },
                                parameterMap: function(options, operation) {
                                    if (operation !== "read" && options.models) {
                                        return {models: kendo.stringify(options.models)};
                                    }
                                }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                model: {
                                    id: "market_data_id",
                                    fields: {
                                        // market_data_id: { editable: false, nullable: true },
                                        // market_name: { validation: { editable: false, required: true } },
                                        // planned_cell_Id: { validation: { editable: false, required: true } },
                                        // cascade_id: { editable: false, validation: { required: true } },
                                        // carrier_sector_id: { editable: false, },//{ type: "number", validation: { required: true, min: 1} },
                                        // phone_type: { editable: false, },
                                        // call_type: { editable: false, },
                                        // cell_site_common_name: { editable: false, },
                                        // inbuildsite:{ editable: false, },
                                        // site_lat: { editable: false, },
                                        // site_long: { editable: false, },
                                        // test_location_lat: { editable: false, },
                                        // test_location_long: { editable: false, },
                                        // PSAP_name: { editable: false, },
                                        // PSAP_fcc_id: { editable: false, },
                                        // pani: { editable: false, },
                                        // enodeB_id: { editable: false, },
                                        // freq_band_class : { editable: false, type: "number", validation: { required: true, min: 1} },
                                        // freq : { editable: false, },
                                        // pci: { editable: false, },
                                        // EARFCN: { editable: false, type: "number", validation: { required: true, min: 1} },
                                        // azimuth: { editable: false, type: "number", validation: { required: true, min: 1} },
                                        // street_add: { editable: false, },
                                        // city: { editable: false, },
                                        // state: { editable: false, },
                                        // CNTY: { editable: false, },
                                        // antenna_lo: { editable: false, },
                                        // test_date: { editable: false, type: "date" },
                                        // team : { editable: false, },
                                        // tester_name: { editable: false, },
                                        // seq:{ editable: false, },
                                        // disatance : { editable: false, },
                                        // PSAP_contact: { editable: false, },
                                        // PSAP_ontact_name: { editable: false, },
                                        // PSAP_requirements: { editable: false, },
                                        // callback: { editable: false, type: "number",  },
                                        // IMSI: { editable: false, },
                                        // cell_id_tested: { editable: false, },
                                        // call_type_final: { editable: false, },
                                        // latitude: { editable: false, },
                                        // longtude: { editable: false, },
                                        // test_time_date: { type: "date" },
                                        // test_time_date_end: { type: "date"  },
                                        // tester_name_final: {  },
                                        // comments: {  },
                                        // ESRK: { type: "number",  },
                                        // ground_truth_latitude: {  },
                                        // ground_truth_longtude: {  },
                                        // LAT_WCQT: {  },
                                        // LONG_WCQT: {  },
                                        // LAM_latitude: {  },
                                        // LAM_longitude: {  },
                                        // LAM_uncertainty: {  },
                                        // disatance_formula: {  },
                                        // attempted_calls: { type: "number",  },
                                        // calls_not_selected: { type: "number",  },
                                        // cell_id: {  },
                                        // sector:{ },
                                        // PSAP: { type: "boolean" },
                                        // locked_cell : { },
                                        // distance_btwn : { },
                                        // lrf_cmnt : { }
                                        market_data_id: { editable: false, nullable: true },
                                        market_name: { validation: { required: true } },
                                        planned_cell_Id: { validation: { required: true } },
                                        cascade_id: { validation: { required: true } },
                                        carrier_sector_id: {  },//{ type: "number", validation: { required: true, min: 1} },
                                        phone_type: { },
                                        call_type: {  },
                                        cell_site_common_name: {  },
                                        inbuildsite:{ },
                                        site_lat: {  },
                                        site_long: {  },
                                        test_location_lat: {  },
                                        test_location_long: {  },
                                        e911_name: {  },
                                        e911_fcc_id: {  },
                                        pani: {  },
                                        enodeB_id: {  },
                                        freq_band_class : { type: "number", validation: { required: true, min: 1} },
                                        freq : {  },
                                        pci: {  },
                                        EARFCN: { type: "number", validation: { required: true, min: 1} },
                                        azimuth: { type: "number", validation: { required: true, min: 1} },
                                        street_add: {  },
                                        city: {  },
                                        state: {  },
                                        CNTY: {  },
                                        antenna_lo: {  },
                                        test_date: { type: "date" },
                                        team: {  },
                                        tester_name: { headerAttributes: { "class": "vishal" } },
                                        seq:{ },
                                        disatance : { },
                                        e911_contact: {  },
                                        e911_ontact_name: {  },
                                        e911_requirements: {  },
                                        callback: { type: "number",  },
                                        IMSI: {  },
                                        cell_id_tested: {  },
                                        call_type_final: {  },
                                        latitude: {  },
                                        longtude: {  },
                                        test_time_date: { type: "date" },
                                        test_time_date_end: { type: "date"  },
                                        tester_name_final: {  },
                                        //position_search: {  },
                                        //position_search1: {  },
                                        comments: {  },
                                        ESRK: { type: "number",  },
                                        ground_truth_latitude: {  },
                                        ground_truth_longtude: {  },
                                        LAT_WCQT: {  },
                                        LONG_WCQT: {  },
                                        LAM_latitude: {  },
                                        LAM_longitude: {  },
                                        LAM_uncertainty: {  },
                                        disatance_formula: {  },
                                        attempted_calls: { type: "number",  },
                                        calls_not_selected: { type: "number",  },
                                        cell_id: {  },
                                        sector:{ },
                                        e911: { type: "boolean" },
                                        locked_cell : { },
                                        distance_btwn : { },
                                        lrf_cmnt : { },
                                        operator_id : { },
                                        operator_id_verify : { },
                                        operator_id_issue : { },
                                        e911address : { },
                                        e911address_verify : { },
                                        e911address_issue : { },
                                        e911phIIcall : { },
                                        e9111strabid : { },
                                        e9112ndrabid : { },
                                        degree_radio : { },
                                        e911contact_verify : { },
                                        e911contact_issue : { },
                                        e911name_verify : { },
                                        E911name_issue : { },
                                        long_verify : { },
                                        long_issue : { },
                                        lat_verify : { },
                                        lat_issue : { },
                                        esrkrange_verify : { },
                                        esrkrange_issue : { },
                                        defaultesrk : { },
                                        defaultesrk_verify : { },
                                        defaultesrk_issue : { },
                                        betweenesrk : { },
                                        betweenesrk_verify : { },
                                        betweenesrk_issue : { },
                                        notinrange : { },
                                        uncertainty : { },
                                        uncertainty_verfiy : { },
                                        uncertainty_issue : { },
                                        uncertaintymeter : { },
                                        uncertaintymeter_verify : { },
                                        uncertaintymeter_issue : { },
                                        cbmatch : {type: "boolean" },
                                        e911match : { },
                                        addressmatch : { },
                                        phase : { },
                                        comments_testing:{ },
                                    }
                                }
                            }
                        });
                    }
                    function categoryDropDownEditor(container, options) {
                        console.log("container",container);
                        console.log("options",options.model.team_id);
                        var crudServiceBaseUrl = "<?php echo base_url(); ?>";
                        $('<input name="' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: false,
                                dataTextField: "user_firstname",
                                dataValueField: "user_firstname",
                                dataSource: {
                                    type: "odata",
                                    transport: {
                                        read: crudServiceBaseUrl + "market/get_users/"+options.model.team_id,
                                    }
                                }
                            });
                    }
                    
                    function callTypeDropDownEditor(container, options) {
                        var categories = [
                            { CategoryName: "LAM"}, 
                            { CategoryName: "PSAP" },
                        ];
                        var crudServiceBaseUrl = "<?php echo base_url(); ?>";
                        $('<input data-value-field="continent" data-bind="value:' + options.field + '" name="' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: false,
                                dataTextField: "CategoryName",
                                dataValueField: "CategoryName",
                                dataSource: {   
                                    data: categories
                                }
                            });
                    }
                    
                    function coordinatorDropDownEditor(container, options) {
                        console.log("container",container);
                        console.log("options",options.model.market_id);
                        var crudServiceBaseUrl = "<?php echo base_url(); ?>";
                        $('<input data-value-field="continent" data-bind="value:' + options.field + '" name="' + options.field + '"/>')
                            .appendTo(container)
                            .kendoDropDownList({
                                autoBind: false,
                                dataTextField: "user_firstname",
                                dataValueField: "user_firstname",
                                dataSource: {   
                                    type: "odata",
                                    transport: {
                                        read: crudServiceBaseUrl + "market/get_coordinator/"+options.model.market_id,
                                    }
                                }
                            });
                    }
                    
                    //console.log(dataSource);
//                    $("#grid1").kendoGrid({
//                        
//                        dataSource: dataSource,
//                        navigatable: true,
//                        pageable: true,
//                        sortable: true,
//                        filterable: {
//                            mode: "row"
//                        },
//                        height: 550,
//                        toolbar: [ { name: "save", iconClass: "fa fa-check", text: "&nbsp; Save Changes" },
//                            { name: "cancel", iconClass: "fa fa-times", text: "&nbsp; Cancel changes" },
//                            { name: "excel", iconClass: "fa fa-file", text: "&nbsp; Export to Excel" },
//                            { template: '<span id="grid_fullscreen" class="k-button" href="\\#" onclick="buttonClickHandler()"><i class="fas fa-expand"></i> &nbsp; Full Screen</span>'}
//                        ],
//                        pdf: {
//                            allPages: true,
//                            avoidLinks: true,
//                            paperSize: "A4",
//                            margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
//                            landscape: true,
//                            repeatHeaders: true,
//                            template: $("#page-template").html(),
//                            scale: 0.8
//                        },
//                        excel: {
//                            fileName: "excel.xlsx",
//                            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
//                            filterable: true,
//                            allPages: true
//                        },
//                        columns: [
//                            // "ProductName1",
//                            //{ field: "market_name", title: "Market Name", width:"250px"},
//                            { field: "cascade_id", title: "Cascade ID", width:"80px",locked: true,lockable: false},
//                            { field: "planned_cell_Id", title: "Planned Cell Id", width:"80px",locked: true,lockable: false},
//                            { field: "carrier_sector_id", title: "Carrier Sector ID", width:"80px"},
//                            { field: "phone_type", title: "Phone Type" , width:"80px"},
//                            { field: "call_type", title: "PSAP OR LAM", width:"80px", editor: callTypeDropDownEditor },
//                            { field: "cell_site_common_name", title: "CellSite Common Name", width:"80px" },
//                            { field: "inbuildsite", title: "In Build Site", width:"80px" },
//                            { field: "site_lat", title: "Site Lat", width:"80px" },
//                            { field: "site_long", title: "SiteLong", width:"80px" },
//                            { field: "test_location_lat", title: "Test Location Lat", width:"80px" },
//                            { field: "test_location_long", title: "Test Location Long", width:"80px" },
//                            { field: "e911_name", title: "E911 Name", width:"80px", headerAttributes: { style: "background: green"}},
//                            { field: "e911_fcc_id", title: "E911 FCC ID", width:"80px" },
//                            { field: "pani", title: "PANI/ESRK/ESRD or DNT", width:"80px" },
//                            { field: "enodeB_id", title: "eNodeB ID", width:"80px" },
//                            { field: "freq_band_class", title: "Freq Band Class", width:"80px" },
//                            { field: "freq", title: "Freq", width:"80px" },
//                            { field: "pci", title: "PCI", width:"80px" },
//                            { field: "EARFCN", title: "EARFCN", width:"80px" },
//                            { field: "azimuth", title: "Azimuth", width:"80px" },
//                            { field: "street_add", title: "STREET ADDRESS", width:"80px" },
//                            { field: "city", title: "CITY", width:"80px" },
//                            { field: "state", title: "ST", width:"80px" },
//                            { field: "CNTY", title: "CNTY", width:"80px" },
//                            { field: "antenna_lo", title: "AntennaLo", width:"80px" },
//                            { field: "test_date", title: "Date", width:"80px" },
//                            { field: "team", title: "team", width:"80px", editor: coordinatorDropDownEditor  },
//                            { field: "tester_name", title: "Tester", width:"80px", editor: categoryDropDownEditor,locked: true,lockable: false },
//                            
//                            { field: "seq", title: "Sequence", width:"80px" },
//                            { field: "disatance", title: "Distance", width:"80px" },
//                            
//                            { field: "e911_contact", title: "E911 Contact", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e911_ontact_name", title: "E911 Contact Name", width:"80px" },
//                            { field: "e911_requirements", title: "E911 Special Requirement", width:"80px"},
//                            { field: "callback", title: "Callback Displayed", width:"80px" },
//                            { field: "IMSI", title: "IMSI", width:"80px" },
//                            { field: "cell_id_tested", title: "CellID Tested", width:"80px" },
//                            { field: "call_type_final", title: "Call Type", width:"80px" },
//                            { field: "latitude", title: "Latitude", width:"80px" },
//                            { field: "longtude", title: "Longitude", width:"80px" },
//                            { field: "test_time_date", title: "Test Call Start Date & Time", width:"80px", format: "{0:yyyy-MM-dd HH:mm:ss}" ,  editor: function(container, options){ var input = $("<input/>"); input.attr("name", options.field); input.appendTo(container); input.kendoDateTimePicker({ format: "yyyy-MM-dd HH:mm:ss" });} },
//                            { field: "test_time_date_end", title: "Test Call End Date & Time", width:"80px", format: "{0:yyyy-MM-dd HH:mm:ss}" ,  editor: function(container, options){ var input = $("<input/>"); input.attr("name", options.field); input.appendTo(container); input.kendoDateTimePicker({ format: "yyyy-MM-dd HH:mm:ss" });}  },
//                            { field: "tester_name_final", title: "Tester Name", width:"80px" ,headerAttributes: { style: "background: yellow"}},
////                            { field: "position_search", title: "Position Search", width:"80px" },
////                            { field: "position_search1", title: "Position Search1", width:"80px" },
//                            { field: "comments", title: "Comments", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "ESRK", title: "ESRK", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "ground_truth_latitude", title: "Ground Truth Latitude", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "ground_truth_longtude", title: "Ground Truth Longitude", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "LAT_WCQT", title: "LATWCQT", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "LONG_WCQT", title: "LongWCQT", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "LAM_latitude", title: "LAML atitude", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "LAM_longitude", title: "LAM Longitude", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "LAM_uncertainty", title: "LAM Uncertainty", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "disatance_formula", title: "Distance Formula", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "attempted_calls", title: "Attempted calls", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "calls_not_selected", title: "Calls not selected", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "cell_id", title: "Cell Id", width:"80px",headerAttributes: { style: "background: yellow"} },
//                            { field: "sector", title: "Section", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "e911", title: "E911", width:"80px" ,headerAttributes: { style: "background: yellow"}},
//                            { field: "locked_cell", title: "Locked Cell", width:"80px" },
//                            { field: "distance_btwn", title: "Distance Between", width:"80px" },
//                            { field: "lrf_cmnt", title: "LRF Comment", width:"80px" },
//                            { field: "operator_id", title: "Operator Id", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "operator_id_verify", title: "Operator Id Verify", width:"80px" },
//                            { field: "operator_id_issue", title: "Operator Id Issue", width:"80px" },
//                            
//                            { field: "e911address", title: "E911 address", width:"80px", headerAttributes: { style: "background: green"} },
//                            { field: "e911address_verify", title: "E911 address Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e911address_issue", title: "E911 address Issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "e911phIIcall", title: "E911 ph IIcall", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e9111strabid", title: "E911 1st Rabid", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e9112ndrabid", title: "E911 2nd Rabid", width:"80px" , headerAttributes: { style: "background: green"}},
//
//                            { field: "degree_radio", title: "Degree Radio", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e911contact_verify", title: "E911 Contact verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e911contact_issue", title: "E911 Contact Issue", width:"80px" , headerAttributes: { style: "background: green"}},      
//                            
//                            { field: "e911name_verify", title: "E911 name verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "E911name_issue", title: "E911 name issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "long_verify", title: "Longitute Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "long_issue", title: "Longitute Issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            
//                            { field: "lat_verify", title: "Latitude Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "lat_issue", title: "Latitude Issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "esrkrange_verify", title: "ESRK range Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "esrkrange_issue", title: "ESRK range Issue", width:"80px", headerAttributes: { style: "background: green"} },
//                            
//                            { field: "defaultesrk", title: "Default ESRK", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "defaultesrk_verify", title: "Default ESRK Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "defaultesrk_issue", title: "Default ESRK Issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "betweenesrk", title: "Between ESRK", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "betweenesrk_verify", title: "Between ESRK Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "betweenesrk_issue", title: "Between ESRK Issue", width:"80px", headerAttributes: { style: "background: green"} },
//                            
//                            { field: "notinrange", title: "Not in range", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "uncertainty", title: "Uncertainty", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "uncertainty_verfiy", title: "Uncertainty Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "uncertainty_issue", title: "Uncertainty Issue", width:"80px" , headerAttributes: { style: "background: green"}},
//                            
//                            { field: "uncertaintymeter", title: "Uncertainty in meter", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "uncertaintymeter_verify", title: "Uncertainty in meter Verify", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "uncertaintymeter_issue", title: "Uncertainty in meter Issue", width:"80px", headerAttributes: { style: "background: green"} },
//                            
//                            { field: "cbmatch", title: "CB match", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "e911match", title: "E911 Match", width:"80px", headerAttributes: { style: "background: green"} },
//                            { field: "addressmatch", title: "Address Match", width:"80px", headerAttributes: { style: "background: green"} },
//                            { field: "phase", title: "Phase", width:"80px" , headerAttributes: { style: "background: green"}},
//                            { field: "comments_testing", title: "Comments Testing", width:"80px", headerAttributes: { style: "background: green"} },
//                            
//                        ],
//                        editable: true,
//                        scrollable:true,
//                        
//                    });
                    $("#grid2").kendoGrid({
                        
                        dataSource: dataSource,
                        navigatable: true,
                        pageable: true,
                        sortable: true,
                        filterable: {
                            mode: "row"
                        },
                        height: 550,
                        toolbar: ["excel"],
                        pdf: {
                            allPages: true,
                            avoidLinks: true,
                            paperSize: "A4",
                            margin: { top: "2cm", left: "1cm", right: "1cm", bottom: "1cm" },
                            landscape: true,
                            repeatHeaders: true,
                            template: $("#page-template").html(),
                            scale: 0.8
                        },
                        excel: {
                            fileName: "excel.xlsx",
                            proxyURL: "https://demos.telerik.com/kendo-ui/service/export",
                            filterable: true,
                            allPages: true
                        },
                        columns: [
                            // "ProductName1",
                            { field: "CascadeID", title: "Cascade ID", width:"250px"},
                            { field: "PlannedCellId", title: "Planned Cell Id", width:"250px"},
                            { field: "CarrierSectorID", title: "Carrier Sector ID", width:"250px"},
                            { field: "PhoneType", title: "Phone Type" , width:"250px"},
                            { field: "PSAPORLAM", title: "PSAP OR LAM", width:"250px" },
                            { field: "CellSiteCommonName", title: "CellSite Common Name", width:"250px" },
                            { field: "SiteLat", title: "Site Lat", width:"250px" },
                            { field: "SiteLong", title: "SiteLong", width:"250px" },
                            { field: "TestLocationLat", title: "Test Location Lat", width:"250px" },
                            { field: "TestLocationLong", title: "Test Location Long", width:"250px" }
                        ],
                        editable: false,
                        scrollable:true,
                        
                    });
                });
//                function exitHandler()
//                {
//                    if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== null)
//                    {
//                        console.log("asd123");
//                        $('#grid_fullscreen').removeClass('hide');
//                    }
//                }
                $(document).on("click", ".extra_class .k-icon", function(event) {
                    
                    $('.k-animation-container').appendTo('#testgrid');
                });
                $(document).on("click", ".extra_class .k-dropdown", function(event) {
                    //console.log("asd1");
                    setTimeout(function(){ 
                        $('.k-animation-container').appendTo('#testgrid');
                    }, 200);   
                });
                 
                $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
                    var state = document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen;
                    var event = state ? 'FullscreenOn' : 'FullscreenOff';
                     if(state)
                     {
                         $('body').addClass('extra_class');
                         $('#grid_attendance').data('kendoGrid').refresh();
                     } 
                     else
                     {
                         $('#grid_fullscreen').removeClass('hide');
                         $('body').removeClass('extra_class');
                         $('.k-popup').appendTo('body');
                         $('#grid1').css('height','550px');
                         $('#testgrid').css('height','550px');
                         $('#grid_attendance').data('kendoGrid').refresh();
                     }
                });
                $(document).on("keyup", ".k-textbox", function(event) {   
                    $(this).keyup(resizeInput)
                });
                $(document).on("focus", ".k-textbox", function(event) {   
                    $(this).keyup(resizeInput)
                });
                function resizeInput() {
                    //console.log("tempo");    
                    $(this).attr('size', $(this).val().length);
                }
                
                function buttonClickHandler()
                {
                        
                        $('#grid1').css('height','100%');
                        $('#testgrid').css('height','100%');
                        var elem = document.getElementById("testgrid");
                        if (elem.requestFullscreen) {
                            elem.requestFullscreen();
                        } else if (elem.mozRequestFullScreen) {
                            elem.mozRequestFullScreen();
                        } else if (elem.webkitRequestFullscreen) {
                            elem.webkitRequestFullscreen();
                        }
                        setTimeout(function(){ 
                            $('#grid_fullscreen').addClass('hide');
                            $('.k-popup').appendTo('#testgrid');
                            //$('.k-animation-container').appendTo('#testgrid');
                        }, 200);
                }
                // var b = "";
                // $(document).ready(function(){
                //     $("a").click(function(){
                //        // alert("ok");
                //         if($(this).parents("body").hasClass("mini-sidebar")) {
                //             b = "mini-sidebar";
                //         }
                //         else{
                //             b = "";
                //         }
                //     });
                //     $(window).load = function(b){
                //         if(b == "mini-sidebar") {
                //         alert(b);
                //             $("body").addClass("mini-sidebar");
                //         }
                //         else{
                //             $("body").removeClass("mini-sidebar");
                //         }
                //     }
                    
                // });
                
            </script>
             
</body>
</html>