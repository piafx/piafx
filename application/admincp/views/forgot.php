<?php echo $header; ?> 
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?php echo $title; ?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background:url(<?php echo base_url('assets/images/background/bg_VTel.jpg'); ?>) no-repeat;background-size:cover">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="post">
                    <a href="javascript:void(0)" class="text-center db"><img src="<?php echo base_url('assets/images/companylogo.png') ?>" alt="Home" width="100px" /><br/>
                        <?php //echo $title; ?></a>
                    <br>
                    <div class="col-xs-12">
                        <?php
                        if ($this->session->flashdata('message')) {
                            ?>
                            <!--  start message-red -->
                            <div class="box-body" id="modal_msg">
                                <div class=" alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            </div>
                            <!--  end message-red -->
                        <?php } ?>
                        <?php
                        if ($this->session->flashdata('success')) {
                            ?>
                            <!--  start message-green -->
                            <div class="alert alert-success alert-dismissable" id="modal_msg">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <!--  end message-green -->
                        <?php } ?>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="user_email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Reset</button>
                            <br><br>
                            <a href="<?php echo base_url('adminlogin'); ?>" id="to-login" class="text-muted float-right"><i class="fa fa-key m-r-5"></i> Back To Login</a> </div>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="<?php echo base_url('adminlogin/forgot') ?>" method="post">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="user_email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Reset</button>
                            <br><br>
                            <a href="javascript:void(0)" id="to-login" class="text-muted float-right"><i class="fa fa-key m-r-5"></i> Back To Login</a> </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <style>
        form p:last-child{
            color: red;
        }
    </style>
<?php echo $footer; ?>