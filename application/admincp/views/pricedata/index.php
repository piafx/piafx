<?php echo $header; ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Price Data</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                    <h2 class="card-title"><i class="ti-money"></i>&nbsp;Price Data</h2>
                                </div>
                                <div class="col-md-12" style="display: inline-block;padding-left: 0px;">

                                    <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                        <label>Exchange</label>
                                        <div>
                                        
                                            <select id="currentmarket" class="js-example-basic-single form-control" onchange="getcurrentmarket_data(this.value)">
                                            <option></option>
                                                <?php 
                                                    foreach ($market as $value) {
                                                        if($value != ''){
                                                ?>
                                                    <option <?php echo $selected_market== $value?'selected="selected"':''; ?> value="<?php echo $value ?>"><?php echo $value ?></option>
                                                <?php 
                                                        }
                                                    }
                                                ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="display: inline-block;padding-left: 0px;">
                                        <label>Choose Tickers</label>
                                        <div id="select_tikers">
                                            <select class="form-control">
                                             <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="details_view" class="col-md-12" style="display: none;padding-left: 0px;">
                                    <div class="col-md-4" style="display: inline-block;padding-left: 0px;">
                                        <label>Ticker Code</label>
                                        <div id="details_info">
                                             
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="display: inline-block;padding-left: 0px;">
                                        <label>Data Provider</label>
                                        <div id="datasource">
                                             <select class="form-control">
                                             <option></option>
                                             </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2 price_btn" style="display: inline-block;padding-left: 0px;padding-top: 30px;float: left;height: 31px;">
                                        <button class="btn btn-info" onclick="settype()" style="height: 30px;line-height: 0.5 !important;">
                                            Apply
                                        </button>
                                    </div>
                                    </div>
                                </div>    
                                <div class="table-responsive m-t-40">
                                    <table id="user_tab" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Open</th>
                                                <th>High</th>
                                                <th>Low</th>
                                                <th>Close</th>
                                                <th>Volume</th>
                                                <th>Open Interest</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!---modal start --->
    <div class="modal fade bs-example-modal-sm" id="modal_status" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    
                    <h4 class="modal-title" id="myModalLabel2">User Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url("user/change_status"); ?>" method="post">
                    <div class="modal-body" id="new_radio">


                    </div>
                    <input type="hidden" id="uid" name="userid" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>    

            </div>
        </div>
    </div>
    <!-- /modals -->
    <div id="modalcontent">

            </div>
<?php echo $footer; ?>
<script>
    $('body').on('DOMNodeInserted', 'select', function () {
            $(this).select2({
                placeholder: "Please select an option",
                allowClear: true
              });
    });
    function settype(){
        $('.preloader').show();
        var typeprovider = $('#type_provider').val();
        var currentticker = $('#currentticker').val();
        var currentcode = $('#ticker_code').val();
        // alert(typeprovider+" "+currentticker)
        var main_uri = '<?php echo base_url('pricedata/updateticker_data');?>';
        $.ajax({
              type: 'POST',
              url: main_uri,
              data: {'ticker_id':currentticker,'type':typeprovider,'code':currentcode},
              success: function(resultData) { 
                    console.log(resultData);
                    $('.preloader').hide();
                    alert("Updated Successfully");
              },
              error: function(error){
                $('.preloader').hide();
                alert("Something went wrong");
              }
        });

    }
    getcurrentmarket_data();
    function getcurrentmarket_data(market_value){

        if (typeof market_value === "undefined") 
        {
            market_value = "<?php echo $selected_market ?>";
        }
        $('.preloader').show();
        // alert(typeprovider+" "+currentticker)s3,b6,rl,tx,pa,ad,ph
        $('#details_view').hide();
        var main_url = '<?php echo base_url('pricedata/getalltickerpermarket');?>';
        $.ajax({
              type: 'POST',
              url: main_url,
              data: {'market_id':market_value},
              success: function(resultData) { 
                    resultData = JSON.parse(resultData);
                    console.log(resultData);
                    $('.preloader').hide();
                    var selected_option = '<?php echo $selected_option; ?>';
                    
                    finhtml = '<select id="currentticker" class="form-control" onchange="getcurrentticker_data(this.value)"><option></option>';  
                    $.each(resultData, function( index, value ) {
                        if(selected_option == value._id) {
                            finhtml += "<option selected value='"+value._id+"'>"+value.ticker_name+"</option>";
                            getcurrentticker_data(selected_option);
                        }
                        else{
                            finhtml += "<option value='"+value._id+"'>"+value.ticker_name+"</option>";
                        }
                    })
                    finhtml += '</select>';
                    console.log(finhtml);
                    $('#select_tikers').html(finhtml);
              },
              error: function(error){
                $('.preloader').hide();
                alert("Something went wrong");
              }
        });

    }

    // getcurrentticker_data();
    function getcurrentticker_data(value1='')
    {
        $('.preloader').show();
        if(value1 == '')
        {
            value1 = "<?php echo $selected_option ?>";
        }
        html = '';
        html1 = '';
        $('#user_tab').DataTable().clear().destroy();
        var main_url_ajax = '<?php echo base_url('pricedata/getticker_data');?>';
        $.ajax({
              type: 'POST',
              url: main_url_ajax,
              data: {'ticker_id':value1},
              success: function(resultData) { 
                resultData = JSON.parse(resultData);
                console.log(resultData);
                $.each(resultData.pricedata, function( index, value ) {
                    var editurl = "<?php echo base_url(); ?>";
                    html += '<tr><td>'+value.date+'</td><td>'+value.open+'</td><td>'+value.high+'</td><td>'+value.low+'</td><td>'+value.close+'</td><td>'+value.volume+'</td><td>'+value.openinterest+'</td><td><a href="'+editurl+'pricedata/edit/'+value1+'/'+value._id.$oid+'"><i class="fa fa-pencil-alt"></i></a> &nbsp; <span style="color:#007bff" onclick=deleteconfirm("'+editurl+'pricedata/delete/'+value1+'/'+value._id.$oid+'")><i class="fa fa-trash"></i></span></td></tr>';
                    // console.log(index);
                });
                var slected_val = 0;
                if(resultData.tickerdata.length > 0){
                    slected_val = resultData.tickerdata[0].type;
                }
                
                slected_val0 = '';
                slected_val1 = '';
                slected_val2 = '';
                slected_val3 = '';
                if(slected_val == 0){
                    slected_val0 = 'selected';
                }
                if(slected_val == 1){
                    slected_val1 = 'selected';
                    slected_text = resultData.tickerdata[0].eod_data;
                }
                if(slected_val == 2){
                    slected_val2 = 'selected';
                    slected_text = resultData.tickerdata[0].barchart;
                }
                if(slected_val == 3){
                    slected_val3 = 'selected';
                    slected_text = resultData.tickerdata[0].alpha_vantag;
                }
                
                html1 = '<select id="type_provider" style="width:200px !important" class="js-example-basic-single form-control col-md-12"><option '+slected_val0+' value="0">Please select an option</option><option '+slected_val1+' value="1">Eod data</option><option '+ slected_val2+' value="2">Barchart</option><option '+slected_val3+' value="3">Alpha Vantage</option></select>';
                var info = '<input style="height: 28px;min-height: auto;" id="ticker_code" name="code" class="form-control" value="'+slected_text+'" >'; 

                $('#tbody').html(html);
                $('#datasource').html(html1);
                $('#details_info').html(info);
                $('#details_view').show();

                setTimeout(function(){ 

                    if ( $.fn.dataTable.isDataTable( '#user_tab' ) ) {
                            
                            $('#user_tab').DataTable().clear().destroy();
                            setTimeout(function(){ 
                                $('#tbody').html(html);
                                var table = $('#user_tab').DataTable({
                                    "displayLength": 25,
                                    "aaSorting": [[0, 'desc']],
                                    "columnDefs": [
                                        { orderable: false, targets: -1 }
                                     ]
                                });
                            }, 500);
                    }
                    else {
                        var table = $('#user_tab').DataTable({
                            "displayLength": 25,
                            "aaSorting": [[0, 'desc']],
                            "columnDefs": [
                                { orderable: false, targets: -1 }
                             ]
                        });
                    }
                    
                }, 1000);
                $('.preloader').hide();
              }
        });
        
    }
    function deleteconfirm(url){
        var result = confirm("Want to delete?");
        if (result) {
            window.location.replace(url);
        }
    }
    $(".mystatus").click(function () {


        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        $('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        //var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Pending" value="Pending"> Pending</div>';
        
        var html = '<div class="col-md-5"><input type="radio" name="status" class="myradio" id="Disable"  value="Disable"> <label for="Disable">Disabled</label></div>';
        html += '<div class="col-md-5"><input type="radio" name="status" class="myradio" id="Enable" value="Enable"> <label for="Enable">Enabled</label></div>';
//        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> <label for="Deleted">Delete</label></div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#uid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });
        
            $(document).ready(function() {
                $('.js-example-basic-single').select2({
                placeholder: "Please select an option",
                allowClear: true
              });
                var table = $('#user_tab').DataTable({
                    "displayLength": 25,
                    "aaSorting": [[0, 'desc']],
                });
            });
            
            function viewModal(mode, status, id) {
                    var ajaxStatus;  // The variable that makes Ajax possible!
                    try
                    {

                        // Opera 8.0+, Firefox, Safari
                        ajaxStatus = new XMLHttpRequest();
                    } catch (e)
                    {

                        // Internet Explorer Browsers
                        try
                        {
                            ajaxStatus = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e)
                        {

                            try
                            {
                                ajaxStatus = new ActiveXObject("Microsoft.XMLHTTP");
                            } catch (e)
                            {

                                // Something went wrong
                                alert("Your browser broke!");
                                return false;
                            }
                        }
                    }
                    ajaxStatus.onreadystatechange = function () {

                        if (ajaxStatus.readyState == 4)
                        {
                            var ajaxDisplay = document.getElementById('modalcontent');
//                              alert(ajaxStatus.responseText);
                            ajaxDisplay.innerHTML = ajaxStatus.responseText;
                            $("#statusmodel").modal({show: true});
                        }
                    }
                    ajaxStatus.open("POST", "user/viewmodal/" + mode  + '/' + id, true);
                    ajaxStatus.send(null);
                }

    
</script>