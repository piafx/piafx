    <!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $title; ?>| <?php echo $section_title ?></title>
          <link rel="shortcut icon" type="image/png" href="<?php echo base_url("../image/favicon.ico") ?>"/>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>../design/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url(); ?>../design/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="<?php echo base_url(); ?>../design/vendors/nprogress/nprogress.css" rel="stylesheet">
        <!-- Animate.css -->
        <link href="<?php echo base_url(); ?>../design/vendors/animate.css/animate.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="<?php echo base_url(); ?>../design/build/css/custom.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>../design/vendors/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url();?>../design/build/js/custom.min.js"></script>
       <script src="<?php echo base_url();?>../design/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    </head>

    <body class="login" style="background:#33334c">
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>


            <div class="login_wrapper">
                 
                <div class="animate form login_form">
                    <section class="login_content">
                        <form action="<?php echo base_url() . $this->uri->segment(1) ?>" method="post">

                            <img src="<?php echo base_url("../image/logo_2.png");?>" height="100px" >
                            <br>
                            <br>

                            <div class="col-xs-12">
                                <?php
                                if ($this->session->flashdata('message')) {
                                    ?>
                                    <!--  start message-red -->
                                    <div class="box-body" id="modal_msg">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?>
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php
                                if ($this->session->flashdata('success')) {
                                    ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable" id="modal_msg" >
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                            </div>
                            <div>
                                <input type="email" class="form-control" name="user_email" placeholder="Email" required="" />
                            </div>
                            
                            <div>
                                <button  type="submit" class="btn btn-default submit" href="" style="background:#e33c6d ">Reset Password</button>
                                <br>
                                <center><a class="reset_pass" href="<?php echo base_url('adminlogin') ;  ?>">Back to Login</a></center>
                            </div>

                            <div class="clearfix"></div>

                            
                        </form>
                    </section>
                </div>

                <div id="register" class="animate form registration_form">
                    <section class="login_content">
                       
                    </section>
                </div>
            </div>
        </div>
        <script>
            
            $(document ).ready(function() {
                
  
            $(".close").click(function () {
                
               $("#modal_msg").html("");
            });
            });
        </script>
    </body>
</html>
