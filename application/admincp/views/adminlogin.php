<?php echo $header; ?> 
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label"><?php echo $title; ?></p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background:url(<?php echo base_url('assets/images/background/bg_VTel.jpg'); ?>) no-repeat;background-size:cover">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="post">
                    <a href="javascript:void(0)" class="text-center db"><img src="<?php echo base_url('assets/images/companylogo.png') ?>" alt="Home" width="100px" /><br/>
                        <?php //echo $title; ?></a>
                    <br>
                    <div class="col-xs-12">
                        <?php
                        if ($this->session->flashdata('message')) {
                            ?>
                            <!--  start message-red -->
                            <div class="box-body" id="modal_msg">
                                <div class=" alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            </div>
                            <!--  end message-red -->
                        <?php } ?>
                        <?php
                        if ($this->session->flashdata('success')) {
                            ?>
                            <!--  start message-green -->
                            <div class="alert alert-success alert-dismissable" id="modal_msg">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <!--  end message-green -->
                        <?php } ?>
                    </div>
                    <div class="col-xs-12">
                        <h3>Login</h3>
                    </div>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input name="user_email" value="<?php echo set_value('user_email'); ?>" class="form-control is-invalid" type="email" required="" placeholder="Email">
                            <?php echo form_error('user_email'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input name="user_password" class="form-control" type="password" required="" minlength="6" maxlength="20" placeholder="Password">
                            <?php echo form_error('user_password'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
<!--                            <div class="checkbox checkbox-primary float-left p-t-0">
                                <input id="checkbox-signup" name="remember_me" type="checkbox" class="filled-in chk-col-light-blue">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>-->
                            <a href="<?php echo base_url('adminlogin/forgot'); ?>" id="to-recover" class="text-muted float-right"><i class="fa fa-lock m-r-5"></i> Forgot Password?</a> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit" style="box-shadow: none;">LogIn</button>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fab fa-facebook-f"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fab fa-google-plus-g"></i> </a> </div>
                        </div> -->
                    </div>
                    <div class="form-group m-b-0">
                        <!-- <div class="col-sm-12 text-center">
                            Don't have an account? <a href="pages-register2.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                        </div> -->
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="<?php echo base_url('adminlogin/forgot') ?>" method="post">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="user_email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit" box-shadow: none;>Reset</button>
                            <br><br>
                            <a href="javascript:void(0)" id="to-login" class="text-muted float-right"><i class="fa fa-key m-r-5"></i> Back To Login</a> </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <style>
        form p:last-child{
            color: red;
        }
    </style>
<?php echo $footer; ?>
<script>
        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload(); 
            }
        });
</script>