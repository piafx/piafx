<?php echo $header; ?>
<style>
    .hide_for_now{
        display: none;
    }
</style>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles dash_div">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"><i class="mdi mdi-gauge"></i>&nbsp;Dashboard</h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active" title="Dashboard">Dashboard</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Sales overview chart -->
                <!-- ============================================================== -->
                <div class="col-12 dashboard_cust">
                                <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                </div>
                
                
                <div>
                
                
                <div class="row">
                    <div class="col-lg-4" >
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div class="m-r-20 align-self-center"><span class="lstick m-r-20"></span><i class="fa fa-chart-line" style="font-size: 50px;"></i></div>
                                    <div class="align-self-center">
                                        <h6 class="text-muted m-t-10 m-b-0"> Markets </h6>
                                        <h2 class="m-t-0"><?php echo count($markets); ?></h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div class="m-r-20 align-self-center"><span class="lstick m-r-20"></span><i class="fa fa-file" style="font-size: 50px;"></i></div>
                                    <div class="align-self-center">
                                        <h6 class="text-muted m-t-10 m-b-0">COTS</h6>
                                        <h2 class="m-t-0"><?php echo count($cots); ?></h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex no-block">
                                    <div class="m-r-20 align-self-center"><span class="lstick m-r-20"></span><i class="fa fa-check-circle" style="font-size: 50px;"></i></div>
                                    <div class="align-self-center">
                                        <h6 class="text-muted m-t-10 m-b-0">Tickers</h6>
                                        <h2 class="m-t-0"><?php echo count($tickers); ?></h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Activity widget find scss into widget folder-->
                <!-- ============================================================== -->
                </div>
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<?php echo $footer; ?>
    
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--sparkline JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--morris JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/d3/d3.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/c3-master/c3.min.js"></script>
    <!-- Popup message jquery -->
    <script src="<?php echo base_url(); ?>assets/plugins/toast-master/js/jquery.toast.js"></script>
    <!-- Chart JS -->
    <script src="<?php echo base_url(); ?>js/dashboard1.js"></script>
    <!-- Flot Charts JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/flot/excanvas.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot/jquery.flot.crosshair.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo base_url(); ?>js/flot-data.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    
    
<script>

        $( document ).ready(function() {
            
            var psap_failcount = '<?php echo isset($psap_failcount)?$psap_failcount:0; ?>';
            var psap_passcount = '<?php echo isset($psap_passcount)?$psap_passcount:0; ?>';
            var psap_nottestedcount = '<?php echo isset($psap_nottestedcount)?$psap_nottestedcount:0; ?>';
            
            var cell_failcount = '<?php echo isset($cell_failcount)?$cell_failcount:0; ?>';
            var cell_passcount = '<?php echo isset($cell_passcount)?$cell_passcount:0; ?>';
            var cell_nottestedcount = '<?php echo isset($cell_nottestedcount)?$cell_nottestedcount:0; ?>';
            var team_aray = <?php echo json_encode($team_aray) ?>;
            var team_aray1 = <?php echo json_encode($team_aray1) ?>;
            var date_graph = <?php echo json_encode($date_graph) ?>;
            //console.log(team_aray);
            //Flot Pie Chart
            $(function () {
                var data = [{
                    label: "Fail"
                    , data: psap_failcount
                    , color: "Red"
                , },
                {
                    label: "Pass"
                    , data: psap_passcount
                    , color: "Green"
                , }, 
                {
                    label: "Not Intrested"
                    , data: psap_nottestedcount
                    , color: "#009efb"
                , }
                ];
                var plotObj = $.plot($("#flot-pie-chart"), data, {
                    series: {
                        pie: {
                            
                            radius: 3/4
                            , show: true
                        }
                    }
                    , grid: {
                        hoverable: true
                    }
                    , color: null
                    , tooltip: true
                    , tooltipOpts: {
                        content: "%y.0, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20
                            , y: 0
                        }
                        , defaultTheme: false
                    }
                });
            });
            $(function () {
                    var data = [{
                       label: "Fail"
                       , data: cell_failcount
                       , color: "Red"
                   , },
                   {
                       label: "Pass"
                       , data: cell_passcount
                       , color: "Green"
                   , }, 
                   {
                       label: "Not Intrested"
                       , data: cell_nottestedcount
                       , color: "#009efb"
                   , }, 
                   ];
                   var plotObj = $.plot($("#flot-pie-chart1"), data, {
                       series: {
                           pie: {
                               radius: 3/4
                               , show: true
                           }
                       }
                       , grid: {
                           hoverable: true
                       }
                       , color: null
                       , tooltip: true
                       , tooltipOpts: {
                           content: "%y.0, %s", // show percentages, rounding to 2 decimal places
                           shifts: {
                               x: 20
                               , y: 0
                           }
                           , defaultTheme: false
                       }
                   });
               });
            // sales bar chart
            $(function () {
                //some data
                var ds = new Array();
                var d1 = new Array();
                var d2 = new Array();
                var xaxis =  new Array();
                var i = 0;
                $.each( team_aray, function( key, value ) {
                   d1.push([i, value.assign]);
                   d2.push([i, value.tested]);
                   xaxis.push([parseInt(i) + 0.3 , key]);
                   i++;
                });
                ds.push({   
                    label: "Total Cell Ids Tested"
                    , data: d1
                    , bars: {
                        order: 1
                    }
                });
                ds.push({
                    label: "Total Cell Ids assigned"
                    , data: d2
                    , bars: {
                        order: 2
                    }
                });
                var stack = 0
                    , bars = true
                    , lines = true
                    , steps = true;
                var options = {
                    bars: {
                        show: true
                        , barWidth: 0.8
                        , fill: 1
                    }
                    , grid: {
                        show: true
                        , aboveData: false
                        , labelMargin: 5
                        , axisMargin: 0
                        , borderWidth: 1
                        , minBorderMargin: 5
                        , clickable: true
                        , hoverable: true
                        , autoHighlight: false
                        , mouseActiveRadius: 20
                        , borderColor: '#f5f5f5'
                    }
                    , series: {
                        stack: stack
                    }
                    , legend: {
                        position: "ne"
                        , margin: [0, 0]
                        , noColumns: 0
                        , labelBoxBorderColor: null
                        , labelFormatter: function (label, series) {
                            // just add some space to labes
                            return '' + label + '&nbsp;&nbsp;';
                        }
                        , width: 30
                        , height: 5
                    }
                    , yaxis: {
                        tickColor: '#f5f5f5'
                        , font: {
                            color: '#bdbdbd'
                        }
                    }
                    , xaxis: {
                        ticks: xaxis,
                        tickColor: '#f5f5f5'
                        , font: {
                            color: '#bdbdbd'
                        }
                    }
                    , colors: ["#4F5467", "#009efb", "#26c6da"]
                    , tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y"
                        , shifts: {
                            x: -30
                            , y: -50
                        }
                    }
                };
                $.plot($(".sales-bars-chart"), ds, options);
            });
            //Flot Bar Chart
            $(function () {
                
                var a1 = new Array();
                var a2 = new Array();
                var xsas = new Array();
                var i = 0;
                $.each( team_aray1, function( key, value ) {
                   a1.push([i, value.assign]);
                   a2.push([ parseInt(i) + 0.13, value.tested]);
                   xsas.push([parseInt(i) + 0.13 , key]);
                   i++;
                });
                
                var data = [
                    {
                        data: a1
                    },
                    {
                        data: a2
                    },
                ];

                $.plot($("#flot-bar-chart"), data, {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.13,
                            order: 1
                        }
                    },
                    xaxis: {
                        ticks: xsas
                    },
                    valueLabels: {
                        show: true
                    }
                    , tooltip: true
                    , tooltipOpts: {
                        content: "%y",
                        shifts: {
                          x: -60,
                          y: 25
                        }
                    }
                });
            });
            //Flot Line Chart

            var offset = 0;
            plot();

            function plot() {
           
                var data1 = new Array();
                
                $.each( date_graph, function( key, value ) {
                   var cur_date = value.date.split("-");
                   data1.push([gd(cur_date[0], cur_date[1] - 1, cur_date[2]), value.value]);
                });
                
                function gd(year, month, day) {

                    return new Date(year, month , day).getTime();
                }
                var options = {
                    series: {
                        lines: {
                            show: true
                        }
                        , points: {
                            show: true
                        }
                    }
                    , grid: {
                        hoverable: true //IMPORTANT! this is needed for tooltip to work
                    }
                    ,xaxis: {
                        mode: "time",
                        timeformat: "%d/%m/%y",
//                        tickSize: [1, "month"],
//                        tickLength: 0,
                        axisLabel: "2012",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding: 10
                    }
                    , colors: ["#009efb", "#26c6da"]
                    , grid: {
                        color: "#AFAFAF"
                        , hoverable: true
                        , borderWidth: 0
                        , backgroundColor: '#FFF'
                    }
                    , tooltip: true
                    , tooltipOpts: {
                        content: "%y on %x"
                        , shifts: {
                            x: -60
                            , y: 25
                        }
                    }
                };
                var plotObj = $.plot($("#flot-line-chart"), [{
                    data: data1
                    , label: "Average calls"
                , }], options);
            }
        });
        var intit = '<?php echo isset($fromdate)?1:0; ?>';
        if(intit == '1')
        {
            
            var todate = "<?php echo isset($todate)?$todate:''; ?>";
            var fromdate = "<?php echo isset($fromdate)?$fromdate:''; ?>";
            
            $('#mdate1').val(todate);
            $('#mdate').val(fromdate);
            
            $('#mdate').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false })
            .on('change', function(e, date) { 
                var dt = formatDate(date);
                $('#mdate1').val(dt);
                $('#mdate1').bootstrapMaterialDatePicker('setMinDate', date);
            });
            var dt1 = strtodate(fromdate);
            $('#mdate1').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false});
            $('#mdate1').bootstrapMaterialDatePicker('setMinDate', dt1);
            
        }
        else
        {
            $('#mdate').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false })
            .on('change', function(e, date) { 
                 var dt = formatDate(date);
                 $('#mdate1').val(dt);
                 $('#mdate1').bootstrapMaterialDatePicker('setMinDate', date); 
            });
            $('#mdate1').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY', weekStart: 0, time: false, minDate : new Date() });
        }
        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('-');
        }
        function strtodate(date)
        {
            var parts =date.split('-');
            var mydate = new Date(parts[2], parts[0] - 1, parts[1]); 
            return mydate;
        }
        //test
        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 5,
                border: '1px solid #fdd',
                padding: '2px',
                'background-color': '#fee',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        $("#flot-bar-chart").bind("plothover", function (event, pos, item) {
            $("#x").text(pos.x.toFixed(2));
            $("#y").text(pos.y.toFixed(2));

            if ($("#enableTooltip:checked").length > 0) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(2),
                            y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY,
                                    item.series.label + " of " + x + " = " + y);
                    }
                }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;            
                }
            }
        });

        $("#flot-bar-chart").bind("plotclick", function (event, pos, item) {
            if (item) {
                $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
                plot.highlight(item.series, item.datapoint);
            }
        });
</script>