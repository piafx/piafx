
<?php echo $header; ?>
<?php echo $sidebar; ?>
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Profile page</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="white-box">
                    <div class="user-bg"> <img width="100%" alt="user" src="<?php echo base_url(); ?>plugins/images/large/img1.jpg">
                        <div class="overlay-box">
                            <div class="user-content"> <a href="javascript:void(0)"><img src="<?php if(!empty($data[0]['image'])) {  echo base_url().'uploads/user/'.$data[0]['image']; }else{  echo base_url().'uploads/user/default.jpg';} ?>" class="thumb-lg img-circle" alt="img"></a>
                                <h4 class="text-white"><?php echo $data[0]['adminname']; ?></h4>
                                <h5 class="text-white"><?php echo $data[0]['adminemail'] ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="user-btm-box">
                        <div class="col-md-6 col-sm-6 text-center">
<!--                            <p class="text-purple">Joining Date</p>
                            <h4></h4>-->
                        </div>

                       
                    </div>

                </div>
            </div>
            <div class="col-md-8 col-xs-12">


                <?php
                if ($this->session->flashdata('message')) {
                    ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php
                if ($this->session->flashdata('success')) {
                    ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
                <div class="white-box">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="<?php
                        if ($this->uri->segment(1) == "profile") {
                            echo "active";
                        }
                        ?> tab"><a href="<?php echo base_url("profile"); ?>" > <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Profile</span> </a> </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?php
                        if ($this->uri->segment(1) == "profile") {
                            echo "active";
                        }
                        ?>" id="home">
                            <form method="post" enctype="multipart/form-data" action="<?php echo base_url('profile/update'); ?>" class="form-horizontal form-material">
                                <div class="form-group">
                                    <label class="col-md-12">Profile Image</label>
                                    <div class="col-md-12">
                                        <input type="file" name="image" class="form-control form-control-line">
                                        <span class="help-block with-errors text-danger"><ul class="list-unstyled"><li><?php echo form_error('image'); ?></li></ul></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="name" placeholder="First Name" required=""  value="<?php echo set_value('name') ? set_value('name') :$data[0]['adminname']; ?>" class="form-control form-control-line">
                                        <span class="help-block with-errors text-danger"><ul class="list-unstyled"><li><?php echo form_error('name'); ?></li></ul></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="text" name="email" placeholder="Email" required=""  value="<?php echo set_value('email') ? set_value('email') :$data[0]['adminemail']; ?>" class="form-control form-control-line">
                                        <span class="help-block with-errors text-danger"><ul class="list-unstyled"><li><?php echo form_error('email'); ?></li></ul></span>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- .right-sidebar -->
        <div class="right-sidebar">
            <div class="slimscrollright">
                <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                <div class="r-panel-body">
                    <ul>
                        <li><b>Layout Options</b></li>
                        <li>
                            <div class="checkbox checkbox-info">
                                <input id="checkbox1" type="checkbox" class="fxhdr">
                                <label for="checkbox1"> Fix Header </label>
                            </div>
                        </li>
                        <li>
                            <div class="checkbox checkbox-warning">
                                <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                <label for="checkbox2"> Fix Sidebar </label>
                            </div>
                        </li>
                        <li>
                            <div class="checkbox checkbox-success">
                                <input id="checkbox4" type="checkbox" class="open-close">
                                <label for="checkbox4" > Toggle Sidebar </label>
                            </div>
                        </li>
                    </ul>
                    <ul id="themecolors" class="m-t-20">
                        <li><b>With Light sidebar</b></li>
                        <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                        <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                        <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                        <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                        <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                        <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                        <li><b>With Dark sidebar</b></li>
                        <br/>
                        <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                        <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                        <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>

                        <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                        <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                        <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>

                    </ul>
                    <ul class="m-t-20 chatonline">
                        <li><b>Chat option</b></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
                        <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>    plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.right-sidebar -->
    </div>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

    <?php echo $footer; ?> 