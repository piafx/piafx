<?php echo $header; ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home" color="black"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                    <div>
                    </div>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
               
                <div class="row">
                    <div class="col-12">
                         <div class="confirm-div" ><?php echo $this->session->flashdata('msg'); ?></div>
                                   <?php if ($this->session->flashdata('message')) { ?>
                                    <!--  start message-red -->
                                    <div class="box-body">
                                        <div class=" alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                            <?php echo $this->session->flashdata('message'); ?> 
                                        </div>
                                    </div>
                                    <!--  end message-red -->
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <!--  start message-green -->
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                        <?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                    <!--  end message-green -->
                                <?php } ?>
                        <div class="card">
                            <div class="card-body">
                                    <div class="col-md-4" style="display: inline-block;padding-left: 0px;">
                                        <h2 class="card-title"><i class="ti-user"></i>&nbsp;Users</h2>
                                    </div>
                                    <div class="col-md-8 cust_link <?php echo ((in_array('user_add', $permission_list))?'':'hide'); ?>" style="display: inline-block">
                                        <a href="<?php echo base_url('user/add'); ?>" class="new_user_link"><button title="Add New User" class="btn btn-success pull-right add_new_user_btn" style="float: right">Add New User</button></a>
                                    </div>
                                <div class="table-responsive m-t-40">
                                    <table id="user_tab" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Role</th>
                                                <th>Email</th>
                                                <th>Phone Model</th>
                                                <th>Current Status</th>
                                                <th class="<?php echo ((!in_array('user_delete', $permission_list)) && (!in_array('user_edit', $permission_list)) ?'hide':''); ?>">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data as $value)
                                            {
                                            ?>
                                            <tr>
                                                <td><?php echo $value['user_firstname'].' '.$value['user_lastname']; ?></td>
                                                <td><?php echo $value['userdetails'][0]['role_name']; ?></td>
                                                <td><?php echo $value['user_email']; ?></td>
                                                <td><?php echo $value['user_contact']!=''?$value['user_contact']:'--'; ?></td>
                                                <td>
                                                    <span
                                                        data-id="<?php echo $value['_id']; ?>" data-val="<?php echo $value['user_status']; ?>"
                                                        <?php
                                                        if ($value['user_status'] == "Enable") {
                                                            echo ' class="btn btn-success mystatus" style="width: 120px;height: 36px;" title="Enabled">Enabled</span>';
                                                        }elseif ($value['user_status'] == "Disable") {
                                                            echo ' class="btn btn-danger mystatus" style="width: 120px;height: 36px;" title="Disabled">Disabled</span>';
                                                        }
                                                        else{
                                                            echo ' class="btn btn-danger mystatus" style="width: 120px;height: 36px;" title="Disabled">Disabled</span>';
                                                        }
                                                        ?>
                                                </td>
                                                <td class="<?php echo ((!in_array('user_delete', $permission_list)) && (!in_array('user_edit', $permission_list)) ?'hide':''); ?>">   
                                                    <a class="<?php echo ((in_array('user_edit', $permission_list))?'':'hide'); ?>" title="Edit" href="<?php echo base_url('user').'/edit/'.base64_encode($value['_id']); ?>"><i class="fa fa-pencil-alt"></i></a> &nbsp;
                                                
                                                    
                                                    <a class="<?php echo ((in_array('user_delete', $permission_list))?'':'hide'); ?>" title="Delete" onclick="viewModal('delete', 'null',<?php echo stripslashes(($value['_id'])); ?>)" data-toggle="modal" href="#" ><i class="fa fa-trash"></i> </a>
                                                </td>
                                            </tr>
                                            <?php 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!---modal start --->
    <div class="modal fade bs-example-modal-sm" id="modal_status" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    
                    <h4 class="modal-title" id="myModalLabel2">User Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url("user/change_status"); ?>" method="post">
                    <div class="modal-body" id="new_radio">


                    </div>
                    <input type="hidden" id="uid" name="userid" value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>    

            </div>
        </div>
    </div>
    <!-- /modals -->
    <div id="modalcontent">

            </div>
<?php echo $footer; ?>
<script>



    $(".mystatus").click(function () {


        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        $('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        //var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Pending" value="Pending"> Pending</div>';
        
        var html = '<div class="col-md-5"><input type="radio" name="status" class="myradio" id="Disable"  value="Disable"> <label for="Disable">Disabled</label></div>';
        html += '<div class="col-md-5"><input type="radio" name="status" class="myradio" id="Enable" value="Enable"> <label for="Enable">Enabled</label></div>';
//        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> <label for="Deleted">Delete</label></div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#uid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });
        
            $(document).ready(function() {
                var table = $('#user_tab').DataTable({
                    "displayLength": 25,
                    "aaSorting": [],
                    "columnDefs": [
                        { orderable: false, targets: -1 }
                     ]
                });
            });
            
            function viewModal(mode, status, id) {
                    var ajaxStatus;  // The variable that makes Ajax possible!
                    try
                    {

                        // Opera 8.0+, Firefox, Safari
                        ajaxStatus = new XMLHttpRequest();
                    } catch (e)
                    {

                        // Internet Explorer Browsers
                        try
                        {
                            ajaxStatus = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e)
                        {

                            try
                            {
                                ajaxStatus = new ActiveXObject("Microsoft.XMLHTTP");
                            } catch (e)
                            {

                                // Something went wrong
                                alert("Your browser broke!");
                                return false;
                            }
                        }
                    }
                    ajaxStatus.onreadystatechange = function () {

                        if (ajaxStatus.readyState == 4)
                        {
                            var ajaxDisplay = document.getElementById('modalcontent');
//                              alert(ajaxStatus.responseText);
                            ajaxDisplay.innerHTML = ajaxStatus.responseText;
                            $("#statusmodel").modal({show: true});
                        }
                    }
                    ajaxStatus.open("POST", "user/viewmodal/" + mode  + '/' + id, true);
                    ajaxStatus.send(null);
                }

    
</script>