<?php echo $header; ?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                            <li class="breadcrumb-item" title="Users"><a href="<?php echo base_url('user') ?>">Users</a></li>
                            <li class="breadcrumb-item active" title="Add New User">Add New User</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Add New User</h2>
                                <form class="form-material m-t-40" method="post" action="<?php echo base_url('user/add_data'); ?>">
                                    <div class="form-group">
                                        <label>First Name <span style="color: red">*</span></label>
                                        <input title="First Name" name="first_name" type="text" class="form-control form-control-line" required="" value="<?php echo set_value('first_name'); ?>"> 
                                        <?php echo form_error('first_name'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name <span style="color: red">*</span></label>
                                        <input title="Last Name" name="last_name" type="text" class="form-control form-control-line" required="" value="<?php echo set_value('last_name'); ?>"> 
                                        <?php echo form_error('last_name'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Role  <span style="color: red">*</span></label>
                                        <select title="Role" class="form-control" name="role" id="user_type" required="">
                                            <option value="">Select Role</option>
                                            <?php
                                            foreach ($role as $value)
                                            {
                                               ?> 
                                                <option value="<?php echo $value['_id']; ?>"><?php echo $value['role_name']; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <?php echo form_error('role'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">Email <span style="color: red">*</span></label>
                                        <input title="Email" type="email" name="email" class="form-control" required="" value="<?php echo set_value('email'); ?>">
                                        <?php echo form_error('email'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email">Contact Number</label>
                                        <input title="Contact Number" type="text" id="number" name="contact" class="form-control" value="<?php echo set_value('contact'); ?>" maxlength="16" minlength="10">
                                        <?php echo form_error('contact'); ?>
                                    </div>
                                        <button title="Submit" type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                        <a href="<?php echo base_url('user'); ?>"><button title="Cancel" type="button" class="btn btn-inverse waves-effect waves-light">Cancel</button></a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== --> 
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<?php echo $footer; ?>