<?php echo $header; ?>
<!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Profile</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin') ?>">Home</a></li>
                        <!--<li class="breadcrumb-item">pages</li>-->
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
                <div>
                    <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <?php
                        if ($this->session->flashdata('message')) {
                            ?>
                            <!--  start message-red -->
                            <div class="box-body" id="modal_msg">
                                <div class=" alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                            </div>
                            <!--  end message-red -->
                        <?php } ?>
                        <?php
                        if ($this->session->flashdata('success')) {
                            ?>
                            <!--  start message-green -->
                            <div class="alert alert-success alert-dismissable" id="modal_msg">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $this->session->flashdata('success'); ?>
                            </div>
                            <!--  end message-green -->
                        <?php } ?>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab"> Change Password </a> </li>
                            </ul>
                            
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <!--second tab-->
                                <div class="tab-pane active" id="profile" role="tabpanel">
                                    <div class="card-body">
                                        
                                        <form class="form-horizontal form-material" action="<?php echo base_url('profile/add_data'); ?>" method="post">
                                                    <div class="col-md-12"> <strong>First Name  <span style="color: red">*</span></strong>
                                                        <br>
                                                        <input class="form-control form-control-line" type="text" name="first_name" value="<?php echo $data[0]['user_firstname']; ?>">
                                                        <?php echo form_error('first_name'); ?>
                                                    </div>
                                                    <div class="col-md-12"> <strong>Last Name  <span style="color: red">*</span></strong>
                                                        <br>
                                                        <input class="form-control form-control-line" type="text" name="last_name" value="<?php echo $data[0]['user_lastname']; ?>">
                                                        <?php echo form_error('last_name'); ?>
                                                    </div>
                                                    <div class="col-md-12"> <strong>Email  <span style="color: red">*</span></strong>
                                                        <br>
                                                        <input class="form-control" disabled="" type="text" name="email" value="<?php echo $data[0]['user_email']; ?>">
                                                    </div>
                                                    <div class="col-md-12"> <strong>Contact  <span style="color: red">*</span></strong>
                                                        <br>
                                                        <input id="number" class="form-control form-control-line" type="text" name="contact" value="<?php echo $data[0]['user_contact']; ?>" maxlength="16" minlength="10">
                                                        <?php echo form_error('contact'); ?>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6">
                                                        
                                                        <button type="submit" class="btn btn-success pr_save_btn">Save</button>
                                                    </div>
                                            </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings" role="tabpanel">
                                    <div class="card-body">
                                        <form class="form-horizontal form-material" method="post" action="<?php echo base_url('profile/change_password'); ?>">
                                            <div class="form-group">
                                                <label class="col-md-12">Current Password  <span style="color: red">*</span></label>
                                                <div class="col-md-12">
                                                    <input name="current_password" type="Password" class="form-control form-control-line" required="" minlength="6" maxlength="20">
                                                    <?php echo form_error('current_password'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">New Password  <span style="color: red">*</span></label>
                                                <div class="col-md-12">
                                                    <input name="new_password" type="Password" class="form-control form-control-line" required="" minlength="6" maxlength="20">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Confirm New Password  <span style="color: red">*</span></label>
                                                <div class="col-md-12">
                                                    <input name="confirm_new_password" type="Password" class="form-control form-control-line" required="" minlength="6" maxlength="20">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-success">Update Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                 © 2019 Insight
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<?php echo $footer; ?>