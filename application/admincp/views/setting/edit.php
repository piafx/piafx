<?php echo $header; ?>
  <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor"></h3>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item" title="Home"><a href="<?php echo base_url('admin');?>">Home</a></li>
                            <li class="breadcrumb-item" title="Setting"><a href="<?php echo base_url('setting');?>">Setting</a></li>
                            <li class="breadcrumb-item active" title="Edit Setting">Edit Setting</li>
                        </ol>
                    </div>
                    <div class="">
                        <!--<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>-->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php
                    $attributes = array('class' => 'has-validation-callback form-material m-t-40' , 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('setting/update/', $attributes);
                    ?>
                    <?php echo form_input(array('name' => 'settingid', 'type' => 'hidden', 'value' => base64_encode($settings[0]['settingid']))); ?>
                    <?php echo form_input(array('name' => 'settingname', 'type' => 'hidden', 'value' => $settings[0]['settingname'])); ?>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Edit Setting</h2>
                                    <div class="form-group">
                                                
                                                <div class="form-group" id="firstnameerror">
                                                    <label><?php echo $settings[0]['settingname']; ?></label>
                                                    <div class="controls">
                                                        <?php if(($settings[0]['settingid'] == 1) || ($settings[0]['settingid'] == 2) || ($settings[0]['settingid'] == 4)){ ?>
                                                        <input name="settingvalue" data-validation="required" data-validation-error-msg="Please enter value." id="settingvalue" type="text" class="form-control" required="required" value="<?php echo $settings[0]['settingvalue']; ?>" />
                                                        <?php }if($settings[0]['settingid'] == 10){ ?>
                                                        
                                                        <table>
                                                            <tr>
                                                                <td> <input name="home_image" data-validation="required mime size" 
                                                                   data-validation-allowing="jpeg, png, jpg"
                                                                   data-validation-max-size="2048kb"
                                                                   data-validation-error-msg-mime="Only .jpg, .jpeg or .png file can be upload"
                                                                   data-validation-error-msg-size="Image size must be less than 2MB"
                                                                   data-validation-error-msg-required="Please select file up to 2MB in size only."
                                                                   id="home_image" type="file" 
                                                                   class=""  /></td>
                                                                <td><img width="100" height="100" src="<?php echo base_url().$this->config->item('home_image_upload_path').$settings[0]['settingvalue']; ?>" /></td>
                                                            </tr>
                                                        </table>
                                                          
                                                           
                                                      
                                                    
                                                        <?php }
                                                        if(($settings[0]['settingid'] == 7) || ($settings[0]['settingid'] == 8) || ($settings[0]['settingid'] == 9)){ ?>

                    <input name="settingvalue" data-validation="required length" data-validation-length='0-100' data-validation-error-msg="Please enter value." id="settingvalue" type="text" class="form-control" value="<?php echo $settings[0]['settingvalue']; ?>"  maxlength='100' style="width:830px !important"  />
                    <label>  (<span id="address-max-length">100</span> characters left)</label>
                                                        
                                                        <?php }?>
                    <?php 
                                                        if(($settings[0]['settingid'] == 11)){ ?>
                                                        <textarea name="settingvalue" data-validation="required" data-validation-error-msg="Please enter value." id="settingvalue" type="text" class="form-control" rows='5' maxlength='300'  ><?php echo $settings[0]['settingvalue']; ?> </textarea>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                                <!--<input type="submit" name="add" value="Update" class="btn btn-success btn-small" />-->
                                              <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 btn-small" name="add" value="Update" title="Update">Update</button>
                                             <a href="<?php echo base_url('setting'); ?>"> <input title="Cancel" type="button" class="btn btn-inverse btn-small" value="Cancel" onclick="window.location.href = '<?php echo site_url('setting'); ?>'"/></a>
                                                <script>
                                                    $.validate({
                                                        modules: 'location, date, security, file',
                                                        onModulesLoaded: function() {
                                                        }
                                                    });
                                                </script>
                                            </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- ============================================================== -->
                <!-- End Page Content -->
                <!-- ============================================================== -->
                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©  <?php echo date('Y').' - '.$title; ?> </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

<?php echo $footer; ?>