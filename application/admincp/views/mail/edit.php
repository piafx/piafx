<?php echo $header; ?>
<?php echo $sidebar; ?>
 <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Edit Mail Configurations</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" >  
            <div class="col-lg-3 col-sm-8 col-md-8 col-xs-12 pull-right">
                    <a href="<?php echo base_url('mail_config'); ?>" class="btn btn-info pull-right">< Back</a>
            </div>
        </div>
      </div>
      <div class="row">
          <div class="col-xs-12">
              <?php
              if ($this->session->flashdata('message'))
              {
                  ?>
                  <!--  start message-red -->
                  <div class="box-body" id="modal_msg">
                      <div class=" alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?>
                      </div>
                  </div>
                  <!--  end message-red -->
              <?php } ?>
              <?php
              if ($this->session->flashdata('success'))
              {
                  ?>
                  <!--  start message-green -->
                  <div class="alert alert-success alert-dismissable" id="modal_msg">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                      <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                  <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <!--  end message-green -->
          <?php } ?>
          </div>
         <div class="col-sm-12">
          <div class="white-box">
            
              <form class="form-horizontal" method="post" action="<?php echo base_url().'mail_config/edit_data' ?>">
              <div class="form-group">
                
               
                    <label class="col-md-12" style="padding-left: 0px;">Mail Configuration Host</label>
                    <input type="hidden" name="id" value="<?php echo base64_encode($id); ?>">
                    <input type="text" class="form-control" placeholder="Mail Host" name="host" required="required" value="<?php echo $edit_id!=''?$host:set_value('host'); ?>">
                    <span style="color:red"><?php echo form_error('host'); ?></span>    
               
                  </div>
               <div class="form-group">
                    <label class="col-md-12" style="padding-left: 0px;">Mail Configuration Port</label>
                    <input type="text" class="form-control" placeholder="Middle Name" name="port" value="<?php echo $edit_id!=''?$port:set_value('port'); ?>">
                    <span style="color:red"><?php echo form_error('port'); ?></span>    
               </div>
                    <div class="form-group">
               
                    <label class="col-md-12" style="padding-left: 0px;">Mail Configuration User</label>
                    <input type="text" class="form-control" placeholder="User" name="user" required="required" value="<?php echo $edit_id!=''?$user:set_value('user'); ?>">
                    <span style="color:red"><?php echo form_error('user'); ?></span>    
               </div>
              
              
                           
              <div class="form-group">
                <label class="col-md-12" for="example-email" style="padding-left: 0px;">Mail Configuration Password</label>
                
                    <input type="password" class="form-control" placeholder="Password" name="password" required="required" value="<?php echo $edit_id!=''?$password:set_value('password'); ?>">
                    <span style="color:red"><?php echo form_error('password'); ?></span>    
                
              </div>  
                  
             <div class="form-group">
                
                <div class="col-md-12">
                    <input type="submit" name="submit" class="btn btn-info" value="Submit">
                </div>
              </div>
             
            </form>
          </div>
        </div>
      </div>
    </div>
 </div>

<script src="<?php echo base_url() ?>../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() ?>../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url() ?>../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



<script>

// Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: "dd-mm-yyyy",
        setDate : "('#datepicker-autoclose').value"
      });
      
    jQuery('#date-range').datepicker({
        toggleActive: true
      });
    jQuery('#datepicker-inline').datepicker({
        
        todayHighlight: true
      });

</script>
<?php echo $footer; ?>
<script>
    
      function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

</script>