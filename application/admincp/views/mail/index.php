<?php echo $header ?>
<?php echo $sidebar ?>
<link href="<?php echo base_url() ?>../cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  

<!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Email Configurations</h4>
        </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12" >  
          <div class="col-lg-3 col-sm-8 col-md-8 col-xs-12 pull-right">
         
            
              </div>
      </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
          <div class="col-xs-12">
              <?php
              if ($this->session->flashdata('message'))
              {
                  ?>
                  <!--  start message-red -->
                  <div class="box-body" id="modal_msg">
                      <div class=" alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?>
                      </div>
                  </div>
                  <!--  end message-red -->
              <?php } ?>
              <?php
              if ($this->session->flashdata('success'))
              {
                  ?>
                  <!--  start message-green -->
                  <div class="alert alert-success alert-dismissable" id="modal_msg">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                      <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                  <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <!--  end message-green -->
          <?php } ?>
          </div>
        <div class="col-sm-12">
          <div class="white-box">
              
            <div class="table-responsive">
                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            
                            <th>Email SMTP Host</th>
                            <th>Email SMTP Port</th>
                            <th>Email User</th>
                            <th>Email Password</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Email SMTP Host</th>
                            <th>Email SMTP Port</th>
                            <th>Email User</th>
                            <th>Email Password</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                         <?php 
                            
                                foreach ($data as $emp)
                                {
                            ?>
                        <tr>
                            
                            <td><?php echo $emp['mail_smpt_host']; ?></td>
                            <td><?php echo $emp['mail_smpt_port']; ?></td>
                            <td><?php echo $emp['mail_smpt_user']; ?></td>
                            <td><?php echo $emp['mail_smpt_password']; ?></td>
                            <td><a href="<?php echo base_url().'mail_config/edit/'. base64_encode($emp['mail_id']); ?>" class="btn btn-info"><i class="icon-pencil"></i></a></td>
                        </tr>
                        <?php
                        
                                }
                        ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
     
    </div>
      
      
      
  </div>
  <!-- /#page-wrapper -->

  <?php echo $footer; ?>
<script src="<?php echo base_url() ?>../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="<?php echo base_url() ?>../cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>../cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url() ?>../cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="<?php echo base_url() ?>../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>../cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>../cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->

<script>



    $(".mystatus").click(function () {


        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        $('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        //var html = '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Pending" value="Pending"> Pending</div>';
        
        var html = '<div class="col-md-2"><input type="radio"  name="status" class="myradio" id="Disable"  value="Disable"> Disable</div>';
        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Enable" value="Enable"> Enable</div>';
        html += '<div class="col-md-2"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> Delete</div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#uid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });

    $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        } );

    // Order by the grouping
    $('#example tbody').on( 'click', 'tr.group', function () {
      var currentOrder = table.order()[0];
      if ( currentOrder[0] === 2 && currentOrder[1] === 'asc' ) {
        table.order( [ 2, 'desc' ] ).draw();
      }
      else {
        table.order( [ 2, 'asc' ] ).draw();
      }
    });
  });
    });
    $('#example23').DataTable( {
     
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        'columnDefs': [
                {orderable: false, targets: [7]}
          ]
//     buttons[1].className = 'btn btn-info';
        
    });

  </script>

</body>

</html>
